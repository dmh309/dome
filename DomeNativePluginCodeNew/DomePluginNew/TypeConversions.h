// TypeConversions.h: utility functions for converting basic types to strings.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_TYPECONVERSIONS_H
#define DOME_TYPECONVERSIONS_H

#include <string>
#include <sstream>
#include <vector>
//#include "stdafx.h"

namespace DOME {
namespace Utilities {
namespace TypeConversions{

    template<class T>
    T fromString(const std::string& s);

    std::string str(const char& t);

    std::string str(const int& t);

    std::string str(const float& t);

    std::string str(const double& t);

    std::string str(const bool& t);

    std::string str(const std::vector<int>& v);

    std::string str(const std::vector<double>& v);

    std::string str(std::vector< std::vector<int> > v);

    std::string str(std::vector< std::vector<double> > v);

} // namespace TypeConversions
} // namespace Utilities
} // namespace DOME

#endif // DOME_TYPECONVERSIONS_H
