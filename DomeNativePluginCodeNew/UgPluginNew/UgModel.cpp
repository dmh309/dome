// UgModel.cpp: implementation of the UgModel class.
//
//////////////////////////////////////////////////////////////////////

#include "UgModel.h"
#include "UgIncludes.h"
#include <istream.h>


namespace DOME{
namespace UgPlugin{

UgModel::UgModel(string directory, string mod)
{
    dirName.assign(directory.c_str());
    model.assign(mod.c_str());
	fullModelName.assign(dirName);
	fullModelName.append("\\");
	fullModelName.append(model);
	fullModelName.append(".prt");
	_isModelLoaded = false;
	import_directory.assign(dirName);
	import_directory.append("\\import");
	export_directory.assign(dirName);
	export_directory.append("\\export");
	storedImport = -1;
	exportNow.setValue(false);
	loadedAlready = false;
	vrmlIndex = -1;
	vrmlIndexOld = -1;
	UgSession::initialize();
	UgInfoWindow::open();
}

UgModel::~UgModel()
{
//	char	buf[MAX_LN_SIZE+1];
	int i;

	for ( i = 0; i < components.size(); i++ )
		components[i]->cleanAll();

	while (!components.empty())
		components.pop_back();
	while (!imports.empty())
		imports.pop_back();

/*	if ( storedImport > 0 )
	{
    	UgSession::closeAllParts();
		sprintf(buf, "del %s\\*.prt", import_directory.c_str());
		system(buf);
		sprintf(buf, "copy %s\\temp\\*.* %s", import_directory.c_str(), import_directory.c_str());
		system(buf);
	}*/
	UgSession::terminate();
}

UgComponent* UgModel::createComponent(string componentName) /*throw(DomeException)*/
{
    UgComponent* comp = new UgComponent(componentName);
    if (components.size()==0) {
        comp->setTopLevel(true);
    }
	comp->setCompIndex(components.size());
    components.push_back(comp);
    return comp;
}

UgComponent* UgModel::createComponent(string componentName, string unitName) /*throw(DomeException)*/
{
    UgComponent* comp = new UgComponent(componentName, unitName);
    if (components.size()==0) {
        comp->setTopLevel(true);
    }
	comp->setCompIndex(components.size());
    components.push_back(comp);
    return comp;
}

UgImportFile* UgModel::createImportFile(string impName)
{
    UgImportFile * iFile = new UgImportFile(impName);
    imports.push_back(iFile);
    return iFile;
}

UgVrmlFile* UgModel::createVrmlFile(string vName)
{
    UgVrmlFile * iFile = new UgVrmlFile(vName);
	vrmlFile = iFile;
    return iFile;
}

bool UgModel::isModelLoaded()
{
    return _isModelLoaded;
}

void UgModel::loadModel()
{
	char	buf[MAX_LN_SIZE+1];
	int i;

    try
    {        
		if ( validatePart() == 0 )
		{
			startUgDOME();
			if ( isModelLoaded() && loadedAlready )
			{
				UG_DEBUG("in UgModel::loadModel -> model already loaded, now reload values!!!");
				// swap in latest _zkr import parts/assemblies only
				for ( i = 0; i < components.size(); i++ )
				{
					if ( components[i]->getExists() ) components[i]->updateDimInValues();
				}
				for ( i = 0; i < components.size(); i++ )
				{
					if ( components[i]->getExists() ) components[i]->updateOutputValues();
				}

				UgSession::setWorkPart( components[0]->getCompPtr() );		
			}
		}
		else
		{
			sprintf(buf, "part specified: %s", fullModelName.c_str());
			UG_DEBUG(buf);
		}
	}
    catch ( UgException &exception )  // Begin exception handling
    {
        UG_ERROR( "AN ERROR !!!!" );
        UG_ERROR( exception.askErrorText().c_str() );
		cleanAll();
    }
}

void UgModel::execute()
{
	int i;

	if ( isModelLoaded() )
	{
		loadedAlready = true;
		for ( i = 0; i < components.size(); i++ )
		{
			if ( components[i]->getExists() )
				components[i]->updateDimInValues();
		}
		for ( i = 0; i < components.size(); i++ )
		{
			if ( components[i]->getExists() )
				components[i]->updateOutputValues();
		}
		// if import needs to be done, do so here
		import();
		// done with import!

		// update bounding boxes for top assembly visualization in vrml environment
		UgSession::setDisplayPart( components[0]->getCompPtr() );
		UgSession::setWorkPart( components[0]->getCompPtr() );	
		// more efficiency: check for vrmlIndex, if it matches an assembly, get bounding box for its children [later]
		for ( i = 1; i < components.size(); i++ )
		{
			if ( components[i]->getExists() )
				components[i]->updateBoundingBoxForAss();
		}
		UG_DEBUG("bounding box for assembly has been updated");
		for ( i = 0; i < imports.size(); i++ )
		{
//			this was commented out because it is giving me problems
//			the problem is that when we update the bouding box on the assembly
//			the newly imported part is pointing on a null part.
//			if ( imports[i]->getExists() )
//				imports[i]->updateBoundingBoxForAss();
		}

		startVRML();
		vrmlFile->exportVRML(components[vrmlIndex]->getIsAssembly(), vrmlIndex); // make 0 a variable
		UgSession::setDisplayPart( components[0]->getCompPtr() );
		UgSession::setWorkPart( components[0]->getCompPtr() );	
		export();
	}
}

void UgModel::unloadModel()
{
	if ( isModelLoaded() )
		cleanAll();
//   	UgSession::closeAllParts();
}

int UgModel::validatePart() // this method checks that the file specified exists
{
	char	buf[MAX_LN_SIZE+1];
	FILE *fp;

	try
	{
		sprintf(buf, "%s", fullModelName.c_str());
		fp = fopen(buf, "r");
		if (fp == NULL)
		{
			UG_ERROR("Part does not exist: invalid directory path and/or invalid part extension");
			UG_ERROR("Check to make sure your directory path is correct and of the following syntax...");
			UG_ERROR("Example syntax: D:\\temp\\part.prt");
			return 1; // stop all process, incorrect filepath and/or extension specified
		}
		else
		{
			fclose(fp);
			return 0; // part exists, hence correct filepath and extension, continue with program
		}
	}
	catch(...)
	{
		UG_ERROR("validatePart: unknown exception caught");
		return 1;
	}
}

void UgModel::backupImport()
{
	char buf[MAX_LN_SIZE+1];
	int i, j;

	UG_DEBUG("in backupImport");
	try
	{
		if ( imports.size() > 0 )
		{
			sprintf(buf, "mkdir %s\\temp", import_directory.c_str());
			system(buf);
		}
		for (i = 0; i < imports.size(); i++)
		{
			if ( imports[i]->getExists() )
			{
				sprintf(buf, "imports[%d]: children size = %d", i, imports[i]->getChildrenSize());
				UG_DEBUG(buf);
				for ( j = 0; j < imports[i]->getChildrenSize(); j++ )
				{
					sprintf(buf, "copy %s\\%s.prt %s\\temp\\%s.prt", import_directory.c_str(), 
						imports[i]->getChild(j).c_str(), import_directory.c_str(), imports[i]->getChild(j).c_str());
					system( buf );
					UG_DEBUG(buf);
				}
				sprintf(buf, "copy %s\\%s.prt %s\\temp\\%s.prt", import_directory.c_str(), 
					imports[i]->getName().c_str(), import_directory.c_str(), imports[i]->getName().c_str());
				system( buf );
				UG_DEBUG(buf);
			}
		}
	}
	catch (...)
	{
		UG_ERROR("UgModel::backupImport: unknown exception caught");
		cleanAll();
	}
}

void UgModel::startUgDOME()
{
	char	buf[MAX_LN_SIZE+1], buf2[MAX_LN_SIZE+1];
	int	part_match;
	logical	sub_dir[] = { FALSE, FALSE }; // don't search in subdirectories, only these directories
	tag_t	part_tag;
	UF_ASSEM_options_t assem_options;
	UgPart *part;
	string	part_fspec, part_fspec_pre;

	try
	{
		UG_DEBUG("Inside startUgDOME");
		// Get the work part 
		part_tag = UF_ASSEM_ask_work_part(); 
		
		/* 
		 * returns the tag of the current work part
		 * else returns NULL_TAG if there is no current work part
		 */

		if(components.size())
		{
			if ( part_tag == NULL ) // no current working part; no part open => open part
			{
				part = UgPart::open( fullModelName );// opens a UG part
				part_fspec = part->getFullName(); // returns the full model name of the part
			}	
			else
			{

				/*
				* this else statement should not be entered in 
				* the beginning as the work part does not exist
				*/

				part = UgSession::getWorkPart();
				part_fspec_pre = part->getFullName();
				part_match = part_fspec_pre.compare( fullModelName );
				if (part_match != 0)
				{
					UgSession::closeAllParts();
					part = UgPart::open( fullModelName );
				}
				part_fspec = part->getFullName();
				UG_DEBUG("Inside else-if statement");
			}
			if( part )
			{

				UgSession::closeAllParts();

				sprintf(buf, "%s", dirName.c_str());  /* buf = E:\\DOME\\MODELS\\UG\\ */
				sprintf(buf2, "%s", import_directory.c_str()); // buf2 = E:\\DOME\\MODELS\\UG\\import 
				char *dir_list[] = { buf, buf2 };

				/*
				* so here we get an errorCode when there is no /import directory
				* in my model directory...
				*/

				int errorCode = UF_ASSEM_ask_assem_options(&assem_options);
				if(errorCode)
				{
					UG_ERROR("leaving method..."); return;
				}
				assem_options.load_options = UF_ASSEM_load_from_search_dirs; // sets the UF_ASSEM load options to load from search directories

				errorCode = UF_ASSEM_set_assem_options( &assem_options ); // applies the new UF_ASSEM settings
				errorCode = UF_ASSEM_set_search_directories( 2, dir_list, sub_dir ); 
				part = UgPart::open( fullModelName ); // opens UG part
				progFail = 0;

				// I. Create temp directory to house all exported and imported files

				createTempDirectory();

				if ( progFail == 0 )
				{
					// II. Top part exists 
					components[0]->setCompPtr(part);
					components[0]->setExists(true);
				
					// III. Get all components within assembly
	
					getAllComponentParts();

					if ( progFail == 0 )
					{
						// IV. Extract Information from Components/Part
						getPartsInfo();
									
						if ( progFail == 0 )
						{
							
							// V. Export VRML
							vrmlFile->findFileName(export_directory, model, components[0]->getCompPtr());
							UG_DEBUG("VrmlFile name is: ");
							if ( progFail == 0 )
							{
								
								vrmlFile->getVrmlViews();
								UG_DEBUG("Getting VrmlFile views.");
								if ( progFail == 0 )
								{
									vrmlFile->setVrmlZoomFactor(1.0);
									UG_DEBUG("Setting Vrml zoom factor");
									if ( progFail == 0 )
									{
										setVrmlIndex(0); // 0 at startup for top; change to other values to test
										UG_DEBUG("starting vrml ..."); 
										startVRML();
										UG_DEBUG("finished vrml ..."); 
										if ( progFail == 0 )
										{
											vrmlFile->exportVRML(components[vrmlIndex]->getIsAssembly(), vrmlIndex);
											UgSession::setDisplayPart( components[0]->getCompPtr() );
											UgSession::setWorkPart( components[0]->getCompPtr() );		
											if ( progFail == 0 )
											{
												// if is first time loading model -> make backup of import files
												backupImport();
												if ( progFail == 0 )
												{
													if (components[0]->getExportsSize() > 0) createDensityFile();
													if ( progFail == 0 ) _isModelLoaded = true;
													// check isAssembly
													for ( int i = 0; i < components.size(); i++ )
													{
														if ( components[i]->getIsAssembly() )
														{
															sprintf(buf, "components[%d] = assembly", i);
															UG_DEBUG(buf);
														}
														else
														{
															sprintf(buf, "components[%d] = part", i);
															UG_DEBUG(buf);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	catch (...)
	{
		UG_ERROR("UgModel::startUgDOME: unknown exception caught");
		cleanAll();
	}
	UG_DEBUG("UG_DOME Completed!!!"); 
}

void UgModel::createDensityFile()
{
	int	body_type,  errorCode, i, /*j*/ part_match = 1, subtype, type, notNew, numChilds;
	char buf[MAX_LN_SIZE+1];
	tag_t	member, part_occur, *child_part_occs=NULL;
	UgAssemblyNode	*node;
	string	extract_name, name;
	UgTypedObject	*pCurObj;
	UgPart* part;
	FILE *fp;
	UgComponent* comp;
    vector <UgComponent*> compArray;
	string temp;
	double density;

	UG_DEBUG("in createDensityFile");
	try
	{
		part = components[0]->getCompPtr();
		UgSession::setWorkPart( part );

		for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
		{
			member = pCurObj->getTag();
			errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
			if (type == UF_component_type)
			{
				node = dynamic_cast <UgAssemblyNode *> (pCurObj);
				name = (node->askPart())->getFullName();
				extract_name = extract_part_name( name );
				notNew = -1;
				for (i = 0; i < compArray.size(); i++)
				{
					part_match = extract_name.compare(compArray[i]->getCompName());
					if (part_match == 0) 
					{
						notNew = 1;
						break;
					}
				}
				if (notNew < 0)
				{
					temp.assign(extract_name.c_str());
					comp = new UgComponent(temp);
					comp->setCompPtr(node->askPart());

					part_occur = node->getTag();
					numChilds = UF_ASSEM_ask_part_occ_children ( part_occur, &child_part_occs );
					if ( numChilds > 0 )
					{
						comp->setTopLevel(true); // assembly -> not base child part
						UF_free( child_part_occs );
					}
					compArray.push_back(comp);
				}
			}
		}
		// have an array of unique base children, now obtain their density and output to file
		sprintf(buf, "%s\\density.txt", export_directory.c_str());
		fp = fopen(buf, "w");
		if ( compArray.size() > 0 ) // dealing with an assembly
		{
			for (i = 0; i < compArray.size(); i++)
			{
				if ( !compArray[i]->getTopLevel() )
				{
					part = compArray[i]->getCompPtr();
					UgSession::setWorkPart( part );
					for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
					{
						member = pCurObj->getTag();
						errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
						if ( type == UF_solid_type && subtype == UF_solid_body_subtype ) // solid objects: bodies and sheets
						{
							errorCode = UF_MODL_ask_body_type( member, &body_type );
							if ( body_type == UF_MODL_SOLID_BODY )
							{
								errorCode = UF_MODL_ask_body_density(member, UF_MODL_pounds_inches, &density);
								sprintf(buf, "%s = %f\n", compArray[i]->getCompName().c_str(), density);
								fputs( buf, fp );
								break;
							}
						}
					}
				}
			}
		}
		else // dealing with a single part
		{
			for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
			{
				member = pCurObj->getTag();
				errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
				if ( type == UF_solid_type && subtype == UF_solid_body_subtype ) // solid objects: bodies and sheets
				{
					errorCode = UF_MODL_ask_body_type( member, &body_type );
					if ( body_type == UF_MODL_SOLID_BODY )
					{
						errorCode = UF_MODL_ask_body_density(member, UF_MODL_pounds_inches, &density);
						sprintf(buf, "%s = %f\n", model.c_str(), density);
						fputs( buf, fp );
						break;
					}
				}
			}
		}
		fclose(fp);
		for (i = 0; i < compArray.size(); i++)
			compArray[i]->clean();
		while (!compArray.empty())
			compArray.pop_back();
		UgSession::setWorkPart(components[0]->getCompPtr());
	}
	catch (...)
	{
		UG_ERROR("UgModel::createDensityFile: unknown exception caught");
		cleanAll();
	}
}

void UgModel::parseDensityFile()
{
	FILE *fp;
	string ln, ln2;
	int length, /*length2*/ i, j;
	char	buf[MAX_LN_SIZE+1], line[500];

	try
	{
		sprintf(buf, "%s\\density.txt", import_directory.c_str());
		fp = fopen( buf, "r" );
		if ( fp != NULL )
		{
			while (	fgets(line, 500, fp) )
			{
				ln.assign( string(line) );
				length = ln.size();
				j = ln.find_last_of("=", length);
				ln2.assign( ln, 0, j-1 );
				baseNames.push_back(ln2);
				ln2.assign( ln, j+2, length-j-2);
				sprintf(buf, "%s", ln2.c_str());
				baseDensities.push_back(atof(buf));
			}
			fclose( fp );
		}
		for (i = 0; i < baseNames.size(); i++)
		{
			sprintf(buf, "base[%d]: %s = %f", i, baseNames[i].c_str(), baseDensities[i]);
			UG_DEBUG(buf);
		}
	}
	catch (...)
	{
		UG_ERROR("UgModel::parseDensityFile: unknown exception caught");
		cleanAll();
	}
}

void UgModel::extractMatingObjects()
{
	int	body_type, color, errorCode, i, subtype, type;
	tag_t	member;
	UF_OBJ_disp_props_t	disp_props;
	UgTypedObject	*pCurObj;
	UgPart* part;

	// this function needs to be called only once in the beginning to record color of translated step files
	try
	{
		UG_DEBUG( "in UgModel::extractMatingObjects" );
		for ( i = 0; i < imports.size(); i++ ) //cycle through all translated top components
		{
			if ( imports[i]->getExists() )
			{
				part = imports[i]->getCompPtr();
				UgSession::setWorkPart( part );
				color = 0;
				for (pCurObj = part->iterateFirst(); pCurObj && color==0; pCurObj = part->iterateNext(pCurObj))
				{
					member = pCurObj->getTag();
					errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
					if ( color == 0 )
					{
						if ( type == UF_solid_type && subtype == UF_solid_body_subtype )
						{
							errorCode = UF_MODL_ask_body_type( member, &body_type );
							if ( body_type == UF_MODL_SOLID_BODY )
							{
  								errorCode = UF_OBJ_ask_display_properties( member, &disp_props );
								imports[i]->setColor( disp_props.color );
							}
						}
					}
				}
			}
		}
		UgSession::setWorkPart( components[0]->getCompPtr() );
	}
	catch (...)
	{
		UG_ERROR( "UgModel::extractMatingObjects: unknown exception caught" );
		cleanAll();
	}
}

void UgModel::findParents()
{
	int	errorCode, i, j, part_match = 1, subtype, type;//, k;
	int skipImport;
//	char buf[MAX_LN_SIZE+1];
	tag_t	member;
	UgAssemblyNode	*node;
	string	extract_name, name;
	UgTypedObject	*pCurObj;
	UgPart* part;

	try
	{
		components[0]->setParent(-1); // means absolute root
		// already initialized parent to 0 during the object declaration [DO THIS FOR IDEAS!!!]
		for ( i = 0; i < parentIndex.size(); i++ ) // go through all assemblies in the order encountered
		{
			part = components[parentIndex[i]]->getCompPtr();
			UgSession::setWorkPart( part );

			UG_DEBUG("about to cycle...");
			for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
			{
				member = pCurObj->getTag();
				errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
				if (type == UF_component_type)
				{
					node = dynamic_cast <UgAssemblyNode *> (pCurObj);
					name = (node->askPart())->getFullName();
					extract_name = extract_part_name( name );
					skipImport = -1;
					for ( j = 0; j < imports.size(); j++ ) {
						if ( imports[j]->getExists() )
						{
							part_match = extract_name.compare(imports[j]->getName());
							if (part_match == 0) 
							{
								imports[j]->setParent(parentIndex[i]);
								skipImport = 1;
								break;
							}
						}
					}
					if ( skipImport < 0 )
					{
						for ( j = 1; j < components.size(); j++ ) {
							if ( components[j]->getExists() )
							{
								part_match = extract_name.compare(components[j]->getCompName());
								if (part_match == 0) 
								{
									components[j]->setParent(parentIndex[i]);
									break;
								}
							}
						}
					}
				}
			}
		}
		while (!parentIndex.empty())
			parentIndex.pop_back();
	}
	catch (...)
	{
		UG_ERROR("UgModel::findParents: unknown exception caught");
		cleanAll();
	}
}

string UgModel::extract_part_name( string part_name ) {
	int	j, part_fspec_length;
	string	extract_name;

	part_fspec_length = part_name.length()-5; // knocks out .prt, .igs, .stp, any 3 letter extension
	j=part_name.find_last_of("\\",part_fspec_length);  // need position to start search from end
	extract_name.assign(part_name,j+1,part_fspec_length-j);

	return extract_name;
}

void UgModel::createTempDirectory()
{
	char	buf[MAX_LN_SIZE+1];
	// creates temp directory within part directory, will export to and import from it
	// vrml.htt template file will be created and placed within it as well

	UG_DEBUG("Inside createTempDirectory()");
	try
	{
		sprintf(buf,"%s",getenv("UGII_BASE_DIR")); // only valid for Windows based OS
		UGII_BASE_DIR.assign( buf ); // buf = e:\ugs180
		sprintf(buf, "path: %s", UGII_BASE_DIR.c_str()); // buf = path: e:\ugs180
		sprintf(buf, "mkdir %s", export_directory.c_str()); // creates export directory
		system(buf); 
		vrmlFile->autoVrmlTemplateCreate(export_directory); UG_DEBUG("we are still ok...");
		if (imports.size() > 0)
			parseDensityFile();
/*		if ( imports.size() > 0 ) // && first time loading model
		{
			sprintf(buf, "mkdir %s\\temp", import_directory.c_str());
			system(buf);
			sprintf(buf, "copy %s\\*.prt %s\\temp", import_directory.c_str(), import_directory.c_str());
			system(buf);
			storedImport = 1;
		}*/
	}
	catch (...)
	{
		UG_ERROR("UgModel::createTempDirectory: unknown exception caught");
		cleanAll();
	}
}

void UgModel::getAllComponentParts()
{
	double	bounding_box[6]; //, dim_origin[3], *array;
	int	body_type,  errorCode, first_time = 1, i, j, part_match = 1, subtype, type, k, l;
	int skipImport/*, dim_subtype, ii, jj*/, numChilds;
	char buf[MAX_LN_SIZE+1]; //, buf2[MAX_LN_SIZE+1];
//	UF_DRF_dim_info_t *dim_info;
	tag_t	member, temp_instance, /*dim,*/ *child_part_occs=NULL, part_occur;
	UgAssemblyNode	*node;
	string	extract_name, name;
	UgTypedObject	*pCurObj;
	UgPart* part;
	FILE *fp;

	try
	{
		for ( i = 0; i < components.size(); i++ ) components[i]->setTopCompName( components[0]->getCompName() );
		part = components[0]->getCompPtr();
		UG_DEBUG("Component name: " << components[0]->getCompName().c_str());
//		numChilds = UF_ASSEM_ask_part_occ_children( part->getTag(), &child_part_occs );
//		sprintf(buf, "num child = %d", numChilds);
//		UG_DEBUG(buf);
//		if ( numChilds > 0 )
//		{
//			components[0]->setIsAssembly(true);
//			UF_free( child_part_occs );
//		}
//		else
//			components[0]->setIsAssembly(false);

		k = components.size()-1;
		l = imports.size();

		UgSession::setWorkPart( part );

		for ( i = 0; i < imports.size(); i++ )
		{
			imports[i]->findImportType(); UG_DEBUG("Import Type is: " << imports[i]->getType());
			imports[i]->findFileName(dirName);
			sprintf(buf, "import[%d] = %s", i, imports[i]->getFileName().c_str());
			UG_DEBUG(buf);

		}
		for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
		{
			member = pCurObj->getTag(); 
			errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype); 
			if(errorCode) UG_ERROR("ERROR");
			// obtain bounding box limits
			if ( type == UF_solid_type && subtype == UF_solid_body_subtype ) // solid objects: bodies and sheets
			{
				errorCode = UF_MODL_ask_body_type( member, &body_type ); 
				if(errorCode) UG_ERROR("ERROR");
				if ( body_type == UF_MODL_SOLID_BODY )
				{
					UG_DEBUG("body type");
					errorCode = UF_MODL_ask_bounding_box( member, bounding_box ); 
					if(errorCode) UG_ERROR("ERROR");
					if (first_time == 1)
					{
						UG_DEBUG("first_time = 1");
						for (j = 0; j < 6; j++){
							vrmlFile->setBBox(j, bounding_box[j]); }
						first_time = 0;
					}
					else
					{
						for (j = 0; j < 3; j++) // obtain min
						{
							if (bounding_box[j] < vrmlFile->getBBox(j))
								vrmlFile->setBBox(j, bounding_box[j]);
						}
						for (j = 3; j < 6; j++) // obtain max
						{
							if (bounding_box[j] > vrmlFile->getBBox(j))
								vrmlFile->setBBox(j, bounding_box[j]);
						}
					}
				}
			}
			if (type == UF_component_type)
			{
				UG_DEBUG("type = UF_component_type");
				components[0]->setIsAssembly(true);
				node = dynamic_cast <UgAssemblyNode *> (pCurObj);
				name = (node->askPart())->getFullName();
				extract_name = extract_part_name( name );
				skipImport = -1;
				sprintf(buf, "component: %s", extract_name.c_str()); 
				if ( l > 0 )
				{
					UG_DEBUG("Print l..." << l);
					for ( i=0; i< imports.size(); i++ ) {
						// assume all parts referenced by unsaved model file do not have _zkr; if have _zkr
						// take care at loadModel()
						part_match = extract_name.compare(imports[i]->getName()); UG_DEBUG("Part_Match = " << part_match);
						if (part_match == 0) 
						{
							temp_instance = UF_ASSEM_ask_inst_of_part_occ( member );
							imports[i]->setNeutralInstance(temp_instance);
							if ( !imports[i]->getExists() )
							{
								sprintf(buf, "%s", imports[i]->getFileName().c_str());
								fp = fopen(buf, "r");
								if (fp != NULL)
								{
									imports[i]->setCompPtr(node->askPart());
									imports[i]->setExists(true);
									imports[i]->setInstanceTag(member);
									imports[i]->findFileName(dirName);
									fclose(fp);
									// ADDED 3/30
									// check to see whether import is assembly or not
									part_occur = node->getTag();
									numChilds = UF_ASSEM_ask_part_occ_children( part_occur, &child_part_occs );
									if ( numChilds > 0 )
									{
										imports[i]->setIsAssembly(true);
										UF_free( child_part_occs );
									}
									else
										imports[i]->setIsAssembly(false);
								}
							}
							skipImport = 1;
							--l;
							break;
						}
					}
				}
				if ( skipImport < 0 )
				{
					if ( k > 0 )
					{
						UG_DEBUG("check with components...");
						for ( i=1; i< components.size(); i++ ) {
							if ( !components[i]->getExists() )
							{
								part_match = extract_name.compare(components[i]->getCompName());
								if (part_match == 0) 
								{
									UG_DEBUG("found match with component!");
									components[i]->setCompPtr(node->askPart());
									components[i]->setInstanceTag(member);
									components[i]->setExists(true);
									// check to see whether part is assembly or not
									part_occur = node->getTag();
									sprintf(buf, "instanceTag = %d member = %d", part_occur, member);
									UG_DEBUG(buf); // values same
									numChilds = UF_ASSEM_ask_part_occ_children( part_occur, &child_part_occs );
//									sprintf(buf, "num child = %d", numChilds);
//									UG_DEBUG(buf);
									if ( numChilds > 0 )
									{
										components[i]->setIsAssembly(true);
										parentIndex.push_back(i);
										UF_free( child_part_occs );
									}
									else
										components[i]->setIsAssembly(false);
									--k;
									break;
								}
							}
						}
					}
				}
			}
		}
		UG_DEBUG("Out of for loop");
		// find all parents for components
		findParents();
		// set info for vrml
		for ( i = 0; i < components.size(); i++ )
		{
			if ( components[i]->getExists() )
			{
				if ( components[i]->getIsAssembly() )
				{
					sprintf(buf, "-1 \"%s\" %d %d", components[i]->getCompName().c_str(), i,
						components[i]->getParent());
					allCompExists.push_back(string(buf));
				}
				else
				{
					sprintf(buf, "-2 \"%s\" %d %d", components[i]->getCompName().c_str(), i, 
						components[i]->getParent());
					allCompExists.push_back(string(buf));
				}
			}
		}
		if ( imports.size() > 0 )
		{
			extractMatingObjects();
			findImportChildren();
			// TO DO: need to add stuff to the VRML file as well: darn!!!
			for ( i = 0; i < imports.size(); i++ )
			{
				if ( imports[i]->getExists() )
				{
					if ( imports[i]->getIsAssembly() )
					{
						sprintf(buf, "-3 \"%s\" %d %d", imports[i]->getName().c_str(), i, imports[i]->getParent());
						allCompExists.push_back(string(buf));
					}
					else
					{
						sprintf(buf, "-4 \"%s\" %d %d", imports[i]->getName().c_str(), i, imports[i]->getParent());
						allCompExists.push_back(string(buf));
					}
				}
			}
		}
	}
	catch (...)
	{
		UG_ERROR("UgModel::getAllComponentParts: unknown exception caught");
		cleanAll();
	}
}

void UgModel::findImportChildren()
{
	int	errorCode, i, j, subtype, type, match, numChilds;
	tag_t	member, part_occur, *child_part_occs=NULL;
	UgTypedObject	*pCurObj;
	UgPart* part;
	UgAssemblyNode	*node;
	string	extract_name, name;
//	char buf[MAX_LN_SIZE+1];

	try
	{
		UG_DEBUG( "in UgModel::findImportChildren" );
		for ( i = 0; i < imports.size(); i++ ) // cycle through all translated top components
		{
			if ( imports[i]->getExists() )
			{
				part = imports[i]->getCompPtr();
				UgSession::setWorkPart( part );
				for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
				{
					member = pCurObj->getTag();
					errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
					if (type == UF_component_type)
					{
						node = dynamic_cast <UgAssemblyNode *> (pCurObj);
						name = (node->askPart())->getFullName();
						extract_name = extract_part_name( name );
						match = -1;
						for ( j = 0; j < imports[i]->getChildrenSize(); j++ )
						{
							if ( !imports[i]->getChild(j).compare(extract_name) )
							{
								match = 1;
								break;
							}
						}
						if ( match < 0 ) // found unique children
						{
							imports[i]->addChild(extract_name);
							part_occur = node->getTag();
							numChilds = UF_ASSEM_ask_part_occ_children ( part_occur, &child_part_occs );
							if ( numChilds > 0 )
								UF_free( child_part_occs );
							else
								imports[i]->setBase(extract_name);
						}
					}
				}
			}
		}
		UgSession::setWorkPart( components[0]->getCompPtr() );
		// better idea to copy all .prt assembly and children files to /import/temp from here by
		// accessing children[i]
	}
	catch (...)
	{
		UG_ERROR( "UgModel::findImportChildren: unknown exception caught" );
		cleanAll();
	}
}

void UgModel::getPartsInfo()
{
	int	i;

	try
	{
		storeDOMEunit();
		for ( i = 0; i < components.size(); i++ )
		{
			if ( components[i]->getExists() )
			{
				components[i]->prepPart();
				components[i]->findDimObjects();
				components[i]->findMassValues(MassName);
			}
		}
		
		if ( components[0]->getExportsSize() > 0 ) components[0]->findExportInfo(dirName);
		
		UgSession::setDisplayPart(components[0]->getCompPtr());
		UgSession::setWorkPart(components[0]->getCompPtr());
		
		for ( i = 1; i < components.size(); i++ )
		{
			if ( components[i]->getExists() ) components[i]->updateBoundingBoxForAss();
		}
		for ( i = 0; i < imports.size(); i++ )
		{
			if ( imports[i]->getExists() )
				imports[i]->updateBoundingBoxForAss(); 
		}
	}
	catch (...)
	{
		UG_ERROR("UgModel::getPartsInfo: unknown exception caught");
		cleanAll();
	}
}

void UgModel::storeDOMEunit()
{
	MassName.push_back( string("density") ); // 0 [0]
	MassName.push_back( string("volume") ); // 1 [1]
	MassName.push_back( string("area") ); // 2 [2]
	MassName.push_back( string("mass") ); // 3 [3]
	MassName.push_back( string("Mxc") ); // 4 first moment [4]
	MassName.push_back( string("Myc") ); // 5
	MassName.push_back( string("Mzc") ); // 6
	MassName.push_back( string("Xcbar") ); // 7 center of gravity [5]
	MassName.push_back( string("Ycbar") ); // 8
	MassName.push_back( string("Zcbar") ); // 9
	MassName.push_back( string("Ixxw") ); // 10 moments of inertia work
	MassName.push_back( string("Iyyw") ); // 11
	MassName.push_back( string("Izzw") ); // 12
	MassName.push_back( string("Ixx") ); // 13 moments of inertia centroidal
	MassName.push_back( string("Iyy") ); // 14
	MassName.push_back( string("Izz") ); // 15
	MassName.push_back( string("Pyzw") ); // 16 product of inertia work
	MassName.push_back( string("Pxzw") ); // 17
	MassName.push_back( string("Pxyw") ); // 18
	MassName.push_back( string("Pyz") ); // 19 product of inertia centroidal
	MassName.push_back( string("Pxz") ); // 20
	MassName.push_back( string("Pxy") ); // 21
	MassName.push_back( string("Rgxw") ); // 22 radii of gyration work
	MassName.push_back( string("Rgyw") ); // 23
	MassName.push_back( string("Rgzw") ); // 24
	MassName.push_back( string("Rgx") ); // 25 radii of gyration centroidal
	MassName.push_back( string("Rgy") ); // 26
	MassName.push_back( string("Rgz") ); // 27
	MassName.push_back( string("Ixxp") ); // 28 principal moments of inertia
	MassName.push_back( string("Iyyp") ); // 29
	MassName.push_back( string("Izzp") ); // 30
	MassName.push_back( string("Xp_X") ); // 31 principal axes
	MassName.push_back( string("Xp_Y") ); // 32
	MassName.push_back( string("Xp_Z") ); // 33
	MassName.push_back( string("Yp_X") ); // 34
	MassName.push_back( string("Yp_Y") ); // 35
	MassName.push_back( string("Yp_Z") ); // 36
	MassName.push_back( string("Zp_X") ); // 37
	MassName.push_back( string("Zp_Y") ); // 38
	MassName.push_back( string("Zp_Z") ); // 39
}

void UgModel::cleanAll()
{
	int i;
	char buf[MAX_LN_SIZE+1];

	UG_DEBUG("in UgModel::cleanAll");
	progFail = 1;
	if(imports.size() > 0)
				storedImport = 1;
	for ( i = 0; i < components.size(); i++ )
		components[i]->clean();
	for ( i = 0; i < imports.size(); i++ )
		imports[i]->clean();
	vrmlFile->clean();
	while (!MassName.empty())
		MassName.pop_back();

	exportNow.setValue(false);
	_isModelLoaded = false;
	while (!baseNames.empty())
		baseNames.pop_back();
	while (!baseDensities.empty())
		baseDensities.pop_back();
	while (!allCompExists.empty())
		allCompExists.pop_back();
	while (!parentIndex.empty())
		parentIndex.pop_back();

	vrmlIndex = -1;
	vrmlIndexOld = -1;
	exportNow.setValue(false);

	if ( storedImport > 0 ) // have record of name of children -> delete only those children files!!!
	{
    		UgSession::closeAllParts();
			// modify below!
			sprintf(buf, "del %s\\*.prt", import_directory.c_str());
			system(buf);
			sprintf(buf, "copy %s\\temp\\*.* %s", import_directory.c_str(), import_directory.c_str());
			system(buf);
			sprintf(buf, "del %s\\temp\\*.prt", import_directory.c_str());
			system(buf);
			sprintf(buf, "rd %s\\temp", import_directory.c_str());
			system(buf);
	}
	storedImport = -1;
	
	UG_DEBUG("done with UgModel::cleanAll");
	UgSession::terminate();
}

void UgModel::startVRML()
{
	char fspec[MAX_LN_SIZE+1], buf[MAX_LN_SIZE+1];
	int	i, errorCode, index;
	string wrl;
	double ix, iy, iz, ax, ay, az, min, max;
	FILE *fp, *fp2;

	try
	{

		UG_DEBUG("UgModel::startVRML()");
		UgSession::setDisplayPart( components[vrmlIndex]->getCompPtr() );
		UgSession::setWorkPart( components[vrmlIndex]->getCompPtr() );
		UG_DEBUG("vrmlIndex: " << vrmlIndex << " vrmlIndexOld: " << vrmlIndexOld);
		if ( vrmlIndex != vrmlIndexOld )
		{
			sprintf(buf, "in startVRML: about to update bBox");
			UG_DEBUG(buf);
			for (i = 0; i < 6; i++) 
				vrmlFile->setBBox(i, components[vrmlIndex]->getBoundingBox(i));
			vrmlFile->getVrmlViews();
			vrmlIndexOld = vrmlIndex;
		}
		sprintf(fspec, "%s\\%s_temp", export_directory.c_str(), model.c_str());
		sprintf(buf, "in exportVRML to export: %s.wrl", fspec);
		UG_DEBUG(export_directory.c_str());
		sprintf(buf, "%s\\vrml.htt", export_directory.c_str()); // location of template file
/*		errorCode = UF_WEB_author_html(fspec, buf, FALSE); 
		UF_CALL(errorCode); UG_DEBUG(fspec);
		sprintf(buf, "del %s", fspec); // UF_WEB_author_html generates a non needed file, delete it
		system(buf); 
*/
		sprintf(buf,"%s.wrl", fspec);
		errorCode = UF_STD_create_vrml_file(buf, 1.0, UF_STD_OUTPUT_LIGHTS | UF_STD_OUTPUT_MATERIALS | UF_STD_OUTPUT_TEXTURES | UF_STD_OUTPUT_BACKGROUND | UF_STD_OUTPUT_VRML_2);
		if(errorCode)
		{
			UF_CALL(errorCode);
		}
		wrl.assign(buf); 
	
		/*
		 * at this point only the template file vrml.htt is created
		 */
		
		// print bounding box info out only if vrmlIndex == 0 and is for assembly
		index = 1; 
		if ( (vrmlIndex == 0) && (components[0]->getExists()) )
		{
			UG_DEBUG("Inside 1st if statement");
			if ( components[0]->getIsAssembly() )
			{
				UG_DEBUG("Inside 2nd if statement");
				sprintf(buf, "%s\\bd", export_directory.c_str());
				fp = fopen(buf, "w");
				sprintf(buf, "%s\\bd2", export_directory.c_str());
				fp2 = fopen(buf, "w"); 
				for ( i = 1; i < components.size(); i++ )
				{
					if ( components[i]->getExists() )
					{
						UG_DEBUG("There is one more component...");
						min = index - 0.1;
						max = index + 0.1;
						sprintf(buf, "\nDEF Comp%d Script {\n", index);
						fputs( buf, fp );
						sprintf(buf, "    eventIn SFFloat turnOff eventIn SFFloat highlight\n");
						fputs( buf, fp );
						sprintf(buf, "    eventOut SFBool wasTurnedOn eventOut SFFloat transparency\n\n");
						fputs( buf, fp );
						sprintf(buf, "    url \"javascript:\n");
						fputs( buf, fp );
						sprintf(buf, "    function initialize() { wasTurnedOn = false; transparency = 1.0; }\n");
						fputs( buf, fp );
						sprintf(buf, "    function turnOff(value) {\n");
						fputs( buf, fp );
						sprintf(buf, "        if (((value > %.1f) || (value < %.1f)) && wasTurnedOn)\n", max, min);
						fputs( buf, fp );
						sprintf(buf, "        { wasTurnedOn = false; transparency = 1.0; }\n");
						fputs( buf, fp );
						sprintf(buf, "    }\n    function highlight(value) {\n");
						fputs( buf, fp );
						sprintf(buf, "        if ((value < %.1f) && (value > %.1f) && !wasTurnedOn)\n",	max, min);
						fputs( buf, fp );
						sprintf(buf, "        { wasTurnedOn = true; transparency = 0.0; }\n    }\"\n}\n");
						fputs( buf, fp ); UG_DEBUG("before getting bounding box coordinates...");
						// 0 for min x, 1 for min y, 2 for min z, 3 for max x, 4 for max y, 5 for max z
						ix = components[i]->getBoundingBoxAssCoord(0); 
						iy = components[i]->getBoundingBoxAssCoord(1);
						iz = components[i]->getBoundingBoxAssCoord(2);
						ax = components[i]->getBoundingBoxAssCoord(3);
						ay = components[i]->getBoundingBoxAssCoord(4);
						az = components[i]->getBoundingBoxAssCoord(5);
						sprintf(buf, "\nGroup { children [\n");
						fputs( buf, fp );
						sprintf(buf, "        Transform { children [ Shape { geometry IndexedLineSet {\n");
						fputs( buf, fp );
						sprintf(buf, "           coord Coordinate { point [ %.4f %.4f %.4f, %.4f %.4f %.4f,\n",
							ix, iy, iz, ix, iy, az); // 0, 1
						fputs( buf, fp );
						sprintf(buf, "             %.4f %.4f %.4f, %.4f %.4f %.4f, %.4f %.4f %.4f,\n",
							ix, ay, iz, ix, ay, az, ax, iy, iz); // 2, 3, 4
						fputs( buf, fp );
						sprintf(buf, "             %.4f %.4f %.4f, %.4f %.4f %.4f, %.4f %.4f %.4f ] }\n",
							ax, iy, az, ax, ay, iz, ax, ay, az); // 5, 6, 7
						fputs( buf, fp );
						sprintf(buf, "           coordIndex [0 1 -1 1 3 -1 3 2 -1 2 0 -1 0 4 -1 4 5 -1 5 1 -1 4 6 -1\n");
						fputs( buf, fp );
						sprintf(buf, "                       6 2 -1 6 7 -1 7 3 -1 5 7 ]\n");
						fputs( buf, fp );
						sprintf(buf, "           color Color { color [ 1 1 1, 1 1 1, 1 1 1, 1 1 1, 1 1 1, 1 1 1,\n");
						fputs( buf, fp );
						sprintf(buf, "                                 1 1 1, 1 1 1, 1 1 1, 1 1 1, 1 1 1, 1 1 1 ] } }\n");
						fputs( buf, fp );
						sprintf(buf, "           appearance Appearance { material DEF Mcomp%d Material {\n", index);
						fputs( buf, fp );
						sprintf(buf, "               diffuseColor 1 1 1 emissiveColor 1 1 1 transparency 0.0 } }\n", index);
						fputs( buf, fp );
						sprintf(buf, "} ] } ] }\n");
						fputs( buf, fp );
						// now print routes
						sprintf(buf, "ROUTE MatSet.ambientIntensity TO Comp%d.highlight\n", index);
						fputs( buf, fp2 );
						sprintf(buf, "ROUTE MatSet.ambientIntensity TO Comp%d.turnOff\n", index);
						fputs( buf, fp2 );
						sprintf(buf, "ROUTE Comp%d.transparency TO Mcomp%d.set_transparency\n", index, index);
						fputs( buf, fp2 );
						++index;
						UG_DEBUG("printing component...");
					}
				}
				for ( i = 0; i < imports.size(); i++ )
				{
					if ( imports[i]->getExists() )
					{
						UG_DEBUG("i don't even see this");
						min = index - 0.1;
						max = index + 0.1;
						sprintf(buf, "\nDEF Comp%d Script {\n", index); UG_DEBUG("somewhere in the middle1");
						fputs( buf, fp );
						sprintf(buf, "    eventIn SFFloat turnOff eventIn SFFloat highlight\n");
						fputs( buf, fp );
						sprintf(buf, "    eventOut SFBool wasTurnedOn eventOut SFFloat transparency\n\n");
						fputs( buf, fp );
						sprintf(buf, "    url \"javascript:\n");
						fputs( buf, fp );
						sprintf(buf, "    function initialize() { wasTurnedOn = false; transparency = 1.0; }\n");
						fputs( buf, fp );
						sprintf(buf, "    function turnOff(value) {\n");
						fputs( buf, fp );
						sprintf(buf, "        if (((value > %.1f) || (value < %.1f)) && wasTurnedOn)\n", max, min);
						fputs( buf, fp );
						sprintf(buf, "        { wasTurnedOn = false; transparency = 1.0; }\n");
						fputs( buf, fp );
						sprintf(buf, "    }\n    function highlight(value) {\n");
						fputs( buf, fp );
						sprintf(buf, "        if ((value < %.1f) && (value > %.1f) && !wasTurnedOn)\n",	max, min);
						fputs( buf, fp );
						sprintf(buf, "        { wasTurnedOn = true; transparency = 0.0; }\n    }\"\n}\n");
						fputs( buf, fp ); UG_DEBUG("this should print hopefully...");
						// 0 for min x, 1 for min y, 2 for min z, 3 for max x, 4 for max y, 5 for max z
						ix = imports[i]->getBoundingBoxAssCoord(0);
						iy = imports[i]->getBoundingBoxAssCoord(1);
						iz = imports[i]->getBoundingBoxAssCoord(2);
						ax = imports[i]->getBoundingBoxAssCoord(3);
						ay = imports[i]->getBoundingBoxAssCoord(4);
						az = imports[i]->getBoundingBoxAssCoord(5); UG_DEBUG("this should print maybe...");
						sprintf(buf, "\nGroup { children [\n");
						fputs( buf, fp );
						sprintf(buf, "        Transform { children [ Shape { geometry IndexedLineSet {\n");
						fputs( buf, fp );
						sprintf(buf, "           coord Coordinate { point [ %.4f %.4f %.4f, %.4f %.4f %.4f,\n",
							ix, iy, iz, ix, iy, az); // 0, 1
						fputs( buf, fp );
						sprintf(buf, "             %.4f %.4f %.4f, %.4f %.4f %.4f, %.4f %.4f %.4f,\n",
							ix, ay, iz, ix, ay, az, ax, iy, iz); // 2, 3, 4
						fputs( buf, fp );
						sprintf(buf, "             %.4f %.4f %.4f, %.4f %.4f %.4f, %.4f %.4f %.4f ] }\n",
							ax, iy, az, ax, ay, iz, ax, ay, az); // 5, 6, 7
						fputs( buf, fp );
						sprintf(buf, "           coordIndex [0 1 -1 1 3 -1 3 2 -1 2 0 -1 0 4 -1 4 5 -1 5 1 -1 4 6 -1\n");
						fputs( buf, fp );
						sprintf(buf, "                       6 2 -1 6 7 -1 7 3 -1 5 7 ]\n");
						fputs( buf, fp );
						sprintf(buf, "           color Color { color [ 1 1 1, 1 1 1, 1 1 1, 1 1 1, 1 1 1, 1 1 1,\n");
						fputs( buf, fp );
						sprintf(buf, "                                 1 1 1, 1 1 1, 1 1 1, 1 1 1, 1 1 1, 1 1 1 ] } }\n");
						fputs( buf, fp );
						sprintf(buf, "           appearance Appearance { material DEF Mcomp%d Material {\n", index);
						fputs( buf, fp );
						sprintf(buf, "               diffuseColor 1 1 1 emissiveColor 1 1 1 transparency 0.0 } }\n", index);
						fputs( buf, fp );
						sprintf(buf, "} ] } ] }\n");
						fputs( buf, fp );
						UG_DEBUG("somewhere in the middle...");
						// now print routes
						sprintf(buf, "ROUTE MatSet.ambientIntensity TO Comp%d.highlight\n", index);
						fputs( buf, fp2 );
						sprintf(buf, "ROUTE MatSet.ambientIntensity TO Comp%d.turnOff\n", index);
						fputs( buf, fp2 );
						sprintf(buf, "ROUTE Comp%d.transparency TO Mcomp%d.set_transparency\n", index, index);
						fputs( buf, fp2 );
						++index;
					}
				}
				fclose(fp);
				fclose(fp2);
			}
		}
		// only print 1 component at a time
		if ( components[vrmlIndex]->getExists() )
		{
			components[vrmlIndex]->printToVRML(wrl, export_directory, allCompExists);
		}
/*		for ( i = 0; i < components.size(); i++ )
		{
			if ( components[i]->getExists() )
				components[i]->printToVRML(wrl, export_directory);
		}*/
	}
	catch (...)
	{
		UG_ERROR("UgModel::startVRML: unknown exception caught");
		cleanAll();
	}
}

void UgModel::export()
{
	UG_DEBUG( "in UgModel::export" );

	try
	{
		if ( isModelLoaded() )
			components[0]->exportAll(dirName, export_directory, UGII_BASE_DIR);
	}
	catch(...)
	{
		UG_ERROR( "Unexpected exception in UgModel::export" );
		cleanAll();
	}
}

void UgModel::import()
{
	int i;

	UG_DEBUG( "in UgModel::import" );

	try
	{
		if(isModelLoaded())
		{
			UG_DEBUG("Model is loaded...");
			for ( i = 0; i < imports.size(); i++ )
			{
				UG_DEBUG("Import size: " << imports.size());
				UG_DEBUG("Is Changed? " << imports[i]->getChanged());
				UG_DEBUG("Is exists? " << imports[i]->getExists());
				if ( imports[i]->getChanged() && imports[i]->getExists() )
				{
					UG_DEBUG("are we in here...");
					autoImport(i);
					imports[i]->setChanged(false);
				}
			}
			// ADDED 3/30
/*			for ( i = 0; i < imports.size(); i++ )
			{
				if ( imports[i]->getExists() )
					imports[i]->updateBoundingBoxForAss();
			}*/
		}
		
	}
	catch(...)
	{
		UG_ERROR( "Unexpected exception in UgModel::import" );
		cleanAll();
	}
}

void UgModel::autoImport( int index )
{
	char	buf[MAX_LN_SIZE+1], filename[MAX_LN_SIZE+1], instance_name[MAX_ENTITY_NAME_SIZE+1], msg[MAX_LN_SIZE+1];
	char	part_name[MAX_FSPEC_SIZE+1], refset_name[MAX_ENTITY_NAME_SIZE+1];
    double	instance_matrix[9], instance_origin[3], instance_trans[4][4], matrix[6] = { 1,0,0,0,1,0 };
	double	origin[3] = { 0, 0, 0 };
	int	errorCode, i=0, j, recalc_bbox=1, type;
    UF_PART_load_status_t	load_status;
	UgAssemblyNode *node;
	UgPart *part;
	string	extract_name, name;
	UgTypedObject	*pCurObj;
	tag_t tag;

	try
	{
		UG_DEBUG( "in UgModel::autoImport" );
		sprintf(buf, "# copies of imports[%d] = %d", index, imports[index]->getNeutralInstanceSize());
		UG_DEBUG(buf);
		UgSession::setDisplayPart( components[0]->getCompPtr() );
		UgSession::setWorkPart( components[0]->getCompPtr() );

		// convert neutral files into UG prt files before adding to assembly
		sprintf(buf, "in autoImport, about to translate %s", imports[index]->getFileName().c_str());
		UG_DEBUG(buf);
		importPart( index ); // should create new parts, check
		sprintf(buf, "in autoImport, done loading %s", imports[index]->getFileName().c_str());
		UG_DEBUG(buf);
		if ( imports[index]->getMatingDir() == 1 ) // append _zkr
			sprintf(buf, "%s\\%s_zkr.prt", import_directory.c_str(), imports[index]->getName().c_str());
		else // don't append to original neutral_part_name_array
			sprintf(buf, "%s\\%s.prt", import_directory.c_str(), imports[index]->getName().c_str());
		part = UgPart::open( buf );
		UG_DEBUG( "opened part successfully for incorporation into assembly" );

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		UgSession::setWorkPart( part );
		UgSession::setDisplayPart( part );

		assignMatingObjects( index, part );

		UG_DEBUG( "about to save" );
		part->save(); // saves part with assigned mating surface names
		part->close(TRUE);
		UG_DEBUG( "done saving");
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		for (i = 0; i < imports[index]->getNeutralInstanceSize(); i++) // substitute instances of translated parts via Alternates method
		{
			if ( imports[index]->getMatingDir() == 1 )
			{
				sprintf(filename, "%s\\%s_zkr.prt", import_directory.c_str(), imports[index]->getName().c_str());
				sprintf(buf, "about to substitute into assembly %s", filename);
			}
			else
			{
				sprintf(filename, "%s\\%s.prt", import_directory.c_str(), imports[index]->getName().c_str());
				sprintf(buf, "about to substitute into assembly %s", filename);
			}
			UG_DEBUG(buf);

			errorCode = UF_ASSEM_ask_component_data(imports[index]->getNeutralInstance(i), part_name, refset_name,
			instance_name, instance_origin, instance_matrix, instance_trans);
			sprintf(buf, "part_name: %s instance_name: %s", part_name, instance_name);
			UG_DEBUG(buf);
			tag = imports[index]->getNeutralInstance(i);
			errorCode = UF_ASSEM_use_alternate(&tag, filename, instance_name, 
				refset_name, &load_status);
			for ( j = 0; j < load_status.n_parts; j++ )
			{
				UF_get_fail_message(load_status.statuses[j], msg);
			    sprintf(buf, "    %s - %s", load_status.file_names[j], msg);
				UG_DEBUG(buf);
			}
			if (load_status.n_parts > 0)
			{
				UF_free(load_status.statuses);
				UF_free_string_array(load_status.n_parts, load_status.file_names);
			}
		}

		// first unload and then delete older version of this translated assembly/component
		imports[index]->getCompPtr()->close(TRUE);
		for ( i = 0; i < imports[index]->getChildrenSize(); i++ )
		{
			sprintf(buf, "del %s\\%s.prt", import_directory.c_str(), imports[index]->getChild(i).c_str());
			system( buf );
		}
		imports[index]->updateChild(); // don't need anymore if have import2 directory; but keep anyway...

		// update next matingDir
		if ( imports[index]->getMatingDir() == 1 )
		{
			imports[index]->setMatingDir(2);
			sprintf(buf, "del %s\\%s.prt", import_directory.c_str(), imports[index]->getName().c_str());
			system( buf );
		}
		else
		{
			imports[index]->setMatingDir(1);
			sprintf(buf, "del %s\\%s_zkr.prt", import_directory.c_str(), imports[index]->getName().c_str());
			system( buf );
		}
		sprintf(buf, "new matingDir[%d] = %d", index, imports[index]->getMatingDir());
		UG_DEBUG(buf);

		// obtain latest ImNeutrals pointer
		part = components[0]->getCompPtr();
		for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
		{
			type = pCurObj->askType();
			if (type == UF_component_type)
			{
				node = dynamic_cast <UgAssemblyNode *> (pCurObj);
				name = (node->askPart())->getFullName();
				sprintf(buf, "with name: %s", name.c_str());
				UG_DEBUG(buf);
				imports[index]->setCompPtr(node->askPart());
				break;
			}
		}
		UG_DEBUG( "done in UgModel::autoImport" ); 
	}
	catch (...)
	{
		UG_ERROR( "UgModel::autoImport: unknown exception caught" );
		cleanAll();
	}
}

void UgModel::importPart( int i )
{
	string	base_dir, in, log, out, iges_to_ug, step_to_ug;
	char	buf[MAX_LN_SIZE+1];

	try
	{
		// import options: 0->none, 1->.igs; 2->203.stp, latest is 214.stp though
		sprintf(buf, "import translator type: %d", imports[i]->getType());
		UG_DEBUG(buf);

		in = imports[i]->getFileName();
		if ( imports[i]->getMatingDir() == 1 )
			sprintf(buf,"o=%s\\%s_zkr.prt", import_directory.c_str(), imports[i]->getName().c_str());
		else // translate to import directory
			sprintf(buf,"o=%s\\%s.prt", import_directory.c_str(), imports[i]->getName().c_str());
		out = string( buf );

		switch (imports[i]->getType())
		{
			case 1:
				sprintf(buf, "%s\\IGES\\iges.cmd", UGII_BASE_DIR.c_str());
				base_dir = string( buf );
				sprintf(buf, "d=%s\\IGES\\igesimport.def", UGII_BASE_DIR.c_str());
				iges_to_ug = string( buf );
				if ( imports[i]->getMatingDir() == 1 )
					sprintf(buf, "l=%s\\%s_zkr_igesUg.log", import_directory.c_str(), imports[i]->getName().c_str());
				else
					sprintf(buf, "l=%s\\%s_igesUg.log", import_directory.c_str(), imports[i]->getName().c_str());
				log = string( buf );
				sprintf(buf,"%s %s %s %s %s", base_dir.c_str(), in.c_str(), out.c_str(), log.c_str(),
					iges_to_ug.c_str());
				system(buf); 
				break;
			case 2:
				sprintf(buf, "%s\\STEP203UG\\step203ug.cmd", UGII_BASE_DIR.c_str());
				base_dir = string( buf );
				sprintf(buf, "d=%s\\STEP203UG\\step203ug.def", UGII_BASE_DIR.c_str());
				step_to_ug = string( buf );
				if ( imports[i]->getMatingDir() == 1 )
					sprintf(buf, "l=%s\\%s_zkr_stepUg.log", import_directory.c_str(), imports[i]->getName().c_str());
				else
					sprintf(buf, "l=%s\\%s_stepUg.log", import_directory.c_str(), imports[i]->getName().c_str());
				log = string( buf );
				sprintf(buf,"%s %s %s %s %s", base_dir.c_str(), in.c_str(), out.c_str(), log.c_str(),
					step_to_ug.c_str());
				system(buf); 
				break;
		}
	}
	catch (...)
	{
		UG_ERROR("UgModel::importPart: unknown exception caught");
		cleanAll();
	}
}

void UgModel::assignMatingObjects( int index, UgPart* part )
{
	char	buf[MAX_LN_SIZE+1], obj_name[30];
	int	count, i, /*j,*/ part_match, errorCode, type, subtype, body_type, numChilds, max_count;
	int	faceNum, extract_num;
	tag_t	member, part_occur, member2, face_tag, *child_part_occs=NULL, *part_tag=NULL, object_tag=NULL_TAG;
	UgAssemblyNode *node;
	string	filedir, extract_name, iname, name, obj_string;
	UgTypedObject	*pCurObj;
	vector <UgPart*> part_assembly, part_base;
	vector <tag_t> allChildren; // associates with all children in the order cycled
	vector <int> allChildToPartBase;// same size as allChildren, maps to index for parts
	vector <int> minFaceCount; // same size as allChildren;
	vector <int> minFace; // same size as parts
	vector <int> isBase, num_faces, part_base_index, base_isFirst, total_index;
	vector <string> new_neutralChildrenName;
	vector <int> startNum;
	vector <string> base;
	double density;

	UG_DEBUG( "in UgModel::assignMatingObjects" );

	try
	{
		// NOTE: UG does not allow for you to check to see if a component within
		// an assembly is the base component, therefore, must cycle through all
		// components
		// ALSO: names assigned are base level carries over in export but names
		// assigned in assembly level do not carry over in export
		// this applies only to step files, not to iges files
		for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
		{
			member = pCurObj->getTag();
			errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
			// assign name of components[0] to all solid bodies
			if ( type == UF_solid_type && subtype == UF_solid_body_subtype )
			{
				errorCode = UF_MODL_ask_body_type( member, &body_type );
				if ( body_type == UF_MODL_SOLID_BODY ) // limit to solid bodies only, no sheet bodies
				{
					sprintf(buf, "%s", model.c_str());
					errorCode = UF_OBJ_set_name( member, buf );
				}
			}
			// extract names of unique children
			if (type == UF_component_type)
			{
				node = dynamic_cast <UgAssemblyNode *> (pCurObj);
				name = (node->askPart())->getFullName();
				extract_name = extract_part_name( name );
				part_match = -1;
				for ( i = 0; i < new_neutralChildrenName.size(); i++ ) 
				{
					part_match = extract_name.compare(new_neutralChildrenName[i]);
					if (part_match == 0) 
					{
						if ( isBase[i] == 1 )
						{
							allChildren.push_back( member );
							allChildToPartBase.push_back( total_index[i] );
							minFaceCount.push_back( 0 );
							base_isFirst.push_back( 0 );
							sprintf(buf, "%d repeat base: %s; allChildToPartBase[%d] = %d", 
								allChildren.size()-1, new_neutralChildrenName[i].c_str(), 
								allChildToPartBase.size()-1,
								allChildToPartBase[allChildToPartBase.size()-1]);
							UG_DEBUG(buf);
//							UG_DEBUG( "u2 tag = " << member );
						}
						else
						{
							sprintf(buf, "repeat assembly: %s", new_neutralChildrenName[i].c_str());
							UG_DEBUG(buf);
//							UG_DEBUG( "u2b tag = " << member );
						}
						break;
					}
				}
				if ( part_match != 0) // new component
				{
					new_neutralChildrenName.push_back( extract_name );
					sprintf(buf, "num new_neutralChildrenName for neutral[%d] = %d; %s", index, 
						new_neutralChildrenName.size()-1, 
						new_neutralChildrenName[new_neutralChildrenName.size()-1].c_str());
					UG_DEBUG(buf);
					// figure out whether component is base component
					part_occur = node->getTag();
					numChilds = UF_ASSEM_ask_part_occ_children ( part_occur, &child_part_occs );
					sprintf(buf, "num child = %d", numChilds);
					UG_DEBUG(buf);
					if ( numChilds > 0 )
					{
						isBase.push_back( 0 );
						sprintf(buf, "new assembly: %s", 
							new_neutralChildrenName[new_neutralChildrenName.size()-1].c_str());
						UG_DEBUG(buf);
						total_index.push_back( -1 );
//						UG_DEBUG( "u1b tag = " << member );
					}
					else
					{
						part_base.push_back( node->askPart() );
						isBase.push_back( 1 );
						allChildren.push_back( member );
						minFaceCount.push_back( 0 );
						part_base_index.push_back( allChildren.size()-1 );
						allChildToPartBase.push_back( part_base_index.size()-1 );
						base_isFirst.push_back( 1 );
						total_index.push_back( part_base_index.size()-1 );
						sprintf(buf, "%d new base: %s; allChildToPartBase[%d] = %d", 
							allChildren.size()-1, 
							new_neutralChildrenName[new_neutralChildrenName.size()-1].c_str(), 
							allChildToPartBase.size()-1, 
							allChildToPartBase[allChildToPartBase.size()-1]);
						UG_DEBUG(buf);
//						UG_DEBUG( "u1 tag = " << member );
						base.push_back(new_neutralChildrenName[new_neutralChildrenName.size()-1]);
					}
					// free child_part_occs
					if ( numChilds > 0 )
						UF_free( child_part_occs );
				}
			}
		}
		for ( i = 0; i < part_base_index.size(); i++ )
		{
			sprintf(buf, "part_base_index[%d] = %d", i, part_base_index[i]);
			UG_DEBUG(buf);
		}
		for ( i = 0; i < allChildren.size(); i++ )
		{
			sprintf(buf, "allChildToPartBase[%d] = %d", i, allChildToPartBase[i]);
			UG_DEBUG(buf);
		}
		count = 1;
		for ( i = 0; i < part_base.size(); i++ )
		{
			minFace.push_back( count - 1 );
			minFaceCount[part_base_index[i]] = minFace[i];

			density = matchDensity(index, base[i]);

			UgSession::setWorkPart( part_base[i] );
			for (pCurObj = part_base[i]->iterateFirst(); pCurObj; pCurObj = part_base[i]->iterateNext(pCurObj))
			{
				member = pCurObj->getTag();
				errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
				// assign name to solid face
				if ( type == UF_solid_type && subtype == UF_solid_face_subtype )
				{
					sprintf(buf, "%d", count);
					errorCode = UF_OBJ_set_name( member, buf );
					++count;
				}
				// assign color to solid body & assign density to all solid bodies
				else
				{
					if ( type == UF_solid_type && subtype == UF_solid_body_subtype ) // solid objects: bodies and sheets
					{
						errorCode = UF_MODL_ask_body_type( member, &body_type );
						if ( body_type == UF_MODL_SOLID_BODY )
						{
							errorCode = UF_MODL_set_body_density(member, UF_MODL_pounds_inches, density);
							errorCode = UF_OBJ_set_color( member, imports[index]->getColor() );
						}
					}
				}
//				if ( neutralColor[index] > -1 && type == UF_solid_type && subtype == UF_solid_body_subtype )
/*				if ( type == UF_solid_type && subtype == UF_solid_body_subtype )
				{
					errorCode = UF_MODL_ask_body_type( member, &body_type );
					if ( body_type == UF_MODL_SOLID_BODY )
						errorCode = UF_OBJ_set_color( member, imports[index]->getColor() );
				}*/
			}
			sprintf(buf, "minFace[%d] = %d", i, minFace[i]);
			UG_DEBUG(buf);
			part_base[i]->save();
		}
		for ( i = 0; i < minFaceCount.size(); i++ )
		{
			sprintf(buf, "%d: minFaceCount[%d] = %d", allChildToPartBase[i], i, minFaceCount[i]);
			UG_DEBUG(buf);
		}
		// assign minFaceCount to all arrayed elements
		max_count = count-1;
		minFace.push_back( max_count );
		for ( i = 1; i < minFaceCount.size(); i++ )
		{
			if ( minFaceCount[i] == 0 )
			{
				minFaceCount[i] = max_count;
				max_count = max_count + minFace[allChildToPartBase[i]+1] - minFace[allChildToPartBase[i]];
				sprintf(buf, "new max_count = %d", max_count);
				UG_DEBUG(buf);
			}
		}
		UG_DEBUG( "Printout of minFaceCount" );
		for ( i = 0; i < minFaceCount.size(); i++ )
		{
			sprintf(buf, "%d: minFaceCount[%d] = %d", allChildToPartBase[i], i, minFaceCount[i]);
			UG_DEBUG(buf);
		}
		for ( i = 0; i < allChildToPartBase.size(); i++ )
		{
			sprintf(buf, "allChildren[%d] = %d", i, allChildren[i]);
			UG_DEBUG(buf);
		}
		// extract name of faces from base component and assign to top most assembly
		if ( part_base.size() > 0 )
		{
			for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
			{
				member = pCurObj->getTag();
				errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
				if ( type == UF_solid_type && subtype == UF_solid_face_subtype )
				{
					member2 = UF_ASSEM_ask_prototype_of_occ ( member );
					errorCode = UF_OBJ_ask_name( member2, obj_name );
					if ( strlen(obj_name) > 0 ) //should always be
					{
						// convert characters to number
						sprintf(buf, "%s", obj_name );
						extract_num = atoi( buf );
						// assign proper face number id
						face_tag = UF_ASSEM_ask_part_occurrence( member );
						for ( i = 0; i < allChildToPartBase.size(); i++ )
						{
							if ( face_tag == allChildren[i] )
							{
								faceNum = minFaceCount[i]+extract_num-minFace[allChildToPartBase[i]];
								sprintf(buf, "found tag[%d]: oldface %d newface %d", 
									i, extract_num, faceNum);
								UG_DEBUG(buf);
								break;
							}
						}
						sprintf(obj_name, "%d", faceNum);
						errorCode = UF_OBJ_set_name( member, obj_name );
					}
				}
			}
		}
		else // component is a single part, not assembly
		{
			count = 1;
			density = matchDensity2(index);
			for (pCurObj = part->iterateFirst(); pCurObj; pCurObj = part->iterateNext(pCurObj))
			{
				member = pCurObj->getTag();
				errorCode = UF_OBJ_ask_type_and_subtype(member, &type, &subtype);
				if ( type == UF_solid_type && subtype == UF_solid_face_subtype )
				{
					sprintf(buf, "%d", count);
					errorCode = UF_OBJ_set_name( member, buf );
					++count;
				}
				else
				{
					if ( type == UF_solid_type && subtype == UF_solid_body_subtype ) // solid objects: bodies and sheets
					{
						errorCode = UF_MODL_ask_body_type( member, &body_type );
						if ( body_type == UF_MODL_SOLID_BODY )
						{
							errorCode = UF_MODL_set_body_density(member, UF_MODL_pounds_inches, density);
							errorCode = UF_OBJ_set_color( member, imports[index]->getColor() );
						}
					}
				}
			}
		}
		// must be in this order: setDisplayPart, then setWorkPart
		// set top assembly as current active viewing part
		UgSession::setDisplayPart( components[0]->getCompPtr() );
		UgSession::setWorkPart( components[0]->getCompPtr() );
		while ( !part_base.empty() )
		{
			part_base[part_base.size()-1]->close(TRUE);
			part_base.pop_back();
			part_base_index.pop_back();
			allChildren.pop_back();
			allChildToPartBase.pop_back();
			minFaceCount.pop_back();
			minFace.pop_back();
			base_isFirst.pop_back();
			base.pop_back();
		}
		for ( i = 0; i < new_neutralChildrenName.size(); i++ )
			imports[index]->setChildrenNew(new_neutralChildrenName[i]);
		while ( !isBase.empty() )
		{
			isBase.pop_back();
			new_neutralChildrenName.pop_back();
		}
	}
	catch (...)
	{
		UG_ERROR( "UgModel::assignMatingObjects: unknown exception caught");
		cleanAll();
	}
}

double UgModel::matchDensity(int index, string base)
{
	double density;
	int j, length, len1, part_match;
	string ccname;
	char	buf[MAX_LN_SIZE+1];

	try
	{
		density = 1.0;
		length = base.length(); // if iges, of form c_NAME###; if step, of form NAME###
		if ( imports[index]->getType() == 1 ) // iges
		{
			for ( j = 0; j < baseDensities.size(); j++ )
			{
				sprintf(buf, "c_%s", baseNames[j].c_str());
				ccname.assign(string(buf));
				len1 = ccname.length();
				part_match = base.find(ccname);
				if ( (part_match == 0) && (length-len1 >= 0) && (length-len1 <= 4) )
				{
					density = baseDensities[j];
					return density;
				}
			}
		}
		else // step
		{
			for ( j = 0; j < baseDensities.size(); j++ )
			{
				sprintf(buf, "%s", baseNames[j].c_str());
				ccname.assign(string(buf));
				len1 = ccname.length();
				part_match = base.find(ccname);
				if ( (part_match == 0) && (length-len1 > -1) && (length-len1 < 5) )
				{
					density = baseDensities[j];
					return density;
				}
			}
		}
		return density;
	}
	catch (...)
	{
		UG_ERROR( "UgModel::matchDensity: unknown exception caught");
		cleanAll();
	}

	return 0.0;
}

double UgModel::matchDensity2(int index)
{
	double density;
	int j, part_match;
	string ccname;

	try
	{
		density = 1.0;
		for ( j = 0; j < baseDensities.size(); j++ )
		{
			part_match = imports[index]->getName().compare(baseNames[j]);
			if (part_match == 0)
			{
				density = baseDensities[j];
				return density;
			}
		}
		return density;
	}
	catch (...)
	{
		UG_ERROR( "UgModel::matchDensity2: unknown exception caught");
		cleanAll();
	}

	return 0.0;
}

} // namespace UgPlugin
} // namespace DOME