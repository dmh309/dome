// SolidworksData.cpp: implementation of the SolidworksData class.
//
//////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include "SolidworksData.h"
namespace DOME {
namespace SolidworksPlugin {

SolidworksData::SolidworksData()
{
	_swModel = NULL;
}

SolidworksData::~SolidworksData()
{
	disconnect();
}

void SolidworksData::connect(IModelDoc* swModel) throw(DomeException) // does not have to be implemented
{
	if (swModel != NULL)
	{
		_swModel = swModel;}
	else
		SOLIDWORKS_ERROR("SolidworksData::connect: Solidworks ModelDoc pointer is null. Unable to connect.");
}

void SolidworksData::disconnect() // does not have to be implemented
{
	_swModel = NULL;
}

SolidworksDimension::SolidworksDimension(string dimensionName) // implemented
{
	_dimensionName = dimensionName;
	_dimension = NULL;
}

SolidworksDimension::~SolidworksDimension()
{
	disconnect();
}

double SolidworksDimension::getValue() throw(DomeException) // implemented
{
	if (_dimension != NULL)
	{
		return _dimension->GetValue();
	}
	else 
	{
		string message = "Unable to get value because _dimension is null.";
		SOLIDWORKS_ERROR("SolidworksDimension::getValue: "+message);
	}
}

void SolidworksDimension::setValue(double value) throw(DomeException) // implemented
{
	swSetValueReturnStatus_e successEnum;

	if (_dimension != NULL)
	{

		successEnum  = (swSetValueReturnStatus_e) _dimension->SetValue2(value, swSetValue_InAllConfigurations);

	} else {
		
		string message = "Unable to set value because _dimension is null.";
		SOLIDWORKS_ERROR("SolidworksDimension::setValue: "+message);
	}
	//SOLIDWORKS_DEBUG(successEnum);
	if (successEnum == swSetValue_Failure)
	SOLIDWORKS_ERROR("SolidworksDimension::setValue: Unknown failure setting value.");
	else if (successEnum == swSetValue_InvalidValue)
	SOLIDWORKS_ERROR("SolidworksDimension::setValue: Invalid value setting value.");
	else if (successEnum == swSetValue_DrivenDimension)
	SOLIDWORKS_ERROR("SolidworksDimension::setValue: Error attempting to set driven value.");
	else if (successEnum == swSetValue_ModelNotLoaded)
	SOLIDWORKS_ERROR("SolidworksDimension::setValue: Model not loaded, Unable to set value.");
}

void SolidworksDimension::connect(IModelDoc* model) throw(DomeException) // ???? does it have to be implemented?
{
	if (model == NULL) SOLIDWORKS_ERROR("Unable to connect, model is null.");

	SolidworksData::connect(model);

	LPDISPATCH dimDisp = model->Parameter(_dimensionName.c_str());
	if (!dimDisp){
		_dimension = NULL;
		string message = "Unable to find dimension named ";
		message += _dimensionName;
		message += " Check that the dimension's name is correct.";
		SOLIDWORKS_ERROR("SolidworksDimension::connect: "+message);
}
	_dimension = new IDimension(dimDisp);
	dimDisp->AddRef();
	dimDisp->Release();
}

void SolidworksDimension::disconnect() // does not need to be implemented?
{
	delete _dimension;
	_dimension = NULL;
	SolidworksData::disconnect();
}

//***********************SolidworksFile********************
SolidworksFile::SolidworksFile(string filePath) // implemented
{
	_filePath = filePath;
	SOLIDWORKS_DEBUG("Filepath is: " << _filePath.c_str());
}

SolidworksFile::~SolidworksFile()
{
	disconnect();
}

void SolidworksFile::connect(IModelDoc* model) throw(DomeException) // ???? does it have to be implemented?
{
	if (model == NULL) SOLIDWORKS_ERROR("Unable to connect, model is null.");

	SolidworksData::connect(model);
}

void SolidworksFile::disconnect() // does not need to be implemented?
{
	SolidworksData::disconnect();
}

bool SolidworksFile::save()
{
	if (_swModel == NULL) 
		SOLIDWORKS_ERROR("SolidworksFile::save: ModelDoc pointer is null. Unable to save file.");

    int retval;
	retval = _swModel->SaveAs2(_T(_filePath.c_str()),0,TRUE,TRUE);  //->_T(_filePath.c_str());  //,0,0x1,&retval
	SOLIDWORKS_DEBUG("Saving file!!!!!");
    if(retval==0) return true;
	else return false;
}
//***********************SolidworksFile********************


string SolidworksLengthUnit::getValue() throw(DomeException) // implemented
{
	string unit = "unitless";
	if (_swModel == NULL) 
		SOLIDWORKS_ERROR("SolidworksLengthUnit::getValue: ModelDoc pointer is null. Unable to get length unit.");

	switch( (swLengthUnit_e) _swModel->GetLengthUnit() )
	{
		case(swMM):
			unit = "millimeter";
			break;
		case(swCM):
			unit = "centimeter";
			break;
		case(swMETER):
			unit = "meter";
			break;
		case(swINCHES):
			unit = "inch";
			break;
		case(swFEET):
			unit = "foot";
			break;
		case(swFEETINCHES):
			unit = "feet-inches";
			break;
		default:
			SOLIDWORKS_ERROR("SolidworksLengthUnit::getValue: Unknown length unit found.");
	}
	
	return unit;
}
void SolidworksLengthUnit::setValue(string value)// throw(DomeException) // not supported
{
	DOME_NOT_SUPPORTED("SolidworksLengthUnit::setValue");
}

string SolidworksAngleUnit::getValue() throw(DomeException) // implemented
{
	string unit = "unitless";
	if (_swModel == NULL) 
		SOLIDWORKS_ERROR("SolidworksAngleUnit::getValue: ModelDoc pointer is null. Unable to get angle unit.");

	VARIANT v = _swModel->GetAngularUnits();			
	SAFEARRAY* psa =  V_ARRAY(&v);
	short* data;
	HRESULT res = SafeArrayAccessData(psa, (void **)&data);

	switch( (swAngleUnit_e) data[0] )
	{
		case(swDEGREES):
			unit = "degree";
			break;
		case(swDEG_MIN):
			unit = "degree-minute";
			break;
		case(swDEG_MIN_SEC):
			unit = "degree-minute-second";
			break;
		case(swRADIANS):
			unit = "radian";
			break;
		default:
			SOLIDWORKS_ERROR("SolidworksAngleUnit::getValue: Unknown angle unit found.");
	}
	res = SafeArrayUnaccessData(psa);				// Unaccess the SafeArray
	res =  SafeArrayDestroy(psa);	// Destroy the SafeArray
	return unit;
}

void SolidworksAngleUnit::setValue(string value)// throw(DomeException) // not supported
{
	DOME_NOT_SUPPORTED("SolidworksLengthUnit::setValue");
}

double SolidworksMass::getValue() throw(DomeException) // implemented
{
	VARIANT v = _swModel->GetMassProperties();			
	SAFEARRAY* psa =  V_ARRAY(&v);
	double* data;
	HRESULT res = SafeArrayAccessData(psa, (void **)&data);
	res = SafeArrayUnaccessData(psa);				// Unaccess the SafeArray
	res =  SafeArrayDestroy(psa);
	// Destroy the SafeArray
	return data[5];
}

void SolidworksMass::setValue(double value)// throw(DomeException) // not supported
{
	DOME_NOT_SUPPORTED("SolidworksMass::setValue");
}

double SolidworksVolume::getValue() throw(DomeException) // implemented
{
	VARIANT v = _swModel->GetMassProperties();			
	SAFEARRAY* psa =  V_ARRAY(&v);
	double* data;
	HRESULT res = SafeArrayAccessData(psa, (void **)&data);
	res = SafeArrayUnaccessData(psa);				// Unaccess the SafeArray
	res =  SafeArrayDestroy(psa);					// Destroy the SafeArray
	return data[3];
}

void SolidworksVolume::setValue(double value)// throw(DomeException) // not supported
{
	DOME_NOT_SUPPORTED("SolidworksVolume::setValue");
}

double SolidworksSurfaceArea::getValue()// throw(DomeException) // implemented
{
	VARIANT v = _swModel->GetMassProperties();			
	SAFEARRAY* psa =  V_ARRAY(&v);
	double* data;
	HRESULT res = SafeArrayAccessData(psa, (void **)&data);
	res = SafeArrayUnaccessData(psa);				// Unaccess the SafeArray
	res =  SafeArrayDestroy(psa);					// Destroy the SafeArray
	return data[4];
}

void SolidworksSurfaceArea::setValue(double value)// throw(DomeException) // not supported
{
	DOME_NOT_SUPPORTED("SolidworksSurfaceArea::setValue");
}


vector <double> SolidworksColors::getValues() throw(DomeException) // NEEDS TO BE IMPLEMENTED!!!!
{
	try
	{
		VARIANT v = _swModel->GetMaterialPropertyValues();			
		SAFEARRAY* psa =  V_ARRAY(&v);
		double* data;
		HRESULT res = SafeArrayAccessData(psa, (void **)&data);
		res = SafeArrayUnaccessData(psa);				// Unaccess the SafeArray
		res =  SafeArrayDestroy(psa);					// Destroy the SafeArray
		
		vector<double> colors;
		colors.push_back(data[0]);//red
		colors.push_back(data[1]);//green
		colors.push_back(data[2]);//blue
		return colors;
		//SOLIDWORKS_DEBUG("Leaving SolidworksColors::getValues");
		
	} catch(...) {
		SOLIDWORKS_ERROR("SolidworksColors::getValues: Method not implemented");
	}
}

void SolidworksColors::setValues(vector <double> values)// throw(DomeException) // not supported
{
	DOME_NOT_SUPPORTED("SolidworksColors::setValues");
}

double SolidworksColors::getElement(int index) throw(DomeException) // not implemented
{
	try
	{
		return getValues()[index];
	} catch(...) {
		throw;
	}
}

void SolidworksColors::setElement(int index)// throw(DomeException) // not supported
{
	DOME_NOT_SUPPORTED("SolidworksColors::setElement");
}

double SolidworksColors::getRed() throw(DomeException) // implemented
{
	try
	{
		return getElement(0);
	} catch(...) {
		throw;
	}
}

double SolidworksColors::getGreen() throw(DomeException) // implemented
{
	try
	{
		return getElement(1);
	} catch(...) {
		throw;
	}
}

double SolidworksColors::getBlue() throw(DomeException) // implemented
{
	try
	{
		return getElement(2);
	} catch(...) {
		throw;
	}
}

int SolidworksColors::getSize() // not implemented in Plug-In
{
	return 3;
}

void SolidworksColors::setSize(int size)// throw(DomeException) // not supported
{
	DOME_NOT_SUPPORTED("SolidworksColors::setSize");
}

} // namespace SolidworksPlugin
} // DOME
