//----------------------------------------------------------------------------
// SolidworksDelegate.cpp
//
// Purpose: unwraps command, data objects and data values supplied by
// the caller. Instantiates model and data objects. Delegates work
// to the model and data objects.
//
// *********** PLUGIN AUTHOR MODIFIES EVERYTHING BELOW THIS LINE ************
// ***********					  AS NEEDED                      ************
//----------------------------------------------------------------------------

#include <stdafx.h>

#ifdef _WINDOWS
#pragma warning(disable:4786)	// stl warnings
#endif
#include <vector>
#include <string>
#include <iostream>
using namespace std;

// java native interface stuff
#ifdef _WINDOWS
#include <jni.h>
#else
#include "../jni_linux/jni.h"
#endif
#include "JNIHelper.h"

// Solidworks function prototypes
#include "SolidworksIncludes.h"

// data and model classes
#include "SolidworksData.h"
#include "SolidworksModel.h"
using namespace DOME::SolidworksPlugin;

// definitions for this class
#include "SolidworksPluginCaller.h"
#include "SolidworksDelegate.h"



/////////////////////////////////////////////////////////////////////
// invoke all functions that return an object (casted to a long)
/////////////////////////////////////////////////////////////////////
long SolidworksDelegate::callObjectFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case SolidworksPluginCaller_MODEL_INIT:
		iObjectPtr = (long) initializeModel (env, args, iNumArgs);
		break;

	case SolidworksPluginCaller_MODEL_CREATE_ANGLEUNIT:
	case SolidworksPluginCaller_MODEL_CREATE_LENGTHUNIT:
	case SolidworksPluginCaller_MODEL_CREATE_COLORS:
	case SolidworksPluginCaller_MODEL_CREATE_MASS:
	case SolidworksPluginCaller_MODEL_CREATE_SURFACEAREA:
	case SolidworksPluginCaller_MODEL_CREATE_VOLUME:
		iObjectPtr = (long) createModelObject (iObjectPtr,iFunctionIndex);
		break;

	case SolidworksPluginCaller_MODEL_CREATE_DIMENSION:
		iObjectPtr = (long) createDimension (env, args, iNumArgs, iObjectPtr);
		break;

	case SolidworksPluginCaller_MODEL_CREATE_FILE:
		iObjectPtr = (long) createFile (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		SOLIDWORKS_DEBUG ("callObjectFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return iObjectPtr;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return void
/////////////////////////////////////////////////////////////////////
void SolidworksDelegate::callVoidFunctions (JNIEnv* env, jobjectArray args, 
									   unsigned int iNumArgs,
									   long iObjectPtr,
									   unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case SolidworksPluginCaller_MODEL_LOAD:
		loadModel (iObjectPtr);
		break;

	case SolidworksPluginCaller_MODEL_EXECUTE:
		executeModel (iObjectPtr);
		break;

	case SolidworksPluginCaller_MODEL_EXEC_BF_INPUT:
		break;

	case SolidworksPluginCaller_MODEL_EXEC_AF_INPUT:
		break;

	case SolidworksPluginCaller_MODEL_UNLOAD:
		unloadModel (env, args, iNumArgs, iObjectPtr);
		break;

	case SolidworksPluginCaller_MODEL_DESTROY:
		destroyModel (iObjectPtr);
		break;

	case SolidworksPluginCaller_DIMENSION_SET_VALUE:
		setRealValue (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		SOLIDWORKS_DEBUG ("callVoidFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return an integer
/////////////////////////////////////////////////////////////////////
int SolidworksDelegate::callIntegerFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	int value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		SOLIDWORKS_DEBUG ("callIntegerFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a boolean
/////////////////////////////////////////////////////////////////////
bool SolidworksDelegate::callBooleanFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	bool value = false;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case SolidworksPluginCaller_MODEL_IS_LOADED:
		value = isModelLoaded (iObjectPtr);
		break;
		
	default:
		SOLIDWORKS_DEBUG ("callBooleanFunctions: Undefined function index ("
					<< iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a double
/////////////////////////////////////////////////////////////////////
double SolidworksDelegate::callDoubleFunctions (JNIEnv* env, jobjectArray args, 
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	//for debug
	//cout << "Get into SolidworksDelegate::callDoubleFunctions!" << endl;
	//cout << "iFunctionIndex = " << iFunctionIndex << ", SolidworksPluginCaller_REAL_GET_VALUE = "<< SolidworksPluginCaller_REAL_GET_VALUE << endl;
	
	double value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case SolidworksPluginCaller_COLORS_GET_RED:
	case SolidworksPluginCaller_COLORS_GET_GREEN:
	case SolidworksPluginCaller_COLORS_GET_BLUE:
	case SolidworksPluginCaller_DIMENSION_GET_VALUE:
	case SolidworksPluginCaller_MASS_GET_VALUE:
	case SolidworksPluginCaller_SURFACEAREA_GET_VALUE:
	case SolidworksPluginCaller_VOLUME_GET_VALUE:
		value = getDoubleValue (iObjectPtr, iFunctionIndex);
		break;

	default:
		SOLIDWORKS_DEBUG ("callDoubleFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a character string
/////////////////////////////////////////////////////////////////////
const 
char* SolidworksDelegate::callStringFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	const char * value = NULL;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case SolidworksPluginCaller_ANGLEUNIT_GET_VALUE:
	case SolidworksPluginCaller_LENGTHUNIT_GET_VALUE:
		value = getStringValue (iObjectPtr, iFunctionIndex);
		break;

	default:
		SOLIDWORKS_DEBUG ("callStringFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of doubles
/////////////////////////////////////////////////////////////////////
vector<double>
SolidworksDelegate::callDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	vector<double> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case SolidworksPluginCaller_COLORS_GET_VALUES:
		result = getDoubleArrayValue (iObjectPtr);
		break;

	default:
		SOLIDWORKS_DEBUG ("callDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of integers
/////////////////////////////////////////////////////////////////////
vector<int>
SolidworksDelegate::callIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	vector<int> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		SOLIDWORKS_DEBUG ("callIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of integers
/////////////////////////////////////////////////////////////////////
vector< vector<int> >
SolidworksDelegate::call2DIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
											unsigned int iNumArgs,
											long iObjectPtr,
											unsigned int iFunctionIndex)
{
	vector < vector<int> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		SOLIDWORKS_DEBUG ("call2DIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of doubles
/////////////////////////////////////////////////////////////////////
vector< vector<double> > 
SolidworksDelegate::call2DDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	vector< vector<double> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		SOLIDWORKS_DEBUG ("call2DDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// void functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////////
// load the model
/////////////////////////////////////////////////////
void SolidworksDelegate::loadModel (long iObjectPtr)
{
	SolidworksModel *model = (SolidworksModel*) iObjectPtr;

	if (model == NULL) {
		SOLIDWORKS_DEBUG ("loadModel: Model does not exist" << endl);
	}
	else
	{
		model->loadModel ();
	}
}


/////////////////////////////////////////////////////
// unload the model
/////////////////////////////////////////////////////
void SolidworksDelegate::unloadModel (JNIEnv* env, jobjectArray args,
									  unsigned int iNumArgs, long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 1) {
		SOLIDWORKS_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the unloadModel function");
	}
	else 
	{
		// unpack the arguments
		int err2;
		void* iNumModels = NULL;
		err2 = getArgument (env, args, 0, JAVA_INTEGER, iNumModels);

	
		SolidworksModel *model = (SolidworksModel*) iObjectPtr;

		if (model == NULL) {
			SOLIDWORKS_DEBUG ("unloadModel: Model does not exist" << endl);
		}
		else
		if (err2 != ERR_OK) {
			SOLIDWORKS_DEBUG ("unloadModel: Invalid number of arguments (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
		model->unloadModel (*((int*)iNumModels));
		}
	}
}


/////////////////////////////////////////////////////
// destroy the model
/////////////////////////////////////////////////////
void SolidworksDelegate::destroyModel (long iObjectPtr)
{
	SolidworksModel *model = (SolidworksModel*) iObjectPtr;

	if (model == NULL) {
		SOLIDWORKS_DEBUG ("destroyModel: Model does not exist" << endl);
	}
	else
	{
		delete model;
	}
}


/////////////////////////////////////////////////////
// set values
/////////////////////////////////////////////////////
void SolidworksDelegate::setRealValue (JNIEnv* env, jobjectArray args,
								   unsigned int iNumArgs,
								   long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 1) {
		SOLIDWORKS_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setValue function");
	}
	else 
	{
		//SolidworksReal *parameter = (SolidworksReal*) iObjectPtr;
		SolidworksData *parameter = (SolidworksData*) iObjectPtr;
		if (parameter == NULL) {
			SOLIDWORKS_DEBUG ("setValue: Parameter does not exist" << endl);
		}
		else
		{
			int err;
			void *value = NULL;

			// unpack the argument and call the function
			err = getArgument (env, args, 0, JAVA_DOUBLE, value);
			if (err != ERR_OK) {
				SOLIDWORKS_DEBUG ("setValue: Invalid parameter value (ERR = "
							 << err << ")" << endl);
			}
			else {
				//cout << "setRealValue: " << *((double*)value) << endl;
				((SolidworksDimension*)parameter)->setValue ( *((double*)value) );
			}

			// clean up
			if (value != NULL) delete (double*) value;
		}
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// object functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// create the model
///////////////////////////////////////////
SolidworksModel *SolidworksDelegate::initializeModel (JNIEnv* env, jobjectArray args,
											  unsigned int iNumArgs)
{
	SolidworksModel *model = NULL;

	// check argument list length
	if (iNumArgs != 2) {
		SOLIDWORKS_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the initializeModel function" << endl);
	}
	else 
	{
		int err1, err2;
		void* szFileName = NULL;
		void* bIsVisible = NULL;

		// unpack the arguments
		err1 = getArgument (env, args, 0, JAVA_STRING, szFileName);
		err2 = getArgument (env, args, 1, JAVA_BOOLEAN, bIsVisible);

		if (err1 != ERR_OK) {
			SOLIDWORKS_DEBUG ("initializeModel: Invalid model file name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			SOLIDWORKS_DEBUG ("initializeModel: Invalid argument: bIsVisible (ERR = " 
						 << err2 << ")" << endl);
		}
		else {
			// call the constructor
			model = new SolidworksModel (  (char*)szFileName, 
									 *((bool*)bIsVisible));
		}

		// clean up
		if (szFileName != NULL) delete (char*) szFileName;
		if (bIsVisible != NULL)	delete (bool*) bIsVisible;
	}

	return model;
}


///////////////////////////////////////////
// create individual model objects
///////////////////////////////////////////
SolidworksData *SolidworksDelegate::createModelObject (long iObjectPtr,unsigned int iFunctionIndex)
{
	SolidworksData *modelObject = NULL;


		SolidworksModel *model = (SolidworksModel*) iObjectPtr;

		if (model == NULL) {
			SOLIDWORKS_DEBUG ("createModelObject: Model does not exist" << endl);
		}
		else
		{

			switch (iFunctionIndex)
			{
			case SolidworksPluginCaller_MODEL_CREATE_ANGLEUNIT:
				modelObject = model->createAngleUnit();
				break;
			case SolidworksPluginCaller_MODEL_CREATE_LENGTHUNIT:
				modelObject = model->createLengthUnit();
				break;
			case SolidworksPluginCaller_MODEL_CREATE_COLORS:
				modelObject = model->createColors();
				break;
			case SolidworksPluginCaller_MODEL_CREATE_MASS:
				modelObject = model->createMass();
				break;
			case SolidworksPluginCaller_MODEL_CREATE_SURFACEAREA:
				modelObject = model->createSurfaceArea();
				break;
			case SolidworksPluginCaller_MODEL_CREATE_VOLUME:
				modelObject = model->createVolume();
				break;

			default:
				SOLIDWORKS_DEBUG ("createModelObject: Undefined function index ("
							 << iFunctionIndex << ")" << endl);
			}
		}


	return modelObject;
}

///////////////////////////////////////////
// create dimension objects
///////////////////////////////////////////
SolidworksData *SolidworksDelegate::createDimension (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	SolidworksData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		SOLIDWORKS_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createDimension function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		SolidworksModel *model = (SolidworksModel*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (model == NULL) {
			SOLIDWORKS_DEBUG ("createDimension: Model does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			SOLIDWORKS_DEBUG ("createDimension: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			modelObject = model->createDimension (name);
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
	}

	return modelObject;
}


///////////////////////////////////////////
// create file objects
///////////////////////////////////////////
SolidworksData *SolidworksDelegate::createFile (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	SolidworksData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 1) {
		SOLIDWORKS_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createFile function" << endl);
	}
	else 
	{
		int err;
		void* szParameterName = NULL;

		// unpack the arguments
		SolidworksModel *model = (SolidworksModel*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_STRING, szParameterName);

		if (model == NULL) {
			SOLIDWORKS_DEBUG ("createFile: Model does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			SOLIDWORKS_DEBUG ("createFile: Invalid parameter name (ERR = "
						 << err << ")" << endl);
		}
		else
		{
			// call the function
			string name ((char*)szParameterName);
			modelObject = model->createFile (name);
		}

		// clean up
		if (szParameterName != NULL) delete (char*) szParameterName;
	}

	return modelObject;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// real functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
double SolidworksDelegate::getDoubleValue (long iObjectPtr, unsigned int iFunctionIndex)
{
	//for debug
	//cout << "Get into SolidworksDelegate::getDoubleValue" << endl;

	double value = 0;
	//SolidworksReal *data = (SolidworksReal*) iObjectPtr;
	SolidworksData *data = (SolidworksData*) iObjectPtr;

	if (data == NULL) {
		SOLIDWORKS_DEBUG ("getDoubleValue: SolidworksReal does not exist" << endl);
	}
	else {

			switch (iFunctionIndex)
			{
			case SolidworksPluginCaller_COLORS_GET_RED:
				value = ((SolidworksColors*)data)->getRed ();
				break;
			case SolidworksPluginCaller_COLORS_GET_GREEN:
				value = ((SolidworksColors*)data)->getGreen ();
				break;
			case SolidworksPluginCaller_COLORS_GET_BLUE:
				value = ((SolidworksColors*)data)->getBlue ();
				break;
			case SolidworksPluginCaller_DIMENSION_GET_VALUE:
				value = ((SolidworksDimension*)data)->getValue ();
				break;
			case SolidworksPluginCaller_MASS_GET_VALUE:
				value = ((SolidworksMass*)data)->getValue ();
				break;
			case SolidworksPluginCaller_SURFACEAREA_GET_VALUE:
				value = ((SolidworksSurfaceArea*)data)->getValue ();
				break;
			case SolidworksPluginCaller_VOLUME_GET_VALUE:
				value = ((SolidworksVolume*)data)->getValue ();
				break;

			default:
				SOLIDWORKS_DEBUG ("getDoubleValue: Undefined function index ("
							 << iFunctionIndex << ")" << endl);
			}
	}

	return value;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// integer functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get an integer value
///////////////////////////////////////////
int SolidworksDelegate::getIntegerValue (long iObjectPtr,
									 unsigned int iFunctionIndex)
{
	int value = 0;

	return value;
}


///////////////////////////////////////////
// execute the model
///////////////////////////////////////////
void SolidworksDelegate::executeModel (long iObjectPtr)
{
	SolidworksModel *model = (SolidworksModel*) iObjectPtr;

	if (model == NULL) {
		SOLIDWORKS_DEBUG ("executeModel: SolidworksModel does not exist" << endl);
	}
	else {
		// execute the model
		model->execute ();
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// boolean functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
bool SolidworksDelegate::getBooleanValue (long iObjectPtr)
{
	bool value = false;
	return value;
}

///////////////////////////////////////////
// check if model is loaded
///////////////////////////////////////////
bool SolidworksDelegate::isModelLoaded (long iObjectPtr)
{
	bool value = false;
	SolidworksModel *model = (SolidworksModel*) iObjectPtr;

	if (model == NULL) {
		SOLIDWORKS_DEBUG ("isModelLoaded: Model does not exist" << endl);
	}
	else {
		// get the value
		value = model->isModelLoaded ();
	}

	return value;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// string functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a string parameter
///////////////////////////////////////////
const char* SolidworksDelegate::getStringValue (long iObjectPtr, unsigned int iFunctionIndex)
{
	char* value = NULL;
	string str = NULL;

	SolidworksData *data = (SolidworksData*) iObjectPtr;

	if (data == NULL) {
		SOLIDWORKS_DEBUG ("getStringValue: SolidworksUnit does not exist" << endl);
	}
	else {

			switch (iFunctionIndex)
			{

			case SolidworksPluginCaller_ANGLEUNIT_GET_VALUE:
				str = ((SolidworksAngleUnit*)data)->getValue ();
				break;

			case SolidworksPluginCaller_LENGTHUNIT_GET_VALUE:
				str = ((SolidworksLengthUnit*)data)->getValue ();
				break;

			default:
				SOLIDWORKS_DEBUG ("getStringValue: Undefined function index ("
							 << iFunctionIndex << ")" << endl);
			}

			int length = str.length();
			value = new char[length+1];
			str.copy(value, length, 0);

	}


	return value;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// 1D array functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////
// get the value from a double array parameter
/////////////////////////////////////////////////
vector<double> 
SolidworksDelegate::getDoubleArrayValue (long iObjectPtr)
{
	vector<double> value;

	SolidworksData *data = (SolidworksData*) iObjectPtr;

	if (data == NULL) {
		SOLIDWORKS_DEBUG ("getDoubleArrayValue: SolidworksColors does not exist" << endl);
	}
	else {
		value = ((SolidworksColors*)data)->getValues ();
	}

	
	return value;
}



