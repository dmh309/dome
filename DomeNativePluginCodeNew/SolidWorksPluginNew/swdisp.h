// Machine generated IDispatch wrapper class(es) created with ClassWizard
/////////////////////////////////////////////////////////////////////////////
// ISldWorks wrapper class
#include <afxdisp.h>

class ISldWorks : public COleDispatchDriver
{
public:
	ISldWorks() {}		// Calls COleDispatchDriver default constructor
	ISldWorks(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISldWorks(const ISldWorks& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetActiveDoc();
	LPDISPATCH GetIActiveDoc();
	LPDISPATCH OpenDoc(LPCTSTR Name, long type);
	LPDISPATCH IOpenDoc(LPCTSTR Name, long type);
	LPDISPATCH ActivateDoc(LPCTSTR Name);
	LPDISPATCH IActivateDoc(LPCTSTR Name);
	void SendMsgToUser(LPCTSTR Message);
	LPDISPATCH Frame();
	LPDISPATCH IFrameObject();
	void ExitApp();
	void CloseDoc(LPCTSTR Name);
	LPDISPATCH NewPart();
	LPDISPATCH INewPart();
	LPDISPATCH NewAssembly();
	LPDISPATCH INewAssembly();
	LPDISPATCH NewDrawing(long templateToUse);
	LPDISPATCH INewDrawing(long templateToUse);
	long DateCode();
	CString RevisionNumber();
	BOOL LoadFile(LPCTSTR fileName);
	BOOL AddFileOpenItem(LPCTSTR CallbackFcnAndModule, LPCTSTR Description);
	BOOL AddFileSaveAsItem(LPCTSTR CallbackFcnAndModule, LPCTSTR Description, long type);
	void PreSelectDwgTemplateSize(long templateToUse, LPCTSTR templateName);
	void DocumentVisible(BOOL Visible, long type);
	LPDISPATCH DefineAttribute(LPCTSTR Name);
	LPDISPATCH IDefineAttribute(LPCTSTR Name);
	BOOL GetVisible();
	void SetVisible(BOOL bNewValue);
	BOOL GetUserControl();
	void SetUserControl(BOOL bNewValue);
	void DisplayStatusBar(BOOL onOff);
	void CreateNewWindow();
	void ArrangeIcons();
	void ArrangeWindows(long Style);
	void QuitDoc(LPCTSTR Name);
	LPDISPATCH GetModeler();
	LPDISPATCH IGetModeler();
	LPDISPATCH GetEnvironment();
	LPDISPATCH IGetEnvironment();
	LPDISPATCH NewDrawing2(long templateToUse, LPCTSTR templateName, long paperSize, double Width, double height);
	LPDISPATCH INewDrawing2(long templateToUse, LPCTSTR templateName, long paperSize, double Width, double height);
	BOOL SetOptions(LPCTSTR Message);
	BOOL PreviewDoc(long* hWnd, LPCTSTR FullName);
	CString GetSearchFolders(long folderType);
	BOOL SetSearchFolders(long folderType, LPCTSTR folders);
	BOOL GetUserPreferenceToggle(long userPreferenceToggle);
	void SetUserPreferenceToggle(long userPreferenceValue, BOOL onFlag);
	double GetUserPreferenceDoubleValue(long userPreferenceValue);
	BOOL SetUserPreferenceDoubleValue(long userPreferenceValue, double Value);
	CString GetActivePrinter();
	void SetActivePrinter(LPCTSTR lpszNewValue);
	BOOL LoadFile2(LPCTSTR fileName, LPCTSTR ArgString);
	long GetUserPreferenceIntegerValue(long userPreferenceValue);
	BOOL SetUserPreferenceIntegerValue(long userPreferenceValue, long Value);
	BOOL RemoveMenuPopupItem(long DocType, long SelectType, LPCTSTR Item, LPCTSTR CallbackFcnAndModule, LPCTSTR CustomNames, long Unused);
	BOOL RemoveMenu(long DocType, LPCTSTR MenuItemString, LPCTSTR CallbackFcnAndModule);
	BOOL RemoveFileOpenItem(LPCTSTR CallbackFcnAndModule, LPCTSTR Description);
	BOOL RemoveFileSaveAsItem(LPCTSTR CallbackFcnAndModule, LPCTSTR Description, long type);
	BOOL ReplaceReferencedDocument(LPCTSTR referencingDocument, LPCTSTR referencedDocument, LPCTSTR newReference);
	long AddMenuItem(long DocType, LPCTSTR Menu, long Postion, LPCTSTR CallbackModuleAndFcn);
	long AddMenuPopupItem(long DocType, long selType, LPCTSTR Item, LPCTSTR CallbackFcnAndModule, LPCTSTR CustomNames);
	BOOL RemoveUserMenu(long DocType, long menuIdIn, LPCTSTR moduleName);
	long AddToolbar(LPCTSTR moduleName, LPCTSTR title, long smallBitmapHandle, long largeBitmapHandle);
	BOOL AddToolbarCommand(LPCTSTR moduleName, long toolbarId, long toolbarIndex, LPCTSTR commandString);
	BOOL ShowToolbar(LPCTSTR moduleName, long toolbarId);
	BOOL HideToolbar(LPCTSTR moduleName, long toolbarId);
	BOOL RemoveToolbar(LPCTSTR Module, long toolbarId);
	BOOL GetToolbarState(LPCTSTR Module, long toolbarId, long toolbarState);
	CString GetUserPreferenceStringListValue(long userPreference);
	void SetUserPreferenceStringListValue(long userPreference, LPCTSTR Value);
	BOOL EnableStereoDisplay(BOOL bEnable);
	BOOL IEnableStereoDisplay(BOOL bEnable);
	VARIANT GetDocumentDependencies(LPCTSTR document, long traverseflag, long searchflag);
	CString IGetDocumentDependencies(LPCTSTR document, long traverseflag, long searchflag);
	long GetDocumentDependenciesCount(LPCTSTR document, long traverseflag, long searchflag);
	LPDISPATCH OpenDocSilent(LPCTSTR fileName, long type, long* errors);
	LPDISPATCH IOpenDocSilent(LPCTSTR fileName, long type, long* errors);
	long CallBack(LPCTSTR callBackFunc, long defaultRetVal, LPCTSTR callBackArgs);
	long SendMsgToUser2(LPCTSTR Message, long icon, long buttons);
	LPUNKNOWN EnumDocuments();
	long LoadAddIn(LPCTSTR fileName);
	long UnloadAddIn(LPCTSTR fileName);
	BOOL RecordLine(LPCTSTR text);
	VARIANT VersionHistory(LPCTSTR fileName);
	CString IVersionHistory(LPCTSTR fileName);
	long IGetVersionHistoryCount(LPCTSTR fileName);
	BOOL AllowFailedFeatureCreation(BOOL yesNo);
	LPDISPATCH GetFirstDocument();
	CString GetCurrentWorkingDirectory();
	BOOL SetCurrentWorkingDirectory(LPCTSTR currentWorkingDirectory);
	CString GetDataFolder(BOOL bShowErrorMsg);
	BOOL GetSelectionFilter(long selType);
	void SetSelectionFilter(long selType, BOOL state);
	LPDISPATCH ActivateDoc2(LPCTSTR Name, BOOL silent, long* errors);
	LPDISPATCH IActivateDoc2(LPCTSTR Name, BOOL silent, long* errors);
	BOOL GetMouseDragMode(long command);
	CString GetCurrentLanguage();
	LPDISPATCH IGetFirstDocument();
	BOOL SanityCheck(long swItemToCheck, long* P1, long* P2);
	long AddMenu(long DocType, LPCTSTR Menu, long position);
	long CheckpointConvertedDocument(LPCTSTR docName);
	LPDISPATCH OpenDoc2(LPCTSTR fileName, long type, BOOL ReadOnly, BOOL viewOnly, BOOL silent, long* errors);
	LPDISPATCH IOpenDoc2(LPCTSTR fileName, long type, BOOL ReadOnly, BOOL viewOnly, BOOL silent, long* errors);
	VARIANT GetMassProperties(LPCTSTR filePathName, LPCTSTR configurationName);
	BOOL IGetMassProperties(LPCTSTR filePathName, LPCTSTR configurationName, double* mPropsData);
	CString GetLocalizedMenuName(long menuId);
	VARIANT GetDocumentDependencies2(LPCTSTR document, BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo);
	CString IGetDocumentDependencies2(LPCTSTR document, BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo);
	long IGetDocumentDependenciesCount2(LPCTSTR document, BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo);
	VARIANT GetSelectionFilters();
	void SetSelectionFilters(const VARIANT& selType, BOOL state);
	BOOL GetApplySelectionFilter();
	void SetApplySelectionFilter(BOOL state);
	LPDISPATCH NewDocument(LPCTSTR templateName, long paperSize, double Width, double height);
	LPDISPATCH INewDocument(LPCTSTR templateName, long paperSize, double Width, double height);
	CString GetDocumentTemplate(long mode, LPCTSTR templateName, long paperSize, double Width, double height);
	long IGetSelectionFiltersCount();
	long IGetSelectionFilters();
	void ISetSelectionFilters(long count, long* selType, BOOL state);
	CString GetCurrSolidWorksRegSubKey();
	void SolidWorksExplorer();
	CString GetUserPreferenceStringValue(long userPreference);
	BOOL SetUserPreferenceStringValue(long userPreference, LPCTSTR Value);
	CString GetCurrentMacroPathName();
	LPDISPATCH GetOpenDocumentByName(LPCTSTR documentName);
	LPDISPATCH IGetOpenDocumentByName(LPCTSTR documentName);
	void GetCurrentKernelVersions(BSTR* version1, BSTR* version2, BSTR* version3);
	CString CreatePrunedModelArchive(LPCTSTR pathname, LPCTSTR zipPathName);
	LPDISPATCH OpenDoc3(LPCTSTR fileName, long type, BOOL ReadOnly, BOOL viewOnly, BOOL RapidDraft, BOOL silent, long* errors);
	LPDISPATCH IOpenDoc3(LPCTSTR fileName, long type, BOOL ReadOnly, BOOL viewOnly, BOOL RapidDraft, BOOL silent, long* errors);
	long AddToolbar2(LPCTSTR moduleNameIn, LPCTSTR titleIn, long smallBitmapHandleIn, long largeBitmapHandleIn, long menuPosIn, long decTemplateTypeIn);
	LPDISPATCH OpenModelConfiguration(LPCTSTR pathname, LPCTSTR configName);
	long GetToolbarDock(LPCTSTR ModuleIn, long toolbarIDIn);
	void SetToolbarDock(LPCTSTR ModuleIn, long toolbarIDIn, long docStatePosIn);
	LPDISPATCH GetMathUtility();
	LPDISPATCH IGetMathUtility();
	LPDISPATCH OpenDoc4(LPCTSTR fileName, long type, long options, LPCTSTR configuration, long* errors);
	LPDISPATCH IOpenDoc4(LPCTSTR fileName, long type, long options, LPCTSTR configuration, long* errors);
	BOOL IsRapidDraft(LPCTSTR fileName);
	VARIANT GetTemplateSizes(LPCTSTR fileName);
	BOOL IGetTemplateSizes(LPCTSTR fileName, long* paperSize, double* Width, double* height);
	LPDISPATCH GetColorTable();
	LPDISPATCH IGetColorTable();
	void SetMissingReferencePathName(LPCTSTR fileName);
	LPDISPATCH GetUserUnit(long UnitType);
	LPDISPATCH IGetUserUnit(long UnitType);
	BOOL SetMouseDragMode(long command);
	void SetPromptFilename(LPCTSTR fileName);
};
/////////////////////////////////////////////////////////////////////////////
// IModelDoc wrapper class

class IModelDoc : public COleDispatchDriver
{
public:
	IModelDoc() {}		// Calls COleDispatchDriver default constructor
	IModelDoc(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IModelDoc(const IModelDoc& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetSelectionManager();
	LPDISPATCH GetISelectionManager();
	void SetSelectionManager(LPDISPATCH newValue);
	LPDISPATCH GetActiveView();
	LPDISPATCH GetIActiveView();
	void SetActiveView(LPDISPATCH newValue);
	long GetLengthUnit();
	void SetLengthUnit(long nNewValue);
	CString GetLightSourceUserName(long Id);
	void SetLightSourceUserName(long Id, LPCTSTR lpszNewValue);
	VARIANT GetLightSourcePropertyValues(long Id);
	void SetLightSourcePropertyValues(long Id, const VARIANT& newValue);
	CString GetSceneName();
	void SetSceneName(LPCTSTR lpszNewValue);
	CString GetSceneUserName();
	void SetSceneUserName(LPCTSTR lpszNewValue);
	void FeatureFillet(double r1, BOOL propagate, BOOL ftyp, BOOL varRadTyp, long overFlowType);
	void GridOptions(BOOL dispGrid, double gridSpacing, BOOL snap, BOOL dotStyle, short nMajor, short nMinor, BOOL align2edge, BOOL angleSnap, double angleUnit, BOOL minorAuto);
	void SetUnits(short uType, short fractBase, short fractDenom, short sigDigits, BOOL roundToFraction);
	void LBDownAt(long flags, double x, double y, double z);
	void LBUpAt(long flags, double x, double y, double z);
	void DragTo(long flags, double x, double y, double z);
	void SelectAt(long flags, double x, double y, double z);
	void CreateLineVB(double x1, double y1, double z1, double x2, double y2, double z2);
	BOOL CreateLine(const VARIANT& P1, const VARIANT& P2);
	void CreateCenterLineVB(double x1, double y1, double z1, double x2, double y2, double z2);
	BOOL CreateCenterLine(const VARIANT& P1, const VARIANT& P2);
	void CreateArcVB(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z, short dir);
	BOOL CreateArc(const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, short dir);
	void CreateCircleVB(double p1x, double p1y, double p1z, double radius);
	BOOL CreateCircleByRadius(const VARIANT& P1, double radius);
	VARIANT GetLines();
	void SketchTrim(long op, long selEnd, double x, double y);
	void SketchOffsetEdges(double val);
	void SketchRectangle(double val1, double val2, double z1, double val3, double val4, double z2, BOOL val5);
	void SketchPoint(double x, double y, double z);
	void FeatureCut(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2);
	void FeatureBoss(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2);
	void SimpleHole(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2);
	void FeatureLinearPattern(long num1, double spacing1, long num2, double spacing2, BOOL flipDir1, BOOL flipDir2, LPCTSTR dName1, LPCTSTR dName2);
	void NameView(LPCTSTR vName);
	void ShowNamedView(LPCTSTR vName);
	void CreatePlaneAtOffset(double val, BOOL flipDir);
	void Toolbars(BOOL m, BOOL vw, BOOL skMain, BOOL sk, BOOL feat, BOOL constr, BOOL macro);
	void CreatePlaneAtAngle(double val, BOOL flipDir);
	void SetParamValue(double val);
	void AddRelation(LPCTSTR relStr);
	void DeleteAllRelations();
	void HoleWizard(double depth, short endType, BOOL flip, BOOL dir, long hType, double d1, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12);
	BOOL SaveAs(LPCTSTR newName);
	void ActivateSelectedFeature();
	void SelectByName(long flags, LPCTSTR idStr);
	void SketchAddConstraints(LPCTSTR idStr);
	void SketchConstraintsDel(long constrInd, LPCTSTR idStr);
	void SketchConstraintsDelAll();
	void Lock();
	void UnLock();
	void InsertFeatureShell(double thickness, BOOL outward);
	void SketchFillet(double rad);
	void FeatureChamfer(double Width, double angle, BOOL flip);
	void InsertMfDraft(double angle, BOOL flipDir, BOOL isEdgeDraft, long propType);
	void ParentChildRelationship();
	void SketchSpline(long morePts, double x, double y, double z);
	void SelectSketchPoint(double x, double y, long incidence);
	void SelectSketchLine(double x0, double y0, long inc0, double x1, double y1, long inc1);
	void SelectSketchArc(double x0, double y0, long inc0, double x1, double y1, long inc1, double xC, double yC, long incC, long rotDir);
	void SelectSketchSpline(long size, double x0, double y0, long inc0, double x1, double y1, long inc1, double xC, double yC, long incC);
	BOOL CreateTangentArc(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z);
	BOOL Create3PointArc(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z);
	BOOL CreateArcByCenter(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z);
	BOOL CreateCircle(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z);
	BOOL AddDimension(double x, double y, double z);
	BOOL AddHorizontalDimension(double x, double y, double z);
	BOOL AddVerticalDimension(double x, double y, double z);
	BOOL SelectSketchItem(long selOpt, LPCTSTR Name, double x, double y, double z);
	void ClearSelection();
	void Select(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z);
	void AndSelect(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z);
	BOOL CreatePoint(double pointX, double pointY, double pointZ);
	BOOL CreateLineDB(double sx, double sy, double sz, double ex, double ey, double ez);
	BOOL CreateArcDB(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, short dir);
	BOOL CreateCircleDB(double cx, double cy, double cz, double radius);
	BOOL CreatePointDB(double x, double y, double z);
	CString GetTitle();
	CString GetPathName();
	long GetType();
	void InsertObject();
	void EditClearAll();
	void EditCopy();
	void EditCut();
	void ObjectDisplayContent();
	void ObjectDisplayAsIcon();
	void ObjectResetsize();
	void WindowRedraw();
	void SetPickMode();
	void ViewRotateminusx();
	void ViewRotateminusy();
	void ViewRotateminusz();
	void ViewRotateplusx();
	void ViewRotateplusy();
	void ViewRotateplusz();
	void ViewTranslateminusx();
	void ViewTranslateminusy();
	void ViewTranslateplusx();
	void ViewTranslateplusy();
	void ViewRotXMinusNinety();
	void ViewRotYMinusNinety();
	void ViewRotYPlusNinety();
	void ViewZoomin();
	void ViewZoomout();
	void ViewDisplayHiddenremoved();
	void ViewDisplayWireframe();
	void ViewDisplayShaded();
	void ViewRwShading();
	void ViewOglShading();
	void ViewZoomtofit();
	void ViewRotate();
	void ViewTranslate();
	void ViewZoomto();
	void ViewDisplayHiddengreyed();
	void ViewDisplayFaceted();
	void ViewConstraint();
	void UserFavors();
	void FeatureCirPattern(long num, double spacing, BOOL flipDir, LPCTSTR dName);
	void EditSketch();
	void FeatEdit();
	void FeatEditDef();
	void InsertPoint();
	void InsertFamilyTableNew();
	void InsertFamilyTableEdit();
	void ToolsMacro();
	void ToolsGrid();
	void SketchCenterline();
	void SketchAlign();
	void SketchArc();
	void SketchTangentArc();
	void SketchCircle();
	void SketchUndo();
	void UserPreferences();
	void Lights();
	void SketchConstrainCoincident();
	void SketchConstrainConcentric();
	void SketchConstrainPerp();
	void SketchConstrainTangent();
	void SketchConstrainParallel();
	void SketchUseEdge();
	void SketchUseEdgeCtrline();
	void SketchMirror();
	void Save();
	void Close();
	void ViewDispRefaxes();
	void ViewDispRefplanes();
	void InsertSketch();
	void InsertProtrusionSwept(BOOL propagate, BOOL alignment, BOOL keepNormalConstant);
	void InsertProtrusionBlend(BOOL closed);
	void ToolsMassProps();
	void PropertySheet();
	void BlankRefGeom();
	void UnBlankRefGeom();
	void EditDelete();
	void InsertProjectedSketch();
	BOOL CreatePlaneFixed(const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, BOOL useGlobal);
	void DebugCheckBody();
	void DimPreferences();
	void UnblankSketch();
	void EditSketchOrSingleSketchFeature();
	void DebugCheckIgesGeom();
	void BlankSketch();
	double GetDefaultTextHeight();
	BOOL IsActive(LPCTSTR compStr);
	BOOL CreateEllipse(const VARIANT& center, const VARIANT& major, const VARIANT& minor);
	BOOL CreateEllipseVB(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ);
	BOOL CreateEllipticalArcByCenter(const VARIANT& center, const VARIANT& major, const VARIANT& minor, const VARIANT& start, const VARIANT& end);
	BOOL CreateEllipticalArcByCenterVB(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ, double startX, double startY, double startZ, double endX, 
		double endY, double endZ);
	LPDISPATCH GetActiveSketch();
	LPDISPATCH IGetActiveSketch();
	long GetTessellationQuality();
	void SetTessellationQuality(long qualityNum);
	LPDISPATCH Parameter(LPCTSTR stringIn);
	LPDISPATCH IParameter(LPCTSTR stringIn);
	BOOL SelectByID(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z);
	BOOL AndSelectByID(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z);
	void Insert3DSketch();
	CString GetLightSourceName(long Id);
	BOOL AddLightSource(LPCTSTR idName, long lTyp, LPCTSTR userName);
	long AddLightSourceExtProperty(long Id, const VARIANT& PropertyExtension);
	void ResetLightSourceExtProperty(long Id);
	void DeleteLightSource(long Id);
	VARIANT GetLightSourceExtProperty(long Id, long PropertyId);
	long AddLightToScene(LPCTSTR lpszNewValue);
	long AddSceneExtProperty(const VARIANT& PropertyExtension);
	void ResetSceneExtProperty();
	VARIANT GetSceneExtProperty(long PropertyId);
	void FileSummaryInfo();
	VARIANT GetGridSettings();
	void ToolsSketchTranslate();
	void ToolsDistance();
	void SkToolsAutoConstr();
	void ToolsSketchScale();
	void Paste();
	void ToolsConfiguration();
	void EntityProperties();
	BOOL GetArcCentersDisplayed();
	void SetArcCentersDisplayed(BOOL setting);
	void AutoSolveToggle();
	double IGetLines();
	long GetLineCount();
	void ICreateEllipse(double* center, double* major, double* minor);
	void ICreateEllipticalArcByCenter(double* center, double* major, double* minor, double* start, double* end);
	double GetILightSourcePropertyValues(long Id);
	void SetILightSourcePropertyValues(long Id, double* newValue);
	void InsertCutSwept(BOOL propagate, BOOL alignment, BOOL keepNormalConstant);
	void InsertCutBlend(BOOL closed);
	void InsertHelix(BOOL reversed, BOOL clockwised, BOOL tapered, BOOL outward, long helixdef, double height, double pitch, double revolution, double taperangle, double startangle);
	void ICreateLine(double* P1, double* P2);
	void ICreateCenterLine(double* P1, double* P2);
	void ICreateArc(double* P1, double* P2, double* P3, short dir);
	void ICreateCircleByRadius(double* P1, double radius);
	void GraphicsRedraw();
	BOOL GetVisibilityOfConstructPlanes();
	BOOL GetDisplayWhenAdded();
	void SetDisplayWhenAdded(BOOL setting);
	BOOL GetAddToDB();
	void SetAddToDB(BOOL setting);
	BOOL DeSelectByID(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z);
	BOOL GetVisible();
	void SetVisible(BOOL bNewValue);
	void PrintDirect();
	void PrintPreview();
	void Quit();
	void CreatePlaneThru3Points();
	void ViewRotXPlusNinety();
	VARIANT GetUnits();
	void SetAngularUnits(short uType, short fractBase, short fractDenom, short sigDigits);
	VARIANT GetAngularUnits();
	short IGetUnits();
	void ISetAngularUnits(short uType, short fractBase, short fractDenom, short sigDigits);
	short IGetAngularUnits();
	void ShowConfiguration(LPCTSTR configurationName);
	void ResetConfiguration();
	void AddConfiguration(LPCTSTR Name, LPCTSTR comment, LPCTSTR alternateName, BOOL suppressByDefault, BOOL hideByDefault, BOOL minFeatureManager, BOOL inheritProperties, unsigned long flags);
	void DeleteConfiguration(LPCTSTR configurationName);
	void EditConfiguration(LPCTSTR Name, LPCTSTR newName, LPCTSTR comment, LPCTSTR alternateName, BOOL suppressByDefault, BOOL hideByDefault, BOOL minFeatureManager, BOOL inheritProperties, unsigned long flags);
	void CreatePlanePerCurveAndPassPoint(BOOL origAtCurve);
	LPDISPATCH CreateFeatureMgrView(long* bitmap);
	BOOL AddFeatureMgrView(long* bitmap, long* appView);
	VARIANT GetStandardViewRotation(long viewId);
	double IGetStandardViewRotation(long viewId);
	void FeatureExtruRefSurface(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2);
	LPUNKNOWN IGet3rdPartyStorage(LPCTSTR stringIn, BOOL isStoring);
	void DeleteFeatureMgrView(long* appView);
	VARIANT GetMassProperties();
	BOOL IGetMassProperties(double* mPropsData);
	long GetLightSourceCount();
	long GetLightSourceIdFromName(LPCTSTR lightName);
	void SetNextSelectionGroupId(long Id);
	void ISetNextSelectionGroupId(long Id);
	LPDISPATCH InsertMidSurfaceExt(double placement, BOOL knitFlag);
	LPDISPATCH IInsertMidSurfaceExt(double placement, BOOL knitFlag);
	void ICreatePlaneFixed(double* P1, double* P2, double* P3, BOOL useGlobal);
	BOOL SelectByMark(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z, long mark);
	BOOL AndSelectByMark(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z, long mark);
	VARIANT GetDependencies(long traverseflag, long searchflag);
	CString IGetDependencies(long traverseflag, long searchflag);
	long GetNumDependencies(long traverseflag, long searchflag);
	long IGetNumDependencies(long traverseflag, long searchflag);
	LPDISPATCH FirstFeature();
	LPDISPATCH IFirstFeature();
	void UnderiveSketch();
	void DeriveSketch();
	BOOL IsExploded();
	BOOL DeleteSelection(BOOL confirmFlag);
	BOOL DeleteNamedView(LPCTSTR viewname);
	BOOL SetLightSourceName(long Id, LPCTSTR newName);
	void Insert3DSplineCurve(BOOL curveClosed);
	BOOL SetLightSourcePropertyValuesVB(LPCTSTR idName, long lType, double diff, long rgbColor, double dist, double dirX, double dirY, double dirZ, double spotDirX, double spotDirY, double spotDirZ, double spotAngle, double fallOff0, 
		double fallOff1, double fallOff2, double ambient, double specular, double spotExponent, BOOL bDisable);
	LPDISPATCH ICreateFeatureMgrView(long* bitmap);
	BOOL SelectedEdgeProperties(LPCTSTR edgeName);
	BOOL SelectedFaceProperties(long rgbColor, double ambient, double diffuse, double specular, double shininess, double transparency, double emission, BOOL usePartProps, LPCTSTR faceName);
	BOOL SelectedFeatureProperties(long rgbColor, double ambient, double diffuse, double specular, double shininess, double transparency, double emission, BOOL usePartProps, BOOL suppressed, LPCTSTR featureName);
	void InsertSplitLineSil();
	void InsertSplitLineProject(BOOL isDirectional, BOOL flipDir);
	void InsertRib(BOOL is2Sided, BOOL reverseThicknessDir, double thickness, long referenceEdgeIndex, BOOL reverseMaterialDir, BOOL isDrafted, BOOL draftOutward, double draftAngle);
	BOOL AddRadialDimension(double x, double y, double z);
	BOOL AddDiameterDimension(double x, double y, double z);
	VARIANT GetModelViewNames();
	CString IGetModelViewNames();
	long GetModelViewCount();
	double GetUserPreferenceDoubleValue(long userPreferenceValue);
	BOOL SetUserPreferenceDoubleValue(long userPreferenceValue, double Value);
	void ViewDisplayCurvature();
	void Scale();
	void AddIns();
	BOOL InsertCurveFile(LPCTSTR fileName);
	void InsertCurveFileBegin();
	BOOL InsertCurveFilePoint(double x, double y, double z);
	BOOL InsertCurveFileEnd();
	BOOL ChangeSketchPlane();
	void ViewOrientationUndo();
	void PrintOut(long fromPage, long toPage, long numCopies, BOOL collate, LPCTSTR printer, double Scale, BOOL printToFile);
	void SketchOffsetEntities(double offset, BOOL flip);
	void InsertLibraryFeature(LPCTSTR libFeatPartNameIn);
	void SketchModifyTranslate(double startX, double startY, double endX, double endY);
	void SketchModifyRotate(double centerX, double centerY, double angle);
	void SketchModifyFlip(long axisFlag);
	BOOL SketchModifyScale(double scaleFactor);
	LPDISPATCH GetActiveConfiguration();
	LPDISPATCH IGetActiveConfiguration();
	BOOL GetUserPreferenceToggle(long userPreferenceToggle);
	BOOL SetUserPreferenceToggle(long userPreferenceValue, BOOL onFlag);
	void InsertSweepRefSurface(BOOL propagate, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational);
	void InsertLoftRefSurface(BOOL closed, BOOL keepTangency, BOOL forceNonRational);
	void InsertProtrusionSwept2(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational);
	void InsertProtrusionBlend2(BOOL closed, BOOL keepTangency, BOOL forceNonRational);
	void InsertCutSwept2(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational);
	void InsertCutBlend2(BOOL closed, BOOL keepTangency, BOOL forceNonRational);
	BOOL IsEditingSelf();
	void ShowNamedView2(LPCTSTR vName, long viewId);
	void InsertDome(double height, BOOL reverseDir, BOOL doEllipticSurface);
	CString GetMaterialUserName();
	void SetMaterialUserName(LPCTSTR lpszNewValue);
	CString GetMaterialIdName();
	void SetMaterialIdName(LPCTSTR lpszNewValue);
	VARIANT GetMaterialPropertyValues();
	void SetMaterialPropertyValues(const VARIANT& newValue);
	double GetIMaterialPropertyValues();
	void SetIMaterialPropertyValues(double* newValue);
	long AddPropertyExtension(const VARIANT& PropertyExtension);
	VARIANT GetPropertyExtension(long Id);
	void ResetPropertyExtension();
	long GetUpdateStamp();
	void ViewZoomTo2(double x1, double y1, double z1, double x2, double y2, double z2);
	void ScreenRotate();
	short GetPrintSetup(long setupType);
	void SetPrintSetup(long setupType, short nNewValue);
	void GraphicsRedraw2();
	void InsertCosmeticThread(short type, double depth, double length, LPCTSTR note);
	void HideCosmeticThread();
	void ShowCosmeticThread();
	void SimpleHole2(double dia, BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2);
	void IRelease3rdPartyStorage(LPCTSTR stringIn);
	long FeatureRevolve2(double angle, BOOL reverseDir, double angle2, long revType, long options);
	long FeatureRevolveCut2(double angle, BOOL reverseDir, double angle2, long revType, long options);
	void SetSaveFlag();
	CString GetExternalReferenceName();
	BOOL SelectByRay(const VARIANT& doubleInfoIn, long typeWanted);
	BOOL ISelectByRay(double* pointIn, double* vectorIn, double radiusIn, long typeWanted);
	void SetSceneBkgDIB(long l_dib);
	CString GetSceneBkgImageFileName();
	void SetSceneBkgImageFileName(LPCTSTR lpszNewValue);
	void InsertBkgImage(LPCTSTR newName);
	void DeleteBkgImage();
	void InsertSplinePoint(double x, double y, double z);
	void InsertLoftRefSurface2(BOOL closed, BOOL keepTangency, BOOL forceNonRational, double tessToleranceFactor, short startMatchingType, short endMatchingType);
	void InsertProtrusionBlend3(BOOL closed, BOOL keepTangency, BOOL forceNonRational, double tessToleranceFactor, short startMatchingType, short endMatchingType);
	void InsertCutBlend3(BOOL closed, BOOL keepTangency, BOOL forceNonRational, double tessToleranceFactor, short startMatchingType, short endMatchingType);
	void AlignDimensions();
	void BreakDimensionAlignment();
	void SketchFillet1(double rad);
	void FeatureChamferType(short chamferType, double Width, double angle, BOOL flip, double otherDist, double vertexChamDist1, double vertexChamDist2, double vertexChamDist3);
	void FeatureCutThin(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, double thk1, double thk2, 
		double endThk, long revThinDir, long capEnds, BOOL addBends, double bendRad);
	void FeatureBossThin(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, double thk1, double thk2, 
		double endThk, long revThinDir, long capEnds, BOOL addBends, double bendRad);
	BOOL InsertDatumTargetSymbol(LPCTSTR datum1, LPCTSTR datum2, LPCTSTR datum3, short areaStyle, BOOL areaOutside, double value1, double value2, LPCTSTR valueStr1, LPCTSTR valueStr2, BOOL arrowsSmart, short arrowStyle, short leaderLineStyle, 
		BOOL leaderBent, BOOL showArea, BOOL showSymbol);
	BOOL EditDatumTargetSymbol(LPCTSTR datum1, LPCTSTR datum2, LPCTSTR datum3, short areaStyle, BOOL areaOutside, double value1, double value2, LPCTSTR valueStr1, LPCTSTR valueStr2, BOOL arrowsSmart, short arrowStyle, short leaderLineStyle, 
		BOOL leaderBent, BOOL showArea, BOOL showSymbol);
	void InsertBOMBalloon();
	LPDISPATCH FeatureReferenceCurve(long numOfCurves, const VARIANT& baseCurves, BOOL merge, LPCTSTR fromFileName, long* errorCode);
	// method 'IFeatureReferenceCurve' not emitted because of invalid return type or parameter type
	void FontBold(BOOL Bold);
	void FontItalic(BOOL Italic);
	void FontUnderline(BOOL Underline);
	void FontFace(LPCTSTR face);
	void FontPoints(short Points);
	void FontUnits(double units);
	BOOL SketchSplineByEqnParams(const VARIANT& paramsIn);
	void AlignParallelDimensions();
	void SetBlockingState(long stateIn);
	void ResetBlockingState();
	long GetSceneBkgDIB();
	void InsertHatchedFace();
	LPDISPATCH GetColorTable();
	LPDISPATCH IGetColorTable();
	void InsertSweepRefSurface2(BOOL propagate, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational, short startMatchingType, short endMatchingType);
	void InsertProtrusionSwept3(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational, short startMatchingType, short endMatchingType);
	void InsertCutSwept3(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational, short startMatchingType, short endMatchingType);
	BOOL IsOpenedViewOnly();
	BOOL IsOpenedReadOnly();
	void ViewZoomToSelection();
	void FeatureBossThicken(double thickness, long direction, long faceIndex);
	void FeatureCutThicken(double thickness, long direction, long faceIndex);
	BOOL InsertAxis();
	void EditUndo(unsigned long nSteps);
	void SelectMidpoint();
	long ISketchSplineByEqnParams(long* propArray, double* knotsArray, double* cntrlPntCoordArray);
	VARIANT VersionHistory();
	CString IVersionHistory();
	long IGetVersionHistoryCount();
	void Rebuild(long options);
	void InsertFeatureShellAddThickness(double thickness);
	void InsertOffsetSurface(double thickness, BOOL reverse);
	void SimplifySpline(double toleranceIn);
	CString GetSummaryInfo(long FieldId);
	void SetSummaryInfo(long FieldId, LPCTSTR lpszNewValue);
	CString GetCustomInfo(LPCTSTR FieldName);
	void SetCustomInfo(LPCTSTR FieldName, LPCTSTR lpszNewValue);
	long GetCustomInfoCount();
	CString GetCustomInfoType(LPCTSTR FieldName);
	VARIANT GetCustomInfoNames();
	CString IGetCustomInfoNames();
	BOOL AddCustomInfo(LPCTSTR FieldName, LPCTSTR FieldType, LPCTSTR FieldValue);
	BOOL DeleteCustomInfo(LPCTSTR FieldName);
	void PrintOut2(long fromPage, long toPage, long numCopies, BOOL collate, LPCTSTR printer, double Scale, BOOL printToFile, LPCTSTR ptfName);
	BOOL SetReadOnlyState(BOOL setReadOnly);
	BOOL InsertFamilyTableOpen(LPCTSTR fileName);
	BOOL MultiSelectByRay(const VARIANT& doubleInfoIn, long typeWanted, BOOL append);
	BOOL IMultiSelectByRay(double* pointIn, double* vectorIn, double radiusIn, long typeWanted, BOOL append);
	void InsertNewNote3(LPCTSTR upperText, BOOL noLeader, BOOL bentLeader, short arrowStyle, short leaderSide, double angle, short balloonStyle, short balloonFit, BOOL smartArrow);
	void InsertWeldSymbol2(LPCTSTR dim1, LPCTSTR symbol, LPCTSTR dim2, BOOL symmetric, BOOL fieldWeld, BOOL showOtherSide, BOOL dashOnTop, BOOL peripheral, BOOL hasProcess, LPCTSTR processValue);
	BOOL InsertSurfaceFinishSymbol2(long symType, long leaderType, double locX, double locY, double locZ, long laySymbol, long arrowType, LPCTSTR machAllowance, LPCTSTR otherVals, LPCTSTR prodMethod, LPCTSTR sampleLen, LPCTSTR maxRoughness, 
		LPCTSTR minRoughness, LPCTSTR roughnessSpacing);
	long SaveSilent();
	long SaveAsSilent(LPCTSTR newName, BOOL saveAsCopy);
	BOOL AddCustomInfo2(LPCTSTR FieldName, long FieldType, LPCTSTR FieldValue);
	long GetCustomInfoType2(LPCTSTR FieldName);
	BOOL InsertRefPoint();
	long FeatureFillet2(double r1, BOOL propagate, BOOL ftyp, BOOL varRadTyp, long overFlowType, long nRadii, const VARIANT& radii);
	long IFeatureFillet2(double r1, BOOL propagate, BOOL ftyp, BOOL varRadTyp, long overFlowType, long nRadii, double* radii);
	LPDISPATCH GetFirstAnnotation();
	LPDISPATCH IGetFirstAnnotation();
	BOOL InsertCoordinateSystem(BOOL xFlippedIn, BOOL yFlippedIn, BOOL zFlippedIn);
	BOOL GetToolbarVisibility(long toolbar);
	void SetToolbarVisibility(long toolbar, BOOL visibility);
	void ViewDispCoordinateSystems();
	void ViewDispTempRefaxes();
	void ViewDispRefPoints();
	void ViewDispOrigins();
	VARIANT GetCoordinateSystemXformByName(LPCTSTR nameIn);
	double IGetCoordinateSystemXformByName(LPCTSTR nameIn);
	CString GetCurrentCoordinateSystemName();
	LPUNKNOWN EnumModelViews();
	BOOL InsertCompositeCurve();
	void SketchParabola(double val1, double val2, double z1, double val3, double val4, double z2, double val5, double val6, double z3, double val7, double val8, double z4);
	void InsertRadiateSurface(double distance, BOOL flipDir, BOOL tangentPropagate);
	void InsertSewRefSurface();
	long InsertShape(long pressureOn, long tangentsOn, double pressureGain, double tangentGain, double curveSpringGain, double alpha, double beta, double gamma, double delta, long degree, long split, long tuning);
	void InsertMfDraft2(double angle, BOOL flipDir, BOOL isEdgeDraft, long propType, BOOL stepDraft);
	long GetConfigurationCount();
	VARIANT GetConfigurationNames();
	CString IGetConfigurationNames(long* count);
	void FeatureCut2(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, long keepPieceIndex);
	void InsertCutSurface(BOOL flip, long keepPieceIndex);
	LPDISPATCH GetDetailingDefaults();
	LPDISPATCH IGetDetailingDefaults();
	long ListExternalFileReferencesCount(BOOL useSearchRules);
	VARIANT ListExternalFileReferences(BOOL useSearchRules);
	CString IListExternalFileReferences(BOOL useSearchRules, long numRefs);
	BOOL SketchSplineByEqnParams2(const VARIANT& paramsIn);
	LPDISPATCH GetFirstModelView();
	LPDISPATCH IGetFirstModelView();
	BOOL InsertPlanarRefSurface();
	LPDISPATCH GetNext();
	BOOL GetSaveFlag();
	BOOL AddCustomInfo3(LPCTSTR configuration, LPCTSTR FieldName, long FieldType, LPCTSTR FieldValue);
	CString GetCustomInfo2(LPCTSTR configuration, LPCTSTR FieldName);
	void SetCustomInfo2(LPCTSTR configuration, LPCTSTR FieldName, LPCTSTR lpszNewValue);
	BOOL DeleteCustomInfo2(LPCTSTR configuration, LPCTSTR FieldName);
	long GetCustomInfoCount2(LPCTSTR configuration);
	VARIANT GetCustomInfoNames2(LPCTSTR configuration);
	CString IGetCustomInfoNames2(LPCTSTR configuration);
	long GetCustomInfoType3(LPCTSTR configuration, LPCTSTR FieldName);
	BOOL GetConsiderLeadersAsLines();
	BOOL SetConsiderLeadersAsLines(BOOL leadersAsLines);
	void InsertRevolvedRefSurface(double angle, BOOL reverseDir, double angle2, long revType);
	long GetBendState();
	long SetBendState(long bendState);
	BOOL GetShowFeatureErrorDialog();
	void SetShowFeatureErrorDialog(BOOL bNewValue);
	void ClearUndoList();
	long GetFeatureManagerWidth();
	long SetFeatureManagerWidth(long Width);
	LPDISPATCH InsertProjectedSketch2(long reverse);
	LPDISPATCH IInsertProjectedSketch2(long reverse);
	long GetFeatureCount();
	LPDISPATCH FeatureByPositionReverse(long num);
	LPDISPATCH IFeatureByPositionReverse(long num);
	long RayIntersections(const VARIANT& bodiesIn, const VARIANT& basePointsIn, const VARIANT& vectorsIn, long options, double hitRadius, double offset);
	long IRayIntersections(LPDISPATCH* bodiesIn, long numBodies, double* basePointsIn, double* vectorsIn, long numRays, long options, double hitRadius, double offset);
	VARIANT GetRayIntersectionsPoints();
	double IGetRayIntersectionsPoints();
	VARIANT GetRayIntersectionsTopology();
	LPUNKNOWN IGetRayIntersectionsTopology();
	void EditSeedFeat();
	BOOL EditSuppress();
	BOOL EditUnsuppress();
	BOOL EditUnsuppressDependent();
	BOOL EditRollback();
	long Save2(BOOL silent);
	long SaveAs2(LPCTSTR newName, long saveAsVersion, BOOL saveAsCopy, BOOL silent);
	void SetPopupMenuMode(long modeIn);
	long GetPopupMenuMode();
	void CloseFamilyTable();
	void CreatePlaneAtSurface(long interIndex, BOOL projOpt, BOOL reverseDir, BOOL normalPlane, double angle);
	void SketchOffset(double offset, BOOL contourMode);
	BOOL CreateLinearSketchStepAndRepeat(long numX, long numY, double spacingX, double spacingY, double angleX, double angleY, LPCTSTR deleteInstances);
	BOOL SetAmbientLightProperties(LPCTSTR Name, double ambient, double diffuse, double specular, long colour, BOOL enabled, BOOL fixed);
	BOOL GetAmbientLightProperties(LPCTSTR Name, double* ambient, double* diffuse, double* specular, long* colour, BOOL* enabled, BOOL* fixed);
	BOOL SetPointLightProperties(LPCTSTR Name, double ambient, double diffuse, double specular, long colour, BOOL enabled, BOOL fixed, double x, double y, double z);
	BOOL GetPointLightProperties(LPCTSTR Name, double* ambient, double* diffuse, double* specular, long* colour, BOOL* enabled, BOOL* fixed, double* x, double* y, double* z);
	BOOL SetDirectionLightProperties(LPCTSTR Name, double ambient, double diffuse, double specular, long colour, BOOL enabled, BOOL fixed, double x, double y, double z);
	BOOL GetDirectionLightProperties(LPCTSTR Name, double* ambient, double* diffuse, double* specular, long* colour, BOOL* enabled, BOOL* fixed, double* x, double* y, double* z);
	BOOL SetSpotlightProperties(LPCTSTR Name, double ambient, double diffuse, double specular, long colour, BOOL enabled, BOOL fixed, double posx, double posy, double posz, double targetx, double targety, double targetz, double coneAngle);
	BOOL GetSpotlightProperties(LPCTSTR Name, double* ambient, double* diffuse, double* specular, long* colour, BOOL* enabled, BOOL* fixed, double* x, double* y, double* z, double* targetx, double* targety, double* targetz, double* coneAngle);
	void SplitOpenSegment(double x, double y, double z);
	void AutoInferToggle();
	void SketchRectangleAtAnyAngle(double val1, double val2, double z1, double val3, double val4, double z2, double val3x, double val3y, double z3, BOOL val5);
	BOOL CreateCircularSketchStepAndRepeat(double arcRadius, double arcAngle, long patternNum, double patternSpacing, BOOL patternRotate, LPCTSTR deleteInstances);
	void SplitClosedSegment(double x0, double y0, double z0, double x1, double y1, double z1);
	BOOL IsLightLockedToModel(long lightId);
	BOOL LockLightToModel(long lightId, BOOL fix);
	long FeatureFillet3(double r1, BOOL propagate, long ftyp, BOOL varRadTyp, long overFlowType, long nRadii, const VARIANT& radii, BOOL useHelpPoint, BOOL useTangentHoldLine);
	long IFeatureFillet3(double r1, BOOL propagate, long ftyp, BOOL varRadTyp, long overFlowType, long nRadii, double* radii, BOOL useHelpPoint, BOOL useTangentHoldLine);
	void InsertConnectionPoint();
	void InsertRoutePoint();
	void FeatureBossThicken2(double thickness, long direction, long faceIndex, BOOL fillVolume);
	void FeatureCutThicken2(double thickness, long direction, long faceIndex, BOOL fillVolume);
	LPDISPATCH GetConfigurationByName(LPCTSTR Name);
	LPDISPATCH IGetConfigurationByName(LPCTSTR Name);
	LPDISPATCH CreatePoint2(double pointX, double pointY, double pointZ);
	LPDISPATCH ICreatePoint2(double pointX, double pointY, double pointZ);
	LPDISPATCH CreateLine2(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z);
	LPDISPATCH ICreateLine2(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z);
	LPDISPATCH GetActiveSketch2();
	LPDISPATCH IGetActiveSketch2();
	void DrawLightIcons();
	LPDISPATCH GetLayerManager();
	LPDISPATCH IGetLayerManager();
	LPDISPATCH CreateCircle2(double xC, double yC, double zc, double xp, double yp, double zp);
	LPDISPATCH ICreateCircle2(double xC, double yC, double zc, double xp, double yp, double zp);
	LPDISPATCH CreateCircleByRadius2(double xC, double yC, double zc, double radius);
	LPDISPATCH ICreateCircleByRadius2(double xC, double yC, double zc, double radius);
	LPDISPATCH CreateArc2(double xC, double yC, double zc, double xp1, double yp1, double zp1, double xp2, double yp2, double zp2, short direction);
	LPDISPATCH ICreateArc2(double xC, double yC, double zc, double xp1, double yp1, double zp1, double xp2, double yp2, double zp2, short direction);
	LPDISPATCH CreateEllipse2(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ);
	LPDISPATCH ICreateEllipse2(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ);
	LPDISPATCH CreateEllipticalArc2(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ, double startX, double startY, double startZ, double endX, double endY, 
		double endZ);
	LPDISPATCH ICreateEllipticalArc2(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ, double startX, double startY, double startZ, double endX, double endY, 
		double endZ);
	LPDISPATCH CreateSpline(const VARIANT& pointData);
	LPDISPATCH ICreateSpline(long pointCount, double* pointData);
	void ViewZoomtofit2();
	void SetInferenceMode(BOOL inferenceMode);
	BOOL GetInferenceMode();
	BOOL SetTitle2(LPCTSTR newTitle);
	BOOL SketchFillet2(double rad, short constrainedCorners);
	BOOL IsTessellationValid();
	void EditRoute();
	void FileReload();
	LPDISPATCH GetDesignTable();
	LPDISPATCH IGetDesignTable();
	CString GetEntityName(LPDISPATCH entity);
	CString IGetEntityName(LPDISPATCH entity);
	LPDISPATCH IGetNext();
	BOOL ShowConfiguration2(LPCTSTR configurationName);
	BOOL AddConfiguration2(LPCTSTR Name, LPCTSTR comment, LPCTSTR alternateName, BOOL suppressByDefault, BOOL hideByDefault, BOOL minFeatureManager, BOOL inheritProperties, unsigned long flags);
	BOOL DeleteConfiguration2(LPCTSTR configurationName);
	BOOL EditConfiguration2(LPCTSTR Name, LPCTSTR newName, LPCTSTR comment, LPCTSTR alternateName, BOOL suppressByDefault, BOOL hideByDefault, BOOL minFeatureManager, BOOL inheritProperties, unsigned long flags);
	LPDISPATCH CreateSplineByEqnParams(const VARIANT& paramsIn);
	LPDISPATCH ICreateSplineByEqnParams(long* propArray, double* knotsArray, double* cntrlPntCoordArray);
	LPDISPATCH CreateFeatureMgrView2(long* bitmap, LPCTSTR toolTip);
	LPDISPATCH ICreateFeatureMgrView2(long* bitmap, LPCTSTR toolTip);
	BOOL AddFeatureMgrView2(long* bitmap, long* appView, LPCTSTR toolTip);
	void FeatureCut3(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, long keepPieceIndex);
	LPDISPATCH GetFirstAnnotation2();
	LPDISPATCH IGetFirstAnnotation2();
	void InsertExtendSurface(BOOL extendLinear, long endCondition, double distance);
	void InsertTangencySurface(BOOL oneSide, BOOL isFlip);
	VARIANT CreateSplinesByEqnParams(const VARIANT& paramsIn);
	LPUNKNOWN ICreateSplinesByEqnParams(long* propArray, double* knotsArray, double* cntrlPntCoordArray);
	VARIANT CreateClippedSplines(const VARIANT& paramsIn, double x1, double y1, double x2, double y2);
	LPUNKNOWN ICreateClippedSplines(long* propArray, double* knotsArray, double* cntrlPntCoordArray, double x1, double y1, double x2, double y2);
	BOOL EditSuppress2();
	BOOL EditUnsuppress2();
	BOOL EditUnsuppressDependent2();
	BOOL EditRollback2();
	void HideDimension();
	void ShowFeatureDimensions();
	void HideFeatureDimensions();
	void Sketch3DIntersections();
	long FeatureFillet4(double r1, BOOL propagate, BOOL uniformRadius, long ftyp, BOOL varRadTyp, long overFlowType, long nRadii, const VARIANT& radii, BOOL useHelpPoint, BOOL useTangentHoldLine, BOOL cornerType, long setbackDistCount, 
		const VARIANT& setBackDistances);
	long IFeatureFillet4(double r1, BOOL propagate, BOOL uniformRadius, long ftyp, BOOL varRadTyp, long overFlowType, long nRadii, double* radii, BOOL useHelpPoint, BOOL useTangentHoldLine, BOOL cornerType, long setbackDistCount, 
		double* setBackDistances);
	void InsertDeleteFace();
	VARIANT GetDependencies2(BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo);
	CString IGetDependencies2(BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo);
	long IGetNumDependencies2(BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo);
	void InsertScale(double scaleFactor_x, double scaleFactor_y, double scaleFactor_z, BOOL isUniform, long scaleType);
	void LockAllExternalReferences();
	void UnlockAllExternalReferences();
	void BreakAllExternalReferences();
	BOOL EditDimensionProperties(long tolType, double tolMax, double tolMin, LPCTSTR tolMaxFit, LPCTSTR tolMinFit, BOOL useDocPrec, long precision, long arrowsIn, BOOL useDocArrows, long arrow1, long arrow2);
	BOOL SketchPolygon(double xCenter, double yCenter, double xEdge, double yEdge, long nSides, BOOL bInscribed);
	long GetBlockingState();
	LPDISPATCH CreateFeatureMgrView3(long* bitmap, LPCTSTR toolTip, long whichPane);
	LPDISPATCH ICreateFeatureMgrView3(long* bitmap, LPCTSTR toolTip, long whichPane);
	BOOL AddFeatureMgrView3(long* bitmap, long* appView, LPCTSTR toolTip, long whichPane);
	LPDISPATCH CreatePlaneAtOffset2(double val, BOOL flipDir);
	LPDISPATCH ICreatePlaneAtOffset2(double val, BOOL flipDir);
	LPDISPATCH CreatePlaneAtAngle2(double val, BOOL flipDir);
	LPDISPATCH ICreatePlaneAtAngle2(double val, BOOL flipDir);
	LPDISPATCH CreatePlaneThru3Points2();
	LPDISPATCH ICreatePlaneThru3Points2();
	LPDISPATCH CreatePlanePerCurveAndPassPoint2(BOOL origAtCurve);
	LPDISPATCH ICreatePlanePerCurveAndPassPoint2(BOOL origAtCurve);
	LPDISPATCH CreatePlaneAtSurface2(long interIndex, BOOL projOpt, BOOL reverseDir, BOOL normalPlane, double angle);
	LPDISPATCH ICreatePlaneAtSurface2(long interIndex, BOOL projOpt, BOOL reverseDir, BOOL normalPlane, double angle);
	long GetUserPreferenceIntegerValue(long userPreferenceValue);
	BOOL SetUserPreferenceIntegerValue(long userPreferenceValue, long Value);
	LPDISPATCH GetUserPreferenceTextFormat(long userPreferenceValue);
	LPDISPATCH IGetUserPreferenceTextFormat(long userPreferenceValue);
	BOOL SetUserPreferenceTextFormat(long userPreferenceValue, LPDISPATCH Value);
	BOOL ISetUserPreferenceTextFormat(long userPreferenceValue, LPDISPATCH Value);
	void InsertRib2(BOOL is2Sided, BOOL reverseThicknessDir, double thickness, long referenceEdgeIndex, BOOL reverseMaterialDir, BOOL isDrafted, BOOL draftOutward, double draftAngle, BOOL isNormToSketch);
	BOOL InsertObjectFromFile(LPCTSTR filePath, BOOL createLink, double xx, double yy, double zz);
	void InspectCurvature();
	void RemoveInspectCurvature();
	LPDISPATCH InsertDatumTag2();
	LPDISPATCH IInsertDatumTag2();
	long ActivateFeatureMgrView(long* appView);
	void FeatureSketchDrivenPattern(BOOL useCentroid);
	void HideShowBodies();
	void HideSolidBody();
	void ShowSolidBody();
	void InsertFramePoint(double xx, double yy, double zz);
	void LockFramePoint();
	void UnlockFramePoint();
	LPDISPATCH InsertGtol();
	LPDISPATCH IInsertGtol();
	BOOL DeActivateFeatureMgrView(long* appView);
	LPDISPATCH InsertNote(LPCTSTR text);
	LPDISPATCH IInsertNote(LPCTSTR text);
	void SetSaveAsFileName(LPCTSTR fileName);
	void ClosePrintPreview();
	void HideComponent2();
	void ShowComponent2();
	BOOL SaveBMP(LPCTSTR filenameIn, long widthIn, long heightIn);
	void InsertSketch2(BOOL updateEditRebuild);
	void Insert3DSketch2(BOOL updateEditRebuild);
	void InsertDeleteHole();
	void PreTrimSurface(BOOL bMutualTrimIn);
	void PostTrimSurface(BOOL bSewSurfaceIn);
	void SketchConvertIsoCurves(double percentRatio, BOOL vORuDir, BOOL doConstrain, BOOL skipHoles);
	void SelectLoop();
	void InsertSheetMetalBaseFlange(double thickness, BOOL thickenDir, double radius, double extrudeDist1, double extrudeDist2, BOOL flipExtruDir, long endCondition1, long endCondition2, long dirToUse);
	void InsertSheetMetalFold();
	void InsertSheetMetalUnfold();
	void InsertSheetMetalMiterFlange(BOOL useReliefRatio, BOOL useDefaultGap, BOOL useAutoRelief, double globalRadius, double ripGap, double autoReliefRatio, double autoReliefWidth, double autoReliefDepth, long reliefType, long ripLocation, 
		BOOL trimSideBends);
	void CreateGroup();
	void RemoveItemsFromGroup();
	void RemoveGroups();
	LPDISPATCH InsertBOMBalloon2(long Style, long size, long upperTextStyle, LPCTSTR upperText, long lowerTextStyle, LPCTSTR lowerText);
	LPDISPATCH IInsertBOMBalloon2(long Style, long size, long upperTextStyle, LPCTSTR upperText, long lowerTextStyle, LPCTSTR lowerText);
	void EditRedo(unsigned long nSteps);
	void InsertProtrusionBlend4(BOOL closed, BOOL keepTangency, BOOL forceNonRational, double tessToleranceFactor, short startMatchingType, short endMatchingType, BOOL isThinBody, double thickness1, double thickness2, short thinType);
	void InsertCutBlend4(BOOL closed, BOOL keepTangency, BOOL forceNonRational, double tessToleranceFactor, short startMatchingType, short endMatchingType, BOOL isThinBody, double thickness1, double thickness2, short thinType);
	void InsertProtrusionSwept4(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational, short startMatchingType, short endMatchingType, BOOL isThinBody, double thickness1, double thickness2, 
		short thinType);
	void InsertCutSwept4(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational, short startMatchingType, short endMatchingType, BOOL isThinBody, double thickness1, double thickness2, short thinType);
	void SelectTangency();
	BOOL InsertBendTableOpen(LPCTSTR fileName);
	BOOL InsertBendTableNew(LPCTSTR fileName, LPCTSTR units, LPCTSTR type);
	void InsertBendTableEdit();
	void DeleteBendTable();
	void InsertSheetMetal3dBend(double angle, double radius, BOOL flipDir, short bendPos);
	BOOL CreateTangentArc2(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z, long arcTypeIn);
	VARIANT GetMassProperties2(long* status);
	double IGetMassProperties2(long* status);
	void SketchChamfer(double angleORdist, double dist1, long options);
	void FeatureCut4(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, long keepPieceIndex, BOOL normalCut);
	LPDISPATCH GetPropertyManagerPage(long dialogId, LPCTSTR title, LPUNKNOWN handler);
	void AlignOrdinate();
	void EditOrdinate();
	BOOL ReattachOrdinate();
	LPDISPATCH EditBalloonProperties(long Style, long size, long upperTextStyle, LPCTSTR upperText, long lowerTextStyle, LPCTSTR lowerText);
	BOOL EditDimensionProperties2(long tolType, double tolMax, double tolMin, LPCTSTR tolMaxFit, LPCTSTR tolMinFit, BOOL useDocPrec, long precision, long arrowsIn, BOOL useDocArrows, long arrow1, long arrow2, LPCTSTR prefixText, 
		LPCTSTR suffixText, BOOL showValue, LPCTSTR calloutText1, LPCTSTR calloutText2, BOOL CenterText);
	void InsertSheetMetalClosedCorner();
	BOOL SketchUseEdge2(BOOL chain);
	BOOL SketchOffsetEntities2(double offset, BOOL bothDirections, BOOL chain);
	BOOL SketchOffset2(double offset, BOOL bothDirections, BOOL chain);
	LPDISPATCH AddDimension2(double x, double y, double z);
	LPDISPATCH IAddDimension2(double x, double y, double z);
	LPDISPATCH AddHorizontalDimension2(double x, double y, double z);
	LPDISPATCH IAddHorizontalDimension2(double x, double y, double z);
	LPDISPATCH AddVerticalDimension2(double x, double y, double z);
	LPDISPATCH IAddVerticalDimension2(double x, double y, double z);
	LPDISPATCH AddRadialDimension2(double x, double y, double z);
	LPDISPATCH IAddRadialDimension2(double x, double y, double z);
	LPDISPATCH AddDiameterDimension2(double x, double y, double z);
	LPDISPATCH IAddDiameterDimension2(double x, double y, double z);
	LPDISPATCH GetUserUnit(long UnitType);
	LPDISPATCH IGetUserUnit(long UnitType);
	LPDISPATCH InsertWeldSymbol3();
	LPDISPATCH IInsertWeldSymbol3();
	long SaveAs3(LPCTSTR newName, long saveAsVersion, long options);
	CString GetUserPreferenceStringValue(long userPreference);
	BOOL SetUserPreferenceStringValue(long userPreference, LPCTSTR Value);
	void DeleteDesignTable();
};
/////////////////////////////////////////////////////////////////////////////
// ISelectionMgr wrapper class

class ISelectionMgr : public COleDispatchDriver
{
public:
	ISelectionMgr() {}		// Calls COleDispatchDriver default constructor
	ISelectionMgr(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISelectionMgr(const ISelectionMgr& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetSelectedObjectCount();
	long GetSelectedObjectType(long AtIndex);
	LPDISPATCH GetSelectedObject(long AtIndex);
	LPUNKNOWN IGetSelectedObject(long AtIndex);
	VARIANT GetSelectionPoint(long AtIndex);
	VARIANT GetSelectionPointInSketchSpace(long AtIndex);
	LPDISPATCH GetSelectedObject2(long AtIndex);
	LPUNKNOWN IGetSelectedObject2(long AtIndex);
	BOOL IsInEditTarget(long AtIndex);
	LPDISPATCH GetSelectedObjectsComponent(long AtIndex);
	LPDISPATCH IGetSelectedObjectsComponent(long AtIndex);
	LPDISPATCH GetSelectedObject3(long AtIndex);
	LPUNKNOWN IGetSelectedObject3(long AtIndex);
	long GetSelectedObjectType2(long AtIndex);
	double IGetSelectionPoint(long AtIndex);
	double IGetSelectionPointInSketchSpace(long AtIndex);
	long GetSelectedObjectMark(long AtIndex);
	BOOL SetSelectedObjectMark(long AtIndex, long mark, long Action);
	long DeSelect(const VARIANT& AtIndex);
	long IDeSelect(long count, long* AtIndex);
};
/////////////////////////////////////////////////////////////////////////////
// IComponent wrapper class

class IComponent : public COleDispatchDriver
{
public:
	IComponent() {}		// Calls COleDispatchDriver default constructor
	IComponent(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IComponent(const IComponent& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetChildren();
	// method 'IGetChildren' not emitted because of invalid return type or parameter type
	long IGetChildrenCount();
	VARIANT GetXform();
	double IGetXform();
	LPDISPATCH GetBody();
	LPDISPATCH IGetBody();
	VARIANT GetMaterialPropertyValues();
	void SetMaterialPropertyValues(const VARIANT& newValue);
	double GetIMaterialPropertyValues();
	void SetIMaterialPropertyValues(double* newValue);
	long AddPropertyExtension(const VARIANT& PropertyExtension);
	VARIANT GetPropertyExtension(long Id);
	void ResetPropertyExtension();
	CString GetMaterialIdName();
	BOOL SetMaterialIdName(LPCTSTR Name);
	CString GetMaterialUserName();
	BOOL SetMaterialUserName(LPCTSTR Name);
	VARIANT GetSectionedBodies(LPDISPATCH viewIn);
	VARIANT GetBox(BOOL includeRefPlanes, BOOL includeSketches);
	double IGetBox(BOOL includeRefPlanes, BOOL includeSketches);
	BOOL SetXform(const VARIANT& xformIn);
	BOOL ISetXform(double* xformIn);
	LPDISPATCH GetModelDoc();
	LPDISPATCH IGetModelDoc();
	BOOL IsFixed();
	LPUNKNOWN EnumRelatedBodies();
	BOOL IsSuppressed();
	LPUNKNOWN EnumSectionedBodies(LPDISPATCH viewIn);
	BOOL IsHidden(BOOL considerSuppressed);
	CString GetName();
	void SetReferencedConfiguration(LPCTSTR lpszNewValue);
	CString GetReferencedConfiguration();
	long GetSuppression();
	long SetSuppression(long state);
	long GetVisible();
	void SetVisible(long nNewValue);
	CString GetPathName();
	BOOL SetXformAndSolve(const VARIANT& xformIn);
	BOOL ISetXformAndSolve(double* xformIn);
	VARIANT GetTessTriangles(BOOL noConversion);
	float IGetTessTriangles(BOOL noConversion);
	long IGetTessTriangleCount();
	VARIANT GetTessNorms();
	float IGetTessNorms();
	VARIANT GetTessTriStrips(BOOL noConversion);
	float IGetTessTriStrips(BOOL noConversion);
	long IGetTessTriStripSize();
	VARIANT GetTessTriStripNorms();
	float IGetTessTriStripNorms();
	VARIANT GetTessTriStripEdges();
	long IGetTessTriStripEdges();
	long IGetTessTriStripEdgeSize();
	long IsDisplayDataOutOfDate();
	long GetConstrainedStatus();
	BOOL RemoveMaterialProperty();
	long IGetTemporaryBodyID();
	LPDISPATCH FindAttribute(LPDISPATCH attributeDef, long whichOne);
	LPDISPATCH IFindAttribute(LPDISPATCH attributeDef, long whichOne);
	BOOL Select(BOOL appendFlag);
	BOOL SelectByMark(BOOL appendFlag, long mark);
	BOOL DeSelect();
	CString GetName2();
	void SetName2(LPCTSTR lpszNewValue);
	long GetSolving();
};
/////////////////////////////////////////////////////////////////////////////
// IBody wrapper class

class IBody : public COleDispatchDriver
{
public:
	IBody() {}		// Calls COleDispatchDriver default constructor
	IBody(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IBody(const IBody& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetFirstFace();
	LPDISPATCH IGetFirstFace();
	long GetFaceCount();
	LPDISPATCH CreateNewSurface();
	LPDISPATCH ICreateNewSurface();
	BOOL CreateBodyFromSurfaces();
	LPDISPATCH CreatePlanarSurface(const VARIANT& vRootPoint, const VARIANT& vNormal);
	LPDISPATCH ICreatePlanarSurface(const VARIANT& vRootPoint, const VARIANT& vNormal);
	LPDISPATCH CreateRevolutionSurface(LPDISPATCH profileCurve, const VARIANT& axisPoint, const VARIANT& axisDirection, const VARIANT& profileEndPtParams);
	LPDISPATCH ICreateRevolutionSurface(LPDISPATCH profileCurve, const VARIANT& axisPoint, const VARIANT& axisDirection, const VARIANT& profileEndPtParams);
	LPDISPATCH CreateBsplineSurface(const VARIANT& props, const VARIANT& uKnots, const VARIANT& vKnots, const VARIANT& ctrlPtCoords);
	LPDISPATCH ICreateBsplineSurface(const VARIANT& props, const VARIANT& uKnots, const VARIANT& vKnots, const VARIANT& ctrlPtCoords);
	BOOL CreateTrimmedSurface();
	LPDISPATCH AddProfileLine(const VARIANT& rootPoint, const VARIANT& direction);
	LPDISPATCH IAddProfileLine(const VARIANT& rootPoint, const VARIANT& direction);
	LPDISPATCH AddProfileArc(const VARIANT& center, const VARIANT& axis, double radius, const VARIANT& startPoint, const VARIANT& endPoint);
	LPDISPATCH IAddProfileArc(const VARIANT& center, const VARIANT& axis, double radius, const VARIANT& startPoint, const VARIANT& endPoint);
	LPDISPATCH AddProfileBspline(const VARIANT& props, const VARIANT& Knots, const VARIANT& ctrlPtCoords);
	LPDISPATCH IAddProfileBspline(const VARIANT& props, const VARIANT& Knots, const VARIANT& ctrlPtCoords);
	LPDISPATCH CreateExtrusionSurface(LPDISPATCH profileCurve, const VARIANT& axisDirection);
	LPDISPATCH ICreateExtrusionSurface(LPDISPATCH profileCurve, const VARIANT& axisDirection);
	LPDISPATCH GetFirstSelectedFace();
	LPDISPATCH IGetFirstSelectedFace();
	LPDISPATCH GetNextSelectedFace();
	LPDISPATCH IGetNextSelectedFace();
	long GetSelectedFaceCount();
	BOOL CreateBoundedSurface(BOOL uOpt, BOOL vOpt, const VARIANT& uvParams);
	long GetIgesErrorCount();
	long GetIgesErrorCode(long index);
	LPDISPATCH Copy();
	LPDISPATCH ICopy();
	LPUNKNOWN EnumFaces();
	LPDISPATCH CreateBodyFromFaces(long NumOfFaces, const VARIANT& FaceList);
	LPDISPATCH ICreateBodyFromFaces(long NumOfFaces, const VARIANT& FaceList);
	BOOL DeleteFaces(long NumOfFaces, const VARIANT& FaceList);
	void Display(LPDISPATCH part, long Color);
	void IDisplay(LPDISPATCH part, long Color);
	void Hide(LPDISPATCH part);
	void IHide(LPDISPATCH part);
	LPDISPATCH ICreatePlanarSurfaceDLL(double* rootPoint, double* Normal);
	LPDISPATCH ICreateRevolutionSurfaceDLL(LPDISPATCH profileCurve, double* axisPoint, double* axisDirection, double* profileEndPtParams);
	LPDISPATCH IAddProfileLineDLL(double* rootPoint, double* direction);
	LPDISPATCH IAddProfileArcDLL(double* center, double* axis, double radius, double* startPoint, double* endPoint);
	LPDISPATCH ICreateBsplineSurfaceDLL(long* Properties, double* UKnotArray, double* VKnotArray, double* ControlPointCoordArray);
	LPDISPATCH IAddProfileBsplineDLL(long* Properties, double* KnotArray, double* ControlPointCoordArray);
	LPDISPATCH ICreateExtrusionSurfaceDLL(LPDISPATCH profileCurve, double* axisDirection);
	void ICreateBoundedSurface(BOOL uOpt, BOOL vOpt, double* uvParams);
	void ICombineVolumes(LPDISPATCH ToolBody);
	long ISectionBySheet(LPDISPATCH sheet, long NumMaxSections, LPDISPATCH* SectionedBodies);
	void IGetBodyBox(double* BoxCorners);
	void SetIgesInfo(LPCTSTR systemName, double granularity, BOOL attemptKnitting);
	void DisplayWireFrameXOR(LPDISPATCH part, long Color);
	void IDisplayWireFrameXOR(LPDISPATCH part, long Color);
	void Save(LPUNKNOWN streamIn);
	void ISave(LPUNKNOWN streamIn);
	LPDISPATCH CreateBlendSurface(LPDISPATCH Surface1, double Range1, LPDISPATCH Surface2, double Range2, const VARIANT& StartVec, const VARIANT& EndVec, long HaveHelpVec, const VARIANT& HelpVec, long HaveHelpBox, const VARIANT& HelpBox);
	LPDISPATCH ICreateBlendSurface(LPDISPATCH Surface1, double Range1, LPDISPATCH Surface2, double Range2, double* StartVec, double* EndVec, long HaveHelpVec, double* HelpVec, long HaveHelpBox, double* HelpBox);
	LPDISPATCH CreateOffsetSurface(LPDISPATCH surfaceIn, double distance);
	LPDISPATCH ICreateOffsetSurface(LPDISPATCH surfaceIn, double distance);
	BOOL RemoveRedundantTopology();
	VARIANT GetIntersectionEdges(LPDISPATCH toolBodyIn);
	// method 'IGetIntersectionEdges' not emitted because of invalid return type or parameter type
	long IGetIntersectionEdgeCount(LPDISPATCH toolBodyIn);
	void RemoveFacesFromSheet(long NumOfFaces, const VARIANT& facesToRemove);
	// method 'IRemoveFacesFromSheet' not emitted because of invalid return type or parameter type
	void ICreatePlanarTrimSurfaceDLL(long VertexCount, double* Points, double* Normal);
	VARIANT GetMaterialPropertyValues();
	void SetMaterialPropertyValues(const VARIANT& newValue);
	double GetIMaterialPropertyValues();
	void SetIMaterialPropertyValues(double* newValue);
	long AddPropertyExtension(const VARIANT& PropertyExtension);
	VARIANT GetPropertyExtension(long Id);
	void ResetPropertyExtension();
	CString GetMaterialIdName();
	BOOL SetMaterialIdName(LPCTSTR Name);
	CString GetMaterialUserName();
	BOOL SetMaterialUserName(LPCTSTR Name);
	VARIANT GetMassProperties(double density);
	double IGetMassProperties(double density);
	LPDISPATCH ICreatePsplineSurfaceDLL(long dim, long uorder, long vOrder, long ncol, long nrow, double* coeffs, long basis, double* xform, double scaleFactor);
	BOOL SetXform(const VARIANT& xformIn);
	BOOL ISetXform(double* xformIn);
	LPDISPATCH CreateTempBodyFromSurfaces();
	LPDISPATCH ICreateTempBodyFromSurfaces();
	VARIANT Operations(long operationType, LPDISPATCH ToolBody, long NumMaxSections);
	long IOperations(long operationType, LPDISPATCH ToolBody, long NumMaxSections, LPDISPATCH* resultingBodies);
	LPDISPATCH GetSheetBody(long index);
	LPDISPATCH IGetSheetBody(long index);
	LPDISPATCH GetProcessedBody();
	LPDISPATCH IGetProcessedBody();
	LPDISPATCH GetProcessedBodyWithSelFace();
	LPDISPATCH IGetProcessedBodyWithSelFace();
	long Check();
	VARIANT GetExcessBodyArray();
	// method 'IGetExcessBodyArray' not emitted because of invalid return type or parameter type
	long IGetExcessBodyCount();
	BOOL CreateBaseFeature(LPDISPATCH bodyIn);
	BOOL ICreateBaseFeature(LPDISPATCH bodyIn);
	long DeleteFaces2(long NumOfFaces, const VARIANT& FaceList, long option);
	long IDeleteFaces2(long NumOfFaces, LPDISPATCH* FaceList, long option);
	LPDISPATCH IAddVertexPoint(double* point);
	LPDISPATCH AddVertexPoint(const VARIANT& point);
	BOOL GetExtremePoint(double x, double y, double z, double* outx, double* outy, double* outz);
	long GetType();
	void IDeleteFaces3(long NumOfFaces, LPDISPATCH* FaceList, long option, BOOL doLocalCheck, BOOL* localCheckResult);
	void SetCurrentSurface(LPDISPATCH surfaceIn);
	void ISetCurrentSurface(LPDISPATCH surfaceIn);
	BOOL DraftBody(long NumOfFaces, const VARIANT& FaceList, const VARIANT& EdgeList, double draftAngle, const VARIANT& dir);
	BOOL IDraftBody(long NumOfFaces, LPDISPATCH* FaceList, LPDISPATCH* EdgeList, double draftAngle, double* dir);
	BOOL DeleteBlends(long NumOfFaces, const VARIANT& FaceList);
	BOOL IDeleteBlends(long NumOfFaces, LPDISPATCH* FaceList);
	VARIANT Operations2(long operationType, LPDISPATCH ToolBody, long* errorCode);
	LPUNKNOWN IOperations2(long operationType, LPDISPATCH ToolBody, long* errorCode);
	VARIANT GetBodyBox();
	BOOL DeleteBlends2(long NumOfFaces, const VARIANT& FaceList, BOOL doLocalCheck);
	BOOL IDeleteBlends2(long NumOfFaces, LPDISPATCH* FaceList, BOOL doLocalCheck);
	LPDISPATCH GetTessellation(const VARIANT& FaceList);
	LPDISPATCH IGetTessellation(long NumOfFaces, LPDISPATCH* FaceList);
	BOOL GetVisible();
	VARIANT MatchedBoolean(long operationType, LPDISPATCH ToolBody, long numOfMatchingFaces, const VARIANT& faceList1, const VARIANT& faceList2, long* errorCode);
	LPUNKNOWN IMatchedBoolean(long operationType, LPDISPATCH ToolBody, long numOfMatchingFaces, LPDISPATCH* faceList1, LPDISPATCH* faceList2, long* errorCode);
};
/////////////////////////////////////////////////////////////////////////////
// IFace wrapper class

class IFace : public COleDispatchDriver
{
public:
	IFace() {}		// Calls COleDispatchDriver default constructor
	IFace(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IFace(const IFace& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetNormal();
	void SetNormal(const VARIANT& newValue);
	CString GetMaterialUserName();
	void SetMaterialUserName(LPCTSTR lpszNewValue);
	CString GetMaterialIdName();
	void SetMaterialIdName(LPCTSTR lpszNewValue);
	VARIANT GetMaterialPropertyValues();
	void SetMaterialPropertyValues(const VARIANT& newValue);
	double GetINormal();
	void SetINormal(double* newValue);
	LPDISPATCH GetNextFace();
	LPDISPATCH IGetNextFace();
	LPDISPATCH GetSurface();
	LPDISPATCH IGetSurface();
	long GetEdgeCount();
	VARIANT GetEdges();
	// method 'IGetEdges' not emitted because of invalid return type or parameter type
	LPDISPATCH GetFeature();
	LPDISPATCH IGetFeature();
	long GetFeatureId();
	VARIANT GetTrimCurves(BOOL wantCubic);
	VARIANT GetUVBounds();
	BOOL FaceInSurfaceSense();
	long GetLoopCount();
	LPDISPATCH GetFirstLoop();
	LPDISPATCH IGetFirstLoop();
	BOOL IsSame(LPDISPATCH faceIn);
	BOOL IIsSame(LPDISPATCH faceIn);
	long AddPropertyExtension(const VARIANT& PropertyExtension);
	VARIANT GetPropertyExtension(long Id);
	void ResetPropertyExtension();
	VARIANT GetTessTriangles(BOOL noConversion);
	VARIANT GetTessNorms();
	VARIANT GetTessTriStrips(BOOL noConversion);
	VARIANT GetTessTriStripNorms();
	LPUNKNOWN EnumLoops();
	LPUNKNOWN EnumEdges();
	LPDISPATCH GetBody();
	LPDISPATCH IGetBody();
	float IGetTessTriangles(BOOL noConversion);
	float IGetTessNorms();
	long GetTessTriangleCount();
	float IGetTessTriStrips(BOOL noConversion);
	float IGetTessTriStripNorms();
	long GetTessTriStripSize();
	double IGetUVBounds();
	VARIANT GetClosestPointOn(double x, double y, double z);
	double IGetClosestPointOn(double x, double y, double z);
	void Highlight(BOOL state);
	void IHighlight(BOOL state);
	VARIANT GetTrimCurveTopology();
	// method 'IGetTrimCurveTopology' not emitted because of invalid return type or parameter type
	long GetTrimCurveTopologyCount();
	VARIANT GetTrimCurveTopologyTypes();
	long IGetTrimCurveTopologyTypes();
	BOOL RemoveRedundantTopology();
	LPDISPATCH CreateSheetBodyByFaceExtension(const VARIANT& boxLowIn, const VARIANT& boxHighIn);
	LPDISPATCH ICreateSheetBodyByFaceExtension(double* boxLowIn, double* boxHighIn);
	double GetArea();
	VARIANT GetBox();
	double IGetBox();
	LPDISPATCH RemoveInnerLoops(long numOfLoops, const VARIANT& innerLoopsIn);
	LPDISPATCH IRemoveInnerLoops(long numOfLoops, LPDISPATCH* innerLoopsIn);
	LPDISPATCH CreateSheetBody();
	LPDISPATCH ICreateSheetBody();
	VARIANT GetSilhoutteEdges(double* root, double* Normal);
	// method 'IGetSilhoutteEdges' not emitted because of invalid return type or parameter type
	long IGetSilhoutteEdgeCount(double* root, double* Normal);
	long IGetTrimCurveSize(BOOL wantCubic);
	double IGetTrimCurve();
	double GetIMaterialPropertyValues();
	void SetIMaterialPropertyValues(double* newValue);
	long GetFaceId();
	void SetFaceId(long idIn);
	long IGetTrimCurveSize2(long wantCubic, long wantNRational);
	VARIANT GetSilhoutteEdgesVB(double xroot, double yroot, double zroot, double xnormal, double ynormal, double znormal);
	void RemoveFaceId(long idIn);
	VARIANT GetTrimCurves2(BOOL wantCubic, BOOL wantNRational);
	long GetShellType();
	// method 'IGetFacetData' not emitted because of invalid return type or parameter type
	VARIANT GetTessTriStripEdges();
	long IGetTessTriStripEdges();
	long IGetTessTriStripEdgeSize();
	BOOL RemoveMaterialProperty();
	LPDISPATCH GetPatternSeedFeature();
	LPDISPATCH IGetPatternSeedFeature();
};
/////////////////////////////////////////////////////////////////////////////
// ISurface wrapper class

class ISurface : public COleDispatchDriver
{
public:
	ISurface() {}		// Calls COleDispatchDriver default constructor
	ISurface(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISurface(const ISurface& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetPlaneParams();
	VARIANT GetCylinderParams();
	VARIANT GetConeParams();
	VARIANT GetSphereParams();
	VARIANT GetTorusParams();
	BOOL IsPlane();
	BOOL IsCylinder();
	BOOL IsCone();
	long Identity();
	BOOL IsSphere();
	BOOL IsTorus();
	BOOL IsParametric();
	BOOL IsBlending();
	BOOL IsOffset();
	BOOL IsSwept();
	BOOL IsRevolved();
	BOOL IsForeign();
	VARIANT Parameterization();
	VARIANT Evaluate(double UParam, double VParam, long NumUDeriv, long NumVDeriv);
	VARIANT GetBSurfParams(BOOL wantCubicRational, const VARIANT& vP0);
	BOOL AddTrimmingLoop(long nCrvs, const VARIANT& vOrder, const VARIANT& vDim, const VARIANT& vPeriodic, const VARIANT& vNumKnots, const VARIANT& vNumCtrlPoints, const VARIANT& vKnots, const VARIANT& vCtrlPointDbls);
	LPDISPATCH CreateNewCurve();
	LPDISPATCH ICreateNewCurve();
	VARIANT GetRevsurfParams();
	VARIANT GetExtrusionsurfParams();
	LPDISPATCH GetProfileCurve();
	LPDISPATCH IGetProfileCurve();
	VARIANT ReverseEvaluate(double positionX, double positionY, double positionZ);
	double GetIPlaneParams();
	double GetICylinderParams();
	double GetIConeParams();
	double GetISphereParams();
	double GetITorusParams();
	double IReverseEvaluate(double positionX, double positionY, double positionZ);
	double IGetRevsurfParams();
	double IGetExtrusionsurfParams();
	double IParameterization();
	double IEvaluate(double UParam, double VParam, long NumUDeriv, long NumVDeriv);
	double IGetBSurfParams();
	long IGetBSurfParamsSize(BOOL wantCubicRational, double* Range);
	void IAddTrimmingLoop(long CurveCount, long* order, long* dim, long* Periodic, long* NumKnots, long* NumCtrlPoints, double* Knots, double* CtrlPointDbls);
	VARIANT EvaluateAtPoint(double positionX, double positionY, double positionZ);
	double IEvaluateAtPoint(double positionX, double positionY, double positionZ);
	double GetOffsetSurfParams();
	long IGetBSurfParamsSize2(BOOL wantCubic, BOOL wantNonRational, double* Range);
	LPDISPATCH Copy();
	LPDISPATCH ICopy();
	LPDISPATCH CreateTrimmedSheet(const VARIANT& curves);
	LPDISPATCH ICreateTrimmedSheet(long nCurves, LPDISPATCH* curves);
	BOOL AddTrimmingLoop2(long nCrvs, const VARIANT& vOrder, const VARIANT& vDim, const VARIANT& vPeriodic, const VARIANT& vNumKnots, const VARIANT& vNumCtrlPoints, const VARIANT& vKnots, const VARIANT& vCtrlPointDbls, const VARIANT& uvRange);
	void IAddTrimmingLoop2(long CurveCount, long* order, long* dim, long* Periodic, long* NumKnots, long* NumCtrlPoints, double* Knots, double* CtrlPointDbls, double* uvRange);
	VARIANT GetBSurfParams2(BOOL wantCubic, BOOL wantNonRational, const VARIANT& vP0, double tolerance, BOOL* sense);
	long IGetBSurfParamsSize3(BOOL wantCubic, BOOL wantNonRational, double* Range, double tolerance, BOOL* sense);
	VARIANT GetClosestPointOn(double x, double y, double z);
	double IGetClosestPointOn(double x, double y, double z);
	double GetOffsetSurfParams2(LPDISPATCH* baseSurf, BOOL* sense);
	double IGetOffsetSurfParams2(LPDISPATCH* baseSurf, BOOL* sense);
};
/////////////////////////////////////////////////////////////////////////////
// ICurve wrapper class

class ICurve : public COleDispatchDriver
{
public:
	ICurve() {}		// Calls COleDispatchDriver default constructor
	ICurve(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ICurve(const ICurve& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetLineParams();
	VARIANT GetCircleParams();
	long Identity();
	BOOL IsCircle();
	BOOL IsLine();
	BOOL IsBcurve();
	VARIANT GetBCurveParams(BOOL wantCubicIn);
	VARIANT ConvertLineToBcurve(const VARIANT& startPoint, const VARIANT& endPoint);
	VARIANT ConvertArcToBcurve(const VARIANT& center, const VARIANT& axis, const VARIANT& start, const VARIANT& end);
	LPDISPATCH ReverseCurve();
	LPDISPATCH IReverseCurve();
	VARIANT GetPCurveParams();
	double GetILineParams();
	double GetICircleParams();
	double IGetBCurveParams();
	long IGetBCurveParamsSize(BOOL wantCubicIn);
	long IConvertLineToBcurveSize(double* startPoint, double* endPoint);
	long IConvertArcToBcurveSize(double* center, double* axis, double* start, double* end);
	double IGetPCurveParams();
	long IGetPCurveParamsSize();
	double GetLength(double startParam, double endParam);
	long IConvertPcurveToBcurveSize(long dim, long order, long nsegs, double* coeffs, long basis, double* xform, double scaleFactor);
	VARIANT GetSplinePts(const VARIANT& paramsArrayIn);
	double IGetSplinePts();
	long IGetSplinePtsSize(long* propArray, double* knotsArray, double* cntrlPntCoordArray);
	long IGetBCurveParamsSize2(BOOL wantCubic, BOOL wantNRational);
	LPDISPATCH Copy();
	LPDISPATCH ICopy();
	VARIANT GetTessPts(double chordTolerance, double lengthTolerance, const VARIANT& startPoint, const VARIANT& endPoint);
	double IGetTessPts(double chordTolerance, double lengthTolerance, double* startPoint, double* endPoint);
	long IGetTessPtsSize(double chordTolerance, double lengthTolerance, double* startPoint, double* endPoint);
	VARIANT IntersectCurve(LPDISPATCH otherCurve, const VARIANT& thisStartPoint, const VARIANT& thisEndPoint, const VARIANT& otherStartPoint, const VARIANT& otherEndPoint);
	double IIntersectCurve(LPDISPATCH otherCurve, double* thisStartPoint, double* thisEndPoint, double* otherStartPoint, double* otherEndPoint);
	long IIntersectCurveSize(LPDISPATCH otherCurve, double* thisStartPoint, double* thisEndPoint, double* otherStartPoint, double* otherEndPoint);
	LPDISPATCH CreateTrimmedCurve(double x1, double y1, double z1, double x2, double y2, double z2);
	LPDISPATCH ICreateTrimmedCurve(double* start, double* end);
	BOOL IsEllipse();
	VARIANT GetEllipseParams();
	void IGetEllipseParams(double* paramArray);
	VARIANT Evaluate(double Parameter);
	double IEvaluate(double Parameter);
	VARIANT GetClosestPointOn(double x, double y, double z);
	double IGetClosestPointOn(double x, double y, double z);
	BOOL GetEndParams(double* start, double* end, BOOL* isClosed, BOOL* isPeriodic);
};
/////////////////////////////////////////////////////////////////////////////
// IEdge wrapper class

class IEdge : public COleDispatchDriver
{
public:
	IEdge() {}		// Calls COleDispatchDriver default constructor
	IEdge(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IEdge(const IEdge& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetCurve();
	LPDISPATCH IGetCurve();
	VARIANT GetCurveParams();
	double IGetCurveParams();
	VARIANT Evaluate(double Parameter);
	double IEvaluate(double Parameter);
	VARIANT GetParameter(double x, double y, double z);
	double IGetParameter(double x, double y, double z);
	BOOL EdgeInFaceSense(LPDISPATCH facedisp);
	BOOL IEdgeInFaceSense(LPDISPATCH facedisp);
	VARIANT GetTwoAdjacentFaces();
	void IGetTwoAdjacentFaces(LPDISPATCH* face1, LPDISPATCH* face2);
	LPUNKNOWN EnumCoEdges();
	LPDISPATCH GetStartVertex();
	LPDISPATCH IGetStartVertex();
	LPDISPATCH GetEndVertex();
	LPDISPATCH IGetEndVertex();
	VARIANT GetClosestPointOn(double x, double y, double z);
	double IGetClosestPointOn(double x, double y, double z);
	BOOL RemoveRedundantTopology();
	long GetId();
	void SetId(long idIn);
	void RemoveId();
	VARIANT GetCurveParams2();
	double IGetCurveParams2();
	BOOL IsParamReversed();
	void Highlight(BOOL state);
	VARIANT GetCoEdges();
};
/////////////////////////////////////////////////////////////////////////////
// ICoEdge wrapper class

class ICoEdge : public COleDispatchDriver
{
public:
	ICoEdge() {}		// Calls COleDispatchDriver default constructor
	ICoEdge(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ICoEdge(const ICoEdge& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetEdge();
	LPDISPATCH IGetEdge();
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	LPDISPATCH GetLoop();
	LPDISPATCH IGetLoop();
	BOOL GetSense();
	LPDISPATCH GetPartner();
	LPDISPATCH IGetPartner();
	VARIANT GetCurveParams();
	VARIANT Evaluate(double param);
	double IGetCurveParams();
	double IEvaluate(double param);
	LPDISPATCH GetCurve();
	LPDISPATCH IGetCurve();
};
/////////////////////////////////////////////////////////////////////////////
// ILoop wrapper class

class ILoop : public COleDispatchDriver
{
public:
	ILoop() {}		// Calls COleDispatchDriver default constructor
	ILoop(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ILoop(const ILoop& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	VARIANT GetEdges();
	// method 'IGetEdges' not emitted because of invalid return type or parameter type
	long GetEdgeCount();
	BOOL IsOuter();
	LPDISPATCH GetFirstCoEdge();
	LPDISPATCH IGetFirstCoEdge();
	LPUNKNOWN EnumEdges();
	LPUNKNOWN EnumCoEdges();
	LPDISPATCH GetFace();
	LPDISPATCH IGetFace();
	VARIANT SweepPlanarLoop(double x, double y, double z, double draftAngle);
	LPDISPATCH ISweepPlanarLoop(double x, double y, double z, double draftAngle, LPDISPATCH* stopFacesOut);
	VARIANT RevolvePlanarLoop(double x, double y, double z, double axisx, double axisy, double axisz, double revAngle);
	LPDISPATCH IRevolvePlanarLoop(double x, double y, double z, double axisx, double axisy, double axisz, double revAngle, LPDISPATCH* stopFacesOut);
};
/////////////////////////////////////////////////////////////////////////////
// IVertex wrapper class

class IVertex : public COleDispatchDriver
{
public:
	IVertex() {}		// Calls COleDispatchDriver default constructor
	IVertex(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IVertex(const IVertex& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetPoint();
	double IGetPoint();
	LPUNKNOWN EnumEdges();
	VARIANT GetClosestPointOn(double x, double y, double z);
	double IGetClosestPointOn(double x, double y, double z);
	LPUNKNOWN EnumEdgesOriented();
};
/////////////////////////////////////////////////////////////////////////////
// IFeature wrapper class

class IFeature : public COleDispatchDriver
{
public:
	IFeature() {}		// Calls COleDispatchDriver default constructor
	IFeature(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IFeature(const IFeature& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	CString GetName();
	void SetName(LPCTSTR lpszNewValue);
	LPDISPATCH Parameter(LPCTSTR Name);
	LPDISPATCH IParameter(LPCTSTR Name);
	LPDISPATCH GetNextFeature();
	LPDISPATCH IGetNextFeature();
	CString GetTypeName();
	LPDISPATCH GetSpecificFeature();
	LPUNKNOWN IGetSpecificFeature();
	BOOL GetUIState(long stateType);
	void SetUIState(long stateType, BOOL flag);
	CString GetMaterialUserName();
	BOOL SetMaterialUserName(LPCTSTR Name);
	CString GetMaterialIdName();
	BOOL SetMaterialIdName(LPCTSTR Name);
	VARIANT GetMaterialPropertyValues();
	BOOL SetMaterialPropertyValues(const VARIANT& MaterialPropertyValues);
	long AddPropertyExtension(const VARIANT& PropertyExtension);
	VARIANT GetPropertyExtension(long Id);
	void ResetPropertyExtension();
	LPDISPATCH GetFirstSubFeature();
	LPDISPATCH IGetFirstSubFeature();
	LPDISPATCH GetNextSubFeature();
	LPDISPATCH IGetNextSubFeature();
	double IGetMaterialPropertyValues();
	BOOL ISetMaterialPropertyValues(double* MaterialPropertyValues);
	BOOL IsSuppressed();
	long GetUpdateStamp();
	BOOL SetBody(LPDISPATCH bodyIn);
	BOOL ISetBody(LPDISPATCH bodyIn);
	LPDISPATCH GetBody();
	LPDISPATCH IGetBody();
	LPUNKNOWN EnumDisplayDimensions();
	LPDISPATCH GetDefinition();
	LPUNKNOWN IGetDefinition();
	BOOL ModifyDefinition(LPDISPATCH data, LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IModifyDefinition(LPUNKNOWN data, LPDISPATCH topDoc, LPDISPATCH component);
	long GetFaceCount();
	VARIANT GetFaces();
	LPDISPATCH IGetFaces(long* faceCount);
	LPDISPATCH GetFirstDisplayDimension();
	LPDISPATCH GetNextDisplayDimension(LPDISPATCH dispIn);
	long GetErrorCode();
	long IGetChildCount();
	VARIANT GetChildren();
	LPDISPATCH IGetChildren();
	long IGetParentCount();
	VARIANT GetParents();
	LPDISPATCH IGetParents();
	BOOL SetBody2(LPDISPATCH bodyIn, BOOL applyUserIds);
	BOOL ISetBody2(LPDISPATCH bodyIn, BOOL applyUserIds);
	CString GetImportedFileName();
	BOOL SetImportedFileName(LPCTSTR ImpName);
	BOOL SetSuppression(long suppressState);
	BOOL RemoveMaterialProperty();
	BOOL Select(BOOL appendFlag);
	BOOL SelectByMark(BOOL appendFlag, long mark);
	BOOL DeSelect();
	BOOL GetBox(VARIANT* bBox);
	BOOL IGetBox(double* bBox);
};
/////////////////////////////////////////////////////////////////////////////
// IDimension wrapper class

class IDimension : public COleDispatchDriver
{
public:
	IDimension() {}		// Calls COleDispatchDriver default constructor
	IDimension(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDimension(const IDimension& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	double GetValue();
	void SetValue(double newValue);
	double GetSystemValue();
	void SetSystemValue(double newValue);
	double GetUserValueIn(LPDISPATCH doc);
	double IGetUserValueIn(LPDISPATCH doc);
	void SetUserValueIn(LPDISPATCH doc, double newValue);
	void ISetUserValueIn(LPDISPATCH doc, double newValue);
	CString GetName();
	void SetName(LPCTSTR lpszNewValue);
	long GetToleranceType();
	BOOL SetToleranceType(long newType);
	VARIANT GetToleranceValues();
	double IGetToleranceValues();
	BOOL SetToleranceValues(double tolMin, double tolMax);
	VARIANT GetToleranceFontInfo();
	double IGetToleranceFontInfo();
	BOOL SetToleranceFontInfo(long useFontScale, double tolScale, double tolHeight);
	CString GetToleranceFitValues();
	BOOL SetToleranceFitValues(LPCTSTR newLValue, LPCTSTR newUValue);
	BOOL GetReadOnly();
	void SetReadOnly(BOOL bNewValue);
	CString GetFullName();
	long GetDrivenState();
	void SetDrivenState(long nNewValue);
	BOOL IsReference();
	BOOL IsAppliedToAllConfigurations();
	long SetSystemValue2(double newValue, long whichConfigurations);
	long SetValue2(double newValue, long whichConfigurations);
	long SetUserValueIn2(LPDISPATCH doc, double newValue, long whichConfigurations);
	long ISetUserValueIn2(LPDISPATCH doc, double newValue, long whichConfigurations);
	long GetArcEndCondition(long index);
	long SetArcEndCondition(long index, long condition);
	double GetValue2(LPCTSTR configName);
	double GetSystemValue2(LPCTSTR configName);
};
/////////////////////////////////////////////////////////////////////////////
// IDisplayDimension wrapper class

class IDisplayDimension : public COleDispatchDriver
{
public:
	IDisplayDimension() {}		// Calls COleDispatchDriver default constructor
	IDisplayDimension(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDisplayDimension(const IDisplayDimension& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	LPDISPATCH GetDisplayData();
	LPDISPATCH IGetDisplayData();
	long GetWitnessVisibility();
	void SetWitnessVisibility(long nNewValue);
	long GetLeaderVisibility();
	void SetLeaderVisibility(long nNewValue);
	BOOL GetBrokenLeader();
	void SetBrokenLeader(BOOL bNewValue);
	BOOL GetSmartWitness();
	void SetSmartWitness(BOOL bNewValue);
	BOOL GetShowParenthesis();
	void SetShowParenthesis(BOOL bNewValue);
	long GetArrowSide();
	void SetArrowSide(long nNewValue);
	BOOL GetShowDimensionValue();
	void SetShowDimensionValue(BOOL bNewValue);
	CString GetText(long whichText);
	void SetText(long whichText, LPCTSTR text);
	LPDISPATCH GetDimension();
	LPDISPATCH IGetDimension();
	BOOL GetUseDocTextFormat();
	LPDISPATCH GetTextFormat();
	LPDISPATCH IGetTextFormat();
	BOOL SetTextFormat(long textFormatType, LPDISPATCH textFormat);
	BOOL ISetTextFormat(long textFormatType, LPDISPATCH textFormat);
	LPDISPATCH GetAnnotation();
	LPDISPATCH IGetAnnotation();
	BOOL GetSolidLeader();
	void SetSolidLeader(BOOL bNewValue);
	BOOL GetDiametric();
	void SetDiametric(BOOL bNewValue);
	BOOL GetDisplayAsLinear();
	void SetDisplayAsLinear(BOOL bNewValue);
	BOOL GetUseDocSecondArrow();
	BOOL GetSecondArrow();
	void SetSecondArrow(BOOL useDoc, BOOL secondArrow);
	BOOL GetShortenedRadius();
	void SetShortenedRadius(BOOL bNewValue);
	BOOL GetDimensionToInside();
	void SetDimensionToInside(BOOL bNewValue);
	BOOL GetUseDocDual();
	void SetDual(BOOL useDoc);
	BOOL GetUseDocArrowHeadStyle();
	long GetArrowHeadStyle();
	void SetArrowHeadStyle(BOOL useDoc, long arrowHeadStyle);
	BOOL GetCenterText();
	void SetCenterText(BOOL bNewValue);
	BOOL GetInspection();
	void SetInspection(BOOL bNewValue);
	BOOL GetUseDocPrecision();
	long GetPrimaryPrecision();
	long GetAlternatePrecision();
	long GetPrimaryTolPrecision();
	long GetAlternateTolPrecision();
	long SetPrecision(BOOL useDoc, long primary, long alternate, long primaryTol, long alternateTol);
	BOOL GetAutoArcLengthLeader();
	long GetArcLengthLeader();
	long SetArcLengthLeader(BOOL autoLeader, long leaderType);
	BOOL GetUseDocUnits();
	long GetUnits();
	long GetFractionBase();
	long GetFractionValue();
	BOOL GetRoundToFraction();
	long SetUnits(BOOL useDoc, long uType, long fractBase, long fractDenom, BOOL roundToFraction);
	BOOL GetUseDocBrokenLeader();
	long GetBrokenLeader2();
	long SetBrokenLeader2(BOOL useDoc, long broken);
	LPDISPATCH GetNext2();
	LPDISPATCH IGetNext2();
	long GetType();
	double GetScale2();
	void SetScale2(double newValue);
	BOOL GetDisplayAsChain();
	void SetDisplayAsChain(BOOL bNewValue);
	LPDISPATCH GetNext3();
	LPDISPATCH IGetNext3();
	BOOL AddDisplayEnt(long type, const VARIANT& data);
	BOOL IAddDisplayEnt(long type, double* data);
	BOOL AddDisplayText(LPCTSTR text, const VARIANT& position, LPDISPATCH format, long attachment, double WidthFactor);
	BOOL IAddDisplayText(LPCTSTR text, double* position, LPDISPATCH format, long attachment, double WidthFactor);
	BOOL GetJogged();
	void SetJogged(BOOL bNewValue);
	BOOL AutoJogOrdinate();
};
/////////////////////////////////////////////////////////////////////////////
// IDisplayData wrapper class

class IDisplayData : public COleDispatchDriver
{
public:
	IDisplayData() {}		// Calls COleDispatchDriver default constructor
	IDisplayData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDisplayData(const IDisplayData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetTextCount();
	CString GetTextAtIndex(long index);
	double GetTextHeightAtIndex(long index);
	VARIANT GetTextPositionAtIndex(long index);
	double IGetTextPositionAtIndex(long index);
	double GetTextAngleAtIndex(long index);
	long GetTextRefPositionAtIndex(long index);
	long GetTextInvertAtIndex(long index);
	CString GetTextFontAtIndex(long index);
	long GetLineCount();
	VARIANT GetLineAtIndex(long index);
	double IGetLineAtIndex(long index);
	long GetArcCount();
	VARIANT GetArcAtIndex(long index);
	double IGetArcAtIndex(long index);
	long GetArrowHeadCount();
	VARIANT GetArrowHeadAtIndex(long index);
	double IGetArrowHeadAtIndex(long index);
	long GetTriangleCount();
	VARIANT GetTriangleAtIndex(long index);
	double IGetTriangleAtIndex(long index);
	long GetPolylineCount();
	long GetPolylineSizeAtIndex(long index);
	VARIANT GetPolylineAtIndex(long index);
	double IGetPolylineAtIndex(long index);
	long GetEllipseCount();
	VARIANT GetEllipseAtIndex(long index);
	double IGetEllipseAtIndex(long index);
	double GetTextLineSpacingAtIndex(long index);
	VARIANT GetLineAtIndex2(long index);
	double IGetLineAtIndex2(long index);
	VARIANT GetArcAtIndex2(long index);
	double IGetArcAtIndex2(long index);
	long GetPolylineSizeAtIndex2(long index);
	VARIANT GetPolylineAtIndex2(long index);
	double IGetPolylineAtIndex2(long index);
	VARIANT GetEllipseAtIndex2(long index);
	double IGetEllipseAtIndex2(long index);
	long GetParabolaCount();
	VARIANT GetParabolaAtIndex(long index);
	double IGetParabolaAtIndex(long index);
	long GetPolygonCount();
	long GetPolygonSizeAtIndex(long index);
	VARIANT GetPolygonAtIndex(long index);
	double IGetPolygonAtIndex(long index);
};
/////////////////////////////////////////////////////////////////////////////
// ITextFormat wrapper class

class ITextFormat : public COleDispatchDriver
{
public:
	ITextFormat() {}		// Calls COleDispatchDriver default constructor
	ITextFormat(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ITextFormat(const ITextFormat& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetAllValues();
	double IGetAllValues(long count);
	BOOL GetItalic();
	void SetItalic(BOOL bNewValue);
	BOOL GetUnderline();
	void SetUnderline(BOOL bNewValue);
	BOOL GetStrikeout();
	void SetStrikeout(BOOL bNewValue);
	BOOL GetBold();
	void SetBold(BOOL bNewValue);
	double GetEscapement();
	void SetEscapement(double newValue);
	double GetLineSpacing();
	void SetLineSpacing(double newValue);
	double GetCharHeight();
	void SetCharHeight(double newValue);
	long GetCharHeightInPts();
	void SetCharHeightInPts(long nNewValue);
	BOOL IsHeightSpecifiedInPts();
	CString GetTypeFaceName();
	void SetTypeFaceName(LPCTSTR lpszNewValue);
	double GetWidthFactor();
	void SetWidthFactor(double newValue);
	double GetObliqueAngle();
	void SetObliqueAngle(double newValue);
	double GetLineLength();
	void SetLineLength(double newValue);
	BOOL GetVertical();
	void SetVertical(BOOL bNewValue);
	BOOL GetBackWards();
	void SetBackWards(BOOL bNewValue);
	BOOL GetUpsideDown();
	void SetUpsideDown(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// IAnnotation wrapper class

class IAnnotation : public COleDispatchDriver
{
public:
	IAnnotation() {}		// Calls COleDispatchDriver default constructor
	IAnnotation(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IAnnotation(const IAnnotation& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	LPDISPATCH GetDisplayData();
	LPDISPATCH IGetDisplayData();
	long GetType();
	LPDISPATCH GetSpecificAnnotation();
	LPUNKNOWN IGetSpecificAnnotation();
	VARIANT GetPosition();
	double IGetPosition();
	BOOL SetPosition(double x, double y, double z);
	long GetLeaderCount();
	VARIANT GetLeaderPointsAtIndex(long index);
	double IGetLeaderPointsAtIndex(long index, long pointCount);
	long GetArrowHeadStyleAtIndex(long index);
	long SetArrowHeadStyleAtIndex(long index, long arrowHeadStyle);
	BOOL GetLeader();
	BOOL GetBentLeader();
	long GetLeaderSide();
	BOOL GetSmartArrowHeadStyle();
	long SetLeader(BOOL leader, long leaderSide, BOOL smartArrowHeadStyle, BOOL bentLeader);
	CString GetName();
	BOOL SetName(LPCTSTR nameIn);
	VARIANT GetVisualProperties();
	long IGetVisualProperties();
	CString GetLayer();
	void SetLayer(LPCTSTR lpszNewValue);
	long GetLayerOverride();
	void SetLayerOverride(long nNewValue);
	long GetColor();
	void SetColor(long nNewValue);
	long GetStyle();
	void SetStyle(long nNewValue);
	long GetWidth();
	void SetWidth(long nNewValue);
	long IGetAttachedEntityCount();
	VARIANT GetAttachedEntities();
	LPUNKNOWN IGetAttachedEntities();
	VARIANT GetAttachedEntityTypes();
	long IGetAttachedEntityTypes();
	LPDISPATCH GetNext2();
	LPDISPATCH IGetNext2();
	long GetVisible();
	void SetVisible(long nNewValue);
	long GetTextFormatCount();
	BOOL GetUseDocTextFormat(long index);
	LPDISPATCH GetTextFormat(long index);
	LPDISPATCH IGetTextFormat(long index);
	BOOL SetTextFormat(long index, BOOL useDoc, LPDISPATCH textFormat);
	BOOL ISetTextFormat(long index, BOOL useDoc, LPDISPATCH textFormat);
	BOOL GetLeaderPerpendicular();
	BOOL GetLeaderAllAround();
	long SetLeader2(BOOL leader, long leaderSide, BOOL smartArrowHeadStyle, BOOL bentLeader, BOOL perpendicular, BOOL allAround);
	BOOL Select(BOOL appendFlag);
	BOOL SelectByMark(BOOL appendFlag, long mark);
	BOOL DeSelect();
};
/////////////////////////////////////////////////////////////////////////////
// IPartDoc wrapper class

class IPartDoc : public COleDispatchDriver
{
public:
	IPartDoc() {}		// Calls COleDispatchDriver default constructor
	IPartDoc(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IPartDoc(const IPartDoc& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	CString GetMaterialUserName();
	void SetMaterialUserName(LPCTSTR lpszNewValue);
	CString GetMaterialIdName();
	void SetMaterialIdName(LPCTSTR lpszNewValue);
	VARIANT GetMaterialPropertyValues();
	void SetMaterialPropertyValues(const VARIANT& newValue);
	LPDISPATCH FirstFeature();
	LPDISPATCH IFirstFeature();
	LPDISPATCH FeatureByName(LPCTSTR Name);
	LPDISPATCH IFeatureByName(LPCTSTR Name);
	LPDISPATCH FeatureById(long Id);
	LPDISPATCH IFeatureById(long Id);
	LPDISPATCH Body();
	LPDISPATCH IBodyObject();
	void FeatureExtrusion(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2);
	void FeatureRevolve(double angle, BOOL reverseDir, double angle2, long revType);
	void FeatureRevolveCut(double angle, BOOL reverseDir, double angle2, long revType);
	VARIANT GetTessTriangles(BOOL noConversion);
	VARIANT GetTessNorms();
	LPDISPATCH GetProcessedBody();
	LPDISPATCH IGetProcessedBody();
	VARIANT GetPartBox(BOOL noConversion);
	LPDISPATCH CreateNewBody();
	LPDISPATCH ICreateNewBody();
	void EditRebuild();
	void ForceRebuild();
	void EditUnsuppressDependent();
	void EditRollforward();
	void InsertStockTurned();
	void EditUnsuppress();
	void MakeSection();
	void Dumpfacets();
	void FeatureStock();
	void EditRollback();
	void EditSuppress();
	long AddPropertyExtension(const VARIANT& PropertyExtension);
	VARIANT GetPropertyExtension(long Id);
	void MirrorFeature();
	void ResetPropertyExtension();
	LPDISPATCH GetProcessedBodyWithSelFace();
	LPDISPATCH IGetProcessedBodyWithSelFace();
	VARIANT GetTessTriStrips(BOOL noConversion);
	VARIANT GetTessTriStripNorms();
	void MirrorPart();
	float IGetTessTriangles(BOOL noConversion);
	float IGetTessNorms();
	long GetTessTriangleCount();
	double IGetPartBox(BOOL noConversion);
	float IGetTessTriStrips(BOOL noConversion);
	float IGetTessTriStripNorms();
	long GetTessTriStripSize();
	LPDISPATCH GetEntityByName(LPCTSTR Name, long entityType);
	LPDISPATCH IGetEntityByName(LPCTSTR Name, long entityType);
	CString GetEntityName(LPDISPATCH entity);
	CString IGetEntityName(LPDISPATCH entity);
	BOOL SetEntityName(LPDISPATCH entity, LPCTSTR StringValue);
	BOOL ISetEntityName(LPDISPATCH entity, LPCTSTR StringValue);
	BOOL ReorderFeature(LPCTSTR featureToMove, LPCTSTR moveAfterFeature);
	LPUNKNOWN EnumRelatedBodies();
	LPDISPATCH GetSectionedBody(LPDISPATCH viewIn);
	LPDISPATCH IGetSectionedBody(LPDISPATCH viewIn);
	LPUNKNOWN EnumRelatedSectionedBodies(LPDISPATCH viewIn);
	double GetIMaterialPropertyValues();
	void SetIMaterialPropertyValues(double* newValue);
	void FeatureRevolveThin(double angle, BOOL reverseDir, double angle2, long revType, double thickness1, double thickness2, long reverseThinDir);
	void FeatureRevolveThinCut(double angle, BOOL reverseDir, double angle2, long revType, double thickness1, double thickness2, long reverseThinDir);
	void FeatureExtrusionThin(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, double thk1, double thk2, 
		double endThk, long revThinDir, long capEnds, BOOL addBends, double bendRad);
	BOOL InsertBends(double radius, LPCTSTR useBendTable, double useKfactor, double useBendAllowance, BOOL useAutoRelief, double offsetRatio);
	LPDISPATCH CreateFeatureFromBody(LPDISPATCH Body);
	LPDISPATCH ICreateFeatureFromBody(LPDISPATCH Body);
	LPDISPATCH CreateFeatureFromBody2(LPDISPATCH Body, BOOL makeRefSurface);
	LPDISPATCH ICreateFeatureFromBody2(LPDISPATCH Body, BOOL makeRefSurface);
	long ImportDiagnosis(BOOL closeAllGaps, BOOL removeFaces, BOOL fixFaces, long options);
	BOOL DeleteEntityName(LPDISPATCH entity);
	BOOL IDeleteEntityName(LPDISPATCH entity);
	VARIANT GetTessTriStripEdges();
	long IGetTessTriStripEdges();
	long IGetTessTriStripEdgeSize();
	LPUNKNOWN EnumBodies(long bodyType);
	VARIANT GetBodies(long bodyType);
	VARIANT GetRelatedBodies();
	VARIANT GetRelatedSectionedBodies(LPDISPATCH viewIn);
	BOOL InsertBends2(double radius, LPCTSTR useBendTable, double useKfactor, double useBendAllowance, BOOL useAutoRelief, double offsetRatio, BOOL doFlatten);
	LPDISPATCH CreateFeatureFromBody3(LPDISPATCH Body, BOOL makeRefSurface, long options);
	LPDISPATCH ICreateFeatureFromBody3(LPDISPATCH Body, BOOL makeRefSurface, long options);
	LPDISPATCH GetMateReferenceEntity();
	void FeatureExtrusion2(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, BOOL merge);
	void FeatureExtrusionThin2(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, BOOL merge, double thk1, 
		double thk2, double endThk, long revThinDir, long capEnds, BOOL addBends, double bendRad);
	VARIANT CreateSurfaceFeatureFromBody(LPDISPATCH Body, long options);
	long ICreateSurfaceFeatureFromBodyCount(LPDISPATCH Body, long options);
	LPDISPATCH ICreateSurfaceFeatureFromBody();
};
/////////////////////////////////////////////////////////////////////////////
// IEntity wrapper class

class IEntity : public COleDispatchDriver
{
public:
	IEntity() {}		// Calls COleDispatchDriver default constructor
	IEntity(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IEntity(const IEntity& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long CreateStringAttributeDefinition(LPCTSTR identifierString);
	BOOL CreateStringAttribute(long definitionTag, LPCTSTR StringValue);
	CString FindStringAttribute(long definitionTag);
	BOOL RemoveStringAttribute(long definitionTag);
	LPDISPATCH FindAttribute(LPDISPATCH attributeDef, long whichOne);
	LPDISPATCH IFindAttribute(LPDISPATCH attributeDef, long whichOne);
	BOOL Select(BOOL appendFlag);
	long GetType();
	LPDISPATCH GetComponent();
	LPDISPATCH IGetComponent();
	BOOL SelectByMark(BOOL appendFlag, long mark);
	CString GetModelName();
	void SetModelName(LPCTSTR lpszNewValue);
	void DeleteModelName();
	BOOL DeSelect();
};
/////////////////////////////////////////////////////////////////////////////
// IAttributeDef wrapper class

class IAttributeDef : public COleDispatchDriver
{
public:
	IAttributeDef() {}		// Calls COleDispatchDriver default constructor
	IAttributeDef(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IAttributeDef(const IAttributeDef& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH CreateInstance(LPDISPATCH ownerDoc, LPDISPATCH ownerEntity, LPCTSTR nameIn);
	LPDISPATCH ICreateInstance(LPDISPATCH ownerDoc, LPDISPATCH ownerEntity, LPCTSTR nameIn);
	BOOL AddParameter(LPCTSTR nameIn, long type, double defaultValue, long options);
	BOOL SetOption(long whichOption, long optionValue);
	long GetOption(long whichOption);
	BOOL AddCallback(long whichCallback, LPCTSTR CallbackFcnAndModule, long whichOption);
	BOOL Register();
	LPDISPATCH CreateInstance2(LPDISPATCH ownerDoc, LPDISPATCH ownerEntity, LPCTSTR nameIn, long options);
	LPDISPATCH ICreateInstance2(LPDISPATCH ownerDoc, LPDISPATCH ownerEntity, LPCTSTR nameIn, long options);
	LPDISPATCH CreateInstance3(LPDISPATCH ownerDoc, LPDISPATCH ownerComp, LPDISPATCH ownerEntity, LPCTSTR nameIn, long options, long configurationOption);
	LPDISPATCH ICreateInstance3(LPDISPATCH ownerDoc, LPDISPATCH ownerComp, LPDISPATCH ownerEntity, LPCTSTR nameIn, long options, long configurationOption);
};
/////////////////////////////////////////////////////////////////////////////
// IAttribute wrapper class

class IAttribute : public COleDispatchDriver
{
public:
	IAttribute() {}		// Calls COleDispatchDriver default constructor
	IAttribute(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IAttribute(const IAttribute& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetParameter(LPCTSTR nameIn);
	LPDISPATCH IGetParameter(LPCTSTR nameIn);
	LPDISPATCH GetEntity();
	LPDISPATCH IGetEntity();
	LPDISPATCH GetDefinition();
	LPDISPATCH IGetDefinition();
	CString GetName();
	BOOL GetEntityState(long whichState);
	LPDISPATCH GetComponent();
	LPDISPATCH IGetComponent();
};
/////////////////////////////////////////////////////////////////////////////
// IParameter wrapper class

class IParameter : public COleDispatchDriver
{
public:
	IParameter() {}		// Calls COleDispatchDriver default constructor
	IParameter(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IParameter(const IParameter& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	double GetDoubleValue();
	BOOL SetDoubleValue(double Value);
	long GetType();
	CString GetName();
	long GetOption(long whichOption);
	BOOL SetOption(long whichOption, long optionValue);
	CString GetStringValue();
	BOOL SetStringValue(LPCTSTR StringValue);
	void GetVector(double* x, double* y, double* z);
	BOOL SetVector(double x, double y, double z);
	VARIANT GetVectorVB();
	BOOL SetDoubleValue2(double Value, long configurationOption, LPCTSTR configurationName);
	BOOL SetStringValue2(LPCTSTR StringValue, long configurationOption, LPCTSTR configurationName);
	BOOL SetVector2(double x, double y, double z, long configurationOption, LPCTSTR configurationName);
};
/////////////////////////////////////////////////////////////////////////////
// IModelView wrapper class

class IModelView : public COleDispatchDriver
{
public:
	IModelView() {}		// Calls COleDispatchDriver default constructor
	IModelView(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IModelView(const IModelView& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetXform();
	void SetXform(const VARIANT& newValue);
	VARIANT GetOrientation();
	void SetOrientation(const VARIANT& newValue);
	VARIANT GetTranslation();
	void SetTranslation(const VARIANT& newValue);
	double GetScale();
	void SetScale(double newValue);
	double GetIXform();
	void SetIXform(double* newValue);
	double GetIOrientation();
	void SetIOrientation(double* newValue);
	double GetITranslation();
	void SetITranslation(double* newValue);
	long GetViewHWnd();
	void StartDynamics();
	void StopDynamics();
	void AddPerspective();
	void RemovePerspective();
	BOOL HasPerspective();
	VARIANT GetEyePoint();
	void IGetEyePoint(double* eyept);
	double GetViewPlaneDistance();
	void InitializeShading();
	VARIANT GetOrientation2();
	void SetOrientation2(const VARIANT& newValue);
	VARIANT GetTranslation2();
	void SetTranslation2(const VARIANT& newValue);
	double GetIOrientation2();
	void SetIOrientation2(double* newValue);
	double GetITranslation2();
	void SetITranslation2(double* newValue);
	BOOL GetDisplayState(long displayType);
	long GetViewDIB();
	void ZoomByFactor(double factor);
	void TranslateBy(double x, double y);
	void RotateAboutCenter(double xAngle, double yAngle);
	void RotateAboutPoint(double xAngle, double yAngle, double Ptx, double Pty, double Ptz);
	void RotateAboutAxis(double angle, double Ptx, double Pty, double Ptz, double AxisVecX, double AxisVecY, double AxisVecZ);
	BOOL SetEyePoint(const VARIANT& eyept);
	BOOL ISetEyePoint(double* eyept);
	BOOL SetStereoSeparation(const VARIANT& dfSeparation);
	BOOL ISetStereoSeparation(double* dfSeparation);
	VARIANT GetStereoSeparation();
	double IGetStereoSeparation();
	void GetDIBHeader(long ldib);
	void GetStripsOfDIB(long ldib, long nScanLinesPerStrip, long stripIndex);
	void SetFrameLeft(long nNewValue);
	void SetFrameTop(long nNewValue);
	void SetFrameWidth(long nNewValue);
	void SetFrameHeight(long nNewValue);
	void SetFrameState(long nNewValue);
	double GetObjectSizesAway();
	void SetObjectSizesAway(double newValue);
	long GetDynamicMode();
	void DrawHighlightedItems();
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	double GetScale2();
	void SetScale2(double newValue);
	long GetHlrQuality();
	void SetHlrQuality(long nNewValue);
	BOOL GetXorHighlight();
	void SetXorHighlight(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// ITessellation wrapper class

class ITessellation : public COleDispatchDriver
{
public:
	ITessellation() {}		// Calls COleDispatchDriver default constructor
	ITessellation(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ITessellation(const ITessellation& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	double GetMaxFacetWidth();
	void SetMaxFacetWidth(double newValue);
	double GetCurveChordTolerance();
	void SetCurveChordTolerance(double newValue);
	double GetCurveChordAngleTolerance();
	void SetCurveChordAngleTolerance(double newValue);
	double GetSurfacePlaneTolerance();
	void SetSurfacePlaneTolerance(double newValue);
	double GetSurfacePlaneAngleTolerance();
	void SetSurfacePlaneAngleTolerance(double newValue);
	BOOL GetNeedVertexNormal();
	void SetNeedVertexNormal(BOOL bNewValue);
	BOOL GetNeedVertexParams();
	void SetNeedVertexParams(BOOL bNewValue);
	BOOL GetNeedFaceFacetMap();
	void SetNeedFaceFacetMap(BOOL bNewValue);
	BOOL GetNeedEdgeFinMap();
	void SetNeedEdgeFinMap(BOOL bNewValue);
	BOOL GetNeedErrorList();
	void SetNeedErrorList(BOOL bNewValue);
	BOOL Tessellate();
	long GetFacetCount();
	long GetFinCount();
	long GetVertexCount();
	VARIANT GetFacetFins(long facetId);
	long IGetFacetFinsCount(long facetId);
	long IGetFacetFins(long facetId);
	long GetFinCoFin(long finId);
	VARIANT GetFinVertices(long finId);
	long IGetFinVertices(long finId);
	VARIANT GetVertexPoint(long vertexId);
	double IGetVertexPoint(long vertexId);
	VARIANT GetVertexNormal(long vertexId);
	double IGetVertexNormal(long vertexId);
	VARIANT GetVertexParams(long vertexId);
	double IGetVertexParams(long vertexId);
	VARIANT GetFaceFacets(LPDISPATCH facedisp);
	long IGetFaceFacetsCount(LPDISPATCH faceObj);
	long IGetFaceFacets(LPDISPATCH faceObj);
	LPDISPATCH GetFacetFace(long facetId);
	LPDISPATCH IGetFacetFace(long facetId);
	VARIANT GetEdgeFins(LPDISPATCH edgeDisp);
	long IGetEdgeFinsCount(LPDISPATCH edgeObj);
	long IGetEdgeFins(LPDISPATCH edgeObj);
	LPDISPATCH GetFinEdge(long finId);
	LPDISPATCH IGetFinEdge(long finId);
	void GetErrorList(VARIANT* faceErrArray, VARIANT* facetErrArray, VARIANT* vertexPointErrArray, VARIANT* vertexNormalErrArray, VARIANT* vertexParamsErrArray);
	void IGetErrorListCount(long* faceErrArrayCount, long* facetErrArrayCount, long* vertexPointErrArrayCount, long* vertexNormalErrArrayCount, long* vertexParamsErrArrayCount);
	void IGetErrorList(LPDISPATCH* faceErrArray, long* facetErrArray, long* vertexPointErrArray, long* vertexNormalErrArray, long* vertexParamsErrArray);
};
/////////////////////////////////////////////////////////////////////////////
// ISketch wrapper class

class ISketch : public COleDispatchDriver
{
public:
	ISketch() {}		// Calls COleDispatchDriver default constructor
	ISketch(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketch(const ISketch& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetModelToSketchXform();
	void SetModelToSketchXform(const VARIANT& newValue);
	VARIANT GetLines();
	double IGetLines();
	long GetLineCount();
	VARIANT GetArcs();
	double IGetArcs();
	long GetArcCount();
	VARIANT GetPolylines();
	double IGetPolylines();
	long GetPolylineCount(long* pointCount);
	VARIANT GetSplines();
	double IGetSplines();
	long GetSplineCount(long* pointCount);
	VARIANT GetEllipses();
	double IGetEllipses();
	long GetEllipseCount();
	double GetIModelToSketchXform();
	VARIANT GetUserPoints();
	double IGetUserPoints();
	long GetUserPointsCount();
	VARIANT GetSplinesInterpolate();
	double IGetSplinesInterpolate();
	long GetSplineInterpolateCount(long* pointCount);
	BOOL GetAutomaticSolve();
	BOOL SetAutomaticSolve(BOOL solveFlag);
	long ConstrainAll();
	VARIANT GetSplineParams();
	double IGetSplineParams();
	long GetSplineParamsCount(long* size);
	VARIANT GetParabolas();
	double IGetParabolas();
	long GetParabolaCount();
	VARIANT GetEllipses2();
	double IGetEllipses2();
	VARIANT GetSketchPoints();
	LPUNKNOWN IEnumSketchPoints();
	VARIANT GetSketchSegments();
	LPUNKNOWN IEnumSketchSegments();
	BOOL Is3D();
	long GetSketchPointsCount();
	VARIANT GetSketchHatches();
	LPUNKNOWN IEnumSketchHatches();
	VARIANT GetSplineParams2();
	double IGetSplineParams2();
	long GetSplineParamsCount2(long* size);
	VARIANT GetSketchTextSegments();
	LPUNKNOWN IEnumSketchTextSegments();
	long GetConstrainedStatus();
	BOOL SetEntityCount(long entityType, long entityCount);
	long CheckFeatureUse(long featureType, long* openCount, long* closedCount);
	BOOL MergePoints(double distance);
};
/////////////////////////////////////////////////////////////////////////////
// ISketchPoint wrapper class

class ISketchPoint : public COleDispatchDriver
{
public:
	ISketchPoint() {}		// Calls COleDispatchDriver default constructor
	ISketchPoint(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchPoint(const ISketchPoint& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	double GetX();
	double GetY();
	double GetZ();
	double GetCoords();
	VARIANT GetId();
	long IGetID();
	BOOL Select(BOOL appendFlag);
	BOOL SelectByMark(BOOL appendFlag, long mark);
	BOOL DeSelect();
	CString GetLayer();
	void SetLayer(LPCTSTR lpszNewValue);
	long GetLayerOverride();
	void SetLayerOverride(long nNewValue);
	long GetColor();
	void SetColor(long nNewValue);
	BOOL SetCoords(double xx, double yy, double zz);
	VARIANT GetFramePointTangent(BOOL* status);
	double IGetFramePointTangent(BOOL* status);
	BOOL SetFramePointTangent(const VARIANT& toVector);
	BOOL ISetFramePointTangent(double* toVector);
	LPDISPATCH GetSketch();
};
/////////////////////////////////////////////////////////////////////////////
// ISketchSegment wrapper class

class ISketchSegment : public COleDispatchDriver
{
public:
	ISketchSegment() {}		// Calls COleDispatchDriver default constructor
	ISketchSegment(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchSegment(const ISketchSegment& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	CString GetLayer();
	void SetLayer(LPCTSTR lpszNewValue);
	long GetLayerOverride();
	void SetLayerOverride(long nNewValue);
	long GetColor();
	void SetColor(long nNewValue);
	long GetStyle();
	void SetStyle(long nNewValue);
	long GetWidth();
	void SetWidth(long nNewValue);
	long GetType();
	VARIANT GetId();
	long IGetID();
	BOOL Select(BOOL appendFlag);
	BOOL SelectByMark(BOOL appendFlag, long mark);
	BOOL DeSelect();
	BOOL GetConstructionGeometry();
	void SetConstructionGeometry(BOOL bNewValue);
	LPDISPATCH GetCurve();
	LPDISPATCH IGetCurve();
	LPDISPATCH GetSketch();
	VARIANT GetConstraints();
	long IGetConstraintsCount();
	CString IGetConstraints();
};
/////////////////////////////////////////////////////////////////////////////
// ISketchHatch wrapper class

class ISketchHatch : public COleDispatchDriver
{
public:
	ISketchHatch() {}		// Calls COleDispatchDriver default constructor
	ISketchHatch(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchHatch(const ISketchHatch& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetId();
	long IGetID();
	BOOL Select(BOOL appendFlag);
	BOOL SelectByMark(BOOL appendFlag, long mark);
	BOOL DeSelect();
	CString GetLayer();
	void SetLayer(LPCTSTR lpszNewValue);
	long GetLayerOverride();
	void SetLayerOverride(long nNewValue);
	long GetColor();
	void SetColor(long nNewValue);
	CString GetPattern();
	void SetPattern(LPCTSTR lpszNewValue);
	double GetScale();
	void SetScale(double newValue);
	double GetAngle();
	void SetAngle(double newValue);
	LPDISPATCH GetFace();
	LPDISPATCH IGetFace();
	LPDISPATCH GetSketch();
	double GetScale2();
	void SetScale2(double newValue);
};
/////////////////////////////////////////////////////////////////////////////
// IMidSurface wrapper class

class IMidSurface : public COleDispatchDriver
{
public:
	IMidSurface() {}		// Calls COleDispatchDriver default constructor
	IMidSurface(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMidSurface(const IMidSurface& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetFacePairCount();
	LPDISPATCH GetFirstFacePair(double* thickness, LPDISPATCH* partnerFaceDisp);
	LPDISPATCH IGetFirstFacePair(double* thickness, LPDISPATCH* partnerFaceDisp);
	LPDISPATCH GetNextFacePair(double* thickness, LPDISPATCH* partnerFaceDisp);
	LPDISPATCH IGetNextFacePair(double* thickness, LPDISPATCH* partnerFaceDisp);
	LPDISPATCH GetFirstNeutralSheet();
	LPDISPATCH IGetFirstNeutralSheet();
	LPDISPATCH GetNextNeutralSheet();
	LPDISPATCH IGetNextNeutralSheet();
	long GetNeutralSheetCount();
	long GetFaceCount();
	LPDISPATCH GetFirstFace(LPDISPATCH* fromFace1Disp, LPDISPATCH* fromFace2Disp, double* thickness);
	LPDISPATCH IGetFirstFace(LPDISPATCH* fromFace1Disp, LPDISPATCH* fromFace2Disp, double* thickness);
	LPDISPATCH GetNextFace(LPDISPATCH* fromFace1Disp, LPDISPATCH* fromFace2Disp, double* thickness);
	LPDISPATCH IGetNextFace(LPDISPATCH* fromFace1Disp, LPDISPATCH* fromFace2Disp, double* thickness);
	LPDISPATCH EdgeGetFace(LPDISPATCH edgeInDisp);
	LPDISPATCH IEdgeGetFace(LPDISPATCH edgeInDisp);
	// method 'IGetFirstFaceArray' not emitted because of invalid return type or parameter type
	// method 'IGetNextFaceArray' not emitted because of invalid return type or parameter type
	VARIANT GetFirstFaceArray(double* thickness);
	VARIANT GetNextFaceArray(double* thickness);
};
/////////////////////////////////////////////////////////////////////////////
// IFeatMgrView wrapper class

class IFeatMgrView : public COleDispatchDriver
{
public:
	IFeatMgrView() {}		// Calls COleDispatchDriver default constructor
	IFeatMgrView(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IFeatMgrView(const IFeatMgrView& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetFeatMgrViewWnd();
};
/////////////////////////////////////////////////////////////////////////////
// IConfiguration wrapper class

class IConfiguration : public COleDispatchDriver
{
public:
	IConfiguration() {}		// Calls COleDispatchDriver default constructor
	IConfiguration(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IConfiguration(const IConfiguration& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetRootComponent();
	LPDISPATCH IGetRootComponent();
	CString GetName();
	void SetName(LPCTSTR lpszNewValue);
	CString GetComment();
	void SetComment(LPCTSTR lpszNewValue);
	CString GetAlternateName();
	void SetAlternateName(LPCTSTR lpszNewValue);
	BOOL GetUseAlternateNameInBOM();
	void SetUseAlternateNameInBOM(BOOL bNewValue);
	BOOL GetSuppressNewFeatures();
	void SetSuppressNewFeatures(BOOL bNewValue);
	BOOL GetHideNewComponentModels();
	void SetHideNewComponentModels(BOOL bNewValue);
	BOOL GetSuppressNewComponentModels();
	void SetSuppressNewComponentModels(BOOL bNewValue);
	BOOL GetShowChildComponentsInBOM();
	void SetShowChildComponentsInBOM(BOOL bNewValue);
	long GetNumberOfExplodeSteps();
	LPDISPATCH GetExplodeStep(long explodeStepIndex);
	LPDISPATCH IGetExplodeStep(long explodeStepIndex);
	LPDISPATCH AddExplodeStep(double explDist, BOOL reverseDir, BOOL rigidSubassembly, BOOL explodeRelated);
	LPDISPATCH IAddExplodeStep(double explDist, BOOL reverseDir, BOOL rigidSubassembly, BOOL explodeRelated);
	BOOL DeleteExplodeStep(LPCTSTR explodeStepName);
	CString GetStreamName();
};
/////////////////////////////////////////////////////////////////////////////
// IExplodeStep wrapper class

class IExplodeStep : public COleDispatchDriver
{
public:
	IExplodeStep() {}		// Calls COleDispatchDriver default constructor
	IExplodeStep(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IExplodeStep(const IExplodeStep& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetNumOfComponents();
	CString GetComponentName(long index);
	VARIANT GetComponentXform();
	double IGetComponentXform();
	LPDISPATCH GetComponent(long index);
	LPDISPATCH IGetComponent(long index);
	CString GetName();
	void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// IReferenceCurve wrapper class

class IReferenceCurve : public COleDispatchDriver
{
public:
	IReferenceCurve() {}		// Calls COleDispatchDriver default constructor
	IReferenceCurve(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IReferenceCurve(const IReferenceCurve& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetSegmentCount();
	LPDISPATCH GetFirstSegment();
	LPDISPATCH IGetFirstSegment();
	LPDISPATCH GetNextSegment();
	LPDISPATCH IGetNextSegment();
	BOOL SetColor(long colorIn);
	BOOL SetVisible(BOOL Visible);
};
/////////////////////////////////////////////////////////////////////////////
// IColorTable wrapper class

class IColorTable : public COleDispatchDriver
{
public:
	IColorTable() {}		// Calls COleDispatchDriver default constructor
	IColorTable(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IColorTable(const IColorTable& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetCount();
	CString GetNameAtIndex(long index);
	long GetColorRefAtIndex(long index);
	long GetStandardCount();
	void SetColorRefAtIndex(long index, long colorRef, long applyTo);
	long GetBasicColorCount();
	VARIANT GetBasicColors();
	long IGetBasicColors(long ColorCount);
	long GetCustomColorCount();
	VARIANT GetCustomColors();
	long IGetCustomColors(long ColorCount);
	BOOL SetCustomColor(long index, long colorRef);
};
/////////////////////////////////////////////////////////////////////////////
// IDetailingDefaults wrapper class

class IDetailingDefaults : public COleDispatchDriver
{
public:
	IDetailingDefaults() {}		// Calls COleDispatchDriver default constructor
	IDetailingDefaults(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDetailingDefaults(const IDetailingDefaults& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetAllValues();
	double IGetAllValues(long count);
	LPDISPATCH GetTextFormat();
	LPDISPATCH IGetTextFormat();
	void SetTextFormat(LPDISPATCH textFormat);
	void ISetTextFormat(LPDISPATCH textFormat);
	LPDISPATCH GetDimensionTextFormat();
	LPDISPATCH IGetDimensionTextFormat();
	void SetDimensionTextFormat(LPDISPATCH textFormat);
	void ISetDimensionTextFormat(LPDISPATCH textFormat);
	LPDISPATCH GetSectionTextFormat();
	LPDISPATCH IGetSectionTextFormat();
	void SetSectionTextFormat(LPDISPATCH textFormat);
	void ISetSectionTextFormat(LPDISPATCH textFormat);
	LPDISPATCH GetDetailTextFormat();
	LPDISPATCH IGetDetailTextFormat();
	void SetDetailTextFormat(LPDISPATCH textFormat);
	void ISetDetailTextFormat(LPDISPATCH textFormat);
};
/////////////////////////////////////////////////////////////////////////////
// ILayerMgr wrapper class

class ILayerMgr : public COleDispatchDriver
{
public:
	ILayerMgr() {}		// Calls COleDispatchDriver default constructor
	ILayerMgr(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ILayerMgr(const ILayerMgr& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long AddLayer(LPCTSTR nameIn, LPCTSTR descIn, long colorIn, long styleIn, long widthIn);
	long SetCurrentLayer(LPCTSTR nameIn);
	CString GetCurrentLayer();
	LPDISPATCH GetLayer(LPCTSTR nameIn);
	LPDISPATCH IGetLayer(LPCTSTR nameIn);
	long GetCount();
	VARIANT GetLayerList();
	CString IGetLayerList();
	LPDISPATCH GetLayerById(short layerId);
	LPDISPATCH IGetLayerById(short layerId);
	BOOL DeleteLayer(LPCTSTR Name);
};
/////////////////////////////////////////////////////////////////////////////
// ILayer wrapper class

class ILayer : public COleDispatchDriver
{
public:
	ILayer() {}		// Calls COleDispatchDriver default constructor
	ILayer(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ILayer(const ILayer& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	CString GetName();
	long GetColor();
	long GetStyle();
	long GetWidth();
	BOOL GetVisible();
	void SetVisible(BOOL bNewValue);
	CString GetDescription();
	void SetDescription(LPCTSTR lpszNewValue);
	long GetId();
};
/////////////////////////////////////////////////////////////////////////////
// IDesignTable wrapper class

class IDesignTable : public COleDispatchDriver
{
public:
	IDesignTable() {}		// Calls COleDispatchDriver default constructor
	IDesignTable(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDesignTable(const IDesignTable& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetRowCount();
	long GetColumnCount();
	CString GetHeaderText(long col);
	CString GetEntryText(long row, long col);
	BOOL Attach();
	void Detach();
	VARIANT GetEntryValue(long row, long col);
	CString GetTitle();
	long GetTotalRowCount();
	long GetTotalColumnCount();
	long GetVisibleRowCount();
	long GetVisibleColumnCount();
	void SetEntryText(long row, long col, LPCTSTR textIn);
	BOOL AddRow(const VARIANT& cellValues);
	BOOL UpdateModel();
};
/////////////////////////////////////////////////////////////////////////////
// IRefPlane wrapper class

class IRefPlane : public COleDispatchDriver
{
public:
	IRefPlane() {}		// Calls COleDispatchDriver default constructor
	IRefPlane(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IRefPlane(const IRefPlane& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetRefPlaneParams();
	double IGetRefPlaneParams();
};
/////////////////////////////////////////////////////////////////////////////
// IDatumTag wrapper class

class IDatumTag : public COleDispatchDriver
{
public:
	IDatumTag() {}		// Calls COleDispatchDriver default constructor
	IDatumTag(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDatumTag(const IDatumTag& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	long GetTextCount();
	CString GetTextAtIndex(long index);
	double GetTextHeightAtIndex(long index);
	VARIANT GetTextPositionAtIndex(long index);
	double IGetTextPositionAtIndex(long index);
	double GetTextAngleAtIndex(long index);
	long GetTextRefPositionAtIndex(long index);
	long GetTextInvertAtIndex(long index);
	long GetLineCount();
	VARIANT GetLineAtIndex(long index);
	double IGetLineAtIndex(long index);
	long GetArcCount();
	VARIANT GetArcAtIndex(long index);
	double IGetArcAtIndex(long index);
	long GetArrowHeadCount();
	VARIANT GetArrowHeadAtIndex(long index);
	double IGetArrowHeadAtIndex(long index);
	long GetTriangleCount();
	VARIANT GetTriangleAtIndex(long index);
	double IGetTriangleAtIndex(long index);
	LPDISPATCH GetAnnotation();
	LPDISPATCH IGetAnnotation();
	CString GetLabel();
	BOOL SetLabel(LPCTSTR label);
	BOOL GetFilledTriangle();
	void SetFilledTriangle(BOOL bNewValue);
	BOOL GetShoulder();
	void SetShoulder(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// IGtol wrapper class

class IGtol : public COleDispatchDriver
{
public:
	IGtol() {}		// Calls COleDispatchDriver default constructor
	IGtol(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IGtol(const IGtol& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	void SetFrameValues(short frameNumber, LPCTSTR tol1, LPCTSTR tol2, LPCTSTR datum1, LPCTSTR datum2, LPCTSTR datum3);
	void SetFrameSymbols(short frameNumber, short GCS, BOOL tolDia1, short tolMC1, BOOL tolDia2, short tolMC2, short datumMC1, short datumMC2, short datumMC3);
	void SetPTZHeight(LPCTSTR ptzHt, BOOL displayIn);
	void SetDatumIdentifier(LPCTSTR datumIdentifier);
	LPDISPATCH GetNextGTOL();
	LPDISPATCH IGetNextGTOL();
	VARIANT GetFrameValues(short frameNumber);
	VARIANT GetFrameSymbols(short frameNumber);
	CString GetPTZHeight();
	CString GetDatumIdentifier();
	double GetHeight();
	VARIANT GetAttachPos();
	VARIANT GetFontInfo();
	VARIANT GetLeaderInfo();
	BOOL IsAttached();
	BOOL HasExtraLeader();
	VARIANT GetTextPoint();
	VARIANT GetArrowHeadInfo();
	double IGetAttachPos();
	double IGetFontInfo();
	double IGetLeaderInfo(long* pointCount);
	double IGetTextPoint();
	double IGetArrowHeadInfo();
	CString GetSymName(short symIdx);
	CString GetSymDesc(short symIdx);
	VARIANT GetSymText(short symIdx);
	VARIANT GetSymEdgeCounts(short symIdx);
	VARIANT GetSymLines(short symIdx);
	VARIANT GetSymArcs(short symIdx);
	VARIANT GetSymCircles(short symIdx);
	VARIANT GetSymTextPoints(short symIdx);
	short IGetSymEdgeCounts(short symIdx);
	double IGetSymLines(short symIdx);
	double IGetSymArcs(short symIdx);
	double IGetSymCircles(short symIdx);
	double IGetSymTextPoints(short symIdx);
	long GetTextCount();
	CString GetTextAtIndex(long index);
	double GetTextHeightAtIndex(long index);
	VARIANT GetTextPositionAtIndex(long index);
	double IGetTextPositionAtIndex(long index);
	double GetTextAngleAtIndex(long index);
	long GetTextRefPositionAtIndex(long index);
	long GetTextInvertAtIndex(long index);
	long GetLineCount();
	VARIANT GetLineAtIndex(long index);
	double IGetLineAtIndex(long index);
	long GetArcCount();
	VARIANT GetArcAtIndex(long index);
	double IGetArcAtIndex(long index);
	long GetArrowHeadCount();
	VARIANT GetArrowHeadAtIndex(long index);
	double IGetArrowHeadAtIndex(long index);
	long GetTriangleCount();
	VARIANT GetTriangleAtIndex(long index);
	double IGetTriangleAtIndex(long index);
	CString IGetFrameValues(short frameNumber);
	short IGetFrameSymbols(short frameNumber);
	CString IGetSymText(short symIdx);
	void SetFrameSymbols2(short frameNumber, LPCTSTR GCS, BOOL tolDia1, LPCTSTR tolMC1, BOOL tolDia2, LPCTSTR tolMC2, LPCTSTR datumMC1, LPCTSTR datumMC2, LPCTSTR datumMC3);
	CString GetTextFont();
	long GetLeaderCount();
	VARIANT GetLeaderAtIndex(long index);
	double IGetLeaderAtIndex(long index, long* pointCount);
	void SetPosition(double x, double y, double z);
	BOOL GetUseDocTextFormat();
	LPDISPATCH GetTextFormat();
	LPDISPATCH IGetTextFormat();
	BOOL SetTextFormat(long textFormatType, LPDISPATCH textFormat);
	BOOL ISetTextFormat(long textFormatType, LPDISPATCH textFormat);
	BOOL GetCompositeFrame();
	void SetCompositeFrame(BOOL composite);
	BOOL GetBetweenTwoPoints();
	CString GetBetweenTwoPointsText(long index);
	void SetBetweenTwoPoints(BOOL between, LPCTSTR textFrom, LPCTSTR textTo);
	BOOL GetAllAround();
	long GetLeaderSide();
	void SetLeader(BOOL leader, long leaderSide, BOOL bentLeader, BOOL allAround);
	LPDISPATCH GetAnnotation();
	LPDISPATCH IGetAnnotation();
	VARIANT GetFrameSymbols2(short frameNumber);
	CString IGetFrameSymbols2(short frameNumber);
	VARIANT GetFrameDiameterSymbols(short frameNumber);
	BOOL IGetFrameDiameterSymbols(short frameNumber);
};
/////////////////////////////////////////////////////////////////////////////
// INote wrapper class

class INote : public COleDispatchDriver
{
public:
	INote() {}		// Calls COleDispatchDriver default constructor
	INote(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	INote(const INote& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	CString GetText();
	double GetHeight();
	VARIANT GetAttachPos();
	VARIANT GetFontInfo();
	VARIANT GetLeaderInfo();
	BOOL IsAttached();
	BOOL HasExtraLeader();
	BOOL HasBalloon();
	VARIANT GetBalloonInfo();
	VARIANT GetTextPoint();
	VARIANT GetArrowHeadInfo();
	double IGetAttachPos();
	double IGetFontInfo();
	double IGetLeaderInfo(long* pointCount);
	double IGetBalloonInfo();
	double IGetTextPoint();
	double IGetArrowHeadInfo();
	VARIANT GetUpperRight();
	double IGetUpperRight();
	VARIANT GetExtent();
	double IGetExtent();
	BOOL IsCompoundNote();
	BOOL AddText(LPCTSTR txt, double x, double y, double z);
	long GetTextCount();
	CString GetTextAtIndex(long index);
	double GetHeightAtIndex(long index);
	VARIANT GetTextOffsetAtIndex(long index);
	double IGetTextOffsetAtIndex(long index);
	void BeginSketchEdit();
	void EndSketchEdit();
	VARIANT GetExtentAtIndex(long index);
	double IGetExtentAtIndex(long index);
	void SetTextOffsetAtIndex(long index, double x, double y, double z);
	LPDISPATCH GetSketch();
	LPDISPATCH IGetSketch();
	BOOL SetTextAtIndex(long index, LPCTSTR txt);
	BOOL SetText(LPCTSTR txt);
	BOOL SetZeroLengthLeader(BOOL flag);
	void SetHeight(double heightIn);
	long GetHeightInPoints();
	void SetHeightInPoints(long heightIn);
	CString GetName();
	BOOL SetName(LPCTSTR nameIn);
	double GetTextHeightAtIndex(long index);
	VARIANT GetTextPositionAtIndex(long index);
	double IGetTextPositionAtIndex(long index);
	double GetTextAngleAtIndex(long index);
	long GetTextRefPositionAtIndex(long index);
	long GetTextInvertAtIndex(long index);
	long GetLineCount();
	VARIANT GetLineAtIndex(long index);
	double IGetLineAtIndex(long index);
	long GetArcCount();
	VARIANT GetArcAtIndex(long index);
	double IGetArcAtIndex(long index);
	long GetArrowHeadCount();
	VARIANT GetArrowHeadAtIndex(long index);
	double IGetArrowHeadAtIndex(long index);
	long GetTriangleCount();
	VARIANT GetTriangleAtIndex(long index);
	double IGetTriangleAtIndex(long index);
	CString GetTextFontAtIndex(long index);
	void SetTextPoint(double x, double y, double z);
	double GetTextLineSpacingAtIndex(long index);
	long GetLeaderCount();
	VARIANT GetLeaderAtIndex(long index);
	double IGetLeaderAtIndex(long index, long* pointCount);
	LPDISPATCH GetTextFormat();
	LPDISPATCH IGetTextFormat();
	BOOL SetTextFormat(long textFormatType, LPDISPATCH textFormat);
	BOOL ISetTextFormat(long textFormatType, LPDISPATCH textFormat);
	LPDISPATCH GetTextFormatAtIndex(long index);
	LPDISPATCH IGetTextFormatAtIndex(long index);
	void SetTextFormatAtIndex(long index, LPDISPATCH textFormat);
	void ISetTextFormatAtIndex(long index, LPDISPATCH textFormat);
	long GetTextJustification();
	void SetTextJustification(long justification);
	long GetTextJustificationAtIndex(long index);
	void SetTextJustificationAtIndex(long index, long justification);
	BOOL GetUseDocTextFormat();
	CString GetHyperlinkText();
	BOOL SetHyperlinkText(LPCTSTR text);
	LPDISPATCH GetAnnotation();
	LPDISPATCH IGetAnnotation();
	double GetAngle();
	void SetAngle(double newValue);
	long GetBalloonStyle();
	long GetBalloonSize();
	BOOL SetBalloon(long Style, long size);
	long IGetTextTokenCount();
	VARIANT GetTextTokens(VARIANT* positions, VARIANT* widths, VARIANT* heights);
	CString IGetTextTokens(double* positions, double* widths, double* heights);
	BOOL IsBomBalloon();
	long GetBomBalloonTextStyle(BOOL whichOne);
	CString GetBomBalloonText(BOOL whichOne);
	BOOL SetBomBalloonText(long upperTextStyle, LPCTSTR upperText, long lowerTextStyle, LPCTSTR lowerText);
};
/////////////////////////////////////////////////////////////////////////////
// IPropertyManagerPage wrapper class

class IPropertyManagerPage : public COleDispatchDriver
{
public:
	IPropertyManagerPage() {}		// Calls COleDispatchDriver default constructor
	IPropertyManagerPage(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IPropertyManagerPage(const IPropertyManagerPage& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long SetButtons(long ButtonTypes);
	long SetGroupRange(long FirstGroupId, long FirstCheckId, long GroupCount);
	long Show();
	long GetDialogWindow(long* status);
	BOOL GetGroupVisible(long GroupId, long* status);
	long SetGroupVisible(long GroupId, BOOL Visible);
	BOOL GetGroupExpanded(long GroupId, long* status);
	long SetGroupExpanded(long GroupId, BOOL Expanded);
};
/////////////////////////////////////////////////////////////////////////////
// IUserUnit wrapper class

class IUserUnit : public COleDispatchDriver
{
public:
	IUserUnit() {}		// Calls COleDispatchDriver default constructor
	IUserUnit(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IUserUnit(const IUserUnit& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetFractionBase();
	void SetFractionBase(long nNewValue);
	long GetFractionValue();
	void SetFractionValue(long nNewValue);
	BOOL GetRoundToFraction();
	void SetRoundToFraction(BOOL bNewValue);
	long GetSignificantDigits();
	void SetSignificantDigits(long nNewValue);
	BOOL GetDisplayLeadingZero();
	void SetDisplayLeadingZero(BOOL bNewValue);
	BOOL GetPadZero();
	void SetPadZero(BOOL bNewValue);
	CString GetSeparatorCharacter();
	void SetSeparatorCharacter(LPCTSTR lpszNewValue);
	double GetConversionFactor();
	BOOL IsMetric();
	CString GetFullUnitName(BOOL plural);
	CString ConvertToUserUnit(double valueIn, BOOL showUsernames, BOOL nameInEnglish);
	BOOL ConvertToSystemValue(LPCTSTR unitText, double* computedValue);
	double ConvertDoubleToSystemValue(double userValue);
	CString GetUnitsString(BOOL inEnglish);
	long GetUnitType();
	long GetSpecificUnitType();
	void SetSpecificUnitType(long nNewValue);
	double GetUserAngleTolerance();
};
/////////////////////////////////////////////////////////////////////////////
// IWeldSymbol wrapper class

class IWeldSymbol : public COleDispatchDriver
{
public:
	IWeldSymbol() {}		// Calls COleDispatchDriver default constructor
	IWeldSymbol(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IWeldSymbol(const IWeldSymbol& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	long GetTextCount();
	CString GetTextAtIndex(long index);
	double GetTextHeightAtIndex(long index);
	VARIANT GetTextPositionAtIndex(long index);
	double IGetTextPositionAtIndex(long index);
	double GetTextAngleAtIndex(long index);
	long GetTextRefPositionAtIndex(long index);
	long GetTextInvertAtIndex(long index);
	long GetLineCount();
	VARIANT GetLineAtIndex(long index);
	double IGetLineAtIndex(long index);
	long GetArcCount();
	VARIANT GetArcAtIndex(long index);
	double IGetArcAtIndex(long index);
	long GetArrowHeadCount();
	VARIANT GetArrowHeadAtIndex(long index);
	double IGetArrowHeadAtIndex(long index);
	long GetTriangleCount();
	VARIANT GetTriangleAtIndex(long index);
	double IGetTriangleAtIndex(long index);
	BOOL IsAttached();
	BOOL HasExtraLeader();
	long GetLeaderCount();
	VARIANT GetLeaderAtIndex(long index);
	double IGetLeaderAtIndex(long index, long* pointCount);
	double IGetArrowHeadInfo();
	VARIANT GetArrowHeadInfo();
	LPDISPATCH GetAnnotation();
	LPDISPATCH IGetAnnotation();
	CString GetText(long whichText);
	long GetContour(BOOL top);
	long GetSymmetric();
	BOOL GetPeripheral();
	long GetFieldWeld();
	BOOL GetProcess();
	BOOL GetProcessReference();
	BOOL GetStagger();
	BOOL SetText(BOOL top, LPCTSTR left, LPCTSTR symbol, LPCTSTR right, LPCTSTR stagger, long contour);
	BOOL SetSymmetric(long symmetric);
	BOOL SetPeripheral(BOOL peripheral);
	BOOL SetFieldWeld(long fieldWeld);
	BOOL SetProcess(BOOL process, LPCTSTR text, BOOL reference);
	BOOL SetStagger(BOOL stagger);
};
/////////////////////////////////////////////////////////////////////////////
// IFrame wrapper class

class IFrame : public COleDispatchDriver
{
public:
	IFrame() {}		// Calls COleDispatchDriver default constructor
	IFrame(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IFrame(const IFrame& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AddMenuItem(LPCTSTR Menu, LPCTSTR Item, long position, LPCTSTR CallbackFcnAndModule);
	BOOL AddMenu(LPCTSTR Menu, long position);
	long GetMenuState(LPCTSTR MenuItemString);
	void RenameMenu(LPCTSTR MenuItemString, LPCTSTR newName);
	void RemoveMenu(LPCTSTR MenuItemString);
	BOOL AddMenuPopupItem(long DocType, long SelectType, LPCTSTR Item, LPCTSTR CallbackFcnAndModule, LPCTSTR CustomNames, long Unused);
};
/////////////////////////////////////////////////////////////////////////////
// IAssemblyDoc wrapper class

class IAssemblyDoc : public COleDispatchDriver
{
public:
	IAssemblyDoc() {}		// Calls COleDispatchDriver default constructor
	IAssemblyDoc(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IAssemblyDoc(const IAssemblyDoc& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AddComponent(LPCTSTR compName, double x, double y, double z);
	void FeatureExtrusion();
	void EditRebuild();
	void ToolsCheckInterference();
	LPDISPATCH GetFirstMember();
	LPDISPATCH GetSelectedMember();
	LPDISPATCH IGetFirstMember();
	LPDISPATCH IGetSelectedMember();
	void ViewCollapseAssembly();
	void ViewExplodeAssembly();
	void TranslateComponent();
	void InsertNewPart();
	void RotateComponent();
	void FileDeriveComponentPart();
	void InsertCavity();
	void HideComponent();
	void ShowComponent();
	void FixComponent();
	void UnfixComponent();
	void EditAssembly();
	void EditPart();
	void OpenCompFile();
	void UpdateFeatureScope();
	void AddMate(long mateType, long align, BOOL flip, double dist, double angle);
	void CompConfigProperties(BOOL m_suppressed, BOOL m_show_component, BOOL m_fdetail);
	BOOL AddToFeatureScope(LPCTSTR compName);
	BOOL RemoveFromFeatureScope(LPCTSTR compName);
	void EditExplodeParameters();
	void RotateComponentAxis();
	void ViewFeatureManagerByFeatures();
	void ComponentReload();
	void ViewFeatureManagerByDependencies();
	void AssemblyPartToggle();
	LPDISPATCH FeatureByName(LPCTSTR Name);
	LPDISPATCH IFeatureByName(LPCTSTR Name);
	void InsertJoin();
	void UpdateBox();
	void InsertWeld(LPCTSTR type, LPCTSTR shape, double topDelta, double bottomDelta, double radius, LPCTSTR part);
	void ForceRebuild();
	LPDISPATCH GetEditTarget();
	LPDISPATCH IGetEditTarget();
	void InsertCavity2(double scaleFactor, long scaleType);
	BOOL AutoExplode();
	BOOL ShowExploded(BOOL showIt);
	LPDISPATCH AddComponent2(LPCTSTR compName, double x, double y, double z);
	LPDISPATCH IAddComponent2(LPCTSTR compName, double x, double y, double z);
	void EditMate(long mateType, long align, BOOL flip, double dist, double angle);
	BOOL InsertDerivedPattern();
	long ResolveAllLightWeightComponents(BOOL warnUser);
	long GetLightWeightComponentCount();
	void InsertCavity3(double scaleFactor, long scaleType, long keepPieceIndex);
	long ComponentReload2(LPDISPATCH component, BOOL ReadOnly, long options);
	long IComponentReload2(LPDISPATCH component, BOOL ReadOnly, long options);
	BOOL CompConfigProperties2(long suppression, BOOL visibility, BOOL featureDetails);
	long AddPipePenetration();
	BOOL AddPipingFitting(LPCTSTR pathname, LPCTSTR configName, short alignmentIndex);
	BOOL IsComponentTreeValid();
	void ForceRebuild2(BOOL topOnly);
	BOOL SetDroppedFileConfigName(LPCTSTR configName);
	VARIANT AddComponents(const VARIANT& names, const VARIANT& transforms);
	LPDISPATCH IAddComponents(long* count, BSTR* names, double* transforms);
	long EditPart2(BOOL silent, BOOL allowReadOnly, long* information);
	void InsertCavity4(double scaleFactor_x, double scaleFactor_y, double scaleFactor_z, BOOL isUniform, long scaleType, long keepPieceIndex);
	void ToolsCheckInterference2(long numComponents, const VARIANT& lpComponents, BOOL coincidentInterference, VARIANT* pComp, VARIANT* pFace);
	void IToolsCheckInterference2(long numComponents, LPDISPATCH* lpComponents, BOOL coincidentInterference, VARIANT* pComp, VARIANT* pFace);
	LPDISPATCH GetDroppedAtEntity();
	BOOL CompConfigProperties3(long suppression, long Solving, BOOL visibility, BOOL featureDetails);
};
/////////////////////////////////////////////////////////////////////////////
// IMember wrapper class

class IMember : public COleDispatchDriver
{
public:
	IMember() {}		// Calls COleDispatchDriver default constructor
	IMember(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMember(const IMember& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
};
/////////////////////////////////////////////////////////////////////////////
// IDrawingDoc wrapper class

class IDrawingDoc : public COleDispatchDriver
{
public:
	IDrawingDoc() {}		// Calls COleDispatchDriver default constructor
	IDrawingDoc(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDrawingDoc(const IDrawingDoc& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	void NewNote(LPCTSTR text, double height);
	void NewSheet(LPCTSTR Name, short paperSize, short templateIn, double scale1, double Scale2);
	void SetupSheet(LPCTSTR Name, short paperSize, short templateIn, double scale1, double Scale2);
	LPDISPATCH NewGtol();
	LPDISPATCH INewGtol();
	LPDISPATCH EditSelectedGtol();
	LPDISPATCH IEditSelectedGtol();
	BOOL CreateLinearDim(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot);
	BOOL CreateAngDim(const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, const VARIANT& vP4, const VARIANT& vP5, const VARIANT& vP6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, 
		double witnessOvershoot);
	BOOL CreateDiamDim(double dimVal, const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot);
	CString CreateViewport(double LowerLeftX, double LowerLeftY, double UpperRightX, double UpperRightY, short sketchSize);
	BOOL ActivateView(LPCTSTR viewname);
	BOOL Create1stAngleViews(LPCTSTR ModelName);
	BOOL Create3rdAngleViews(LPCTSTR ModelName);
	BOOL CreateDrawViewFromModelView(LPCTSTR ModelName, LPCTSTR viewname, double locX, double locY, double locZ);
	BOOL CreateUnfoldedViewAt(double x, double y, double z);
	BOOL CreateText(LPCTSTR textString, double textX, double textY, double textZ, double textHeight, double textAngle);
	void EditRebuild();
	void ViewFullPage();
	void CreateSectionView();
	void SheetNext();
	void Dimensions();
	void InsertGroup();
	void SheetPrevious();
	void AlignVert();
	void AlignHorz();
	void InsertRefDim();
	void MakeSectionLine();
	void InsertBaseDim();
	void EditSketch();
	void CreateDetailView();
	void CreateAuxiliaryView();
	void StartDrawing();
	void EndDrawing();
	LPDISPATCH GetFirstView();
	LPDISPATCH IGetFirstView();
	VARIANT GetInsertionPoint();
	void AttachDimensions();
	void InsertModelDimensions(long option);
	void EditTemplate();
	void InsertOrdinate();
	void UnsuppressView();
	void HideShowDrawingViews();
	void SuppressView();
	void AlignOrdinate();
	void SketchDim();
	void CenterMark();
	void EditSheet();
	void InsertHorizontalOrdinate();
	void EditOrdinate();
	void InsertVerticalOrdinate();
	void ChangeOrdDir();
	long GetLineFontCount();
	CString GetLineFontName(long index);
	VARIANT GetLineFontInfo(long index);
	void ICreateLinearDim(double* p0, double* P1, double* P2, double* P3, double* p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot);
	void ICreateAngDim(double* p0, double* P1, double* P2, double* P3, double* p4, double* P5, double* P6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot);
	void ICreateDiamDim(double DimValue, double* p0, double* P1, double* P2, double* P3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot);
	double IGetInsertionPoint();
	LPDISPATCH CreateCompoundNote(double height, double x, double y, double z);
	LPDISPATCH ICreateCompoundNote(double height, double x, double y, double z);
	BOOL CreateOrdinateDim(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot);
	void ICreateOrdinateDim(double* p0, double* P1, double* P2, double* P3, double* p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot);
	void InsertNewNote(LPCTSTR text, BOOL noLeader, BOOL balloonNote, BOOL bentLeader, short arrowStyle, short leaderSide);
	BOOL AddCenterMark(double cmSize, BOOL cmShowLines);
	void InsertWeldSymbol(LPCTSTR dim1, LPCTSTR symbol, LPCTSTR dim2, BOOL symmetric, BOOL fieldWeld, BOOL showOtherSide, BOOL dashOnTop, BOOL peripheral, BOOL hasProcess, LPCTSTR processValue);
	BOOL InsertSurfaceFinishSymbol(long symType, long leaderType, double locX, double locY, double locZ, long laySymbol, long arrowType, LPCTSTR machAllowance, LPCTSTR otherVals, LPCTSTR prodMethod, LPCTSTR sampleLen, LPCTSTR maxRoughness, 
		LPCTSTR minRoughness, LPCTSTR roughnessSpacing);
	BOOL ModifySurfaceFinishSymbol(long symType, long leaderType, double locX, double locY, double locZ, long laySymbol, long arrowType, LPCTSTR machAllowance, LPCTSTR otherVals, LPCTSTR prodMethod, LPCTSTR sampleLen, LPCTSTR maxRoughness, 
		LPCTSTR minRoughness, LPCTSTR roughnessSpacing);
	LPDISPATCH GetCurrentSheet();
	LPDISPATCH IGetCurrentSheet();
	void CreateConstructionGeometry();
	void ViewDisplayHidden();
	void InsertBreakHorizontal();
	void ViewDisplayWireframe();
	void BreakLineZigZagCut();
	void BreakView();
	void ViewDisplayHiddengreyed();
	void ViewTangentEdges();
	void BreakLineSplineCut();
	void InsertBreakVertical();
	void UnBreakView();
	void BreakLineStraightCut();
	void InsertDatumTag();
	void ToggleGrid();
	void FlipSectionLine();
	LPDISPATCH FeatureByName(LPCTSTR Name);
	LPDISPATCH IFeatureByName(LPCTSTR Name);
	BOOL NewSheet2(LPCTSTR Name, long paperSize, long templateIn, double scale1, double Scale2, BOOL firstAngle, LPCTSTR templateName, double Width, double height);
	BOOL CreateLinearDim2(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double val, long primPrec, LPCTSTR text, const VARIANT& TextPoint, double angle, double textHeight, LPCTSTR prefix, 
		LPCTSTR suffix, LPCTSTR callout1, LPCTSTR callout2, long tolType, LPCTSTR tolMin, LPCTSTR tolMax, long tolPrec, double arrowSize, long arrowStyle, long arrowDir, double witnessGap, double witnessOvershoot, BOOL dualDisplay, long dualPrec);
	void ICreateLinearDim2(double* p0, double* P1, double* P2, double* P3, double* p4, double val, long primPrec, LPCTSTR text, double* TextPoint, double angle, double textHeight, LPCTSTR prefix, LPCTSTR suffix, LPCTSTR callout1, 
		LPCTSTR callout2, long tolType, LPCTSTR tolMin, LPCTSTR tolMax, long tolPrec, double arrowSize, long arrowStyle, long arrowDir, double witnessGap, double witnessOvershoot, BOOL dualDisplay, long dualPrecision);
	BOOL CreateAngDim2(const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, const VARIANT& vP4, const VARIANT& vP5, const VARIANT& vP6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, 
		double witnessOvershoot, const VARIANT& vTextPoint);
	void ICreateAngDim2(double* p0, double* P1, double* P2, double* P3, double* p4, double* P5, double* P6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* TextPoint);
	BOOL CreateDiamDim2(double dimVal, const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, const VARIANT& vTextPoint);
	void ICreateDiamDim2(double DimValue, double* p0, double* P1, double* P2, double* P3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* TextPoint);
	BOOL CreateOrdinateDim2(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, 
		const VARIANT& P5);
	void ICreateOrdinateDim2(double* p0, double* P1, double* P2, double* P3, double* p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* P5);
	void InsertNewNote2(LPCTSTR upperText, LPCTSTR lowerText, BOOL noLeader, BOOL bentLeader, short arrowStyle, short leaderSide, double angle, short balloonStyle, short balloonFit, short upperNoteContent, short lowerNoteContent);
	void DragModelDimension(LPCTSTR viewname, short dropEffect, double x, double y, double z);
	CString CreateViewport2(double LowerLeftX, double LowerLeftY, double UpperRightX, double UpperRightY, short sketchSize, double Scale);
	void SetupSheet2(LPCTSTR Name, short paperSize, short templateIn, double scale1, double Scale2, long skPointsFlag);
	void OnComponentProperties();
	long GetLineFontCount2();
	CString GetLineFontName2(long index);
	VARIANT GetLineFontInfo2(long index);
	void SetLineStyle(LPCTSTR styleName);
	void SetLineWidth(long Width);
	void SetLineColor(long Color);
	void ShowEdge();
	void HideEdge();
	void AddHoleCallout();
	long GetPenCount();
	VARIANT GetPenInfo();
	long IGetPenInfo();
	long GetLineFontId(long index);
	BOOL CreateAuxiliaryViewAt(double x, double y, double z, BOOL notAligned);
	BOOL CreateDetailViewAt(double x, double y, double z);
	BOOL CreateSectionViewAt(double x, double y, double z, BOOL notAligned, BOOL isOffsetSection);
	BOOL CreateUnfoldedViewAt2(double x, double y, double z, BOOL notAligned);
	BOOL SetupSheet3(LPCTSTR Name, long paperSize, long templateIn, double scale1, double Scale2, BOOL firstAngle, LPCTSTR templateName, double Width, double height);
	BOOL InsertModelAnnotations(long option, BOOL allTypes, long types, BOOL allViews);
	void InsertCustomSymbol(LPCTSTR symbolPath);
	long GetSheetCount();
	VARIANT GetSheetNames();
	CString IGetSheetNames(long* count);
	BOOL ActivateSheet(LPCTSTR Name);
	LPDISPATCH CreateText2(LPCTSTR textString, double textX, double textY, double textZ, double textHeight, double textAngle);
	LPDISPATCH ICreateText2(LPCTSTR textString, double textX, double textY, double textZ, double textHeight, double textAngle);
	BOOL NewSheet3(LPCTSTR Name, long paperSize, long templateIn, double scale1, double Scale2, BOOL firstAngle, LPCTSTR templateName, double Width, double height, LPCTSTR propertyViewName);
	BOOL SetupSheet4(LPCTSTR Name, long paperSize, long templateIn, double scale1, double Scale2, BOOL firstAngle, LPCTSTR templateName, double Width, double height, LPCTSTR propertyViewName);
	LPDISPATCH CreateLinearDim3(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double val, long primPrec, LPCTSTR text, const VARIANT& TextPoint, double angle, double textHeight, LPCTSTR prefix, 
		LPCTSTR suffix, LPCTSTR callout1, LPCTSTR callout2, long tolType, LPCTSTR tolMin, LPCTSTR tolMax, long tolPrec, double arrowSize, long arrowStyle, long arrowDir, double witnessGap, double witnessOvershoot, BOOL dualDisplay, long dualPrec);
	LPDISPATCH ICreateLinearDim3(double* p0, double* P1, double* P2, double* P3, double* p4, double val, long primPrec, LPCTSTR text, double* TextPoint, double angle, double textHeight, LPCTSTR prefix, LPCTSTR suffix, LPCTSTR callout1, 
		LPCTSTR callout2, long tolType, LPCTSTR tolMin, LPCTSTR tolMax, long tolPrec, double arrowSize, long arrowStyle, long arrowDir, double witnessGap, double witnessOvershoot, BOOL dualDisplay, long dualPrecision);
	LPDISPATCH CreateAngDim3(const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, const VARIANT& vP4, const VARIANT& vP5, const VARIANT& vP6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, 
		double witnessOvershoot, const VARIANT& vTextPoint);
	LPDISPATCH ICreateAngDim3(double* p0, double* P1, double* P2, double* P3, double* p4, double* P5, double* P6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* TextPoint);
	LPDISPATCH CreateDiamDim3(double dimVal, const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, const VARIANT& vTextPoint);
	LPDISPATCH ICreateDiamDim3(double DimValue, double* p0, double* P1, double* P2, double* P3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* TextPoint);
	LPDISPATCH CreateOrdinateDim3(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, 
		const VARIANT& P5);
	LPDISPATCH ICreateOrdinateDim3(double* p0, double* P1, double* P2, double* P3, double* p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* P5);
	void ForceRebuild();
	BOOL AddOrdinateDimension(long DimType, double locX, double locY, double locZ);
	BOOL CreateLayer(LPCTSTR layerName, LPCTSTR layerDesc, long layerColor, long layerStyle, long layerWidth, BOOL bOn);
	BOOL SetCurrentLayer(LPCTSTR layerName);
	BOOL DrawingViewRotate(double newAngle);
	BOOL CreateDetailViewAt2(double x, double y, double z);
	void RestoreRotation();
	BOOL GetEditSheet();
	void TranslateDrawing(double deltaX, double deltaY);
	BOOL Create1stAngleViews2(LPCTSTR ModelName);
	BOOL Create3rdAngleViews2(LPCTSTR ModelName);
	void HideShowDimensions();
	LPDISPATCH CreateLinearDim4(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, const VARIANT& TextPoint, double val, double angle, double textHeight);
	LPDISPATCH ICreateLinearDim4(double* p0, double* P1, double* P2, double* P3, double* p4, double* TextPoint, double val, double angle, double textHeight);
	LPDISPATCH CreateDiamDim4(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& TextPoint, double val, double textHeight);
	LPDISPATCH ICreateDiamDim4(double* p0, double* P1, double* P2, double* P3, double* TextPoint, double val, double textHeight);
	LPDISPATCH CreateOrdinateDim4(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, const VARIANT& P5, double val, double angle, double textHeight);
	LPDISPATCH ICreateOrdinateDim4(double* p0, double* P1, double* P2, double* P3, double* p4, double* P5, double val, double angle, double textHeight);
	LPDISPATCH CreateAngDim4(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, const VARIANT& P5, const VARIANT& P6, const VARIANT& TextPoint, double textHeight);
	LPDISPATCH ICreateAngDim4(double* p0, double* P1, double* P2, double* P3, double* p4, double* P5, double* P6, double* TextPoint, double textHeight);
	LPDISPATCH CreateDetailViewAt3(double x, double y, double z, long Style, double scale1, double Scale2, LPCTSTR labelIn, long showtype, BOOL fulloutline);
	LPDISPATCH ICreateDetailViewAt3(double x, double y, double z, long Style, double scale1, double Scale2, LPCTSTR labelIn, long showtype, BOOL fulloutline);
	LPDISPATCH CreateSectionViewAt2(double x, double y, double z, BOOL notAligned, BOOL isOffsetSection, LPCTSTR label, BOOL chgdirection, BOOL scwithmodel, BOOL partial, BOOL dispsurfcut);
	LPDISPATCH ICreateSectionViewAt2(double x, double y, double z, BOOL notAligned, BOOL isOffsetSection, LPCTSTR label, BOOL chgdirection, BOOL scwithmodel, BOOL partial, BOOL dispsurfcut);
	LPDISPATCH CreateAuxiliaryViewAt2(double x, double y, double z, BOOL notAligned, LPCTSTR label, BOOL showarrow, BOOL flip);
	LPDISPATCH ICreateAuxiliaryViewAt2(double x, double y, double z, BOOL notAligned, LPCTSTR label, BOOL showarrow, BOOL flip);
	void MakeCustomSymbol();
	void ExplodeCustomSymbol();
	void SaveCustomSymbol(LPCTSTR filenameIn);
	BOOL CreateBreakOutSection(double depth);
	void InsertThreadCallout();
	BOOL CreateFlatPatternViewFromModelView(LPCTSTR ModelName, LPCTSTR configName, double locX, double locY, double locZ);
	BOOL ChangeRefConfigurationOfFlatPatternView(LPCTSTR ModelName, LPCTSTR configName);
};
/////////////////////////////////////////////////////////////////////////////
// IView wrapper class

class IView : public COleDispatchDriver
{
public:
	IView() {}		// Calls COleDispatchDriver default constructor
	IView(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IView(const IView& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetSuppressState();
	void SetSuppressState(long nNewValue);
	LPDISPATCH GetNextView();
	LPDISPATCH IGetNextView();
	VARIANT GetLines();
	VARIANT GetOutline();
	VARIANT GetXform();
	VARIANT GetArcs();
	VARIANT GetUserPoints();
	VARIANT GetPolylines();
	VARIANT GetSplines();
	VARIANT GetDimensionInfo();
	VARIANT GetDimensionString();
	LPDISPATCH GetFirstNote();
	LPDISPATCH IGetFirstNote();
	LPDISPATCH GetFirstGTOL();
	LPDISPATCH IGetFirstGTOL();
	double IGetLines();
	double IGetOutline();
	double IGetXform();
	double IGetArcs();
	double IGetUserPoints();
	double IGetPolylines();
	double IGetSplines();
	double IGetDimensionInfo();
	CString IGetDimensionString();
	long GetLineCount();
	long GetArcCount();
	long GetUserPointsCount();
	long GetPolylineCount(long* pointCount);
	long GetSplineCount(long* pointCount);
	long GetDimensionCount();
	VARIANT GetEllipses();
	double IGetEllipses();
	long GetEllipseCount();
	long GetDisplayMode();
	void SetDisplayMode(long displayIn);
	BOOL GetDisplayTangentEdges();
	void SetDisplayTangentEdges(BOOL displayIn);
	long GetCenterMarkCount();
	VARIANT GetCenterMarkInfo();
	double IGetCenterMarkInfo();
	long GetSectionLineCount(long* size);
	VARIANT GetSectionLineInfo();
	VARIANT GetSectionLineStrings();
	double IGetSectionLineInfo();
	CString IGetSectionLineStrings();
	void UpdateViewDisplayGeometry();
	long GetDetailCircleCount();
	VARIANT GetDetailCircleInfo();
	double IGetDetailCircleInfo();
	VARIANT GetDetailCircleStrings();
	CString IGetDetailCircleStrings();
	VARIANT GetDimensionIds();
	CString IGetDimensionIds();
	VARIANT GetDimensionDisplayInfo();
	double IGetDimensionDisplayInfo();
	VARIANT GetDimensionDisplayString();
	CString IGetDimensionDisplayString();
	VARIANT GetArcs2();
	double IGetArcs2();
	VARIANT GetEllipses2();
	double IGetEllipses2();
	VARIANT GetViewXform();
	double IGetViewXform();
	LPDISPATCH GetFirstSFSymbol();
	LPDISPATCH IGetFirstSFSymbol();
	LPDISPATCH GetFirstDatumTag();
	LPDISPATCH IGetFirstDatumTag();
	LPDISPATCH GetFirstDatumTargetSym();
	LPDISPATCH IGetFirstDatumTargetSym();
	LPDISPATCH GetFirstWeldSymbol();
	LPDISPATCH IGetFirstWeldSymbol();
	long GetDatumPointsCount();
	VARIANT GetDatumPoints();
	double IGetDatumPoints();
	void UseDefaultAlignment();
	void RemoveAlignment();
	void AlignVerticalTo(LPCTSTR viewNameIn);
	void AlignHorizontalTo(LPCTSTR viewNameIn);
	LPDISPATCH GetDisplayData();
	LPDISPATCH IGetDisplayData();
	long GetPolyLineCount2(long* pointCount);
	VARIANT GetPolylines2();
	double IGetPolylines2();
	BOOL ShowExploded(BOOL showIt);
	BOOL IsExploded();
	void SetReferencedConfiguration(LPCTSTR lpszNewValue);
	CString GetReferencedConfiguration();
	LPDISPATCH GetFirstCThread();
	LPDISPATCH IGetFirstCThread();
	BOOL CreateViewArrow(LPCTSTR drawingViewNameIn, LPCTSTR arrowNameIn);
	void ModifyViewArrow(LPCTSTR drawingViewNameIn, LPCTSTR arrowNameIn);
	void MoveViewArrow(LPCTSTR drawingViewNameIn, double dx, double dy, double dz);
	VARIANT GetPolylines3();
	double IGetPolylines3();
	long GetPolyLineCount3(long* pointCount);
	LPDISPATCH GetFirstDisplayDimension();
	LPDISPATCH IGetFirstDisplayDimension();
	LPDISPATCH GetSketch();
	LPDISPATCH IGetSketch();
	VARIANT GetLines2();
	double IGetLines2();
	VARIANT GetSplines2();
	double IGetSplines2();
	VARIANT GetArcs3();
	double IGetArcs3();
	VARIANT GetEllipses3();
	double IGetEllipses3();
	LPDISPATCH GetBomTable();
	LPDISPATCH IGetBomTable();
	long GetUseSheetScale();
	void SetUseSheetScale(long nNewValue);
	double GetScaleDecimal();
	void SetScaleDecimal(double newValue);
	VARIANT GetScaleRatio();
	void SetScaleRatio(const VARIANT& newValue);
	double GetIScaleRatio();
	void SetIScaleRatio(double* newValue);
	VARIANT GetPosition();
	void SetPosition(const VARIANT& newValue);
	double GetIPosition();
	void SetIPosition(double* newValue);
	BOOL SetXform(const VARIANT& transform);
	BOOL ISetXform(double* transform);
	long GetAlignment();
	CString GetReferencedModelName();
	LPDISPATCH GetFirstAnnotation();
	LPDISPATCH IGetFirstAnnotation();
	LPDISPATCH GetFirstCustomSymbol();
	LPDISPATCH IGetFirstCustomSymbol();
	VARIANT GetDimensionInfo2();
	double IGetDimensionInfo2();
	VARIANT GetParabolas();
	double IGetParabolas();
	long GetParabolaCount();
	VARIANT GetEllipses4();
	double IGetEllipses4();
	VARIANT GetLines3();
	double IGetLines3();
	VARIANT GetArcs4();
	double IGetArcs4();
	VARIANT GetSplines3();
	double IGetSplines3();
	VARIANT GetEllipses5();
	double IGetEllipses5();
	VARIANT GetParabolas2();
	double IGetParabolas2();
	VARIANT GetUserPoints2();
	double IGetUserPoints2();
	VARIANT GetDimensionInfo3();
	double IGetDimensionInfo3();
	VARIANT GetDatumPoints2();
	double IGetDatumPoints2();
	CString GetName();
	long GetType();
	BOOL GetProjectedDimensions();
	void SetProjectedDimensions(BOOL bNewValue);
	BOOL HasDesignTable();
	VARIANT GetDesignTableExtent();
	double IGetDesignTableExtent();
	LPDISPATCH GetDisplayData2();
	LPDISPATCH IGetDisplayData2();
	LPDISPATCH GetFirstAnnotation2();
	LPDISPATCH IGetFirstAnnotation2();
	long GetDimensionCount2();
	VARIANT GetDimensionInfo4();
	double IGetDimensionInfo4();
	VARIANT GetDimensionString2();
	CString IGetDimensionString2();
	VARIANT GetDimensionIds2();
	CString IGetDimensionIds2();
	VARIANT GetDimensionDisplayInfo2();
	double IGetDimensionDisplayInfo2();
	VARIANT GetDimensionDisplayString2();
	CString IGetDimensionDisplayString2();
	LPDISPATCH GetFirstDisplayDimension2();
	LPDISPATCH IGetFirstDisplayDimension2();
	VARIANT GetPolylines4();
	double IGetPolylines4();
	long GetPolyLineCount4(long* pointCount);
	BOOL IsModelLoaded();
	long LoadModel();
	long GetDisplayTangentEdges2();
	void SetDisplayTangentEdges2(long displayIn);
	VARIANT GetSectionLines();
	LPDISPATCH IGetSectionLines();
	LPDISPATCH GetSection();
	LPDISPATCH IGetSection();
	VARIANT GetHiddenComponents();
	LPUNKNOWN EnumHiddenComponents();
	LPUNKNOWN EnumSectionLines();
	LPDISPATCH GetDisplayData3();
	LPDISPATCH IGetDisplayData3();
	long GetDimensionCount3();
	VARIANT GetDimensionInfo5();
	double IGetDimensionInfo5();
	VARIANT GetDimensionString3();
	CString IGetDimensionString3();
	VARIANT GetDimensionIds3();
	CString IGetDimensionIds3();
	VARIANT GetDimensionDisplayInfo3();
	double IGetDimensionDisplayInfo3();
	VARIANT GetDimensionDisplayString3();
	CString IGetDimensionDisplayString3();
	LPDISPATCH GetFirstDisplayDimension3();
	LPDISPATCH IGetFirstDisplayDimension3();
	BOOL IsCropped();
	double GetAngle();
	void SetAngle(double newValue);
	VARIANT GetDetailCircles();
	LPDISPATCH IGetDetailCircles(long NumDetailCircles);
	LPDISPATCH GetProjectionArrow();
	LPDISPATCH IGetProjectionArrow();
	LPDISPATCH GetDetail();
	LPDISPATCH IGetDetail();
	LPDISPATCH GetBaseView();
	LPDISPATCH IGetBaseView();
};
/////////////////////////////////////////////////////////////////////////////
// ISFSymbol wrapper class

class ISFSymbol : public COleDispatchDriver
{
public:
	ISFSymbol() {}		// Calls COleDispatchDriver default constructor
	ISFSymbol(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISFSymbol(const ISFSymbol& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	long GetTextCount();
	CString GetTextAtIndex(long index);
	double GetTextHeightAtIndex(long index);
	VARIANT GetTextPositionAtIndex(long index);
	double IGetTextPositionAtIndex(long index);
	double GetTextAngleAtIndex(long index);
	long GetLineCount();
	VARIANT GetLineAtIndex(long index);
	double IGetLineAtIndex(long index);
	long GetArcCount();
	VARIANT GetArcAtIndex(long index);
	double IGetArcAtIndex(long index);
	long GetArrowHeadCount();
	VARIANT GetArrowHeadAtIndex(long index);
	double IGetArrowHeadAtIndex(long index);
	long GetTextRefPositionAtIndex(long index);
	long GetTextInvertAtIndex(long index);
	CString GetTextFontAtIndex(long index);
	long GetTriangleCount();
	VARIANT GetTriangleAtIndex(long index);
	double IGetTriangleAtIndex(long index);
	BOOL IsAttached();
	BOOL HasExtraLeader();
	long GetLeaderCount();
	VARIANT GetLeaderAtIndex(long index);
	double IGetLeaderAtIndex(long index, long* pointCount);
	double IGetArrowHeadInfo();
	VARIANT GetArrowHeadInfo();
	LPDISPATCH GetAnnotation();
	LPDISPATCH IGetAnnotation();
	long GetSymbolType();
	BOOL SetSymbolType(long symbolType);
	long GetDirectionOfLay();
	BOOL SetDirectionOfLay(long direction);
	CString GetText(long whichOne);
	BOOL SetText(long whichOne, LPCTSTR text);
	BOOL GetRotated();
	void SetRotated(BOOL bNewValue);
	BOOL GetGrinding();
	void SetGrinding(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// IDatumTargetSym wrapper class

class IDatumTargetSym : public COleDispatchDriver
{
public:
	IDatumTargetSym() {}		// Calls COleDispatchDriver default constructor
	IDatumTargetSym(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDatumTargetSym(const IDatumTargetSym& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	long GetTextCount();
	CString GetTextAtIndex(long index);
	double GetTextHeightAtIndex(long index);
	VARIANT GetTextPositionAtIndex(long index);
	double IGetTextPositionAtIndex(long index);
	double GetTextAngleAtIndex(long index);
	long GetTextRefPositionAtIndex(long index);
	long GetTextInvertAtIndex(long index);
	long GetLineCount();
	VARIANT GetLineAtIndex(long index);
	double IGetLineAtIndex(long index);
	long GetArcCount();
	VARIANT GetArcAtIndex(long index);
	double IGetArcAtIndex(long index);
	long GetArrowHeadCount();
	VARIANT GetArrowHeadAtIndex(long index);
	double IGetArrowHeadAtIndex(long index);
	long GetTriangleCount();
	VARIANT GetTriangleAtIndex(long index);
	double IGetTriangleAtIndex(long index);
	LPDISPATCH GetAnnotation();
	LPDISPATCH IGetAnnotation();
	long GetTargetShape();
	CString GetTargetAreaSize(long whichOne);
	BOOL SetTargetArea(long shape, LPCTSTR size1, LPCTSTR size2);
	BOOL GetDisplaySymbol();
	BOOL GetDisplayTargetArea();
	BOOL GetDisplaySizeOutside();
	BOOL SetDisplay(BOOL symbol, BOOL targetArea, BOOL sizeOutside);
	CString GetDatumReferenceLabel(long whichOne);
	BOOL SetDatumReferenceLabel(long whichOne, LPCTSTR text);
};
/////////////////////////////////////////////////////////////////////////////
// ICThread wrapper class

class ICThread : public COleDispatchDriver
{
public:
	ICThread() {}		// Calls COleDispatchDriver default constructor
	ICThread(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ICThread(const ICThread& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	LPDISPATCH GetDisplayData();
	LPDISPATCH IGetDisplayData();
	LPDISPATCH GetAnnotation();
	LPDISPATCH IGetAnnotation();
};
/////////////////////////////////////////////////////////////////////////////
// IBomTable wrapper class

class IBomTable : public COleDispatchDriver
{
public:
	IBomTable() {}		// Calls COleDispatchDriver default constructor
	IBomTable(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IBomTable(const IBomTable& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetExtent();
	double IGetExtent();
	long GetRowCount();
	long GetColumnCount();
	CString GetHeaderText(long col);
	CString GetEntryText(long row, long col);
	void Attach();
	void Detach();
	double GetColumnWidth(long col);
	double GetRowHeight(long row);
	VARIANT GetEntryValue(long row, long col);
	BOOL Attach2();
	LPDISPATCH GetDisplayData();
	LPDISPATCH IGetDisplayData();
	BOOL IsVisible();
};
/////////////////////////////////////////////////////////////////////////////
// ICustomSymbol wrapper class

class ICustomSymbol : public COleDispatchDriver
{
public:
	ICustomSymbol() {}		// Calls COleDispatchDriver default constructor
	ICustomSymbol(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ICustomSymbol(const ICustomSymbol& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetNext();
	LPDISPATCH IGetNext();
	long GetTextCount();
	CString GetTextAtIndex(long index);
	double GetTextHeightAtIndex(long index);
	VARIANT GetTextPositionAtIndex(long index);
	double IGetTextPositionAtIndex(long index);
	double GetTextAngleAtIndex(long index);
	long GetTextRefPositionAtIndex(long index);
	long GetTextInvertAtIndex(long index);
	long GetLineCount();
	VARIANT GetLineAtIndex(long index);
	double IGetLineAtIndex(long index);
	long GetArcCount();
	VARIANT GetArcAtIndex(long index);
	double IGetArcAtIndex(long index);
	long GetArrowHeadCount();
	VARIANT GetArrowHeadAtIndex(long index);
	double IGetArrowHeadAtIndex(long index);
	long GetTriangleCount();
	VARIANT GetTriangleAtIndex(long index);
	double IGetTriangleAtIndex(long index);
	LPDISPATCH GetSketch();
	LPDISPATCH IGetSketch();
	VARIANT GetSketchPosition();
	double IGetSketchPosition();
	BOOL IsAttached();
	BOOL HasExtraLeader();
	long GetLeaderCount();
	VARIANT GetLeaderAtIndex(long index);
	double IGetLeaderAtIndex(long index, long* pointCount);
	double IGetArrowHeadInfo();
	VARIANT GetArrowHeadInfo();
	LPDISPATCH GetAnnotation();
	LPDISPATCH IGetAnnotation();
	double GetAngle();
	void SetAngle(double newValue);
	double GetScale2();
	void SetScale2(double newValue);
	CString GetText(long index);
	BOOL SetText(long index, LPCTSTR text);
	long GetTextJustificationAtIndex(long index);
	void SetTextJustificationAtIndex(long index, long justification);
	double GetTextLineSpacingAtIndex(long index);
	BOOL GetTextVisible();
	void SetTextVisible(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// IDrSection wrapper class

class IDrSection : public COleDispatchDriver
{
public:
	IDrSection() {}		// Calls COleDispatchDriver default constructor
	IDrSection(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDrSection(const IDrSection& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetView();
	LPDISPATCH IGetView();
	LPDISPATCH GetSectionView();
	LPDISPATCH IGetSectionView();
	CString GetLabel();
	BOOL SetLabel(LPCTSTR label);
	BOOL GetUseDocTextFormat();
	LPDISPATCH GetTextFormat();
	LPDISPATCH IGetTextFormat();
	BOOL SetTextFormat(BOOL useDoc, LPDISPATCH textFormat);
	BOOL ISetTextFormat(BOOL useDoc, LPDISPATCH textFormat);
	BOOL GetReversedCutDirection();
	void SetReversedCutDirection(BOOL reversed);
	BOOL GetScaleWithModelChanges();
	void SetScaleWithModelChanges(BOOL scaleWithChanges);
	BOOL GetPartialSection();
	void SetPartialSection(BOOL partial);
	BOOL GetDisplayOnlySurfaceCut();
	void SetDisplayOnlySurfaceCut(BOOL Display);
	BOOL IsAligned();
	CString GetName();
	VARIANT GetTextInfo();
	double IGetTextInfo();
	VARIANT GetArrowInfo();
	double IGetArrowInfo();
	VARIANT GetExcludedComponents();
	LPUNKNOWN EnumExcludedComponents();
	BOOL GetAutoHatch();
	void SetAutoHatch(BOOL autoHatch);
	long IGetLineSegmentCount();
	VARIANT GetLineInfo();
	double IGetLineInfo();
};
/////////////////////////////////////////////////////////////////////////////
// IDetailCircle wrapper class

class IDetailCircle : public COleDispatchDriver
{
public:
	IDetailCircle() {}		// Calls COleDispatchDriver default constructor
	IDetailCircle(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDetailCircle(const IDetailCircle& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetView();
	LPDISPATCH GetDetailView();
	CString GetLabel();
	LPDISPATCH GetTextFormat();
	CString GetName();
	VARIANT GetArrowInfo();
	double IGetArrowInfo();
	long GetStyle();
	long GetDisplay();
	VARIANT GetConnectingLine();
	double IGetConnectingLine();
	BOOL HasFullOutline();
	VARIANT GetLeaderInfo();
	double IGetLeaderInfo();
	BOOL GetUseDocTextFormat();
	BOOL SetTextFormat(BOOL useDoc, LPDISPATCH textFormat);
};
/////////////////////////////////////////////////////////////////////////////
// IProjectionArrow wrapper class

class IProjectionArrow : public COleDispatchDriver
{
public:
	IProjectionArrow() {}		// Calls COleDispatchDriver default constructor
	IProjectionArrow(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IProjectionArrow(const IProjectionArrow& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetView();
	LPDISPATCH IGetView();
	LPDISPATCH GetProjectedView();
	LPDISPATCH IGetProjectedView();
	CString GetLabel();
	BOOL SetLabel(LPCTSTR label);
	BOOL GetUseDocTextFormat();
	LPDISPATCH GetTextFormat();
	LPDISPATCH IGetTextFormat();
	VARIANT GetCoordinates();
	double IGetCoordinates();
};
/////////////////////////////////////////////////////////////////////////////
// ISheet wrapper class

class ISheet : public COleDispatchDriver
{
public:
	ISheet() {}		// Calls COleDispatchDriver default constructor
	ISheet(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISheet(const ISheet& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetBomTable();
	LPDISPATCH IGetBomTable();
	CString GetName();
	void SetName(LPCTSTR nameIn);
	CString GetTemplateName();
	void SetTemplateName(LPCTSTR nameIn);
	VARIANT GetProperties();
	double IGetProperties();
	void SetProperties(long paperSz, long templ, double scale1, double Scale2, BOOL firstAngle, double Width, double height);
	long GetCustomColorsCount();
	long IGetCustomColors();
	long GetOLEObjectCount();
	VARIANT GetOLEObjectSettings(long index, long* byteCount, long* aspect);
	BOOL IGetOLEObjectSettings(long index, long* byteCount, long* aspect, double* position);
	VARIANT GetOLEObjectData(long index);
	// method 'IGetOLEObjectData' not emitted because of invalid return type or parameter type
	BOOL CreateOLEObject(long aspect, const VARIANT& position, const VARIANT& buffer);
	// method 'ICreateOLEObject' not emitted because of invalid return type or parameter type
};
/////////////////////////////////////////////////////////////////////////////
// IModeler wrapper class

class IModeler : public COleDispatchDriver
{
public:
	IModeler() {}		// Calls COleDispatchDriver default constructor
	IModeler(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IModeler(const IModeler& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH CreateBodyFromBox(const VARIANT& boxDimArray);
	LPDISPATCH ICreateBodyFromBox(double* boxDimArray);
	BOOL SetTolerances(long* ToleranceIDArray, double* ToleranceValueArray, long NumTol);
	BOOL UnsetTolerances(long* ToleranceIDArray, long NumTol);
	LPDISPATCH Restore(LPUNKNOWN streamIn);
	LPDISPATCH IRestore(LPUNKNOWN streamIn);
	BOOL SetInitKnitGapWidth(double InitGapWidth);
	double GetInitKnitGapWidth();
	LPDISPATCH CreateBodyFromCyl(const VARIANT& cylDimArray);
	LPDISPATCH ICreateBodyFromCyl(double* cylDimArray);
	LPDISPATCH CreateBodyFromCone(const VARIANT& coneDimArray);
	LPDISPATCH ICreateBodyFromCone(double* coneDimArray);
	LPDISPATCH CreateBrepBody(long type, long nTopologies, const VARIANT& topologies, const VARIANT& edgeToleranceArray, const VARIANT& vertexToleranceArray, const VARIANT& pointArray, const VARIANT& curveArray, const VARIANT& surfaceArray, 
		long nRelations, const VARIANT& parents, const VARIANT& children, const VARIANT& senses);
	LPDISPATCH ICreateBrepBody(long type, long nTopologies, long* topologies, double* edgeTolArray, double* vertexTolArray, double* pointArray, LPDISPATCH* curveArray, LPDISPATCH* surfaceArray, long nRelations, long* parents, long* children, 
		long* senses);
	LPDISPATCH CreatePlanarSurface(const VARIANT& vRootPoint, const VARIANT& vNormal);
	LPDISPATCH ICreatePlanarSurface(double* rootPoint, double* Normal);
	LPDISPATCH CreateExtrusionSurface(LPDISPATCH profileCurve, const VARIANT& axisDirection);
	LPDISPATCH ICreateExtrusionSurface(LPDISPATCH profileCurve, double* axisDirection);
	LPDISPATCH CreateRevolutionSurface(LPDISPATCH profileCurve, const VARIANT& axisPoint, const VARIANT& axisDirection, const VARIANT& profileEndPtParams);
	LPDISPATCH ICreateRevolutionSurface(LPDISPATCH profileCurve, double* axisPoint, double* axisDirection, double* profileEndPtParams);
	LPDISPATCH CreateBsplineSurface(const VARIANT& props, const VARIANT& uKnots, const VARIANT& vKnots, const VARIANT& ctrlPtCoords);
	LPDISPATCH ICreateBsplineSurface(long* Properties, double* UKnotArray, double* VKnotArray, double* ControlPointCoordArray);
	LPDISPATCH CreateOffsetSurface(LPDISPATCH surfaceIn, double distance);
	LPDISPATCH ICreateOffsetSurface(LPDISPATCH surfaceIn, double distance);
	LPDISPATCH CreateLine(const VARIANT& rootPoint, const VARIANT& direction);
	LPDISPATCH ICreateLine(double* rootPoint, double* direction);
	LPDISPATCH CreateArc(const VARIANT& center, const VARIANT& axis, double radius, const VARIANT& startPoint, const VARIANT& endPoint);
	LPDISPATCH ICreateArc(double* center, double* axis, double radius, double* startPoint, double* endPoint);
	LPDISPATCH CreateBsplineCurve(const VARIANT& props, const VARIANT& Knots, const VARIANT& ctrlPtCoords);
	LPDISPATCH ICreateBsplineCurve(long* Properties, double* KnotArray, double* ControlPointCoordArray);
	LPDISPATCH CreatePCurve(LPDISPATCH surface, const VARIANT& props, const VARIANT& Knots, const VARIANT& ctrlPtCoords);
	LPDISPATCH ICreatePCurve(LPDISPATCH surface, long* props, double* Knots, double* ctrlPtCoords);
	VARIANT CreateBodiesFromSheets(const VARIANT& sheets, long options, long* error);
	long ICreateBodiesFromSheets(long nSheets, LPUNKNOWN* sheets, long options, long* nResults, LPUNKNOWN* results);
	LPDISPATCH ICreateBodyFromFaces(long NumOfFaces, LPDISPATCH* faces, BOOL doLocalCheck, BOOL* localCheckResult);
	double FindTwoEdgeMaxDeviation(LPDISPATCH lpEdge1, LPDISPATCH lpEdge2);
	double IFindTwoEdgeMaxDeviation(LPDISPATCH pEdge1, LPDISPATCH pEdge2);
	LPDISPATCH CreateConicalSurface(const VARIANT& center, const VARIANT& direction, double radius, double semiAngle);
	LPDISPATCH ICreateConicalSurface(double* center, double* direction, double radius, double semiAngle);
	LPDISPATCH CreateCylindricalSurface(const VARIANT& center, const VARIANT& direction, double radius);
	LPDISPATCH ICreateCylindricalSurface(double* center, double* direction, double radius);
	LPDISPATCH CreateSphericalSurface(const VARIANT& center, double radius);
	LPDISPATCH ICreateSphericalSurface(double* center, double radius);
	LPDISPATCH CreateToroidalSurface(const VARIANT& center, const VARIANT& axis, const VARIANT& refDirection, double majorRadius, double minorRadius);
	LPDISPATCH ICreateToroidalSurface(double* center, double* axis, double* refDirection, double majorRadius, double minorRadius);
	LPDISPATCH CreateBodyFromFaces2(long NumOfFaces, const VARIANT& faces, long actionType, BOOL doLocalCheck, BOOL* localCheckResult);
	LPDISPATCH ICreateBodyFromFaces2(long NumOfFaces, LPDISPATCH* faces, long actionType, BOOL doLocalCheck, BOOL* localCheckResult);
	double SetToleranceValue(long ToleranceID, double NewToleranceValue);
	double GetToleranceValue(long ToleranceID);
	LPDISPATCH CreateSheetFromSurface(LPDISPATCH surfaceIn, const VARIANT& uvRange);
	LPDISPATCH ICreateSheetFromSurface(LPDISPATCH surfaceIn, double* uvRange);
	BOOL ImprintingFaces(const VARIANT& targetFaceArray, const VARIANT& toolFaceArray, long options, VARIANT* targetEdges, VARIANT* toolEdges, VARIANT* targetVertices, VARIANT* toolVertices);
	void IImprintingFaces(LPDISPATCH* targetEdges, LPDISPATCH* toolEdges, LPDISPATCH* targetVertices, LPDISPATCH* toolVertices);
	BOOL IImprintingFacesCount(long nTargetFaces, LPDISPATCH* targetFaceArray, long nToolFaces, LPDISPATCH* toolFaceArray, long options, long* nTargetEdges, long* ntoolEdges, long* ntargetVertices, long* toolVertices);
	LPDISPATCH CreateSweptSurface(LPDISPATCH curveIn, const VARIANT& dir);
	LPDISPATCH ICreateSweptSurface(LPDISPATCH curveIn, double* dir);
	BOOL ReplaceSurfaces(long nFaces, const VARIANT& faceArray, const VARIANT& newSurfArray, const VARIANT& senseArray, double tolerance);
	BOOL IReplaceSurfaces(long nFaces, LPDISPATCH* faceArray, LPDISPATCH* newSurfArray, long* senseArray, double tolerance);
	VARIANT SplitFaceOnParam(LPDISPATCH facedisp, long UVFlag, double Parameter, BOOL* status);
	long ISplitFaceOnParamCount(LPDISPATCH facedisp, long UVFlag, double Parameter, BOOL* status);
	LPDISPATCH ISplitFaceOnParam();
	BOOL CheckInterference(LPDISPATCH body1, LPDISPATCH body2, BOOL coincidentInterference, VARIANT* body1InterferedFaceArray, VARIANT* body2InterferedFaceArray, VARIANT* intersectedBodyArray);
	BOOL ICheckInterferenceCount(LPDISPATCH body1, LPDISPATCH body2, BOOL coincidentInterference, long* body1InterferedFaceCount, long* body2InterferedFaceCount, long* intersectedBodyCount);
	void ICheckInterference(LPDISPATCH* body1InterferedFaceArray, LPDISPATCH* body2InterferedFaceArray, LPDISPATCH* intersectedBodyArray);
};
/////////////////////////////////////////////////////////////////////////////
// IEnvironment wrapper class

class IEnvironment : public COleDispatchDriver
{
public:
	IEnvironment() {}		// Calls COleDispatchDriver default constructor
	IEnvironment(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IEnvironment(const IEnvironment& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetSymEdgeCounts(LPCTSTR symId);
	short IGetSymEdgeCounts(LPCTSTR symId);
	VARIANT GetSymLines(LPCTSTR symId);
	double IGetSymLines(LPCTSTR symId);
	VARIANT GetSymArcs(LPCTSTR symId);
	double IGetSymArcs(LPCTSTR symId);
	VARIANT GetSymCircles(LPCTSTR symId);
	double IGetSymCircles(LPCTSTR symId);
	VARIANT GetSymTextPoints(LPCTSTR symId);
	double IGetSymTextPoints(LPCTSTR symId);
	VARIANT GetSymText(LPCTSTR symId);
	VARIANT GetSymTriangles(LPCTSTR symId);
	double IGetSymTriangles(LPCTSTR symId);
};
/////////////////////////////////////////////////////////////////////////////
// IMathUtility wrapper class

class IMathUtility : public COleDispatchDriver
{
public:
	IMathUtility() {}		// Calls COleDispatchDriver default constructor
	IMathUtility(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMathUtility(const IMathUtility& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH CreateTransform(const VARIANT& ArrayDataIn);
	LPDISPATCH ICreateTransform(double* ArrayDataIn);
	LPDISPATCH CreateTransformRotateAxis(LPDISPATCH pointObjIn, LPDISPATCH vectorObjIn, double angle);
	LPDISPATCH ICreateTransformRotateAxis(LPDISPATCH pointObjIn, LPDISPATCH vectorObjIn, double angle);
	LPDISPATCH CreatePoint(const VARIANT& ArrayDataIn);
	LPDISPATCH ICreatePoint(double* ArrayDataIn);
	LPDISPATCH CreateVector(const VARIANT& ArrayDataIn);
	LPDISPATCH ICreateVector(double* ArrayDataIn);
};
/////////////////////////////////////////////////////////////////////////////
// IMathTransform wrapper class

class IMathTransform : public COleDispatchDriver
{
public:
	IMathTransform() {}		// Calls COleDispatchDriver default constructor
	IMathTransform(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMathTransform(const IMathTransform& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH Multiply(LPDISPATCH TransformObjIn);
	LPDISPATCH IMultiply(LPDISPATCH TransformObjIn);
	VARIANT GetArrayData();
	void SetArrayData(const VARIANT& newValue);
	double GetIArrayData();
	void SetIArrayData(double* newValue);
	void GetData(LPDISPATCH* xAxisObjOut, LPDISPATCH* yAxisObjOut, LPDISPATCH* zAxisObjOut, LPDISPATCH* TransformObjOut, double* scaleOut);
	void IGetData(LPDISPATCH* xAxisObjOut, LPDISPATCH* yAxisObjOut, LPDISPATCH* zAxisObjOut, LPDISPATCH* TransformObjOut, double* scaleOut);
	void SetData(LPDISPATCH xAxisObjIn, LPDISPATCH yAxisObjIn, LPDISPATCH zAxisObjIn, LPDISPATCH TransformObjIn, double scaleValIn);
	void ISetData(LPDISPATCH xAxisObjIn, LPDISPATCH yAxisObjIn, LPDISPATCH zAxisObjIn, LPDISPATCH TransformObjIn, double scaleValIn);
	LPDISPATCH Inverse();
	LPDISPATCH IInverse();
};
/////////////////////////////////////////////////////////////////////////////
// IMathVector wrapper class

class IMathVector : public COleDispatchDriver
{
public:
	IMathVector() {}		// Calls COleDispatchDriver default constructor
	IMathVector(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMathVector(const IMathVector& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH MultiplyTransform(LPDISPATCH TransformObjIn);
	LPDISPATCH IMultiplyTransform(LPDISPATCH TransformObjIn);
	VARIANT GetArrayData();
	void SetArrayData(const VARIANT& newValue);
	double GetIArrayData();
	void SetIArrayData(double* newValue);
	LPDISPATCH Add(LPDISPATCH vectorObjIn);
	LPDISPATCH IAdd(LPDISPATCH vectorObjIn);
	LPDISPATCH Subtract(LPDISPATCH vectorObjIn);
	LPDISPATCH ISubtract(LPDISPATCH vectorObjIn);
	LPDISPATCH Scale(double valueIn);
	LPDISPATCH IScale(double valueIn);
	double GetLength();
	double Dot(LPDISPATCH vectorObjIn);
	double IDot(LPDISPATCH vectorObjIn);
	LPDISPATCH Cross(LPDISPATCH vectorObjIn);
	LPDISPATCH ICross(LPDISPATCH vectorObjIn);
	LPDISPATCH ConvertToPoint();
	LPDISPATCH IConvertToPoint();
};
/////////////////////////////////////////////////////////////////////////////
// IMathPoint wrapper class

class IMathPoint : public COleDispatchDriver
{
public:
	IMathPoint() {}		// Calls COleDispatchDriver default constructor
	IMathPoint(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMathPoint(const IMathPoint& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH MultiplyTransform(LPDISPATCH TransformObjIn);
	LPDISPATCH IMultiplyTransform(LPDISPATCH TransformObjIn);
	VARIANT GetArrayData();
	void SetArrayData(const VARIANT& newValue);
	double GetIArrayData();
	void SetIArrayData(double* newValue);
	LPDISPATCH AddVector(LPDISPATCH vectorObjIn);
	LPDISPATCH IAddVector(LPDISPATCH vectorObjIn);
	LPDISPATCH SubtractVector(LPDISPATCH vectorObjIn);
	LPDISPATCH ISubtractVector(LPDISPATCH vectorObjIn);
	LPDISPATCH Subtract(LPDISPATCH pointObjIn);
	LPDISPATCH ISubtract(LPDISPATCH pointObjIn);
	LPDISPATCH Scale(double valueIn);
	LPDISPATCH IScale(double valueIn);
	LPDISPATCH ConvertToVector();
	LPDISPATCH IConvertToVector();
};
/////////////////////////////////////////////////////////////////////////////
// IRefAxis wrapper class

class IRefAxis : public COleDispatchDriver
{
public:
	IRefAxis() {}		// Calls COleDispatchDriver default constructor
	IRefAxis(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IRefAxis(const IRefAxis& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetRefAxisParams();
	double IGetRefAxisParams();
};
/////////////////////////////////////////////////////////////////////////////
// IMate wrapper class

class IMate : public COleDispatchDriver
{
public:
	IMate() {}		// Calls COleDispatchDriver default constructor
	IMate(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMate(const IMate& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetMateParams();
	void IGetMateParams(long* mateType, long* alignFlag, long* canBeFlipped);
	VARIANT GetMateDimensionValue();
	double IGetMateDimensionValue();
	VARIANT GetMateEntities();
	void IGetMateEntities(LPDISPATCH* entity1, LPDISPATCH* entity2);
	LPDISPATCH GetEntity(long whichOne);
	LPDISPATCH IGetEntity(long whichOne);
};
/////////////////////////////////////////////////////////////////////////////
// IMateEntity wrapper class

class IMateEntity : public COleDispatchDriver
{
public:
	IMateEntity() {}		// Calls COleDispatchDriver default constructor
	IMateEntity(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMateEntity(const IMateEntity& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetMember();
	LPDISPATCH IGetMember();
	long GetEntityType();
	VARIANT GetEntityParams();
	double IGetEntityParams();
	LPDISPATCH GetComponent();
	LPDISPATCH IGetComponent();
	CString GetComponentName();
};
/////////////////////////////////////////////////////////////////////////////
// ISWPropertySheet wrapper class

class ISWPropertySheet : public COleDispatchDriver
{
public:
	ISWPropertySheet() {}		// Calls COleDispatchDriver default constructor
	ISWPropertySheet(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISWPropertySheet(const ISWPropertySheet& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long AddPage(long page);
	long RemovePage(long page);
};
/////////////////////////////////////////////////////////////////////////////
// IRibFeatureData wrapper class

class IRibFeatureData : public COleDispatchDriver
{
public:
	IRibFeatureData() {}		// Calls COleDispatchDriver default constructor
	IRibFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IRibFeatureData(const IRibFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL GetIsTwoSided();
	void SetIsTwoSided(BOOL bNewValue);
	BOOL GetReverseThicknessDir();
	void SetReverseThicknessDir(BOOL bNewValue);
	double GetThickness();
	void SetThickness(double newValue);
	long GetRefSketchIndex();
	void SetRefSketchIndex(long nNewValue);
	long NextReference();
	BOOL GetFlipSide();
	void SetFlipSide(BOOL bNewValue);
	BOOL GetEnableDraft();
	void SetEnableDraft(BOOL bNewValue);
	BOOL GetDraftOutward();
	void SetDraftOutward(BOOL bNewValue);
	double GetDraftAngle();
	void SetDraftAngle(double newValue);
};
/////////////////////////////////////////////////////////////////////////////
// IDomeFeatureData wrapper class

class IDomeFeatureData : public COleDispatchDriver
{
public:
	IDomeFeatureData() {}		// Calls COleDispatchDriver default constructor
	IDomeFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDomeFeatureData(const IDomeFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	double GetHeight();
	void SetHeight(double newValue);
	BOOL GetReverseDir();
	void SetReverseDir(BOOL bNewValue);
	BOOL GetElliptical();
	void SetElliptical(BOOL bNewValue);
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetFace();
	void SetFace(LPDISPATCH newValue);
	LPDISPATCH GetIFace();
	void SetIFace(LPDISPATCH newValue);
};
/////////////////////////////////////////////////////////////////////////////
// ISketchLine wrapper class

class ISketchLine : public COleDispatchDriver
{
public:
	ISketchLine() {}		// Calls COleDispatchDriver default constructor
	ISketchLine(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchLine(const ISketchLine& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetStartPoint();
	double IGetStartPoint();
	VARIANT GetEndPoint();
	double IGetEndPoint();
	LPDISPATCH GetStartPoint2();
	LPDISPATCH IGetStartPoint2();
	LPDISPATCH GetEndPoint2();
	LPDISPATCH IGetEndPoint2();
};
/////////////////////////////////////////////////////////////////////////////
// ISketchArc wrapper class

class ISketchArc : public COleDispatchDriver
{
public:
	ISketchArc() {}		// Calls COleDispatchDriver default constructor
	ISketchArc(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchArc(const ISketchArc& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetStartPoint();
	double IGetStartPoint();
	VARIANT GetEndPoint();
	double IGetEndPoint();
	VARIANT GetCenterPoint();
	double IGetCenterPoint();
	long IsCircle();
	long GetRotationDir();
	LPDISPATCH GetStartPoint2();
	LPDISPATCH IGetStartPoint2();
	LPDISPATCH GetEndPoint2();
	LPDISPATCH IGetEndPoint2();
	LPDISPATCH GetCenterPoint2();
	LPDISPATCH IGetCenterPoint2();
	double GetRadius();
	BOOL SetRadius(double radius);
	VARIANT GetNormalVector();
	double IGetNormalVector();
};
/////////////////////////////////////////////////////////////////////////////
// ISketchText wrapper class

class ISketchText : public COleDispatchDriver
{
public:
	ISketchText() {}		// Calls COleDispatchDriver default constructor
	ISketchText(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchText(const ISketchText& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetEdges();
	LPUNKNOWN EnumEdges();
};
/////////////////////////////////////////////////////////////////////////////
// ISketchEllipse wrapper class

class ISketchEllipse : public COleDispatchDriver
{
public:
	ISketchEllipse() {}		// Calls COleDispatchDriver default constructor
	ISketchEllipse(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchEllipse(const ISketchEllipse& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetStartPoint();
	double IGetStartPoint();
	VARIANT GetEndPoint();
	double IGetEndPoint();
	VARIANT GetCenterPoint();
	double IGetCenterPoint();
	VARIANT GetMajorPoint();
	double IGetMajorPoint();
	VARIANT GetMinorPoint();
	double IGetMinorPoint();
	LPDISPATCH GetStartPoint2();
	LPDISPATCH IGetStartPoint2();
	LPDISPATCH GetEndPoint2();
	LPDISPATCH IGetEndPoint2();
	LPDISPATCH GetCenterPoint2();
	LPDISPATCH IGetCenterPoint2();
	LPDISPATCH GetMajorPoint2();
	LPDISPATCH IGetMajorPoint2();
	LPDISPATCH GetMinorPoint2();
	LPDISPATCH IGetMinorPoint2();
};
/////////////////////////////////////////////////////////////////////////////
// ISketchParabola wrapper class

class ISketchParabola : public COleDispatchDriver
{
public:
	ISketchParabola() {}		// Calls COleDispatchDriver default constructor
	ISketchParabola(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchParabola(const ISketchParabola& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	VARIANT GetStartPoint();
	double IGetStartPoint();
	VARIANT GetEndPoint();
	double IGetEndPoint();
	VARIANT GetFocalPoint();
	double IGetFocalPoint();
	VARIANT GetApexPoint();
	double IGetApexPoint();
	LPDISPATCH GetStartPoint2();
	LPDISPATCH IGetStartPoint2();
	LPDISPATCH GetEndPoint2();
	LPDISPATCH IGetEndPoint2();
	LPDISPATCH GetFocalPoint2();
	LPDISPATCH IGetFocalPoint2();
	LPDISPATCH GetApexPoint2();
	LPDISPATCH IGetApexPoint2();
};
/////////////////////////////////////////////////////////////////////////////
// ISketchSpline wrapper class

class ISketchSpline : public COleDispatchDriver
{
public:
	ISketchSpline() {}		// Calls COleDispatchDriver default constructor
	ISketchSpline(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchSpline(const ISketchSpline& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetPointCount();
	VARIANT GetPoints();
	double IGetPoints();
	VARIANT GetPoints2();
	LPUNKNOWN IEnumPoints();
};
/////////////////////////////////////////////////////////////////////////////
// ILightDialog wrapper class

class ILightDialog : public COleDispatchDriver
{
public:
	ILightDialog() {}		// Calls COleDispatchDriver default constructor
	ILightDialog(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ILightDialog(const ILightDialog& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AddSubDialog(long page);
	long GetLightId();
};
/////////////////////////////////////////////////////////////////////////////
// ISimpleHoleFeatureData wrapper class

class ISimpleHoleFeatureData : public COleDispatchDriver
{
public:
	ISimpleHoleFeatureData() {}		// Calls COleDispatchDriver default constructor
	ISimpleHoleFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISimpleHoleFeatureData(const ISimpleHoleFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	BOOL GetReverseDirection();
	void SetReverseDirection(BOOL bNewValue);
	BOOL GetDraftWhileExtruding();
	void SetDraftWhileExtruding(BOOL bNewValue);
	BOOL GetDraftOutward();
	void SetDraftOutward(BOOL bNewValue);
	BOOL GetReverseOffset();
	void SetReverseOffset(BOOL bNewValue);
	long GetType();
	void SetType(long nNewValue);
	double GetDiameter();
	void SetDiameter(double newValue);
	double GetDepth();
	void SetDepth(double newValue);
	double GetDraftAngle();
	void SetDraftAngle(double newValue);
	double GetSurfaceOffset();
	void SetSurfaceOffset(double newValue);
	LPDISPATCH GetFace();
	void SetFace(LPDISPATCH newValue);
	LPDISPATCH GetIFace();
	void SetIFace(LPDISPATCH newValue);
	LPDISPATCH GetVertex();
	void SetVertex(LPDISPATCH newValue);
	LPDISPATCH GetIVertex();
	void SetIVertex(LPDISPATCH newValue);
};
/////////////////////////////////////////////////////////////////////////////
// IWizardHoleFeatureData wrapper class

class IWizardHoleFeatureData : public COleDispatchDriver
{
public:
	IWizardHoleFeatureData() {}		// Calls COleDispatchDriver default constructor
	IWizardHoleFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IWizardHoleFeatureData(const IWizardHoleFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	long GetType();
	void SetType(long nNewValue);
	double GetDiameter();
	void SetDiameter(double newValue);
	double GetCounterBoreDiameter();
	void SetCounterBoreDiameter(double newValue);
	double GetCounterDrillDiameter();
	void SetCounterDrillDiameter(double newValue);
	double GetCounterSinkDiameter();
	void SetCounterSinkDiameter(double newValue);
	double GetMinorDiameter();
	void SetMinorDiameter(double newValue);
	double GetMajorDiameter();
	void SetMajorDiameter(double newValue);
	double GetHoleDiameter();
	void SetHoleDiameter(double newValue);
	double GetThruHoleDiameter();
	void SetThruHoleDiameter(double newValue);
	double GetTapDrillDiameter();
	void SetTapDrillDiameter(double newValue);
	double GetThruTapDrillDiameter();
	void SetThruTapDrillDiameter(double newValue);
	double GetNearCounterSinkDiameter();
	void SetNearCounterSinkDiameter(double newValue);
	double GetMidCounterSinkDiameter();
	void SetMidCounterSinkDiameter(double newValue);
	double GetFarCounterSinkDiameter();
	void SetFarCounterSinkDiameter(double newValue);
	double GetThreadDiameter();
	void SetThreadDiameter(double newValue);
	double GetDepth();
	void SetDepth(double newValue);
	double GetCounterBoreDepth();
	void SetCounterBoreDepth(double newValue);
	double GetCounterDrillDepth();
	void SetCounterDrillDepth(double newValue);
	double GetHoleDepth();
	void SetHoleDepth(double newValue);
	double GetThruHoleDepth();
	void SetThruHoleDepth(double newValue);
	double GetTapDrillDepth();
	void SetTapDrillDepth(double newValue);
	double GetThruTapDrillDepth();
	void SetThruTapDrillDepth(double newValue);
	double GetThreadDepth();
	void SetThreadDepth(double newValue);
	double GetCounterDrillAngle();
	void SetCounterDrillAngle(double newValue);
	double GetCounterSinkAngle();
	void SetCounterSinkAngle(double newValue);
	double GetDrillAngle();
	void SetDrillAngle(double newValue);
	double GetNearCounterSinkAngle();
	void SetNearCounterSinkAngle(double newValue);
	double GetMidCounterSinkAngle();
	void SetMidCounterSinkAngle(double newValue);
	double GetFarCounterSinkAngle();
	void SetFarCounterSinkAngle(double newValue);
	double GetThreadAngle();
	void SetThreadAngle(double newValue);
	double GetHeadClearance();
	void SetHeadClearance(double newValue);
	LPDISPATCH GetFace();
	void SetFace(LPDISPATCH newValue);
	LPDISPATCH GetIFace();
	void SetIFace(LPDISPATCH newValue);
	LPDISPATCH GetVertex();
	void SetVertex(LPDISPATCH newValue);
	LPDISPATCH GetIVertex();
	void SetIVertex(LPDISPATCH newValue);
	long GetEndCondition();
	void SetEndCondition(long nNewValue);
	CString GetStandard();
	void SetStandard(LPCTSTR lpszNewValue);
	CString GetFastenerType();
	void SetFastenerType(LPCTSTR lpszNewValue);
	CString GetFastenerSize();
	void SetFastenerSize(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// IChamferFeatureData wrapper class

class IChamferFeatureData : public COleDispatchDriver
{
public:
	IChamferFeatureData() {}		// Calls COleDispatchDriver default constructor
	IChamferFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IChamferFeatureData(const IChamferFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	double GetEdgeChamferDistance(long side);
	void SetEdgeChamferDistance(long side, double distance);
	double GetVertexChamferDistance(long side);
	void SetVertexChamferDistance(long side, double distance);
	long GetType();
	void SetType(long nNewValue);
	double GetEdgeChamferAngle();
	void SetEdgeChamferAngle(double newValue);
};
/////////////////////////////////////////////////////////////////////////////
// IDraftFeatureData wrapper class

class IDraftFeatureData : public COleDispatchDriver
{
public:
	IDraftFeatureData() {}		// Calls COleDispatchDriver default constructor
	IDraftFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDraftFeatureData(const IDraftFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetType();
	double GetAngle();
	void SetAngle(double newValue);
	BOOL GetReverseDirection();
	void SetReverseDirection(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// ISimpleFilletFeatureData wrapper class

class ISimpleFilletFeatureData : public COleDispatchDriver
{
public:
	ISimpleFilletFeatureData() {}		// Calls COleDispatchDriver default constructor
	ISimpleFilletFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISimpleFilletFeatureData(const ISimpleFilletFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetType();
	double GetDefaultRadius();
	void SetDefaultRadius(double newValue);
	long GetOverFlowType();
	void SetOverFlowType(long nNewValue);
	BOOL GetIsMultipleRadius();
	void SetIsMultipleRadius(BOOL bNewValue);
	BOOL GetRoundCorners();
	void SetRoundCorners(BOOL bNewValue);
	BOOL GetPropagateToTangentFaces();
	void SetPropagateToTangentFaces(BOOL bNewValue);
	long GetFilletItemsCount();
	LPDISPATCH GetFilletItemAtIndex(long index);
	LPUNKNOWN IGetFilletItemAtIndex(long index);
	double GetRadius(LPDISPATCH pFilletItem);
	double IGetRadius(LPUNKNOWN pFilletItem);
	void SetRadius(LPDISPATCH pFilletItem, double radius);
	void ISetRadius(LPUNKNOWN pFilletItem, double radius);
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
};
/////////////////////////////////////////////////////////////////////////////
// IVariableFilletFeatureData wrapper class

class IVariableFilletFeatureData : public COleDispatchDriver
{
public:
	IVariableFilletFeatureData() {}		// Calls COleDispatchDriver default constructor
	IVariableFilletFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IVariableFilletFeatureData(const IVariableFilletFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	double GetDefaultRadius();
	void SetDefaultRadius(double newValue);
	long GetOverFlowType();
	void SetOverFlowType(long nNewValue);
	long GetTransitionType();
	void SetTransitionType(long nNewValue);
	BOOL GetPropagateToTangentFaces();
	void SetPropagateToTangentFaces(BOOL bNewValue);
	long GetFilletEdgeCount();
	LPDISPATCH GetFilletEdgeAtIndex(long index);
	LPDISPATCH IGetFilletEdgeAtIndex(long index);
	double GetRadius(LPDISPATCH pFilletItem);
	double IGetRadius(LPDISPATCH pFilletItem);
	void SetRadius(LPDISPATCH pFilletItem, double radius);
	void ISetRadius(LPDISPATCH pFilletItem, double radius);
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
};
/////////////////////////////////////////////////////////////////////////////
// IExtrudeFeatureData wrapper class

class IExtrudeFeatureData : public COleDispatchDriver
{
public:
	IExtrudeFeatureData() {}		// Calls COleDispatchDriver default constructor
	IExtrudeFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IExtrudeFeatureData(const IExtrudeFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetEndCondition(BOOL forward);
	void SetEndCondition(BOOL forward, long endCondition);
	double GetDepth(BOOL forward);
	void SetDepth(BOOL forward, double depth);
	double GetWallThickness(BOOL forward);
	void SetWallThickness(BOOL forward, double wallThickness);
	BOOL GetDraftWhileExtruding(BOOL forward);
	void SetDraftWhileExtruding(BOOL forward, BOOL draftWhileExtrude);
	BOOL GetDraftOutward(BOOL forward);
	void SetDraftOutward(BOOL forward, BOOL draftOutward);
	double GetDraftAngle(BOOL forward);
	void SetDraftAngle(BOOL forward, double draftAngle);
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetFace(BOOL forward);
	void SetFace(BOOL forward, LPDISPATCH face);
	LPDISPATCH IGetFace(BOOL forward);
	void ISetFace(BOOL forward, LPDISPATCH face);
	LPDISPATCH GetVertex(BOOL forward);
	void SetVertex(BOOL forward, LPDISPATCH face);
	LPDISPATCH IGetVertex(BOOL forward);
	void ISetVertex(BOOL forward, LPDISPATCH face);
	BOOL GetReverseDirection();
	void SetReverseDirection(BOOL bNewValue);
	BOOL GetBothDirections();
	void SetBothDirections(BOOL bNewValue);
	BOOL GetFlipSideToCut();
	void SetFlipSideToCut(BOOL bNewValue);
	BOOL IsBossFeature();
	BOOL IsThinFeature();
	BOOL IsBaseExtrude();
	long GetThinWallType();
	void SetThinWallType(long nNewValue);
	BOOL GetCapEnds();
	void SetCapEnds(BOOL bNewValue);
	double GetCapThickness();
	void SetCapThickness(double newValue);
};
/////////////////////////////////////////////////////////////////////////////
// IRevolveFeatureData wrapper class

class IRevolveFeatureData : public COleDispatchDriver
{
public:
	IRevolveFeatureData() {}		// Calls COleDispatchDriver default constructor
	IRevolveFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IRevolveFeatureData(const IRevolveFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	double GetRevolutionAngle(BOOL forward);
	void SetRevolutionAngle(BOOL forward, double angle);
	double GetWallThickness(BOOL forward);
	void SetWallThickness(BOOL forward, double wallThickness);
	long GetType();
	void SetType(long nNewValue);
	BOOL GetReverseDirection();
	void SetReverseDirection(BOOL bNewValue);
	BOOL IsBossFeature();
	BOOL IsThinFeature();
};
/////////////////////////////////////////////////////////////////////////////
// IMirrorPatternFeatureData wrapper class

class IMirrorPatternFeatureData : public COleDispatchDriver
{
public:
	IMirrorPatternFeatureData() {}		// Calls COleDispatchDriver default constructor
	IMirrorPatternFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMirrorPatternFeatureData(const IMirrorPatternFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetPlane();
	void SetPlane(LPDISPATCH newValue);
	long GetMirrorPlaneType();
	VARIANT GetPatternFeatureArray();
	void SetPatternFeatureArray(const VARIANT& newValue);
	long GetPatternFeatureCount();
	LPDISPATCH IGetPatternFeatureArray();
	void ISetPatternFeatureArray(long featCount, LPDISPATCH* ArrayDataIn);
	BOOL GetGeometryPattern();
	void SetGeometryPattern(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// ICircularPatternFeatureData wrapper class

class ICircularPatternFeatureData : public COleDispatchDriver
{
public:
	ICircularPatternFeatureData() {}		// Calls COleDispatchDriver default constructor
	ICircularPatternFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ICircularPatternFeatureData(const ICircularPatternFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetAxis();
	void SetAxis(LPDISPATCH newValue);
	long GetAxisType();
	BOOL GetReverseDirection();
	void SetReverseDirection(BOOL bNewValue);
	double GetSpacing();
	void SetSpacing(double newValue);
	long GetTotalInstances();
	void SetTotalInstances(long nNewValue);
	BOOL GetEqualSpacing();
	void SetEqualSpacing(BOOL bNewValue);
	BOOL GetVarySketch();
	void SetVarySketch(BOOL bNewValue);
	BOOL GetGeometryPattern();
	void SetGeometryPattern(BOOL bNewValue);
	VARIANT GetPatternFeatureArray();
	void SetPatternFeatureArray(const VARIANT& newValue);
	long GetPatternFeatureCount();
	LPDISPATCH IGetPatternFeatureArray();
	void ISetPatternFeatureArray(long featCount, LPDISPATCH* ArrayDataIn);
	VARIANT GetSkippedItemArray();
	void SetSkippedItemArray(const VARIANT& newValue);
	long GetSkippedItemCount();
	long IGetSkippedItemArray();
	void ISetSkippedItemArray(long featCount, long* ArrayDataIn);
};
/////////////////////////////////////////////////////////////////////////////
// ILinearPatternFeatureData wrapper class

class ILinearPatternFeatureData : public COleDispatchDriver
{
public:
	ILinearPatternFeatureData() {}		// Calls COleDispatchDriver default constructor
	ILinearPatternFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ILinearPatternFeatureData(const ILinearPatternFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetD1Axis();
	void SetD1Axis(LPDISPATCH newValue);
	LPDISPATCH GetD2Axis();
	void SetD2Axis(LPDISPATCH newValue);
	long GetD1AxisType();
	long GetD2AxisType();
	BOOL GetD1ReverseDirection();
	void SetD1ReverseDirection(BOOL bNewValue);
	BOOL GetD2ReverseDirection();
	void SetD2ReverseDirection(BOOL bNewValue);
	double GetD1Spacing();
	void SetD1Spacing(double newValue);
	double GetD2Spacing();
	void SetD2Spacing(double newValue);
	long GetD1TotalInstances();
	void SetD1TotalInstances(long nNewValue);
	long GetD2TotalInstances();
	void SetD2TotalInstances(long nNewValue);
	BOOL GetVarySketch();
	void SetVarySketch(BOOL bNewValue);
	BOOL GetGeometryPattern();
	void SetGeometryPattern(BOOL bNewValue);
	VARIANT GetPatternFeatureArray();
	void SetPatternFeatureArray(const VARIANT& newValue);
	long GetPatternFeatureCount();
	LPDISPATCH IGetPatternFeatureArray();
	void ISetPatternFeatureArray(long featCount, LPDISPATCH* ArrayDataIn);
	VARIANT GetSkippedItemArray();
	void SetSkippedItemArray(const VARIANT& newValue);
	long GetSkippedItemCount();
	long IGetSkippedItemArray();
	void ISetSkippedItemArray(long featCount, long* ArrayDataIn);
};
/////////////////////////////////////////////////////////////////////////////
// ITablePatternFeatureData wrapper class

class ITablePatternFeatureData : public COleDispatchDriver
{
public:
	ITablePatternFeatureData() {}		// Calls COleDispatchDriver default constructor
	ITablePatternFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ITablePatternFeatureData(const ITablePatternFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetCoordinateSystem();
	void SetCoordinateSystem(LPDISPATCH newValue);
	LPDISPATCH GetReferencePoint();
	void SetReferencePoint(LPDISPATCH newValue);
	long GetReferencePointType();
	BOOL GetUseCentroid();
	void SetUseCentroid(BOOL bNewValue);
	BOOL SavePointsToFile(LPCTSTR fileName);
	BOOL LoadPointsFromFile(LPCTSTR fileName);
	VARIANT GetBasePoint();
	double IGetBasePoint();
	BOOL GetGeometryPattern();
	void SetGeometryPattern(BOOL bNewValue);
	VARIANT GetPatternFeatureArray();
	void SetPatternFeatureArray(const VARIANT& newValue);
	long GetPatternFeatureCount();
	LPDISPATCH IGetPatternFeatureArray();
	void ISetPatternFeatureArray(long featCount, LPDISPATCH* ArrayDataIn);
	VARIANT GetPointArray();
	void SetPointArray(const VARIANT& newValue);
	long GetPointCount();
	double IGetPointArray();
	void ISetPointArray(long featCount, double* ArrayDataIn);
};
/////////////////////////////////////////////////////////////////////////////
// ISketchPatternFeatureData wrapper class

class ISketchPatternFeatureData : public COleDispatchDriver
{
public:
	ISketchPatternFeatureData() {}		// Calls COleDispatchDriver default constructor
	ISketchPatternFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchPatternFeatureData(const ISketchPatternFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetSketch();
	void SetSketch(LPDISPATCH newValue);
	LPDISPATCH GetReferencePoint();
	void SetReferencePoint(LPDISPATCH newValue);
	long GetReferencePointType();
	BOOL GetUseCentroid();
	void SetUseCentroid(BOOL bNewValue);
	VARIANT GetBasePoint();
	double IGetBasePoint();
	BOOL GetGeometryPattern();
	void SetGeometryPattern(BOOL bNewValue);
	VARIANT GetPatternFeatureArray();
	void SetPatternFeatureArray(const VARIANT& newValue);
	long GetPatternFeatureCount();
	LPDISPATCH IGetPatternFeatureArray();
	void ISetPatternFeatureArray(long featCount, LPDISPATCH* ArrayDataIn);
};
/////////////////////////////////////////////////////////////////////////////
// IMirrorSolidFeatureData wrapper class

class IMirrorSolidFeatureData : public COleDispatchDriver
{
public:
	IMirrorSolidFeatureData() {}		// Calls COleDispatchDriver default constructor
	IMirrorSolidFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMirrorSolidFeatureData(const IMirrorSolidFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetFace();
	void SetFace(LPDISPATCH newValue);
};
/////////////////////////////////////////////////////////////////////////////
// ISheetMetalFeatureData wrapper class

class ISheetMetalFeatureData : public COleDispatchDriver
{
public:
	ISheetMetalFeatureData() {}		// Calls COleDispatchDriver default constructor
	ISheetMetalFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISheetMetalFeatureData(const ISheetMetalFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	double GetBendRadius();
	void SetBendRadius(double newValue);
	double GetThickness();
	void SetThickness(double newValue);
	LPDISPATCH GetFixedReference();
	void SetFixedReference(LPDISPATCH newValue);
	long GetBendAllowanceType();
	void SetBendAllowanceType(long nNewValue);
	CString GetBendTableFile();
	void SetBendTableFile(LPCTSTR lpszNewValue);
	double GetKFactor();
	void SetKFactor(double newValue);
	double GetBendAllowance();
	void SetBendAllowance(double newValue);
	BOOL GetUseAutoRelief();
	void SetUseAutoRelief(BOOL bNewValue);
	long GetAutoReliefType();
	void SetAutoReliefType(long nNewValue);
	double GetReliefRatio();
	void SetReliefRatio(double newValue);
};
/////////////////////////////////////////////////////////////////////////////
// IOneBendFeatureData wrapper class

class IOneBendFeatureData : public COleDispatchDriver
{
public:
	IOneBendFeatureData() {}		// Calls COleDispatchDriver default constructor
	IOneBendFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IOneBendFeatureData(const IOneBendFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	double GetBendRadius();
	void SetBendRadius(double newValue);
	long GetBendAllowanceType();
	void SetBendAllowanceType(long nNewValue);
	CString GetBendTableFile();
	void SetBendTableFile(LPCTSTR lpszNewValue);
	double GetKFactor();
	void SetKFactor(double newValue);
	double GetBendAllowance();
	void SetBendAllowance(double newValue);
	BOOL GetUseAutoRelief();
	void SetUseAutoRelief(BOOL bNewValue);
	long GetAutoReliefType();
	void SetAutoReliefType(long nNewValue);
	BOOL GetUseDefaultBendRadius();
	void SetUseDefaultBendRadius(BOOL bNewValue);
	BOOL GetUseDefaultBendAllowance();
	void SetUseDefaultBendAllowance(BOOL bNewValue);
	BOOL GetUseDefaultBendRelief();
	void SetUseDefaultBendRelief(BOOL bNewValue);
	BOOL GetBendDown();
	void SetBendDown(BOOL bNewValue);
	double GetBendAngle();
	void SetBendAngle(double newValue);
	long GetBendOrder();
	void SetBendOrder(long nNewValue);
	double GetReliefWidth();
	void SetReliefWidth(double newValue);
	double GetReliefDepth();
	void SetReliefDepth(double newValue);
};
/////////////////////////////////////////////////////////////////////////////
// IBendsFeatureData wrapper class

class IBendsFeatureData : public COleDispatchDriver
{
public:
	IBendsFeatureData() {}		// Calls COleDispatchDriver default constructor
	IBendsFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IBendsFeatureData(const IBendsFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	double GetBendRadius();
	void SetBendRadius(double newValue);
	long GetBendAllowanceType();
	void SetBendAllowanceType(long nNewValue);
	CString GetBendTableFile();
	void SetBendTableFile(LPCTSTR lpszNewValue);
	double GetKFactor();
	void SetKFactor(double newValue);
	double GetBendAllowance();
	void SetBendAllowance(double newValue);
	BOOL GetUseDefaultBendRadius();
	void SetUseDefaultBendRadius(BOOL bNewValue);
	BOOL GetUseDefaultBendAllowance();
	void SetUseDefaultBendAllowance(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// IBaseFlangeFeatureData wrapper class

class IBaseFlangeFeatureData : public COleDispatchDriver
{
public:
	IBaseFlangeFeatureData() {}		// Calls COleDispatchDriver default constructor
	IBaseFlangeFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IBaseFlangeFeatureData(const IBaseFlangeFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	long GetOffsetDirections();
	void SetOffsetDirections(long nNewValue);
	long GetD1OffsetType();
	void SetD1OffsetType(long nNewValue);
	long GetD2OffsetType();
	void SetD2OffsetType(long nNewValue);
	long GetD1OffsetReferenceType();
	long GetD2OffsetReferenceType();
	LPDISPATCH GetD1OffsetReference();
	void SetD1OffsetReference(LPDISPATCH newValue);
	LPDISPATCH GetD2OffsetReference();
	void SetD2OffsetReference(LPDISPATCH newValue);
	double GetD1OffsetDistance();
	void SetD1OffsetDistance(double newValue);
	double GetD2OffsetDistance();
	void SetD2OffsetDistance(double newValue);
	double GetThickness();
	void SetThickness(double newValue);
	BOOL GetReverseThickness();
	void SetReverseThickness(BOOL bNewValue);
	double GetBendRadius();
	void SetBendRadius(double newValue);
	BOOL GetReverseDirection();
	void SetReverseDirection(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// IEdgeFlangeFeatureData wrapper class

class IEdgeFlangeFeatureData : public COleDispatchDriver
{
public:
	IEdgeFlangeFeatureData() {}		// Calls COleDispatchDriver default constructor
	IEdgeFlangeFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IEdgeFlangeFeatureData(const IEdgeFlangeFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetEdge();
	void SetEdge(LPDISPATCH newValue);
	BOOL GetUseDefaultBendRadius();
	void SetUseDefaultBendRadius(BOOL bNewValue);
	double GetBendRadius();
	void SetBendRadius(double newValue);
	double GetBendAngle();
	void SetBendAngle(double newValue);
	long GetOffsetType();
	void SetOffsetType(long nNewValue);
	LPDISPATCH GetOffsetReference();
	void SetOffsetReference(LPDISPATCH newValue);
	BOOL GetReverseOffset();
	void SetReverseOffset(BOOL bNewValue);
	double GetOffsetDistance();
	void SetOffsetDistance(double newValue);
	long GetOffsetDimType();
	void SetOffsetDimType(long nNewValue);
	long GetPositionType();
	void SetPositionType(long nNewValue);
	BOOL GetUsePositionTrimSideBends();
	void SetUsePositionTrimSideBends(BOOL bNewValue);
	BOOL GetUsePositionOffset();
	void SetUsePositionOffset(BOOL bNewValue);
	long GetPositionOffsetType();
	void SetPositionOffsetType(long nNewValue);
	long GetPositionReferenceType();
	LPDISPATCH GetPositionOffsetReference();
	void SetPositionOffsetReference(LPDISPATCH newValue);
	double GetPositionOffsetDistance();
	void SetPositionOffsetDistance(double newValue);
	BOOL GetReversePositionOffset();
	void SetReversePositionOffset(BOOL bNewValue);
	long GetAutoReliefType();
	void SetAutoReliefType(long nNewValue);
	BOOL GetUseReliefRatio();
	void SetUseReliefRatio(BOOL bNewValue);
	double GetReliefRatio();
	void SetReliefRatio(double newValue);
	double GetReliefWidth();
	void SetReliefWidth(double newValue);
	double GetReliefDepth();
	void SetReliefDepth(double newValue);
	long GetReliefTearType();
	void SetReliefTearType(long nNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// IMiterFlangeFeatureData wrapper class

class IMiterFlangeFeatureData : public COleDispatchDriver
{
public:
	IMiterFlangeFeatureData() {}		// Calls COleDispatchDriver default constructor
	IMiterFlangeFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IMiterFlangeFeatureData(const IMiterFlangeFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	VARIANT GetEdges();
	void SetEdges(const VARIANT& newValue);
	long IGetEdgesCount();
	LPDISPATCH IGetEdges();
	void ISetEdges(long edgeCount, LPDISPATCH* edgeArray);
	BOOL GetUseDefaultBendRadius();
	void SetUseDefaultBendRadius(BOOL bNewValue);
	double GetBendRadius();
	void SetBendRadius(double newValue);
	long GetPositionType();
	void SetPositionType(long nNewValue);
	BOOL GetUsePositionTrimSideBends();
	void SetUsePositionTrimSideBends(BOOL bNewValue);
	double GetGapDistance();
	void SetGapDistance(double newValue);
};
/////////////////////////////////////////////////////////////////////////////
// ISketchedBendFeatureData wrapper class

class ISketchedBendFeatureData : public COleDispatchDriver
{
public:
	ISketchedBendFeatureData() {}		// Calls COleDispatchDriver default constructor
	ISketchedBendFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISketchedBendFeatureData(const ISketchedBendFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetFixedFace(double* x, double* y, double* z);
	void SetFixedFace(double x, double y, double z, LPDISPATCH edgeArray);
	long GetPositionType();
	void SetPositionType(long nNewValue);
	BOOL GetReverseDirection();
	void SetReverseDirection(BOOL bNewValue);
	double GetBendAngle();
	void SetBendAngle(double newValue);
	BOOL GetUseDefaultBendRadius();
	void SetUseDefaultBendRadius(BOOL bNewValue);
	double GetBendRadius();
	void SetBendRadius(double newValue);
};
/////////////////////////////////////////////////////////////////////////////
// IClosedCornerFeatureData wrapper class

class IClosedCornerFeatureData : public COleDispatchDriver
{
public:
	IClosedCornerFeatureData() {}		// Calls COleDispatchDriver default constructor
	IClosedCornerFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IClosedCornerFeatureData(const IClosedCornerFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	VARIANT GetFaces();
	void SetFaces(const VARIANT& newValue);
	long IGetFacesCount();
	LPDISPATCH IGetFaces();
	void ISetFaces(long faceCount, LPDISPATCH* faceArray);
	long GetCornerType();
	void SetCornerType(long nNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// IFoldsFeatureData wrapper class

class IFoldsFeatureData : public COleDispatchDriver
{
public:
	IFoldsFeatureData() {}		// Calls COleDispatchDriver default constructor
	IFoldsFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IFoldsFeatureData(const IFoldsFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetFixedFace();
	void SetFixedFace(LPDISPATCH newValue);
	VARIANT GetBends();
	void SetBends(const VARIANT& newValue);
	long IGetBendsCount();
	LPDISPATCH IGetBends();
	void ISetBends(long faceCount, LPDISPATCH* faceArray);
};
/////////////////////////////////////////////////////////////////////////////
// IFlatPatternFeatureData wrapper class

class IFlatPatternFeatureData : public COleDispatchDriver
{
public:
	IFlatPatternFeatureData() {}		// Calls COleDispatchDriver default constructor
	IFlatPatternFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IFlatPatternFeatureData(const IFlatPatternFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetFixedFace();
	void SetFixedFace(LPDISPATCH newValue);
	BOOL GetMergeFace();
	void SetMergeFace(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// ILocalLinearPatternFeatureData wrapper class

class ILocalLinearPatternFeatureData : public COleDispatchDriver
{
public:
	ILocalLinearPatternFeatureData() {}		// Calls COleDispatchDriver default constructor
	ILocalLinearPatternFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ILocalLinearPatternFeatureData(const ILocalLinearPatternFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetD1Axis();
	void SetD1Axis(LPDISPATCH newValue);
	LPDISPATCH GetD2Axis();
	void SetD2Axis(LPDISPATCH newValue);
	long GetD1AxisType();
	long GetD2AxisType();
	BOOL GetD1ReverseDirection();
	void SetD1ReverseDirection(BOOL bNewValue);
	BOOL GetD2ReverseDirection();
	void SetD2ReverseDirection(BOOL bNewValue);
	double GetD1Spacing();
	void SetD1Spacing(double newValue);
	double GetD2Spacing();
	void SetD2Spacing(double newValue);
	long GetD1TotalInstances();
	void SetD1TotalInstances(long nNewValue);
	long GetD2TotalInstances();
	void SetD2TotalInstances(long nNewValue);
	VARIANT GetSeedComponentArray();
	void SetSeedComponentArray(const VARIANT& newValue);
	long GetSeedComponentCount();
	LPDISPATCH IGetSeedComponentArray();
	void ISetSeedComponentArray(long featCount, LPDISPATCH* ArrayDataIn);
	VARIANT GetSkippedItemArray();
	void SetSkippedItemArray(const VARIANT& newValue);
	long GetSkippedItemCount();
	long IGetSkippedItemArray();
	void ISetSkippedItemArray(long featCount, long* ArrayDataIn);
};
/////////////////////////////////////////////////////////////////////////////
// ILocalCircularPatternFeatureData wrapper class

class ILocalCircularPatternFeatureData : public COleDispatchDriver
{
public:
	ILocalCircularPatternFeatureData() {}		// Calls COleDispatchDriver default constructor
	ILocalCircularPatternFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ILocalCircularPatternFeatureData(const ILocalCircularPatternFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetAxis();
	void SetAxis(LPDISPATCH newValue);
	long GetAxisType();
	BOOL GetReverseDirection();
	void SetReverseDirection(BOOL bNewValue);
	double GetSpacing();
	void SetSpacing(double newValue);
	long GetTotalInstances();
	void SetTotalInstances(long nNewValue);
	VARIANT GetSeedComponentArray();
	void SetSeedComponentArray(const VARIANT& newValue);
	long GetSeedComponentCount();
	LPDISPATCH IGetSeedComponentArray();
	void ISetSeedComponentArray(long featCount, LPDISPATCH* ArrayDataIn);
	VARIANT GetSkippedItemArray();
	void SetSkippedItemArray(const VARIANT& newValue);
	long GetSkippedItemCount();
	long IGetSkippedItemArray();
	void ISetSkippedItemArray(long featCount, long* ArrayDataIn);
};
/////////////////////////////////////////////////////////////////////////////
// IDerivedPatternFeatureData wrapper class

class IDerivedPatternFeatureData : public COleDispatchDriver
{
public:
	IDerivedPatternFeatureData() {}		// Calls COleDispatchDriver default constructor
	IDerivedPatternFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IDerivedPatternFeatureData(const IDerivedPatternFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	LPDISPATCH GetPatternFeature();
	void SetPatternFeature(LPDISPATCH newValue);
	VARIANT GetSeedComponentArray();
	void SetSeedComponentArray(const VARIANT& newValue);
	long GetSeedComponentCount();
	LPDISPATCH IGetSeedComponentArray();
	void ISetSeedComponentArray(long featCount, LPDISPATCH* ArrayDataIn);
	VARIANT GetSkippedItemArray();
	void SetSkippedItemArray(const VARIANT& newValue);
	long GetSkippedItemCount();
	long IGetSkippedItemArray();
	void ISetSkippedItemArray(long featCount, long* ArrayDataIn);
};
/////////////////////////////////////////////////////////////////////////////
// IProjectionCurveFeatureData wrapper class

class IProjectionCurveFeatureData : public COleDispatchDriver
{
public:
	IProjectionCurveFeatureData() {}		// Calls COleDispatchDriver default constructor
	IProjectionCurveFeatureData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IProjectionCurveFeatureData(const IProjectionCurveFeatureData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	BOOL AccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	BOOL IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component);
	void ReleaseSelectionAccess();
	BOOL GetReverse();
	void SetReverse(BOOL bNewValue);
	VARIANT GetFaceArray();
	void SetFaceArray(const VARIANT& newValue);
	long GetFaceArrayCount();
	LPDISPATCH IGetFaceArray(long faceCount);
	void ISetFaceArray(long faceCount, LPDISPATCH* ArrayDataIn);
	LPDISPATCH GetSketch();
	void SetSketch(LPDISPATCH newValue);
};
/////////////////////////////////////////////////////////////////////////////
// DSldWorksEvents wrapper class

class DSldWorksEvents : public COleDispatchDriver
{
public:
	DSldWorksEvents() {}		// Calls COleDispatchDriver default constructor
	DSldWorksEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	DSldWorksEvents(const DSldWorksEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long FileOpenNotify(LPCTSTR fileName);
	long FileNewNotify(LPDISPATCH newDoc, long DocType);
	long DestroyNotify();
	long ActiveDocChangeNotify();
	long ActiveModelDocChangeNotify();
	long PropertySheetCreateNotify(LPDISPATCH sheet, long sheetType);
	long NonNativeFileOpenNotify(LPCTSTR fileName);
	long LightSheetCreateNotify(LPDISPATCH NewSheet, long sheetType, long lightId);
	long DocumentConversionNotify(LPCTSTR fileName);
	long DocumentLoadNotify(LPCTSTR docTitle, LPCTSTR docPath);
	long FileNewNotify2(LPDISPATCH newDoc, long DocType, LPCTSTR templateName);
	long FileOpenNotify2(LPCTSTR fileName);
	long ReferenceNotFoundNotify(LPCTSTR fileName);
	long PromptForFilenameNotify(long openOrSave, LPCTSTR suggestedFileName, long DocType, long Unused);
};
/////////////////////////////////////////////////////////////////////////////
// DPartDocEvents wrapper class

class DPartDocEvents : public COleDispatchDriver
{
public:
	DPartDocEvents() {}		// Calls COleDispatchDriver default constructor
	DPartDocEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	DPartDocEvents(const DPartDocEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long RegenNotify();
	long DestroyNotify();
	long RegenPostNotify();
	long ViewNewNotify();
	long NewSelectionNotify();
	long FileSaveNotify(LPCTSTR fileName);
	long FileSaveAsNotify(LPCTSTR fileName);
	long LoadFromStorageNotify();
	long SaveToStorageNotify();
	long ActiveConfigChangeNotify();
	long ActiveConfigChangePostNotify();
	long ViewNewNotify2(LPDISPATCH viewBeingAdded);
	long LightingDialogCreateNotify(LPDISPATCH dialog);
	long AddItemNotify(long entityType, LPCTSTR itemName);
	long RenameItemNotify(long entityType, LPCTSTR oldName, LPCTSTR newName);
	long DeleteItemNotify(long entityType, LPCTSTR itemName);
	long ModifyNotify();
	long FileReloadNotify();
	long AddCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType);
	long ChangeCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR oldValue, LPCTSTR newValue, long valueType);
	long DeleteCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType);
	long FeatureEditPreNotify(LPDISPATCH editFeature);
	long FeatureSketchEditPreNotify(LPDISPATCH editFeature, LPDISPATCH featureSketch);
	long FileSaveAsNotify2(LPCTSTR fileName);
	long DeleteSelectionPreNotify();
	long FileReloadPreNotify();
	long BodyVisibleChangeNotify();
};
/////////////////////////////////////////////////////////////////////////////
// DDrawingDocEvents wrapper class

class DDrawingDocEvents : public COleDispatchDriver
{
public:
	DDrawingDocEvents() {}		// Calls COleDispatchDriver default constructor
	DDrawingDocEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	DDrawingDocEvents(const DDrawingDocEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long RegenNotify();
	long DestroyNotify();
	long RegenPostNotify();
	long ViewNewNotify();
	long NewSelectionNotify();
	long FileSaveNotify(LPCTSTR fileName);
	long FileSaveAsNotify(LPCTSTR fileName);
	long LoadFromStorageNotify();
	long SaveToStorageNotify();
	long ActiveConfigChangeNotify();
	long ActiveConfigChangePostNotify();
	long ViewNewNotify2(LPDISPATCH viewBeingAdded);
	long AddItemNotify(long entityType, LPCTSTR itemName);
	long RenameItemNotify(long entityType, LPCTSTR oldName, LPCTSTR newName);
	long DeleteItemNotify(long entityType, LPCTSTR itemName);
	long ModifyNotify();
	long FileReloadNotify();
	long AddCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType);
	long ChangeCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR oldValue, LPCTSTR newValue, long valueType);
	long DeleteCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType);
	long FileSaveAsNotify2(LPCTSTR fileName);
	long DeleteSelectionPreNotify();
	long FileReloadPreNotify();
};
/////////////////////////////////////////////////////////////////////////////
// DAssemblyDocEvents wrapper class

class DAssemblyDocEvents : public COleDispatchDriver
{
public:
	DAssemblyDocEvents() {}		// Calls COleDispatchDriver default constructor
	DAssemblyDocEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	DAssemblyDocEvents(const DAssemblyDocEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long RegenNotify();
	long DestroyNotify();
	long RegenPostNotify();
	long ViewNewNotify();
	long NewSelectionNotify();
	long FileSaveNotify(LPCTSTR fileName);
	long FileSaveAsNotify(LPCTSTR fileName);
	long LoadFromStorageNotify();
	long SaveToStorageNotify();
	long ActiveConfigChangeNotify();
	long ActiveConfigChangePostNotify();
	long BeginInContextEditNotify(LPDISPATCH docBeingEdited, long DocType);
	long EndInContextEditNotify(LPDISPATCH docBeingEdited, long DocType);
	long ViewNewNotify2(LPDISPATCH viewBeingAdded);
	long LightingDialogCreateNotify(LPDISPATCH dialog);
	long AddItemNotify(long entityType, LPCTSTR itemName);
	long RenameItemNotify(long entityType, LPCTSTR oldName, LPCTSTR newName);
	long DeleteItemNotify(long entityType, LPCTSTR itemName);
	long ModifyNotify();
	long ComponentStateChangeNotify(LPDISPATCH componentModel, short oldCompState, short newCompState);
	long FileDropNotify(LPCTSTR fileName);
	long FileReloadNotify();
	long ComponentStateChangeNotify2(LPDISPATCH componentModel, LPCTSTR compName, short oldCompState, short newCompState);
	long AddCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType);
	long ChangeCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR oldValue, LPCTSTR newValue, long valueType);
	long DeleteCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType);
	long FeatureEditPreNotify(LPDISPATCH editFeature);
	long FeatureSketchEditPreNotify(LPDISPATCH editFeature, LPDISPATCH featureSketch);
	long FileSaveAsNotify2(LPCTSTR fileName);
	long InterferenceNotify(VARIANT* pComp, VARIANT* pFace);
	long DeleteSelectionPreNotify();
	long FileReloadPreNotify();
	long ComponentMoveNotify();
	long ComponentVisibleChangeNotify();
	long BodyVisibleChangeNotify();
};
/////////////////////////////////////////////////////////////////////////////
// DModelViewEvents wrapper class

class DModelViewEvents : public COleDispatchDriver
{
public:
	DModelViewEvents() {}		// Calls COleDispatchDriver default constructor
	DModelViewEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	DModelViewEvents(const DModelViewEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long RepaintNotify(long paintType);
	long ViewChangeNotify(const VARIANT& view);
	long DestroyNotify();
	long RepaintPostNotify();
	long BufferSwapNotify();
	long DestroyNotify2(long destroyType);
};
/////////////////////////////////////////////////////////////////////////////
// DFeatMgrViewEvents wrapper class

class DFeatMgrViewEvents : public COleDispatchDriver
{
public:
	DFeatMgrViewEvents() {}		// Calls COleDispatchDriver default constructor
	DFeatMgrViewEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	DFeatMgrViewEvents(const DFeatMgrViewEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long ActivateNotify(VARIANT* view);
	long DeactivateNotify(VARIANT* view);
	long DestroyNotify(VARIANT* view);
};
/////////////////////////////////////////////////////////////////////////////
// DSWPropertySheetEvents wrapper class

class DSWPropertySheetEvents : public COleDispatchDriver
{
public:
	DSWPropertySheetEvents() {}		// Calls COleDispatchDriver default constructor
	DSWPropertySheetEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	DSWPropertySheetEvents(const DSWPropertySheetEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long DestroyNotify();
	long HelpNotify(long page);
};
/////////////////////////////////////////////////////////////////////////////
// ISdmDoc wrapper class

class ISdmDoc : public COleDispatchDriver
{
public:
	ISdmDoc() {}		// Calls COleDispatchDriver default constructor
	ISdmDoc(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	ISdmDoc(const ISdmDoc& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
};
