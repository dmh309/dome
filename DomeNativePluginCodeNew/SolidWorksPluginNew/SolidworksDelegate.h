//----------------------------------------------------------------------------
// SolidworksDelegate.h
//
// *********** PLUGIN AUTHOR MODIFIES EVERYTHING BELOW THIS LINE ************
// ***********          (EXCEPT WHERE NOTED) AS NEEDED           ************
//----------------------------------------------------------------------------


#ifndef DOME_SolidworksDelegate_H
#define DOME_SolidworksDelegate_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "SolidworksModel.h"
using namespace DOME::SolidworksPlugin;


class SolidworksDelegate
{
public:
	SolidworksDelegate () {}


	//////////////////////////////////////////////////////////////////////////////////////
	// entry points to this class
	//////////////////////////////////////////////////////////////////////////////////////
	//
	// *********** PLUGIN AUTHOR DOES NOT MODIFY THIS SECTION (begin) ************

	void     callVoidFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								long iObjectPtr, unsigned int iFunctionIndex);

	int      callIntegerFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	double	 callDoubleFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	bool     callBooleanFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	const
	char*    callStringFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	long     callObjectFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	vector<int>
		callIntegerArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								   long iObjectPtr, unsigned int iFunctionIndex);

	vector<double>
		callDoubleArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
								  long iObjectPtr, unsigned int iFunctionIndex);

	vector< vector<int> >
		call2DIntegerArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
									 long iObjectPtr, unsigned int iFunctionIndex);

	vector< vector<double> > 
		call2DDoubleArrayFunctions (JNIEnv* env, jobjectArray args, unsigned int iNumArgs,
									long iObjectPtr, unsigned int iFunctionIndex);

	// *********** PLUGIN AUTHOR DOES NOT MODIFY THIS SECTION (end) ************
	//////////////////////////////////////////////////////////////////////////////////////





	//////////////////////////////////////////////////////////////////////////////////////
	// object functions
	//////////////////////////////////////////////////////////////////////////////////////

	SolidworksModel  *initializeModel (JNIEnv* env, jobjectArray args, unsigned int iNumArgs);

	SolidworksData   *createModelObject (long iObjectPtr,unsigned int iFunctionIndex);

	SolidworksData   *createDimension (JNIEnv* env, jobjectArray args, 
								unsigned int iNumArgs, long iObjectPtr);

	SolidworksData   *createFile (JNIEnv* env, jobjectArray args, 
								unsigned int iNumArgs, long iObjectPtr);

	//////////////////////////////////////////////////////////////////////////////////////
	// void functions
	//////////////////////////////////////////////////////////////////////////////////////

	void	executeModel (long iObjectPtr);
	void	loadModel (long iObjectPtr);
	void	unloadModel (JNIEnv* env, jobjectArray args,
									  unsigned int iNumArgs, long iObjectPtr);
	void	destroyModel (long iObjectPtr);
	void	setRealValue (JNIEnv* env, jobjectArray args,
						  unsigned int iNumArgs, long iObjectPtr);

	//////////////////////////////////////////////////////////////////////////////////////
	// real functions
	//////////////////////////////////////////////////////////////////////////////////////

	double	getDoubleValue (long iObjectPtr, unsigned int iFunctionIndex);

	//////////////////////////////////////////////////////////////////////////////////////
	// integer functions
	//////////////////////////////////////////////////////////////////////////////////////

	int		getIntegerValue (long iObjectPtr, unsigned int iFunctionIndex);


	//////////////////////////////////////////////////////////////////////////////////////
	// boolean functions
	//////////////////////////////////////////////////////////////////////////////////////

	bool	getBooleanValue (long iObjectPtr);
	bool	isModelLoaded (long iObjectPtr);

	
	//////////////////////////////////////////////////////////////////////////////////////
	// string functions
	//////////////////////////////////////////////////////////////////////////////////////

	const
	char*	getStringValue (long iObjectPtr, unsigned int iFunctionIndex);

	
	//////////////////////////////////////////////////////////////////////////////////////
	// 1D array functions
	//////////////////////////////////////////////////////////////////////////////////////

	vector<double> 
			getDoubleArrayValue (long iObjectPtr);


};


#endif // DOME_SolidworksDelegate_H
