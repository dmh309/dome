// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include "stdafx.h"
#include "swdisp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// ISldWorks properties

/////////////////////////////////////////////////////////////////////////////
// ISldWorks operations

LPDISPATCH ISldWorks::GetActiveDoc()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::GetIActiveDoc()
{
	LPDISPATCH result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::OpenDoc(LPCTSTR Name, long type)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name, type);
	return result;
}

LPDISPATCH ISldWorks::IOpenDoc(LPCTSTR Name, long type)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name, type);
	return result;
}

LPDISPATCH ISldWorks::ActivateDoc(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH ISldWorks::IActivateDoc(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

void ISldWorks::SendMsgToUser(LPCTSTR Message)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Message);
}

LPDISPATCH ISldWorks::Frame()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::IFrameObject()
{
	LPDISPATCH result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ISldWorks::ExitApp()
{
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void ISldWorks::CloseDoc(LPCTSTR Name)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Name);
}

LPDISPATCH ISldWorks::NewPart()
{
	LPDISPATCH result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::INewPart()
{
	LPDISPATCH result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::NewAssembly()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::INewAssembly()
{
	LPDISPATCH result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::NewDrawing(long templateToUse)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		templateToUse);
	return result;
}

LPDISPATCH ISldWorks::INewDrawing(long templateToUse)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		templateToUse);
	return result;
}

long ISldWorks::DateCode()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString ISldWorks::RevisionNumber()
{
	CString result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL ISldWorks::LoadFile(LPCTSTR fileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName);
	return result;
}

BOOL ISldWorks::AddFileOpenItem(LPCTSTR CallbackFcnAndModule, LPCTSTR Description)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		CallbackFcnAndModule, Description);
	return result;
}

BOOL ISldWorks::AddFileSaveAsItem(LPCTSTR CallbackFcnAndModule, LPCTSTR Description, long type)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		CallbackFcnAndModule, Description, type);
	return result;
}

void ISldWorks::PreSelectDwgTemplateSize(long templateToUse, LPCTSTR templateName)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 templateToUse, templateName);
}

void ISldWorks::DocumentVisible(BOOL Visible, long type)
{
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Visible, type);
}

LPDISPATCH ISldWorks::DefineAttribute(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH ISldWorks::IDefineAttribute(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

BOOL ISldWorks::GetVisible()
{
	BOOL result;
	InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISldWorks::SetVisible(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ISldWorks::GetUserControl()
{
	BOOL result;
	InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISldWorks::SetUserControl(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

void ISldWorks::DisplayStatusBar(BOOL onOff)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 onOff);
}

void ISldWorks::CreateNewWindow()
{
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void ISldWorks::ArrangeIcons()
{
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void ISldWorks::ArrangeWindows(long Style)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Style);
}

void ISldWorks::QuitDoc(LPCTSTR Name)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Name);
}

LPDISPATCH ISldWorks::GetModeler()
{
	LPDISPATCH result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::IGetModeler()
{
	LPDISPATCH result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::GetEnvironment()
{
	LPDISPATCH result;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::IGetEnvironment()
{
	LPDISPATCH result;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::NewDrawing2(long templateToUse, LPCTSTR templateName, long paperSize, double Width, double height)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I4 VTS_R8 VTS_R8;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		templateToUse, templateName, paperSize, Width, height);
	return result;
}

LPDISPATCH ISldWorks::INewDrawing2(long templateToUse, LPCTSTR templateName, long paperSize, double Width, double height)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I4 VTS_R8 VTS_R8;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		templateToUse, templateName, paperSize, Width, height);
	return result;
}

BOOL ISldWorks::SetOptions(LPCTSTR Message)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Message);
	return result;
}

BOOL ISldWorks::PreviewDoc(long* hWnd, LPCTSTR FullName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PI4 VTS_BSTR;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		hWnd, FullName);
	return result;
}

CString ISldWorks::GetSearchFolders(long folderType)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		folderType);
	return result;
}

BOOL ISldWorks::SetSearchFolders(long folderType, LPCTSTR folders)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		folderType, folders);
	return result;
}

BOOL ISldWorks::GetUserPreferenceToggle(long userPreferenceToggle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreferenceToggle);
	return result;
}

void ISldWorks::SetUserPreferenceToggle(long userPreferenceValue, BOOL onFlag)
{
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 userPreferenceValue, onFlag);
}

double ISldWorks::GetUserPreferenceDoubleValue(long userPreferenceValue)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		userPreferenceValue);
	return result;
}

BOOL ISldWorks::SetUserPreferenceDoubleValue(long userPreferenceValue, double Value)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_R8;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreferenceValue, Value);
	return result;
}

CString ISldWorks::GetActivePrinter()
{
	CString result;
	InvokeHelper(0x30, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ISldWorks::SetActivePrinter(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x30, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

BOOL ISldWorks::LoadFile2(LPCTSTR fileName, LPCTSTR ArgString)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName, ArgString);
	return result;
}

long ISldWorks::GetUserPreferenceIntegerValue(long userPreferenceValue)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		userPreferenceValue);
	return result;
}

BOOL ISldWorks::SetUserPreferenceIntegerValue(long userPreferenceValue, long Value)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreferenceValue, Value);
	return result;
}

BOOL ISldWorks::RemoveMenuPopupItem(long DocType, long SelectType, LPCTSTR Item, LPCTSTR CallbackFcnAndModule, LPCTSTR CustomNames, long Unused)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		DocType, SelectType, Item, CallbackFcnAndModule, CustomNames, Unused);
	return result;
}

BOOL ISldWorks::RemoveMenu(long DocType, LPCTSTR MenuItemString, LPCTSTR CallbackFcnAndModule)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		DocType, MenuItemString, CallbackFcnAndModule);
	return result;
}

BOOL ISldWorks::RemoveFileOpenItem(LPCTSTR CallbackFcnAndModule, LPCTSTR Description)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		CallbackFcnAndModule, Description);
	return result;
}

BOOL ISldWorks::RemoveFileSaveAsItem(LPCTSTR CallbackFcnAndModule, LPCTSTR Description, long type)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		CallbackFcnAndModule, Description, type);
	return result;
}

BOOL ISldWorks::ReplaceReferencedDocument(LPCTSTR referencingDocument, LPCTSTR referencedDocument, LPCTSTR newReference)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		referencingDocument, referencedDocument, newReference);
	return result;
}

long ISldWorks::AddMenuItem(long DocType, LPCTSTR Menu, long Postion, LPCTSTR CallbackModuleAndFcn)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		DocType, Menu, Postion, CallbackModuleAndFcn);
	return result;
}

long ISldWorks::AddMenuPopupItem(long DocType, long selType, LPCTSTR Item, LPCTSTR CallbackFcnAndModule, LPCTSTR CustomNames)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		DocType, selType, Item, CallbackFcnAndModule, CustomNames);
	return result;
}

BOOL ISldWorks::RemoveUserMenu(long DocType, long menuIdIn, LPCTSTR moduleName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		DocType, menuIdIn, moduleName);
	return result;
}

long ISldWorks::AddToolbar(LPCTSTR moduleName, LPCTSTR title, long smallBitmapHandle, long largeBitmapHandle)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		moduleName, title, smallBitmapHandle, largeBitmapHandle);
	return result;
}

BOOL ISldWorks::AddToolbarCommand(LPCTSTR moduleName, long toolbarId, long toolbarIndex, LPCTSTR commandString)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_BSTR;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		moduleName, toolbarId, toolbarIndex, commandString);
	return result;
}

BOOL ISldWorks::ShowToolbar(LPCTSTR moduleName, long toolbarId)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		moduleName, toolbarId);
	return result;
}

BOOL ISldWorks::HideToolbar(LPCTSTR moduleName, long toolbarId)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		moduleName, toolbarId);
	return result;
}

BOOL ISldWorks::RemoveToolbar(LPCTSTR Module, long toolbarId)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Module, toolbarId);
	return result;
}

BOOL ISldWorks::GetToolbarState(LPCTSTR Module, long toolbarId, long toolbarState)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Module, toolbarId, toolbarState);
	return result;
}

CString ISldWorks::GetUserPreferenceStringListValue(long userPreference)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		userPreference);
	return result;
}

void ISldWorks::SetUserPreferenceStringListValue(long userPreference, LPCTSTR Value)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x43, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 userPreference, Value);
}

BOOL ISldWorks::EnableStereoDisplay(BOOL bEnable)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bEnable);
	return result;
}

BOOL ISldWorks::IEnableStereoDisplay(BOOL bEnable)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bEnable);
	return result;
}

VARIANT ISldWorks::GetDocumentDependencies(LPCTSTR document, long traverseflag, long searchflag)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0x46, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		document, traverseflag, searchflag);
	return result;
}

CString ISldWorks::IGetDocumentDependencies(LPCTSTR document, long traverseflag, long searchflag)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0x47, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		document, traverseflag, searchflag);
	return result;
}

long ISldWorks::GetDocumentDependenciesCount(LPCTSTR document, long traverseflag, long searchflag)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0x48, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		document, traverseflag, searchflag);
	return result;
}

LPDISPATCH ISldWorks::OpenDocSilent(LPCTSTR fileName, long type, long* errors)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_PI4;
	InvokeHelper(0x49, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fileName, type, errors);
	return result;
}

LPDISPATCH ISldWorks::IOpenDocSilent(LPCTSTR fileName, long type, long* errors)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_PI4;
	InvokeHelper(0x4a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fileName, type, errors);
	return result;
}

long ISldWorks::CallBack(LPCTSTR callBackFunc, long defaultRetVal, LPCTSTR callBackArgs)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x4b, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		callBackFunc, defaultRetVal, callBackArgs);
	return result;
}

long ISldWorks::SendMsgToUser2(LPCTSTR Message, long icon, long buttons)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0x4c, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Message, icon, buttons);
	return result;
}

LPUNKNOWN ISldWorks::EnumDocuments()
{
	LPUNKNOWN result;
	InvokeHelper(0x4d, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

long ISldWorks::LoadAddIn(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4e, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long ISldWorks::UnloadAddIn(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

BOOL ISldWorks::RecordLine(LPCTSTR text)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x50, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		text);
	return result;
}

VARIANT ISldWorks::VersionHistory(LPCTSTR fileName)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x51, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		fileName);
	return result;
}

CString ISldWorks::IVersionHistory(LPCTSTR fileName)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x52, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		fileName);
	return result;
}

long ISldWorks::IGetVersionHistoryCount(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x53, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

BOOL ISldWorks::AllowFailedFeatureCreation(BOOL yesNo)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x54, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		yesNo);
	return result;
}

LPDISPATCH ISldWorks::GetFirstDocument()
{
	LPDISPATCH result;
	InvokeHelper(0x55, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString ISldWorks::GetCurrentWorkingDirectory()
{
	CString result;
	InvokeHelper(0x56, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL ISldWorks::SetCurrentWorkingDirectory(LPCTSTR currentWorkingDirectory)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x57, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		currentWorkingDirectory);
	return result;
}

CString ISldWorks::GetDataFolder(BOOL bShowErrorMsg)
{
	CString result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x58, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		bShowErrorMsg);
	return result;
}

BOOL ISldWorks::GetSelectionFilter(long selType)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x59, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		selType);
	return result;
}

void ISldWorks::SetSelectionFilter(long selType, BOOL state)
{
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0x5a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 selType, state);
}

LPDISPATCH ISldWorks::ActivateDoc2(LPCTSTR Name, BOOL silent, long* errors)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL VTS_PI4;
	InvokeHelper(0x5b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name, silent, errors);
	return result;
}

LPDISPATCH ISldWorks::IActivateDoc2(LPCTSTR Name, BOOL silent, long* errors)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL VTS_PI4;
	InvokeHelper(0x5c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name, silent, errors);
	return result;
}

BOOL ISldWorks::GetMouseDragMode(long command)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		command);
	return result;
}

CString ISldWorks::GetCurrentLanguage()
{
	CString result;
	InvokeHelper(0x5e, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::IGetFirstDocument()
{
	LPDISPATCH result;
	InvokeHelper(0x5f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL ISldWorks::SanityCheck(long swItemToCheck, long* P1, long* P2)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4 VTS_PI4;
	InvokeHelper(0x60, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		swItemToCheck, P1, P2);
	return result;
}

long ISldWorks::AddMenu(long DocType, LPCTSTR Menu, long position)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I4;
	InvokeHelper(0x61, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		DocType, Menu, position);
	return result;
}

long ISldWorks::CheckpointConvertedDocument(LPCTSTR docName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x62, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		docName);
	return result;
}

LPDISPATCH ISldWorks::OpenDoc2(LPCTSTR fileName, long type, BOOL ReadOnly, BOOL viewOnly, BOOL silent, long* errors)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_PI4;
	InvokeHelper(0x63, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fileName, type, ReadOnly, viewOnly, silent, errors);
	return result;
}

LPDISPATCH ISldWorks::IOpenDoc2(LPCTSTR fileName, long type, BOOL ReadOnly, BOOL viewOnly, BOOL silent, long* errors)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_PI4;
	InvokeHelper(0x64, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fileName, type, ReadOnly, viewOnly, silent, errors);
	return result;
}

VARIANT ISldWorks::GetMassProperties(LPCTSTR filePathName, LPCTSTR configurationName)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x65, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		filePathName, configurationName);
	return result;
}

BOOL ISldWorks::IGetMassProperties(LPCTSTR filePathName, LPCTSTR configurationName, double* mPropsData)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_PR8;
	InvokeHelper(0x66, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		filePathName, configurationName, mPropsData);
	return result;
}

CString ISldWorks::GetLocalizedMenuName(long menuId)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x67, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		menuId);
	return result;
}

VARIANT ISldWorks::GetDocumentDependencies2(LPCTSTR document, BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x68, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		document, traverseflag, searchflag, addReadOnlyInfo);
	return result;
}

CString ISldWorks::IGetDocumentDependencies2(LPCTSTR document, BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x69, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		document, traverseflag, searchflag, addReadOnlyInfo);
	return result;
}

long ISldWorks::IGetDocumentDependenciesCount2(LPCTSTR document, BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x6a, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		document, traverseflag, searchflag, addReadOnlyInfo);
	return result;
}

VARIANT ISldWorks::GetSelectionFilters()
{
	VARIANT result;
	InvokeHelper(0x6b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ISldWorks::SetSelectionFilters(const VARIANT& selType, BOOL state)
{
	static BYTE parms[] =
		VTS_VARIANT VTS_BOOL;
	InvokeHelper(0x6c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 &selType, state);
}

BOOL ISldWorks::GetApplySelectionFilter()
{
	BOOL result;
	InvokeHelper(0x6d, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISldWorks::SetApplySelectionFilter(BOOL state)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 state);
}

LPDISPATCH ISldWorks::NewDocument(LPCTSTR templateName, long paperSize, double Width, double height)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_R8 VTS_R8;
	InvokeHelper(0x6f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		templateName, paperSize, Width, height);
	return result;
}

LPDISPATCH ISldWorks::INewDocument(LPCTSTR templateName, long paperSize, double Width, double height)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_R8 VTS_R8;
	InvokeHelper(0x70, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		templateName, paperSize, Width, height);
	return result;
}

CString ISldWorks::GetDocumentTemplate(long mode, LPCTSTR templateName, long paperSize, double Width, double height)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I4 VTS_R8 VTS_R8;
	InvokeHelper(0x71, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		mode, templateName, paperSize, Width, height);
	return result;
}

long ISldWorks::IGetSelectionFiltersCount()
{
	long result;
	InvokeHelper(0x72, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ISldWorks::IGetSelectionFilters()
{
	long result;
	InvokeHelper(0x73, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void ISldWorks::ISetSelectionFilters(long count, long* selType, BOOL state)
{
	static BYTE parms[] =
		VTS_I4 VTS_PI4 VTS_BOOL;
	InvokeHelper(0x74, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 count, selType, state);
}

CString ISldWorks::GetCurrSolidWorksRegSubKey()
{
	CString result;
	InvokeHelper(0x75, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ISldWorks::SolidWorksExplorer()
{
	InvokeHelper(0x76, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString ISldWorks::GetUserPreferenceStringValue(long userPreference)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x77, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		userPreference);
	return result;
}

BOOL ISldWorks::SetUserPreferenceStringValue(long userPreference, LPCTSTR Value)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x78, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreference, Value);
	return result;
}

CString ISldWorks::GetCurrentMacroPathName()
{
	CString result;
	InvokeHelper(0x79, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::GetOpenDocumentByName(LPCTSTR documentName)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		documentName);
	return result;
}

LPDISPATCH ISldWorks::IGetOpenDocumentByName(LPCTSTR documentName)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		documentName);
	return result;
}

void ISldWorks::GetCurrentKernelVersions(BSTR* version1, BSTR* version2, BSTR* version3)
{
	static BYTE parms[] =
		VTS_PBSTR VTS_PBSTR VTS_PBSTR;
	InvokeHelper(0x7c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 version1, version2, version3);
}

CString ISldWorks::CreatePrunedModelArchive(LPCTSTR pathname, LPCTSTR zipPathName)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x7d, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		pathname, zipPathName);
	return result;
}

LPDISPATCH ISldWorks::OpenDoc3(LPCTSTR fileName, long type, BOOL ReadOnly, BOOL viewOnly, BOOL RapidDraft, BOOL silent, long* errors)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_PI4;
	InvokeHelper(0x7e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fileName, type, ReadOnly, viewOnly, RapidDraft, silent, errors);
	return result;
}

LPDISPATCH ISldWorks::IOpenDoc3(LPCTSTR fileName, long type, BOOL ReadOnly, BOOL viewOnly, BOOL RapidDraft, BOOL silent, long* errors)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_PI4;
	InvokeHelper(0x7f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fileName, type, ReadOnly, viewOnly, RapidDraft, silent, errors);
	return result;
}

long ISldWorks::AddToolbar2(LPCTSTR moduleNameIn, LPCTSTR titleIn, long smallBitmapHandleIn, long largeBitmapHandleIn, long menuPosIn, long decTemplateTypeIn)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x80, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		moduleNameIn, titleIn, smallBitmapHandleIn, largeBitmapHandleIn, menuPosIn, decTemplateTypeIn);
	return result;
}

LPDISPATCH ISldWorks::OpenModelConfiguration(LPCTSTR pathname, LPCTSTR configName)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x81, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		pathname, configName);
	return result;
}

long ISldWorks::GetToolbarDock(LPCTSTR ModuleIn, long toolbarIDIn)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x82, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ModuleIn, toolbarIDIn);
	return result;
}

void ISldWorks::SetToolbarDock(LPCTSTR ModuleIn, long toolbarIDIn, long docStatePosIn)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0x83, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ModuleIn, toolbarIDIn, docStatePosIn);
}

LPDISPATCH ISldWorks::GetMathUtility()
{
	LPDISPATCH result;
	InvokeHelper(0x84, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::IGetMathUtility()
{
	LPDISPATCH result;
	InvokeHelper(0x85, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::OpenDoc4(LPCTSTR fileName, long type, long options, LPCTSTR configuration, long* errors)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_BSTR VTS_PI4;
	InvokeHelper(0x86, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fileName, type, options, configuration, errors);
	return result;
}

LPDISPATCH ISldWorks::IOpenDoc4(LPCTSTR fileName, long type, long options, LPCTSTR configuration, long* errors)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_BSTR VTS_PI4;
	InvokeHelper(0x87, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fileName, type, options, configuration, errors);
	return result;
}

BOOL ISldWorks::IsRapidDraft(LPCTSTR fileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x88, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName);
	return result;
}

VARIANT ISldWorks::GetTemplateSizes(LPCTSTR fileName)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x89, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		fileName);
	return result;
}

BOOL ISldWorks::IGetTemplateSizes(LPCTSTR fileName, long* paperSize, double* Width, double* height)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_PI4 VTS_PR8 VTS_PR8;
	InvokeHelper(0x8a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName, paperSize, Width, height);
	return result;
}

LPDISPATCH ISldWorks::GetColorTable()
{
	LPDISPATCH result;
	InvokeHelper(0x8b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISldWorks::IGetColorTable()
{
	LPDISPATCH result;
	InvokeHelper(0x8c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ISldWorks::SetMissingReferencePathName(LPCTSTR fileName)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 fileName);
}

LPDISPATCH ISldWorks::GetUserUnit(long UnitType)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		UnitType);
	return result;
}

LPDISPATCH ISldWorks::IGetUserUnit(long UnitType)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		UnitType);
	return result;
}

BOOL ISldWorks::SetMouseDragMode(long command)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x90, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		command);
	return result;
}

void ISldWorks::SetPromptFilename(LPCTSTR fileName)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x91, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 fileName);
}


/////////////////////////////////////////////////////////////////////////////
// IModelDoc properties

/////////////////////////////////////////////////////////////////////////////
// IModelDoc operations

LPDISPATCH IModelDoc::GetSelectionManager()
{
	LPDISPATCH result;
	InvokeHelper(0x10001, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::GetISelectionManager()
{
	LPDISPATCH result;
	InvokeHelper(0x100af, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetSelectionManager(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x10001, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH IModelDoc::GetActiveView()
{
	LPDISPATCH result;
	InvokeHelper(0x10002, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::GetIActiveView()
{
	LPDISPATCH result;
	InvokeHelper(0x100b0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetActiveView(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x10002, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IModelDoc::GetLengthUnit()
{
	long result;
	InvokeHelper(0x10003, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetLengthUnit(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10003, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

CString IModelDoc::GetLightSourceUserName(long Id)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100b4, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms,
		Id);
	return result;
}

void IModelDoc::SetLightSourceUserName(long Id, LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x100b4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 Id, lpszNewValue);
}

VARIANT IModelDoc::GetLightSourcePropertyValues(long Id)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100b5, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms,
		Id);
	return result;
}

void IModelDoc::SetLightSourcePropertyValues(long Id, const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT;
	InvokeHelper(0x100b5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 Id, &newValue);
}

CString IModelDoc::GetSceneName()
{
	CString result;
	InvokeHelper(0x100b6, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetSceneName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x100b6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString IModelDoc::GetSceneUserName()
{
	CString result;
	InvokeHelper(0x100b7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetSceneUserName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x100b7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

void IModelDoc::FeatureFillet(double r1, BOOL propagate, BOOL ftyp, BOOL varRadTyp, long overFlowType)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x10004, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 r1, propagate, ftyp, varRadTyp, overFlowType);
}

void IModelDoc::GridOptions(BOOL dispGrid, double gridSpacing, BOOL snap, BOOL dotStyle, short nMajor, short nMinor, BOOL align2edge, BOOL angleSnap, double angleUnit, BOOL minorAuto)
{
	static BYTE parms[] =
		VTS_BOOL VTS_R8 VTS_BOOL VTS_BOOL VTS_I2 VTS_I2 VTS_BOOL VTS_BOOL VTS_R8 VTS_BOOL;
	InvokeHelper(0x10005, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 dispGrid, gridSpacing, snap, dotStyle, nMajor, nMinor, align2edge, angleSnap, angleUnit, minorAuto);
}

void IModelDoc::SetUnits(short uType, short fractBase, short fractDenom, short sigDigits, BOOL roundToFraction)
{
	static BYTE parms[] =
		VTS_I2 VTS_I2 VTS_I2 VTS_I2 VTS_BOOL;
	InvokeHelper(0x10006, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 uType, fractBase, fractDenom, sigDigits, roundToFraction);
}

void IModelDoc::LBDownAt(long flags, double x, double y, double z)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10007, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 flags, x, y, z);
}

void IModelDoc::LBUpAt(long flags, double x, double y, double z)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10008, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 flags, x, y, z);
}

void IModelDoc::DragTo(long flags, double x, double y, double z)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10009, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 flags, x, y, z);
}

void IModelDoc::SelectAt(long flags, double x, double y, double z)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1000a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 flags, x, y, z);
}

void IModelDoc::CreateLineVB(double x1, double y1, double z1, double x2, double y2, double z2)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1000b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x1, y1, z1, x2, y2, z2);
}

BOOL IModelDoc::CreateLine(const VARIANT& P1, const VARIANT& P2)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x1000c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&P1, &P2);
	return result;
}

void IModelDoc::CreateCenterLineVB(double x1, double y1, double z1, double x2, double y2, double z2)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1000d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x1, y1, z1, x2, y2, z2);
}

BOOL IModelDoc::CreateCenterLine(const VARIANT& P1, const VARIANT& P2)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x1000e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&P1, &P2);
	return result;
}

void IModelDoc::CreateArcVB(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z, short dir)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I2;
	InvokeHelper(0x1000f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z, dir);
}

BOOL IModelDoc::CreateArc(const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, short dir)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_I2;
	InvokeHelper(0x10010, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&P1, &P2, &P3, dir);
	return result;
}

void IModelDoc::CreateCircleVB(double p1x, double p1y, double p1z, double radius)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10011, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 p1x, p1y, p1z, radius);
}

BOOL IModelDoc::CreateCircleByRadius(const VARIANT& P1, double radius)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_R8;
	InvokeHelper(0x10012, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&P1, radius);
	return result;
}

VARIANT IModelDoc::GetLines()
{
	VARIANT result;
	InvokeHelper(0x10013, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IModelDoc::SketchTrim(long op, long selEnd, double x, double y)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_R8 VTS_R8;
	InvokeHelper(0x10014, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 op, selEnd, x, y);
}

void IModelDoc::SketchOffsetEdges(double val)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x10015, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 val);
}

void IModelDoc::SketchRectangle(double val1, double val2, double z1, double val3, double val4, double z2, BOOL val5)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL;
	InvokeHelper(0x10016, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 val1, val2, z1, val3, val4, z2, val5);
}

void IModelDoc::SketchPoint(double x, double y, double z)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10017, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x, y, z);
}

void IModelDoc::FeatureCut(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10018, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2);
}

void IModelDoc::FeatureBoss(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10019, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2);
}

void IModelDoc::SimpleHole(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1001a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2);
}

void IModelDoc::FeatureLinearPattern(long num1, double spacing1, long num2, double spacing2, BOOL flipDir1, BOOL flipDir2, LPCTSTR dName1, LPCTSTR dName2)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_I4 VTS_R8 VTS_BOOL VTS_BOOL VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1001b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 num1, spacing1, num2, spacing2, flipDir1, flipDir2, dName1, dName2);
}

void IModelDoc::NameView(LPCTSTR vName)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1001c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 vName);
}

void IModelDoc::ShowNamedView(LPCTSTR vName)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1001d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 vName);
}

void IModelDoc::CreatePlaneAtOffset(double val, BOOL flipDir)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x1001e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 val, flipDir);
}

void IModelDoc::Toolbars(BOOL m, BOOL vw, BOOL skMain, BOOL sk, BOOL feat, BOOL constr, BOOL macro)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1001f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 m, vw, skMain, sk, feat, constr, macro);
}

void IModelDoc::CreatePlaneAtAngle(double val, BOOL flipDir)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x10020, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 val, flipDir);
}

void IModelDoc::SetParamValue(double val)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x10021, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 val);
}

void IModelDoc::AddRelation(LPCTSTR relStr)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10022, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 relStr);
}

void IModelDoc::DeleteAllRelations()
{
	InvokeHelper(0x10023, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::HoleWizard(double depth, short endType, BOOL flip, BOOL dir, long hType, double d1, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12)
{
	static BYTE parms[] =
		VTS_R8 VTS_I2 VTS_BOOL VTS_BOOL VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10024, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 depth, endType, flip, dir, hType, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12);
}

BOOL IModelDoc::SaveAs(LPCTSTR newName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10025, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		newName);
	return result;
}

void IModelDoc::ActivateSelectedFeature()
{
	InvokeHelper(0x10026, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SelectByName(long flags, LPCTSTR idStr)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x10027, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 flags, idStr);
}

void IModelDoc::SketchAddConstraints(LPCTSTR idStr)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10028, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 idStr);
}

void IModelDoc::SketchConstraintsDel(long constrInd, LPCTSTR idStr)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x10029, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 constrInd, idStr);
}

void IModelDoc::SketchConstraintsDelAll()
{
	InvokeHelper(0x1002a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::Lock()
{
	InvokeHelper(0x1002b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::UnLock()
{
	InvokeHelper(0x1002c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertFeatureShell(double thickness, BOOL outward)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x1002d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 thickness, outward);
}

void IModelDoc::SketchFillet(double rad)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1002e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 rad);
}

void IModelDoc::FeatureChamfer(double Width, double angle, BOOL flip)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_BOOL;
	InvokeHelper(0x1002f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width, angle, flip);
}

void IModelDoc::InsertMfDraft(double angle, BOOL flipDir, BOOL isEdgeDraft, long propType)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x10030, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 angle, flipDir, isEdgeDraft, propType);
}

void IModelDoc::ParentChildRelationship()
{
	InvokeHelper(0x10031, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchSpline(long morePts, double x, double y, double z)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10032, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 morePts, x, y, z);
}

void IModelDoc::SelectSketchPoint(double x, double y, long incidence)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_I4;
	InvokeHelper(0x10033, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x, y, incidence);
}

void IModelDoc::SelectSketchLine(double x0, double y0, long inc0, double x1, double y1, long inc1)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_I4 VTS_R8 VTS_R8 VTS_I4;
	InvokeHelper(0x10034, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x0, y0, inc0, x1, y1, inc1);
}

void IModelDoc::SelectSketchArc(double x0, double y0, long inc0, double x1, double y1, long inc1, double xC, double yC, long incC, long rotDir)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_I4;
	InvokeHelper(0x10035, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x0, y0, inc0, x1, y1, inc1, xC, yC, incC, rotDir);
}

void IModelDoc::SelectSketchSpline(long size, double x0, double y0, long inc0, double x1, double y1, long inc1, double xC, double yC, long incC)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_R8 VTS_R8 VTS_I4;
	InvokeHelper(0x10036, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 size, x0, y0, inc0, x1, y1, inc1, xC, yC, incC);
}

BOOL IModelDoc::CreateTangentArc(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10037, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		p1x, p1y, p1z, p2x, p2y, p2z);
	return result;
}

BOOL IModelDoc::Create3PointArc(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10038, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z);
	return result;
}

BOOL IModelDoc::CreateArcByCenter(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10039, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z);
	return result;
}

BOOL IModelDoc::CreateCircle(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1003a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		p1x, p1y, p1z, p2x, p2y, p2z);
	return result;
}

BOOL IModelDoc::AddDimension(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1003b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

BOOL IModelDoc::AddHorizontalDimension(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1003c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

BOOL IModelDoc::AddVerticalDimension(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1003d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

BOOL IModelDoc::SelectSketchItem(long selOpt, LPCTSTR Name, double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1003e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		selOpt, Name, x, y, z);
	return result;
}

void IModelDoc::ClearSelection()
{
	InvokeHelper(0x1003f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::Select(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10040, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 selID, selParams, x, y, z);
}

void IModelDoc::AndSelect(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10041, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 selID, selParams, x, y, z);
}

BOOL IModelDoc::CreatePoint(double pointX, double pointY, double pointZ)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10042, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		pointX, pointY, pointZ);
	return result;
}

BOOL IModelDoc::CreateLineDB(double sx, double sy, double sz, double ex, double ey, double ez)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10043, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		sx, sy, sz, ex, ey, ez);
	return result;
}

BOOL IModelDoc::CreateArcDB(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, short dir)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I2;
	InvokeHelper(0x10044, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x1, y1, z1, x2, y2, z2, x3, y3, z3, dir);
	return result;
}

BOOL IModelDoc::CreateCircleDB(double cx, double cy, double cz, double radius)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10045, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		cx, cy, cz, radius);
	return result;
}

BOOL IModelDoc::CreatePointDB(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10046, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

CString IModelDoc::GetTitle()
{
	CString result;
	InvokeHelper(0x10047, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

CString IModelDoc::GetPathName()
{
	CString result;
	InvokeHelper(0x10048, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IModelDoc::GetType()
{
	long result;
	InvokeHelper(0x10049, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelDoc::InsertObject()
{
	InvokeHelper(0x1004a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::EditClearAll()
{
	InvokeHelper(0x1004b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::EditCopy()
{
	InvokeHelper(0x1004c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::EditCut()
{
	InvokeHelper(0x1004d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ObjectDisplayContent()
{
	InvokeHelper(0x1004e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ObjectDisplayAsIcon()
{
	InvokeHelper(0x1004f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ObjectResetsize()
{
	InvokeHelper(0x10050, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::WindowRedraw()
{
	InvokeHelper(0x10051, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SetPickMode()
{
	InvokeHelper(0x10053, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotateminusx()
{
	InvokeHelper(0x10054, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotateminusy()
{
	InvokeHelper(0x10055, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotateminusz()
{
	InvokeHelper(0x10056, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotateplusx()
{
	InvokeHelper(0x10057, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotateplusy()
{
	InvokeHelper(0x10058, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotateplusz()
{
	InvokeHelper(0x10059, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewTranslateminusx()
{
	InvokeHelper(0x1005a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewTranslateminusy()
{
	InvokeHelper(0x1005b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewTranslateplusx()
{
	InvokeHelper(0x1005c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewTranslateplusy()
{
	InvokeHelper(0x1005d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotXMinusNinety()
{
	InvokeHelper(0x1005e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotYMinusNinety()
{
	InvokeHelper(0x1005f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotYPlusNinety()
{
	InvokeHelper(0x10060, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewZoomin()
{
	InvokeHelper(0x10061, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewZoomout()
{
	InvokeHelper(0x10062, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewDisplayHiddenremoved()
{
	InvokeHelper(0x10063, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewDisplayWireframe()
{
	InvokeHelper(0x10064, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewDisplayShaded()
{
	InvokeHelper(0x10065, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRwShading()
{
	InvokeHelper(0x10066, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewOglShading()
{
	InvokeHelper(0x10067, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewZoomtofit()
{
	InvokeHelper(0x10068, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotate()
{
	InvokeHelper(0x10069, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewTranslate()
{
	InvokeHelper(0x1006a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewZoomto()
{
	InvokeHelper(0x1006b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewDisplayHiddengreyed()
{
	InvokeHelper(0x1006c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewDisplayFaceted()
{
	InvokeHelper(0x1006d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewConstraint()
{
	InvokeHelper(0x1006e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::UserFavors()
{
	InvokeHelper(0x1006f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::FeatureCirPattern(long num, double spacing, BOOL flipDir, LPCTSTR dName)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_BOOL VTS_BSTR;
	InvokeHelper(0x10070, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 num, spacing, flipDir, dName);
}

void IModelDoc::EditSketch()
{
	InvokeHelper(0x10071, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::FeatEdit()
{
	InvokeHelper(0x10072, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::FeatEditDef()
{
	InvokeHelper(0x10073, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertPoint()
{
	InvokeHelper(0x10074, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertFamilyTableNew()
{
	InvokeHelper(0x10075, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertFamilyTableEdit()
{
	InvokeHelper(0x10077, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ToolsMacro()
{
	InvokeHelper(0x10079, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ToolsGrid()
{
	InvokeHelper(0x1007a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchCenterline()
{
	InvokeHelper(0x1007b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchAlign()
{
	InvokeHelper(0x1007c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchArc()
{
	InvokeHelper(0x1007d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchTangentArc()
{
	InvokeHelper(0x1007e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchCircle()
{
	InvokeHelper(0x1007f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchUndo()
{
	InvokeHelper(0x10080, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::UserPreferences()
{
	InvokeHelper(0x10081, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::Lights()
{
	InvokeHelper(0x10082, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchConstrainCoincident()
{
	InvokeHelper(0x10083, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchConstrainConcentric()
{
	InvokeHelper(0x10084, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchConstrainPerp()
{
	InvokeHelper(0x10085, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchConstrainTangent()
{
	InvokeHelper(0x10086, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchConstrainParallel()
{
	InvokeHelper(0x10087, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchUseEdge()
{
	InvokeHelper(0x10088, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchUseEdgeCtrline()
{
	InvokeHelper(0x10089, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchMirror()
{
	InvokeHelper(0x1008a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::Save()
{
	InvokeHelper(0x1008b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::Close()
{
	InvokeHelper(0x1008c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewDispRefaxes()
{
	InvokeHelper(0x1008d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewDispRefplanes()
{
	InvokeHelper(0x1008e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertSketch()
{
	InvokeHelper(0x1008f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertProtrusionSwept(BOOL propagate, BOOL alignment, BOOL keepNormalConstant)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10090, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 propagate, alignment, keepNormalConstant);
}

void IModelDoc::InsertProtrusionBlend(BOOL closed)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10091, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 closed);
}

void IModelDoc::ToolsMassProps()
{
	InvokeHelper(0x10092, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::PropertySheet()
{
	InvokeHelper(0x10093, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::BlankRefGeom()
{
	InvokeHelper(0x10094, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::UnBlankRefGeom()
{
	InvokeHelper(0x10095, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::EditDelete()
{
	InvokeHelper(0x10096, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertProjectedSketch()
{
	InvokeHelper(0x10097, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::CreatePlaneFixed(const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, BOOL useGlobal)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_BOOL;
	InvokeHelper(0x10098, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&P1, &P2, &P3, useGlobal);
	return result;
}

void IModelDoc::DebugCheckBody()
{
	InvokeHelper(0x1009b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::DimPreferences()
{
	InvokeHelper(0x1009d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::UnblankSketch()
{
	InvokeHelper(0x1009e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::EditSketchOrSingleSketchFeature()
{
	InvokeHelper(0x1009f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::DebugCheckIgesGeom()
{
	InvokeHelper(0x100a0, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::BlankSketch()
{
	InvokeHelper(0x100a1, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

double IModelDoc::GetDefaultTextHeight()
{
	double result;
	InvokeHelper(0x100a2, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::IsActive(LPCTSTR compStr)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x100a3, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		compStr);
	return result;
}

BOOL IModelDoc::CreateEllipse(const VARIANT& center, const VARIANT& major, const VARIANT& minor)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x100a4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&center, &major, &minor);
	return result;
}

BOOL IModelDoc::CreateEllipseVB(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x100a5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		centerX, centerY, centerZ, majorX, majorY, majorZ, minorX, minorY, minorZ);
	return result;
}

BOOL IModelDoc::CreateEllipticalArcByCenter(const VARIANT& center, const VARIANT& major, const VARIANT& minor, const VARIANT& start, const VARIANT& end)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x100a6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&center, &major, &minor, &start, &end);
	return result;
}

BOOL IModelDoc::CreateEllipticalArcByCenterVB(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ, double startX, double startY, double startZ, double endX, 
		double endY, double endZ)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x100a7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		centerX, centerY, centerZ, majorX, majorY, majorZ, minorX, minorY, minorZ, startX, startY, startZ, endX, endY, endZ);
	return result;
}

LPDISPATCH IModelDoc::GetActiveSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x100a8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IGetActiveSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x100b1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IModelDoc::GetTessellationQuality()
{
	long result;
	InvokeHelper(0x100a9, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetTessellationQuality(long qualityNum)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100aa, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 qualityNum);
}

LPDISPATCH IModelDoc::Parameter(LPCTSTR stringIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x100ab, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		stringIn);
	return result;
}

LPDISPATCH IModelDoc::IParameter(LPCTSTR stringIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x100b2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		stringIn);
	return result;
}

BOOL IModelDoc::SelectByID(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x100ac, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		selID, selParams, x, y, z);
	return result;
}

BOOL IModelDoc::AndSelectByID(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x100ad, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		selID, selParams, x, y, z);
	return result;
}

void IModelDoc::Insert3DSketch()
{
	InvokeHelper(0x100ae, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString IModelDoc::GetLightSourceName(long Id)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100b3, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		Id);
	return result;
}

BOOL IModelDoc::AddLightSource(LPCTSTR idName, long lTyp, LPCTSTR userName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x100b8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		idName, lTyp, userName);
	return result;
}

long IModelDoc::AddLightSourceExtProperty(long Id, const VARIANT& PropertyExtension)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT;
	InvokeHelper(0x100b9, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Id, &PropertyExtension);
	return result;
}

void IModelDoc::ResetLightSourceExtProperty(long Id)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100ba, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Id);
}

void IModelDoc::DeleteLightSource(long Id)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100bb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Id);
}

VARIANT IModelDoc::GetLightSourceExtProperty(long Id, long PropertyId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x100bc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		Id, PropertyId);
	return result;
}

long IModelDoc::AddLightToScene(LPCTSTR lpszNewValue)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x100bd, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		lpszNewValue);
	return result;
}

long IModelDoc::AddSceneExtProperty(const VARIANT& PropertyExtension)
{
	long result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x100be, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		&PropertyExtension);
	return result;
}

void IModelDoc::ResetSceneExtProperty()
{
	InvokeHelper(0x100bf, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IModelDoc::GetSceneExtProperty(long PropertyId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100c0, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		PropertyId);
	return result;
}

void IModelDoc::FileSummaryInfo()
{
	InvokeHelper(0x100c1, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IModelDoc::GetGridSettings()
{
	VARIANT result;
	InvokeHelper(0x100c2, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IModelDoc::ToolsSketchTranslate()
{
	InvokeHelper(0x100c3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ToolsDistance()
{
	InvokeHelper(0x100c4, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SkToolsAutoConstr()
{
	InvokeHelper(0x100c5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ToolsSketchScale()
{
	InvokeHelper(0x100c6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::Paste()
{
	InvokeHelper(0x100c7, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ToolsConfiguration()
{
	InvokeHelper(0x100c8, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::EntityProperties()
{
	InvokeHelper(0x100c9, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::GetArcCentersDisplayed()
{
	BOOL result;
	InvokeHelper(0x100ca, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetArcCentersDisplayed(BOOL setting)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x100cb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 setting);
}

void IModelDoc::AutoSolveToggle()
{
	InvokeHelper(0x100cc, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

double IModelDoc::IGetLines()
{
	double result;
	InvokeHelper(0x100ce, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long IModelDoc::GetLineCount()
{
	long result;
	InvokeHelper(0x100cf, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelDoc::ICreateEllipse(double* center, double* major, double* minor)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x100d0, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 center, major, minor);
}

void IModelDoc::ICreateEllipticalArcByCenter(double* center, double* major, double* minor, double* start, double* end)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x100d1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 center, major, minor, start, end);
}

double IModelDoc::GetILightSourcePropertyValues(long Id)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100d2, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, parms,
		Id);
	return result;
}

void IModelDoc::SetILightSourcePropertyValues(long Id, double* newValue)
{
	static BYTE parms[] =
		VTS_I4 VTS_PR8;
	InvokeHelper(0x100d2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 Id, newValue);
}

void IModelDoc::InsertCutSwept(BOOL propagate, BOOL alignment, BOOL keepNormalConstant)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x100d3, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 propagate, alignment, keepNormalConstant);
}

void IModelDoc::InsertCutBlend(BOOL closed)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x100d4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 closed);
}

void IModelDoc::InsertHelix(BOOL reversed, BOOL clockwised, BOOL tapered, BOOL outward, long helixdef, double height, double pitch, double revolution, double taperangle, double startangle)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x100d5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 reversed, clockwised, tapered, outward, helixdef, height, pitch, revolution, taperangle, startangle);
}

void IModelDoc::ICreateLine(double* P1, double* P2)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x100d6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 P1, P2);
}

void IModelDoc::ICreateCenterLine(double* P1, double* P2)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x100d7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 P1, P2);
}

void IModelDoc::ICreateArc(double* P1, double* P2, double* P3, short dir)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_I2;
	InvokeHelper(0x100d8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 P1, P2, P3, dir);
}

void IModelDoc::ICreateCircleByRadius(double* P1, double radius)
{
	static BYTE parms[] =
		VTS_PR8 VTS_R8;
	InvokeHelper(0x100d9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 P1, radius);
}

void IModelDoc::GraphicsRedraw()
{
	InvokeHelper(0x100da, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::GetVisibilityOfConstructPlanes()
{
	BOOL result;
	InvokeHelper(0x100db, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::GetDisplayWhenAdded()
{
	BOOL result;
	InvokeHelper(0x100dc, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetDisplayWhenAdded(BOOL setting)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x100dd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 setting);
}

BOOL IModelDoc::GetAddToDB()
{
	BOOL result;
	InvokeHelper(0x100de, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetAddToDB(BOOL setting)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x100df, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 setting);
}

BOOL IModelDoc::DeSelectByID(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x100e0, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		selID, selParams, x, y, z);
	return result;
}

BOOL IModelDoc::GetVisible()
{
	BOOL result;
	InvokeHelper(0x100e1, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetVisible(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x100e1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

void IModelDoc::PrintDirect()
{
	InvokeHelper(0x100e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::PrintPreview()
{
	InvokeHelper(0x100e3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::Quit()
{
	InvokeHelper(0x100e4, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::CreatePlaneThru3Points()
{
	InvokeHelper(0x100e5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewRotXPlusNinety()
{
	InvokeHelper(0x100e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IModelDoc::GetUnits()
{
	VARIANT result;
	InvokeHelper(0x100e7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetAngularUnits(short uType, short fractBase, short fractDenom, short sigDigits)
{
	static BYTE parms[] =
		VTS_I2 VTS_I2 VTS_I2 VTS_I2;
	InvokeHelper(0x100e8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 uType, fractBase, fractDenom, sigDigits);
}

VARIANT IModelDoc::GetAngularUnits()
{
	VARIANT result;
	InvokeHelper(0x100e9, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

short IModelDoc::IGetUnits()
{
	short result;
	InvokeHelper(0x100ea, DISPATCH_METHOD, VT_I2, (void*)&result, NULL);
	return result;
}

void IModelDoc::ISetAngularUnits(short uType, short fractBase, short fractDenom, short sigDigits)
{
	static BYTE parms[] =
		VTS_I2 VTS_I2 VTS_I2 VTS_I2;
	InvokeHelper(0x100eb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 uType, fractBase, fractDenom, sigDigits);
}

short IModelDoc::IGetAngularUnits()
{
	short result;
	InvokeHelper(0x100ec, DISPATCH_METHOD, VT_I2, (void*)&result, NULL);
	return result;
}

void IModelDoc::ShowConfiguration(LPCTSTR configurationName)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x100ed, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 configurationName);
}

void IModelDoc::ResetConfiguration()
{
	InvokeHelper(0x100ee, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::AddConfiguration(LPCTSTR Name, LPCTSTR comment, LPCTSTR alternateName, BOOL suppressByDefault, BOOL hideByDefault, BOOL minFeatureManager, BOOL inheritProperties, unsigned long flags)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x100ef, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Name, comment, alternateName, suppressByDefault, hideByDefault, minFeatureManager, inheritProperties, flags);
}

void IModelDoc::DeleteConfiguration(LPCTSTR configurationName)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x100f0, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 configurationName);
}

void IModelDoc::EditConfiguration(LPCTSTR Name, LPCTSTR newName, LPCTSTR comment, LPCTSTR alternateName, BOOL suppressByDefault, BOOL hideByDefault, BOOL minFeatureManager, BOOL inheritProperties, unsigned long flags)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x100f1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Name, newName, comment, alternateName, suppressByDefault, hideByDefault, minFeatureManager, inheritProperties, flags);
}

void IModelDoc::CreatePlanePerCurveAndPassPoint(BOOL origAtCurve)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x100f2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 origAtCurve);
}

LPDISPATCH IModelDoc::CreateFeatureMgrView(long* bitmap)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x100f3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		bitmap);
	return result;
}

BOOL IModelDoc::AddFeatureMgrView(long* bitmap, long* appView)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PI4 VTS_PI4;
	InvokeHelper(0x100f4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bitmap, appView);
	return result;
}

VARIANT IModelDoc::GetStandardViewRotation(long viewId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100f5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		viewId);
	return result;
}

double IModelDoc::IGetStandardViewRotation(long viewId)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100f6, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		viewId);
	return result;
}

void IModelDoc::FeatureExtruRefSurface(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x100f7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2);
}

LPUNKNOWN IModelDoc::IGet3rdPartyStorage(LPCTSTR stringIn, BOOL isStoring)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL;
	InvokeHelper(0x100f8, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		stringIn, isStoring);
	return result;
}

void IModelDoc::DeleteFeatureMgrView(long* appView)
{
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x100f9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 appView);
}

VARIANT IModelDoc::GetMassProperties()
{
	VARIANT result;
	InvokeHelper(0x100fa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::IGetMassProperties(double* mPropsData)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x100fb, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		mPropsData);
	return result;
}

long IModelDoc::GetLightSourceCount()
{
	long result;
	InvokeHelper(0x100fc, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IModelDoc::GetLightSourceIdFromName(LPCTSTR lightName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x100fd, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		lightName);
	return result;
}

void IModelDoc::SetNextSelectionGroupId(long Id)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100fe, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Id);
}

void IModelDoc::ISetNextSelectionGroupId(long Id)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x100ff, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Id);
}

LPDISPATCH IModelDoc::InsertMidSurfaceExt(double placement, BOOL knitFlag)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x10100, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		placement, knitFlag);
	return result;
}

LPDISPATCH IModelDoc::IInsertMidSurfaceExt(double placement, BOOL knitFlag)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x10101, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		placement, knitFlag);
	return result;
}

void IModelDoc::ICreatePlaneFixed(double* P1, double* P2, double* P3, BOOL useGlobal)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_BOOL;
	InvokeHelper(0x10102, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 P1, P2, P3, useGlobal);
}

BOOL IModelDoc::SelectByMark(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z, long mark)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_I4;
	InvokeHelper(0x10103, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		selID, selParams, x, y, z, mark);
	return result;
}

BOOL IModelDoc::AndSelectByMark(LPCTSTR selID, LPCTSTR selParams, double x, double y, double z, long mark)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_I4;
	InvokeHelper(0x10104, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		selID, selParams, x, y, z, mark);
	return result;
}

VARIANT IModelDoc::GetDependencies(long traverseflag, long searchflag)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x10105, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		traverseflag, searchflag);
	return result;
}

CString IModelDoc::IGetDependencies(long traverseflag, long searchflag)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x10106, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		traverseflag, searchflag);
	return result;
}

long IModelDoc::GetNumDependencies(long traverseflag, long searchflag)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x10107, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		traverseflag, searchflag);
	return result;
}

long IModelDoc::IGetNumDependencies(long traverseflag, long searchflag)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x10108, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		traverseflag, searchflag);
	return result;
}

LPDISPATCH IModelDoc::FirstFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x10109, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IFirstFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x1010a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IModelDoc::UnderiveSketch()
{
	InvokeHelper(0x1010b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::DeriveSketch()
{
	InvokeHelper(0x1010c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::IsExploded()
{
	BOOL result;
	InvokeHelper(0x1010d, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::DeleteSelection(BOOL confirmFlag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1010e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		confirmFlag);
	return result;
}

BOOL IModelDoc::DeleteNamedView(LPCTSTR viewname)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1010f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		viewname);
	return result;
}

BOOL IModelDoc::SetLightSourceName(long Id, LPCTSTR newName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x10110, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Id, newName);
	return result;
}

void IModelDoc::Insert3DSplineCurve(BOOL curveClosed)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10111, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 curveClosed);
}

BOOL IModelDoc::SetLightSourcePropertyValuesVB(LPCTSTR idName, long lType, double diff, long rgbColor, double dist, double dirX, double dirY, double dirZ, double spotDirX, double spotDirY, double spotDirZ, double spotAngle, double fallOff0, 
		double fallOff1, double fallOff2, double ambient, double specular, double spotExponent, BOOL bDisable)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_R8 VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL;
	InvokeHelper(0x10112, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		idName, lType, diff, rgbColor, dist, dirX, dirY, dirZ, spotDirX, spotDirY, spotDirZ, spotAngle, fallOff0, fallOff1, fallOff2, ambient, specular, spotExponent, bDisable);
	return result;
}

LPDISPATCH IModelDoc::ICreateFeatureMgrView(long* bitmap)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x10113, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		bitmap);
	return result;
}

BOOL IModelDoc::SelectedEdgeProperties(LPCTSTR edgeName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10114, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		edgeName);
	return result;
}

BOOL IModelDoc::SelectedFaceProperties(long rgbColor, double ambient, double diffuse, double specular, double shininess, double transparency, double emission, BOOL usePartProps, LPCTSTR faceName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_BSTR;
	InvokeHelper(0x10115, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		rgbColor, ambient, diffuse, specular, shininess, transparency, emission, usePartProps, faceName);
	return result;
}

BOOL IModelDoc::SelectedFeatureProperties(long rgbColor, double ambient, double diffuse, double specular, double shininess, double transparency, double emission, BOOL usePartProps, BOOL suppressed, LPCTSTR featureName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BSTR;
	InvokeHelper(0x10116, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		rgbColor, ambient, diffuse, specular, shininess, transparency, emission, usePartProps, suppressed, featureName);
	return result;
}

void IModelDoc::InsertSplitLineSil()
{
	InvokeHelper(0x10117, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertSplitLineProject(BOOL isDirectional, BOOL flipDir)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10118, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 isDirectional, flipDir);
}

void IModelDoc::InsertRib(BOOL is2Sided, BOOL reverseThicknessDir, double thickness, long referenceEdgeIndex, BOOL reverseMaterialDir, BOOL isDrafted, BOOL draftOutward, double draftAngle)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_R8 VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8;
	InvokeHelper(0x10119, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 is2Sided, reverseThicknessDir, thickness, referenceEdgeIndex, reverseMaterialDir, isDrafted, draftOutward, draftAngle);
}

BOOL IModelDoc::AddRadialDimension(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1011a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

BOOL IModelDoc::AddDiameterDimension(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1011b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

VARIANT IModelDoc::GetModelViewNames()
{
	VARIANT result;
	InvokeHelper(0x1011c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IModelDoc::IGetModelViewNames()
{
	CString result;
	InvokeHelper(0x1011d, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IModelDoc::GetModelViewCount()
{
	long result;
	InvokeHelper(0x1011e, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

double IModelDoc::GetUserPreferenceDoubleValue(long userPreferenceValue)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1011f, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		userPreferenceValue);
	return result;
}

BOOL IModelDoc::SetUserPreferenceDoubleValue(long userPreferenceValue, double Value)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_R8;
	InvokeHelper(0x10120, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreferenceValue, Value);
	return result;
}

void IModelDoc::ViewDisplayCurvature()
{
	InvokeHelper(0x10121, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::Scale()
{
	InvokeHelper(0x10122, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::AddIns()
{
	InvokeHelper(0x10123, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::InsertCurveFile(LPCTSTR fileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10124, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName);
	return result;
}

void IModelDoc::InsertCurveFileBegin()
{
	InvokeHelper(0x10125, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::InsertCurveFilePoint(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10126, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

BOOL IModelDoc::InsertCurveFileEnd()
{
	BOOL result;
	InvokeHelper(0x10127, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::ChangeSketchPlane()
{
	BOOL result;
	InvokeHelper(0x10128, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::ViewOrientationUndo()
{
	InvokeHelper(0x10129, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::PrintOut(long fromPage, long toPage, long numCopies, BOOL collate, LPCTSTR printer, double Scale, BOOL printToFile)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_BOOL VTS_BSTR VTS_R8 VTS_BOOL;
	InvokeHelper(0x1012a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 fromPage, toPage, numCopies, collate, printer, Scale, printToFile);
}

void IModelDoc::SketchOffsetEntities(double offset, BOOL flip)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x1012b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 offset, flip);
}

void IModelDoc::InsertLibraryFeature(LPCTSTR libFeatPartNameIn)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1012c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 libFeatPartNameIn);
}

void IModelDoc::SketchModifyTranslate(double startX, double startY, double endX, double endY)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1012d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 startX, startY, endX, endY);
}

void IModelDoc::SketchModifyRotate(double centerX, double centerY, double angle)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1012e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 centerX, centerY, angle);
}

void IModelDoc::SketchModifyFlip(long axisFlag)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1012f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 axisFlag);
}

BOOL IModelDoc::SketchModifyScale(double scaleFactor)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x10130, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		scaleFactor);
	return result;
}

LPDISPATCH IModelDoc::GetActiveConfiguration()
{
	LPDISPATCH result;
	InvokeHelper(0x10131, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IGetActiveConfiguration()
{
	LPDISPATCH result;
	InvokeHelper(0x10132, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::GetUserPreferenceToggle(long userPreferenceToggle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10133, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreferenceToggle);
	return result;
}

BOOL IModelDoc::SetUserPreferenceToggle(long userPreferenceValue, BOOL onFlag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0x10134, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreferenceValue, onFlag);
	return result;
}

void IModelDoc::InsertSweepRefSurface(BOOL propagate, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational)
{
	static BYTE parms[] =
		VTS_BOOL VTS_I2 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10135, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 propagate, twistCtrlOption, keepTangency, forceNonRational);
}

void IModelDoc::InsertLoftRefSurface(BOOL closed, BOOL keepTangency, BOOL forceNonRational)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10136, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 closed, keepTangency, forceNonRational);
}

void IModelDoc::InsertProtrusionSwept2(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_I2 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10137, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 propagate, alignment, twistCtrlOption, keepTangency, forceNonRational);
}

void IModelDoc::InsertProtrusionBlend2(BOOL closed, BOOL keepTangency, BOOL forceNonRational)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10138, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 closed, keepTangency, forceNonRational);
}

void IModelDoc::InsertCutSwept2(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_I2 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10139, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 propagate, alignment, twistCtrlOption, keepTangency, forceNonRational);
}

void IModelDoc::InsertCutBlend2(BOOL closed, BOOL keepTangency, BOOL forceNonRational)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1013a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 closed, keepTangency, forceNonRational);
}

BOOL IModelDoc::IsEditingSelf()
{
	BOOL result;
	InvokeHelper(0x1013b, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::ShowNamedView2(LPCTSTR vName, long viewId)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x1013c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 vName, viewId);
}

void IModelDoc::InsertDome(double height, BOOL reverseDir, BOOL doEllipticSurface)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1013d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 height, reverseDir, doEllipticSurface);
}

CString IModelDoc::GetMaterialUserName()
{
	CString result;
	InvokeHelper(0x1013e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetMaterialUserName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1013e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString IModelDoc::GetMaterialIdName()
{
	CString result;
	InvokeHelper(0x1013f, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetMaterialIdName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1013f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

VARIANT IModelDoc::GetMaterialPropertyValues()
{
	VARIANT result;
	InvokeHelper(0x10140, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetMaterialPropertyValues(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x10140, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IModelDoc::GetIMaterialPropertyValues()
{
	double result;
	InvokeHelper(0x10141, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetIMaterialPropertyValues(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x10141, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IModelDoc::AddPropertyExtension(const VARIANT& PropertyExtension)
{
	long result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x10142, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		&PropertyExtension);
	return result;
}

VARIANT IModelDoc::GetPropertyExtension(long Id)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10143, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		Id);
	return result;
}

void IModelDoc::ResetPropertyExtension()
{
	InvokeHelper(0x10144, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IModelDoc::GetUpdateStamp()
{
	long result;
	InvokeHelper(0x10145, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelDoc::ViewZoomTo2(double x1, double y1, double z1, double x2, double y2, double z2)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10146, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x1, y1, z1, x2, y2, z2);
}

void IModelDoc::ScreenRotate()
{
	InvokeHelper(0x10147, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

short IModelDoc::GetPrintSetup(long setupType)
{
	short result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10148, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, parms,
		setupType);
	return result;
}

void IModelDoc::SetPrintSetup(long setupType, short nNewValue)
{
	static BYTE parms[] =
		VTS_I4 VTS_I2;
	InvokeHelper(0x10148, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 setupType, nNewValue);
}

void IModelDoc::GraphicsRedraw2()
{
	InvokeHelper(0x10149, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertCosmeticThread(short type, double depth, double length, LPCTSTR note)
{
	static BYTE parms[] =
		VTS_I2 VTS_R8 VTS_R8 VTS_BSTR;
	InvokeHelper(0x1014a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 type, depth, length, note);
}

void IModelDoc::HideCosmeticThread()
{
	InvokeHelper(0x1014b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ShowCosmeticThread()
{
	InvokeHelper(0x1014c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SimpleHole2(double dia, BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1014d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 dia, sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2);
}

void IModelDoc::IRelease3rdPartyStorage(LPCTSTR stringIn)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1014e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 stringIn);
}

long IModelDoc::FeatureRevolve2(double angle, BOOL reverseDir, double angle2, long revType, long options)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_R8 VTS_I4 VTS_I4;
	InvokeHelper(0x1014f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		angle, reverseDir, angle2, revType, options);
	return result;
}

long IModelDoc::FeatureRevolveCut2(double angle, BOOL reverseDir, double angle2, long revType, long options)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_R8 VTS_I4 VTS_I4;
	InvokeHelper(0x10150, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		angle, reverseDir, angle2, revType, options);
	return result;
}

void IModelDoc::SetSaveFlag()
{
	InvokeHelper(0x10151, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString IModelDoc::GetExternalReferenceName()
{
	CString result;
	InvokeHelper(0x10152, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::SelectByRay(const VARIANT& doubleInfoIn, long typeWanted)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_I4;
	InvokeHelper(0x10153, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&doubleInfoIn, typeWanted);
	return result;
}

BOOL IModelDoc::ISelectByRay(double* pointIn, double* vectorIn, double radiusIn, long typeWanted)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_R8 VTS_I4;
	InvokeHelper(0x10154, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		pointIn, vectorIn, radiusIn, typeWanted);
	return result;
}

void IModelDoc::SetSceneBkgDIB(long l_dib)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10155, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 l_dib);
}

CString IModelDoc::GetSceneBkgImageFileName()
{
	CString result;
	InvokeHelper(0x10156, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetSceneBkgImageFileName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10156, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

void IModelDoc::InsertBkgImage(LPCTSTR newName)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10157, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 newName);
}

void IModelDoc::DeleteBkgImage()
{
	InvokeHelper(0x10158, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertSplinePoint(double x, double y, double z)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10159, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x, y, z);
}

void IModelDoc::InsertLoftRefSurface2(BOOL closed, BOOL keepTangency, BOOL forceNonRational, double tessToleranceFactor, short startMatchingType, short endMatchingType)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_I2 VTS_I2;
	InvokeHelper(0x1015a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 closed, keepTangency, forceNonRational, tessToleranceFactor, startMatchingType, endMatchingType);
}

void IModelDoc::InsertProtrusionBlend3(BOOL closed, BOOL keepTangency, BOOL forceNonRational, double tessToleranceFactor, short startMatchingType, short endMatchingType)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_I2 VTS_I2;
	InvokeHelper(0x1015b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 closed, keepTangency, forceNonRational, tessToleranceFactor, startMatchingType, endMatchingType);
}

void IModelDoc::InsertCutBlend3(BOOL closed, BOOL keepTangency, BOOL forceNonRational, double tessToleranceFactor, short startMatchingType, short endMatchingType)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_I2 VTS_I2;
	InvokeHelper(0x1015c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 closed, keepTangency, forceNonRational, tessToleranceFactor, startMatchingType, endMatchingType);
}

void IModelDoc::AlignDimensions()
{
	InvokeHelper(0x1015d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::BreakDimensionAlignment()
{
	InvokeHelper(0x1015e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchFillet1(double rad)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1015f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 rad);
}

void IModelDoc::FeatureChamferType(short chamferType, double Width, double angle, BOOL flip, double otherDist, double vertexChamDist1, double vertexChamDist2, double vertexChamDist3)
{
	static BYTE parms[] =
		VTS_I2 VTS_R8 VTS_R8 VTS_BOOL VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10160, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 chamferType, Width, angle, flip, otherDist, vertexChamDist1, vertexChamDist2, vertexChamDist3);
}

void IModelDoc::FeatureCutThin(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, double thk1, double thk2, 
		double endThk, long revThinDir, long capEnds, BOOL addBends, double bendRad)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_BOOL VTS_R8;
	InvokeHelper(0x10161, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2, thk1, thk2, endThk, revThinDir, capEnds, addBends, bendRad);
}

void IModelDoc::FeatureBossThin(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, double thk1, double thk2, 
		double endThk, long revThinDir, long capEnds, BOOL addBends, double bendRad)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_BOOL VTS_R8;
	InvokeHelper(0x10162, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2, thk1, thk2, endThk, revThinDir, capEnds, addBends, bendRad);
}

BOOL IModelDoc::InsertDatumTargetSymbol(LPCTSTR datum1, LPCTSTR datum2, LPCTSTR datum3, short areaStyle, BOOL areaOutside, double value1, double value2, LPCTSTR valueStr1, LPCTSTR valueStr2, BOOL arrowsSmart, short arrowStyle, short leaderLineStyle, 
		BOOL leaderBent, BOOL showArea, BOOL showSymbol)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_I2 VTS_BOOL VTS_R8 VTS_R8 VTS_BSTR VTS_BSTR VTS_BOOL VTS_I2 VTS_I2 VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10163, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		datum1, datum2, datum3, areaStyle, areaOutside, value1, value2, valueStr1, valueStr2, arrowsSmart, arrowStyle, leaderLineStyle, leaderBent, showArea, showSymbol);
	return result;
}

BOOL IModelDoc::EditDatumTargetSymbol(LPCTSTR datum1, LPCTSTR datum2, LPCTSTR datum3, short areaStyle, BOOL areaOutside, double value1, double value2, LPCTSTR valueStr1, LPCTSTR valueStr2, BOOL arrowsSmart, short arrowStyle, short leaderLineStyle, 
		BOOL leaderBent, BOOL showArea, BOOL showSymbol)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_I2 VTS_BOOL VTS_R8 VTS_R8 VTS_BSTR VTS_BSTR VTS_BOOL VTS_I2 VTS_I2 VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10164, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		datum1, datum2, datum3, areaStyle, areaOutside, value1, value2, valueStr1, valueStr2, arrowsSmart, arrowStyle, leaderLineStyle, leaderBent, showArea, showSymbol);
	return result;
}

void IModelDoc::InsertBOMBalloon()
{
	InvokeHelper(0x10165, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IModelDoc::FeatureReferenceCurve(long numOfCurves, const VARIANT& baseCurves, BOOL merge, LPCTSTR fromFileName, long* errorCode)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT VTS_BOOL VTS_BSTR VTS_PI4;
	InvokeHelper(0x10166, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		numOfCurves, &baseCurves, merge, fromFileName, errorCode);
	return result;
}

void IModelDoc::FontBold(BOOL Bold)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10168, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Bold);
}

void IModelDoc::FontItalic(BOOL Italic)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10169, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Italic);
}

void IModelDoc::FontUnderline(BOOL Underline)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1016a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Underline);
}

void IModelDoc::FontFace(LPCTSTR face)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1016b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 face);
}

void IModelDoc::FontPoints(short Points)
{
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x1016c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Points);
}

void IModelDoc::FontUnits(double units)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1016d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 units);
}

BOOL IModelDoc::SketchSplineByEqnParams(const VARIANT& paramsIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x1016e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&paramsIn);
	return result;
}

void IModelDoc::AlignParallelDimensions()
{
	InvokeHelper(0x1016f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SetBlockingState(long stateIn)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10170, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 stateIn);
}

void IModelDoc::ResetBlockingState()
{
	InvokeHelper(0x10171, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IModelDoc::GetSceneBkgDIB()
{
	long result;
	InvokeHelper(0x10172, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelDoc::InsertHatchedFace()
{
	InvokeHelper(0x10173, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IModelDoc::GetColorTable()
{
	LPDISPATCH result;
	InvokeHelper(0x10174, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IGetColorTable()
{
	LPDISPATCH result;
	InvokeHelper(0x10175, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IModelDoc::InsertSweepRefSurface2(BOOL propagate, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational, short startMatchingType, short endMatchingType)
{
	static BYTE parms[] =
		VTS_BOOL VTS_I2 VTS_BOOL VTS_BOOL VTS_I2 VTS_I2;
	InvokeHelper(0x10176, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 propagate, twistCtrlOption, keepTangency, forceNonRational, startMatchingType, endMatchingType);
}

void IModelDoc::InsertProtrusionSwept3(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational, short startMatchingType, short endMatchingType)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_I2 VTS_BOOL VTS_BOOL VTS_I2 VTS_I2;
	InvokeHelper(0x10177, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 propagate, alignment, twistCtrlOption, keepTangency, forceNonRational, startMatchingType, endMatchingType);
}

void IModelDoc::InsertCutSwept3(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational, short startMatchingType, short endMatchingType)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_I2 VTS_BOOL VTS_BOOL VTS_I2 VTS_I2;
	InvokeHelper(0x10178, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 propagate, alignment, twistCtrlOption, keepTangency, forceNonRational, startMatchingType, endMatchingType);
}

BOOL IModelDoc::IsOpenedViewOnly()
{
	BOOL result;
	InvokeHelper(0x10179, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::IsOpenedReadOnly()
{
	BOOL result;
	InvokeHelper(0x1017a, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::ViewZoomToSelection()
{
	InvokeHelper(0x1017b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::FeatureBossThicken(double thickness, long direction, long faceIndex)
{
	static BYTE parms[] =
		VTS_R8 VTS_I4 VTS_I4;
	InvokeHelper(0x1017c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 thickness, direction, faceIndex);
}

void IModelDoc::FeatureCutThicken(double thickness, long direction, long faceIndex)
{
	static BYTE parms[] =
		VTS_R8 VTS_I4 VTS_I4;
	InvokeHelper(0x1017d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 thickness, direction, faceIndex);
}

BOOL IModelDoc::InsertAxis()
{
	BOOL result;
	InvokeHelper(0x1017e, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::EditUndo(unsigned long nSteps)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1017f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 nSteps);
}

void IModelDoc::SelectMidpoint()
{
	InvokeHelper(0x10180, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IModelDoc::ISketchSplineByEqnParams(long* propArray, double* knotsArray, double* cntrlPntCoordArray)
{
	long result;
	static BYTE parms[] =
		VTS_PI4 VTS_PR8 VTS_PR8;
	InvokeHelper(0x10181, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propArray, knotsArray, cntrlPntCoordArray);
	return result;
}

VARIANT IModelDoc::VersionHistory()
{
	VARIANT result;
	InvokeHelper(0x10182, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IModelDoc::IVersionHistory()
{
	CString result;
	InvokeHelper(0x10183, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IModelDoc::IGetVersionHistoryCount()
{
	long result;
	InvokeHelper(0x10184, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelDoc::Rebuild(long options)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10185, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 options);
}

void IModelDoc::InsertFeatureShellAddThickness(double thickness)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x10186, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 thickness);
}

void IModelDoc::InsertOffsetSurface(double thickness, BOOL reverse)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x10187, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 thickness, reverse);
}

void IModelDoc::SimplifySpline(double toleranceIn)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x10188, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 toleranceIn);
}

CString IModelDoc::GetSummaryInfo(long FieldId)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10189, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms,
		FieldId);
	return result;
}

void IModelDoc::SetSummaryInfo(long FieldId, LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x10189, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 FieldId, lpszNewValue);
}

CString IModelDoc::GetCustomInfo(LPCTSTR FieldName)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1018a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms,
		FieldName);
	return result;
}

void IModelDoc::SetCustomInfo(LPCTSTR FieldName, LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1018a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 FieldName, lpszNewValue);
}

long IModelDoc::GetCustomInfoCount()
{
	long result;
	InvokeHelper(0x1018b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IModelDoc::GetCustomInfoType(LPCTSTR FieldName)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1018c, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		FieldName);
	return result;
}

VARIANT IModelDoc::GetCustomInfoNames()
{
	VARIANT result;
	InvokeHelper(0x1018d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IModelDoc::IGetCustomInfoNames()
{
	CString result;
	InvokeHelper(0x1018e, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::AddCustomInfo(LPCTSTR FieldName, LPCTSTR FieldType, LPCTSTR FieldValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1018f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FieldName, FieldType, FieldValue);
	return result;
}

BOOL IModelDoc::DeleteCustomInfo(LPCTSTR FieldName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10190, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FieldName);
	return result;
}

void IModelDoc::PrintOut2(long fromPage, long toPage, long numCopies, BOOL collate, LPCTSTR printer, double Scale, BOOL printToFile, LPCTSTR ptfName)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_BOOL VTS_BSTR VTS_R8 VTS_BOOL VTS_BSTR;
	InvokeHelper(0x10191, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 fromPage, toPage, numCopies, collate, printer, Scale, printToFile, ptfName);
}

BOOL IModelDoc::SetReadOnlyState(BOOL setReadOnly)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10192, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		setReadOnly);
	return result;
}

BOOL IModelDoc::InsertFamilyTableOpen(LPCTSTR fileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10193, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName);
	return result;
}

BOOL IModelDoc::MultiSelectByRay(const VARIANT& doubleInfoIn, long typeWanted, BOOL append)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_I4 VTS_BOOL;
	InvokeHelper(0x10194, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&doubleInfoIn, typeWanted, append);
	return result;
}

BOOL IModelDoc::IMultiSelectByRay(double* pointIn, double* vectorIn, double radiusIn, long typeWanted, BOOL append)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_R8 VTS_I4 VTS_BOOL;
	InvokeHelper(0x10195, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		pointIn, vectorIn, radiusIn, typeWanted, append);
	return result;
}

void IModelDoc::InsertNewNote3(LPCTSTR upperText, BOOL noLeader, BOOL bentLeader, short arrowStyle, short leaderSide, double angle, short balloonStyle, short balloonFit, BOOL smartArrow)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL VTS_BOOL VTS_I2 VTS_I2 VTS_R8 VTS_I2 VTS_I2 VTS_BOOL;
	InvokeHelper(0x10196, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 upperText, noLeader, bentLeader, arrowStyle, leaderSide, angle, balloonStyle, balloonFit, smartArrow);
}

void IModelDoc::InsertWeldSymbol2(LPCTSTR dim1, LPCTSTR symbol, LPCTSTR dim2, BOOL symmetric, BOOL fieldWeld, BOOL showOtherSide, BOOL dashOnTop, BOOL peripheral, BOOL hasProcess, LPCTSTR processValue)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BSTR;
	InvokeHelper(0x10197, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 dim1, symbol, dim2, symmetric, fieldWeld, showOtherSide, dashOnTop, peripheral, hasProcess, processValue);
}

BOOL IModelDoc::InsertSurfaceFinishSymbol2(long symType, long leaderType, double locX, double locY, double locZ, long laySymbol, long arrowType, LPCTSTR machAllowance, LPCTSTR otherVals, LPCTSTR prodMethod, LPCTSTR sampleLen, LPCTSTR maxRoughness, 
		LPCTSTR minRoughness, LPCTSTR roughnessSpacing)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x10198, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		symType, leaderType, locX, locY, locZ, laySymbol, arrowType, machAllowance, otherVals, prodMethod, sampleLen, maxRoughness, minRoughness, roughnessSpacing);
	return result;
}

long IModelDoc::SaveSilent()
{
	long result;
	InvokeHelper(0x10199, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IModelDoc::SaveAsSilent(LPCTSTR newName, BOOL saveAsCopy)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL;
	InvokeHelper(0x1019a, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		newName, saveAsCopy);
	return result;
}

BOOL IModelDoc::AddCustomInfo2(LPCTSTR FieldName, long FieldType, LPCTSTR FieldValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x1019b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		FieldName, FieldType, FieldValue);
	return result;
}

long IModelDoc::GetCustomInfoType2(LPCTSTR FieldName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1019c, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		FieldName);
	return result;
}

BOOL IModelDoc::InsertRefPoint()
{
	BOOL result;
	InvokeHelper(0x1019d, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IModelDoc::FeatureFillet2(double r1, BOOL propagate, BOOL ftyp, BOOL varRadTyp, long overFlowType, long nRadii, const VARIANT& radii)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_VARIANT;
	InvokeHelper(0x1019e, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		r1, propagate, ftyp, varRadTyp, overFlowType, nRadii, &radii);
	return result;
}

long IModelDoc::IFeatureFillet2(double r1, BOOL propagate, BOOL ftyp, BOOL varRadTyp, long overFlowType, long nRadii, double* radii)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_PR8;
	InvokeHelper(0x1019f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		r1, propagate, ftyp, varRadTyp, overFlowType, nRadii, radii);
	return result;
}

LPDISPATCH IModelDoc::GetFirstAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x101a0, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IGetFirstAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x101a1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::InsertCoordinateSystem(BOOL xFlippedIn, BOOL yFlippedIn, BOOL zFlippedIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x101a2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		xFlippedIn, yFlippedIn, zFlippedIn);
	return result;
}

BOOL IModelDoc::GetToolbarVisibility(long toolbar)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x101a3, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		toolbar);
	return result;
}

void IModelDoc::SetToolbarVisibility(long toolbar, BOOL visibility)
{
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0x101a4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 toolbar, visibility);
}

void IModelDoc::ViewDispCoordinateSystems()
{
	InvokeHelper(0x101a5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewDispTempRefaxes()
{
	InvokeHelper(0x101a6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewDispRefPoints()
{
	InvokeHelper(0x101a7, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ViewDispOrigins()
{
	InvokeHelper(0x101a8, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IModelDoc::GetCoordinateSystemXformByName(LPCTSTR nameIn)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x101a9, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		nameIn);
	return result;
}

double IModelDoc::IGetCoordinateSystemXformByName(LPCTSTR nameIn)
{
	double result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x101aa, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		nameIn);
	return result;
}

CString IModelDoc::GetCurrentCoordinateSystemName()
{
	CString result;
	InvokeHelper(0x101ab, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IModelDoc::EnumModelViews()
{
	LPUNKNOWN result;
	InvokeHelper(0x101ac, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::InsertCompositeCurve()
{
	BOOL result;
	InvokeHelper(0x101ad, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::SketchParabola(double val1, double val2, double z1, double val3, double val4, double z2, double val5, double val6, double z3, double val7, double val8, double z4)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x101ae, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 val1, val2, z1, val3, val4, z2, val5, val6, z3, val7, val8, z4);
}

void IModelDoc::InsertRadiateSurface(double distance, BOOL flipDir, BOOL tangentPropagate)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x101af, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 distance, flipDir, tangentPropagate);
}

void IModelDoc::InsertSewRefSurface()
{
	InvokeHelper(0x101b0, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IModelDoc::InsertShape(long pressureOn, long tangentsOn, double pressureGain, double tangentGain, double curveSpringGain, double alpha, double beta, double gamma, double delta, long degree, long split, long tuning)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x101b1, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		pressureOn, tangentsOn, pressureGain, tangentGain, curveSpringGain, alpha, beta, gamma, delta, degree, split, tuning);
	return result;
}

void IModelDoc::InsertMfDraft2(double angle, BOOL flipDir, BOOL isEdgeDraft, long propType, BOOL stepDraft)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_I4 VTS_BOOL;
	InvokeHelper(0x101b2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 angle, flipDir, isEdgeDraft, propType, stepDraft);
}

long IModelDoc::GetConfigurationCount()
{
	long result;
	InvokeHelper(0x101b3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IModelDoc::GetConfigurationNames()
{
	VARIANT result;
	InvokeHelper(0x101b4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IModelDoc::IGetConfigurationNames(long* count)
{
	CString result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x101b5, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		count);
	return result;
}

void IModelDoc::FeatureCut2(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, long keepPieceIndex)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x101b6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2, keepPieceIndex);
}

void IModelDoc::InsertCutSurface(BOOL flip, long keepPieceIndex)
{
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x101b7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 flip, keepPieceIndex);
}

LPDISPATCH IModelDoc::GetDetailingDefaults()
{
	LPDISPATCH result;
	InvokeHelper(0x101b8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IGetDetailingDefaults()
{
	LPDISPATCH result;
	InvokeHelper(0x101b9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IModelDoc::ListExternalFileReferencesCount(BOOL useSearchRules)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x101ba, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		useSearchRules);
	return result;
}

VARIANT IModelDoc::ListExternalFileReferences(BOOL useSearchRules)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x101bb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		useSearchRules);
	return result;
}

CString IModelDoc::IListExternalFileReferences(BOOL useSearchRules, long numRefs)
{
	CString result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x101bc, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		useSearchRules, numRefs);
	return result;
}

BOOL IModelDoc::SketchSplineByEqnParams2(const VARIANT& paramsIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x101bd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&paramsIn);
	return result;
}

LPDISPATCH IModelDoc::GetFirstModelView()
{
	LPDISPATCH result;
	InvokeHelper(0x101be, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IGetFirstModelView()
{
	LPDISPATCH result;
	InvokeHelper(0x101bf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::InsertPlanarRefSurface()
{
	BOOL result;
	InvokeHelper(0x101c0, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x101c1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::GetSaveFlag()
{
	BOOL result;
	InvokeHelper(0x101c2, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::AddCustomInfo3(LPCTSTR configuration, LPCTSTR FieldName, long FieldType, LPCTSTR FieldValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x101c3, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		configuration, FieldName, FieldType, FieldValue);
	return result;
}

CString IModelDoc::GetCustomInfo2(LPCTSTR configuration, LPCTSTR FieldName)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x101c4, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms,
		configuration, FieldName);
	return result;
}

void IModelDoc::SetCustomInfo2(LPCTSTR configuration, LPCTSTR FieldName, LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x101c4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 configuration, FieldName, lpszNewValue);
}

BOOL IModelDoc::DeleteCustomInfo2(LPCTSTR configuration, LPCTSTR FieldName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x101c5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		configuration, FieldName);
	return result;
}

long IModelDoc::GetCustomInfoCount2(LPCTSTR configuration)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x101c6, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		configuration);
	return result;
}

VARIANT IModelDoc::GetCustomInfoNames2(LPCTSTR configuration)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x101c7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		configuration);
	return result;
}

CString IModelDoc::IGetCustomInfoNames2(LPCTSTR configuration)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x101c8, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		configuration);
	return result;
}

long IModelDoc::GetCustomInfoType3(LPCTSTR configuration, LPCTSTR FieldName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x101c9, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		configuration, FieldName);
	return result;
}

BOOL IModelDoc::GetConsiderLeadersAsLines()
{
	BOOL result;
	InvokeHelper(0x101ca, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::SetConsiderLeadersAsLines(BOOL leadersAsLines)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x101cb, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		leadersAsLines);
	return result;
}

void IModelDoc::InsertRevolvedRefSurface(double angle, BOOL reverseDir, double angle2, long revType)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_R8 VTS_I4;
	InvokeHelper(0x101cc, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 angle, reverseDir, angle2, revType);
}

long IModelDoc::GetBendState()
{
	long result;
	InvokeHelper(0x101cd, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IModelDoc::SetBendState(long bendState)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x101ce, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		bendState);
	return result;
}

BOOL IModelDoc::GetShowFeatureErrorDialog()
{
	BOOL result;
	InvokeHelper(0x101cf, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::SetShowFeatureErrorDialog(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x101cf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

void IModelDoc::ClearUndoList()
{
	InvokeHelper(0x101d0, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IModelDoc::GetFeatureManagerWidth()
{
	long result;
	InvokeHelper(0x101d1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IModelDoc::SetFeatureManagerWidth(long Width)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x101d2, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Width);
	return result;
}

LPDISPATCH IModelDoc::InsertProjectedSketch2(long reverse)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x101d3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		reverse);
	return result;
}

LPDISPATCH IModelDoc::IInsertProjectedSketch2(long reverse)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x101d4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		reverse);
	return result;
}

long IModelDoc::GetFeatureCount()
{
	long result;
	InvokeHelper(0x101d5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::FeatureByPositionReverse(long num)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x101d6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		num);
	return result;
}

LPDISPATCH IModelDoc::IFeatureByPositionReverse(long num)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x101d7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		num);
	return result;
}

long IModelDoc::RayIntersections(const VARIANT& bodiesIn, const VARIANT& basePointsIn, const VARIANT& vectorsIn, long options, double hitRadius, double offset)
{
	long result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_I4 VTS_R8 VTS_R8;
	InvokeHelper(0x101d8, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		&bodiesIn, &basePointsIn, &vectorsIn, options, hitRadius, offset);
	return result;
}

long IModelDoc::IRayIntersections(LPDISPATCH* bodiesIn, long numBodies, double* basePointsIn, double* vectorsIn, long numRays, long options, double hitRadius, double offset)
{
	long result;
	static BYTE parms[] =
		VTS_PDISPATCH VTS_I4 VTS_PR8 VTS_PR8 VTS_I4 VTS_I4 VTS_R8 VTS_R8;
	InvokeHelper(0x101d9, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		bodiesIn, numBodies, basePointsIn, vectorsIn, numRays, options, hitRadius, offset);
	return result;
}

VARIANT IModelDoc::GetRayIntersectionsPoints()
{
	VARIANT result;
	InvokeHelper(0x101da, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IModelDoc::IGetRayIntersectionsPoints()
{
	double result;
	InvokeHelper(0x101db, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IModelDoc::GetRayIntersectionsTopology()
{
	VARIANT result;
	InvokeHelper(0x101dc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IModelDoc::IGetRayIntersectionsTopology()
{
	LPUNKNOWN result;
	InvokeHelper(0x101dd, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

void IModelDoc::EditSeedFeat()
{
	InvokeHelper(0x101de, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::EditSuppress()
{
	BOOL result;
	InvokeHelper(0x101df, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::EditUnsuppress()
{
	BOOL result;
	InvokeHelper(0x101e0, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::EditUnsuppressDependent()
{
	BOOL result;
	InvokeHelper(0x101e1, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::EditRollback()
{
	BOOL result;
	InvokeHelper(0x101e2, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IModelDoc::Save2(BOOL silent)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x101e3, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		silent);
	return result;
}

long IModelDoc::SaveAs2(LPCTSTR newName, long saveAsVersion, BOOL saveAsCopy, BOOL silent)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x101e4, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		newName, saveAsVersion, saveAsCopy, silent);
	return result;
}

void IModelDoc::SetPopupMenuMode(long modeIn)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x101e5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 modeIn);
}

long IModelDoc::GetPopupMenuMode()
{
	long result;
	InvokeHelper(0x101e6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelDoc::CloseFamilyTable()
{
	InvokeHelper(0x101e7, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::CreatePlaneAtSurface(long interIndex, BOOL projOpt, BOOL reverseDir, BOOL normalPlane, double angle)
{
	static BYTE parms[] =
		VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8;
	InvokeHelper(0x101e8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 interIndex, projOpt, reverseDir, normalPlane, angle);
}

void IModelDoc::SketchOffset(double offset, BOOL contourMode)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x101e9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 offset, contourMode);
}

BOOL IModelDoc::CreateLinearSketchStepAndRepeat(long numX, long numY, double spacingX, double spacingY, double angleX, double angleY, LPCTSTR deleteInstances)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_BSTR;
	InvokeHelper(0x101ea, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		numX, numY, spacingX, spacingY, angleX, angleY, deleteInstances);
	return result;
}

BOOL IModelDoc::SetAmbientLightProperties(LPCTSTR Name, double ambient, double diffuse, double specular, long colour, BOOL enabled, BOOL fixed)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x101eb, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, ambient, diffuse, specular, colour, enabled, fixed);
	return result;
}

BOOL IModelDoc::GetAmbientLightProperties(LPCTSTR Name, double* ambient, double* diffuse, double* specular, long* colour, BOOL* enabled, BOOL* fixed)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_PR8 VTS_PR8 VTS_PR8 VTS_PI4 VTS_PBOOL VTS_PBOOL;
	InvokeHelper(0x101ec, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, ambient, diffuse, specular, colour, enabled, fixed);
	return result;
}

BOOL IModelDoc::SetPointLightProperties(LPCTSTR Name, double ambient, double diffuse, double specular, long colour, BOOL enabled, BOOL fixed, double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x101ed, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, ambient, diffuse, specular, colour, enabled, fixed, x, y, z);
	return result;
}

BOOL IModelDoc::GetPointLightProperties(LPCTSTR Name, double* ambient, double* diffuse, double* specular, long* colour, BOOL* enabled, BOOL* fixed, double* x, double* y, double* z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_PR8 VTS_PR8 VTS_PR8 VTS_PI4 VTS_PBOOL VTS_PBOOL VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x101ee, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, ambient, diffuse, specular, colour, enabled, fixed, x, y, z);
	return result;
}

BOOL IModelDoc::SetDirectionLightProperties(LPCTSTR Name, double ambient, double diffuse, double specular, long colour, BOOL enabled, BOOL fixed, double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x101ef, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, ambient, diffuse, specular, colour, enabled, fixed, x, y, z);
	return result;
}

BOOL IModelDoc::GetDirectionLightProperties(LPCTSTR Name, double* ambient, double* diffuse, double* specular, long* colour, BOOL* enabled, BOOL* fixed, double* x, double* y, double* z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_PR8 VTS_PR8 VTS_PR8 VTS_PI4 VTS_PBOOL VTS_PBOOL VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x101f0, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, ambient, diffuse, specular, colour, enabled, fixed, x, y, z);
	return result;
}

BOOL IModelDoc::SetSpotlightProperties(LPCTSTR Name, double ambient, double diffuse, double specular, long colour, BOOL enabled, BOOL fixed, double posx, double posy, double posz, double targetx, double targety, double targetz, double coneAngle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x101f1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, ambient, diffuse, specular, colour, enabled, fixed, posx, posy, posz, targetx, targety, targetz, coneAngle);
	return result;
}

BOOL IModelDoc::GetSpotlightProperties(LPCTSTR Name, double* ambient, double* diffuse, double* specular, long* colour, BOOL* enabled, BOOL* fixed, double* x, double* y, double* z, double* targetx, double* targety, double* targetz, double* coneAngle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_PR8 VTS_PR8 VTS_PR8 VTS_PI4 VTS_PBOOL VTS_PBOOL VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x101f2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, ambient, diffuse, specular, colour, enabled, fixed, x, y, z, targetx, targety, targetz, coneAngle);
	return result;
}

void IModelDoc::SplitOpenSegment(double x, double y, double z)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x101f3, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x, y, z);
}

void IModelDoc::AutoInferToggle()
{
	InvokeHelper(0x101f4, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SketchRectangleAtAnyAngle(double val1, double val2, double z1, double val3, double val4, double z2, double val3x, double val3y, double z3, BOOL val5)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_BOOL;
	InvokeHelper(0x101f5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 val1, val2, z1, val3, val4, z2, val3x, val3y, z3, val5);
}

BOOL IModelDoc::CreateCircularSketchStepAndRepeat(double arcRadius, double arcAngle, long patternNum, double patternSpacing, BOOL patternRotate, LPCTSTR deleteInstances)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_I4 VTS_R8 VTS_BOOL VTS_BSTR;
	InvokeHelper(0x101f6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		arcRadius, arcAngle, patternNum, patternSpacing, patternRotate, deleteInstances);
	return result;
}

void IModelDoc::SplitClosedSegment(double x0, double y0, double z0, double x1, double y1, double z1)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x101f7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x0, y0, z0, x1, y1, z1);
}

BOOL IModelDoc::IsLightLockedToModel(long lightId)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x101f8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		lightId);
	return result;
}

BOOL IModelDoc::LockLightToModel(long lightId, BOOL fix)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0x101f9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		lightId, fix);
	return result;
}

long IModelDoc::FeatureFillet3(double r1, BOOL propagate, long ftyp, BOOL varRadTyp, long overFlowType, long nRadii, const VARIANT& radii, BOOL useHelpPoint, BOOL useTangentHoldLine)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_I4 VTS_BOOL VTS_I4 VTS_I4 VTS_VARIANT VTS_BOOL VTS_BOOL;
	InvokeHelper(0x101fa, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		r1, propagate, ftyp, varRadTyp, overFlowType, nRadii, &radii, useHelpPoint, useTangentHoldLine);
	return result;
}

long IModelDoc::IFeatureFillet3(double r1, BOOL propagate, long ftyp, BOOL varRadTyp, long overFlowType, long nRadii, double* radii, BOOL useHelpPoint, BOOL useTangentHoldLine)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_I4 VTS_BOOL VTS_I4 VTS_I4 VTS_PR8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x101fb, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		r1, propagate, ftyp, varRadTyp, overFlowType, nRadii, radii, useHelpPoint, useTangentHoldLine);
	return result;
}

void IModelDoc::InsertConnectionPoint()
{
	InvokeHelper(0x101fc, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertRoutePoint()
{
	InvokeHelper(0x101fd, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::FeatureBossThicken2(double thickness, long direction, long faceIndex, BOOL fillVolume)
{
	static BYTE parms[] =
		VTS_R8 VTS_I4 VTS_I4 VTS_BOOL;
	InvokeHelper(0x101fe, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 thickness, direction, faceIndex, fillVolume);
}

void IModelDoc::FeatureCutThicken2(double thickness, long direction, long faceIndex, BOOL fillVolume)
{
	static BYTE parms[] =
		VTS_R8 VTS_I4 VTS_I4 VTS_BOOL;
	InvokeHelper(0x101ff, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 thickness, direction, faceIndex, fillVolume);
}

LPDISPATCH IModelDoc::GetConfigurationByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10200, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IModelDoc::IGetConfigurationByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10201, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IModelDoc::CreatePoint2(double pointX, double pointY, double pointZ)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10202, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		pointX, pointY, pointZ);
	return result;
}

LPDISPATCH IModelDoc::ICreatePoint2(double pointX, double pointY, double pointZ)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10203, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		pointX, pointY, pointZ);
	return result;
}

LPDISPATCH IModelDoc::CreateLine2(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10204, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		p1x, p1y, p1z, p2x, p2y, p2z);
	return result;
}

LPDISPATCH IModelDoc::ICreateLine2(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10205, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		p1x, p1y, p1z, p2x, p2y, p2z);
	return result;
}

LPDISPATCH IModelDoc::GetActiveSketch2()
{
	LPDISPATCH result;
	InvokeHelper(0x10206, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IGetActiveSketch2()
{
	LPDISPATCH result;
	InvokeHelper(0x10207, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IModelDoc::DrawLightIcons()
{
	InvokeHelper(0x10208, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IModelDoc::GetLayerManager()
{
	LPDISPATCH result;
	InvokeHelper(0x10209, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IGetLayerManager()
{
	LPDISPATCH result;
	InvokeHelper(0x1020a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::CreateCircle2(double xC, double yC, double zc, double xp, double yp, double zp)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1020b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		xC, yC, zc, xp, yp, zp);
	return result;
}

LPDISPATCH IModelDoc::ICreateCircle2(double xC, double yC, double zc, double xp, double yp, double zp)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1020c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		xC, yC, zc, xp, yp, zp);
	return result;
}

LPDISPATCH IModelDoc::CreateCircleByRadius2(double xC, double yC, double zc, double radius)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1020d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		xC, yC, zc, radius);
	return result;
}

LPDISPATCH IModelDoc::ICreateCircleByRadius2(double xC, double yC, double zc, double radius)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1020e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		xC, yC, zc, radius);
	return result;
}

LPDISPATCH IModelDoc::CreateArc2(double xC, double yC, double zc, double xp1, double yp1, double zp1, double xp2, double yp2, double zp2, short direction)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I2;
	InvokeHelper(0x1020f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		xC, yC, zc, xp1, yp1, zp1, xp2, yp2, zp2, direction);
	return result;
}

LPDISPATCH IModelDoc::ICreateArc2(double xC, double yC, double zc, double xp1, double yp1, double zp1, double xp2, double yp2, double zp2, short direction)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I2;
	InvokeHelper(0x10210, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		xC, yC, zc, xp1, yp1, zp1, xp2, yp2, zp2, direction);
	return result;
}

LPDISPATCH IModelDoc::CreateEllipse2(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10211, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		centerX, centerY, centerZ, majorX, majorY, majorZ, minorX, minorY, minorZ);
	return result;
}

LPDISPATCH IModelDoc::ICreateEllipse2(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10212, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		centerX, centerY, centerZ, majorX, majorY, majorZ, minorX, minorY, minorZ);
	return result;
}

LPDISPATCH IModelDoc::CreateEllipticalArc2(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ, double startX, double startY, double startZ, double endX, double endY, 
		double endZ)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10213, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		centerX, centerY, centerZ, majorX, majorY, majorZ, minorX, minorY, minorZ, startX, startY, startZ, endX, endY, endZ);
	return result;
}

LPDISPATCH IModelDoc::ICreateEllipticalArc2(double centerX, double centerY, double centerZ, double majorX, double majorY, double majorZ, double minorX, double minorY, double minorZ, double startX, double startY, double startZ, double endX, double endY, 
		double endZ)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10214, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		centerX, centerY, centerZ, majorX, majorY, majorZ, minorX, minorY, minorZ, startX, startY, startZ, endX, endY, endZ);
	return result;
}

LPDISPATCH IModelDoc::CreateSpline(const VARIANT& pointData)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x10215, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&pointData);
	return result;
}

LPDISPATCH IModelDoc::ICreateSpline(long pointCount, double* pointData)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_PR8;
	InvokeHelper(0x10216, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		pointCount, pointData);
	return result;
}

void IModelDoc::ViewZoomtofit2()
{
	InvokeHelper(0x10217, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::SetInferenceMode(BOOL inferenceMode)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10218, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 inferenceMode);
}

BOOL IModelDoc::GetInferenceMode()
{
	BOOL result;
	InvokeHelper(0x10219, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::SetTitle2(LPCTSTR newTitle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1021a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		newTitle);
	return result;
}

BOOL IModelDoc::SketchFillet2(double rad, short constrainedCorners)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_I2;
	InvokeHelper(0x1021b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		rad, constrainedCorners);
	return result;
}

BOOL IModelDoc::IsTessellationValid()
{
	BOOL result;
	InvokeHelper(0x1021c, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::EditRoute()
{
	InvokeHelper(0x1021d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::FileReload()
{
	InvokeHelper(0x1021e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IModelDoc::GetDesignTable()
{
	LPDISPATCH result;
	InvokeHelper(0x1021f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IGetDesignTable()
{
	LPDISPATCH result;
	InvokeHelper(0x10220, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IModelDoc::GetEntityName(LPDISPATCH entity)
{
	CString result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x10221, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		entity);
	return result;
}

CString IModelDoc::IGetEntityName(LPDISPATCH entity)
{
	CString result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x10222, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		entity);
	return result;
}

LPDISPATCH IModelDoc::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x10223, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::ShowConfiguration2(LPCTSTR configurationName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10224, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		configurationName);
	return result;
}

BOOL IModelDoc::AddConfiguration2(LPCTSTR Name, LPCTSTR comment, LPCTSTR alternateName, BOOL suppressByDefault, BOOL hideByDefault, BOOL minFeatureManager, BOOL inheritProperties, unsigned long flags)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x10225, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, comment, alternateName, suppressByDefault, hideByDefault, minFeatureManager, inheritProperties, flags);
	return result;
}

BOOL IModelDoc::DeleteConfiguration2(LPCTSTR configurationName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10226, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		configurationName);
	return result;
}

BOOL IModelDoc::EditConfiguration2(LPCTSTR Name, LPCTSTR newName, LPCTSTR comment, LPCTSTR alternateName, BOOL suppressByDefault, BOOL hideByDefault, BOOL minFeatureManager, BOOL inheritProperties, unsigned long flags)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x10227, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, newName, comment, alternateName, suppressByDefault, hideByDefault, minFeatureManager, inheritProperties, flags);
	return result;
}

LPDISPATCH IModelDoc::CreateSplineByEqnParams(const VARIANT& paramsIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x10228, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&paramsIn);
	return result;
}

LPDISPATCH IModelDoc::ICreateSplineByEqnParams(long* propArray, double* knotsArray, double* cntrlPntCoordArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4 VTS_PR8 VTS_PR8;
	InvokeHelper(0x10229, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		propArray, knotsArray, cntrlPntCoordArray);
	return result;
}

LPDISPATCH IModelDoc::CreateFeatureMgrView2(long* bitmap, LPCTSTR toolTip)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4 VTS_BSTR;
	InvokeHelper(0x1022a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		bitmap, toolTip);
	return result;
}

LPDISPATCH IModelDoc::ICreateFeatureMgrView2(long* bitmap, LPCTSTR toolTip)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4 VTS_BSTR;
	InvokeHelper(0x1022b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		bitmap, toolTip);
	return result;
}

BOOL IModelDoc::AddFeatureMgrView2(long* bitmap, long* appView, LPCTSTR toolTip)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PI4 VTS_PI4 VTS_BSTR;
	InvokeHelper(0x1022c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bitmap, appView, toolTip);
	return result;
}

void IModelDoc::FeatureCut3(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, long keepPieceIndex)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x1022d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2, keepPieceIndex);
}

LPDISPATCH IModelDoc::GetFirstAnnotation2()
{
	LPDISPATCH result;
	InvokeHelper(0x1022e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IGetFirstAnnotation2()
{
	LPDISPATCH result;
	InvokeHelper(0x1022f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IModelDoc::InsertExtendSurface(BOOL extendLinear, long endCondition, double distance)
{
	static BYTE parms[] =
		VTS_BOOL VTS_I4 VTS_R8;
	InvokeHelper(0x10230, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 extendLinear, endCondition, distance);
}

void IModelDoc::InsertTangencySurface(BOOL oneSide, BOOL isFlip)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10231, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 oneSide, isFlip);
}

VARIANT IModelDoc::CreateSplinesByEqnParams(const VARIANT& paramsIn)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x10232, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		&paramsIn);
	return result;
}

LPUNKNOWN IModelDoc::ICreateSplinesByEqnParams(long* propArray, double* knotsArray, double* cntrlPntCoordArray)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_PI4 VTS_PR8 VTS_PR8;
	InvokeHelper(0x10233, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		propArray, knotsArray, cntrlPntCoordArray);
	return result;
}

VARIANT IModelDoc::CreateClippedSplines(const VARIANT& paramsIn, double x1, double y1, double x2, double y2)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_VARIANT VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10234, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		&paramsIn, x1, y1, x2, y2);
	return result;
}

LPUNKNOWN IModelDoc::ICreateClippedSplines(long* propArray, double* knotsArray, double* cntrlPntCoordArray, double x1, double y1, double x2, double y2)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_PI4 VTS_PR8 VTS_PR8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10235, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		propArray, knotsArray, cntrlPntCoordArray, x1, y1, x2, y2);
	return result;
}

BOOL IModelDoc::EditSuppress2()
{
	BOOL result;
	InvokeHelper(0x10236, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::EditUnsuppress2()
{
	BOOL result;
	InvokeHelper(0x10237, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::EditUnsuppressDependent2()
{
	BOOL result;
	InvokeHelper(0x10238, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::EditRollback2()
{
	BOOL result;
	InvokeHelper(0x10239, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelDoc::HideDimension()
{
	InvokeHelper(0x1023a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ShowFeatureDimensions()
{
	InvokeHelper(0x1023b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::HideFeatureDimensions()
{
	InvokeHelper(0x1023c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::Sketch3DIntersections()
{
	InvokeHelper(0x1023d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IModelDoc::FeatureFillet4(double r1, BOOL propagate, BOOL uniformRadius, long ftyp, BOOL varRadTyp, long overFlowType, long nRadii, const VARIANT& radii, BOOL useHelpPoint, BOOL useTangentHoldLine, BOOL cornerType, long setbackDistCount, 
		const VARIANT& setBackDistances)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_I4 VTS_BOOL VTS_I4 VTS_I4 VTS_VARIANT VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_VARIANT;
	InvokeHelper(0x1023e, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		r1, propagate, uniformRadius, ftyp, varRadTyp, overFlowType, nRadii, &radii, useHelpPoint, useTangentHoldLine, cornerType, setbackDistCount, &setBackDistances);
	return result;
}

long IModelDoc::IFeatureFillet4(double r1, BOOL propagate, BOOL uniformRadius, long ftyp, BOOL varRadTyp, long overFlowType, long nRadii, double* radii, BOOL useHelpPoint, BOOL useTangentHoldLine, BOOL cornerType, long setbackDistCount, 
		double* setBackDistances)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_I4 VTS_BOOL VTS_I4 VTS_I4 VTS_PR8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_PR8;
	InvokeHelper(0x1023f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		r1, propagate, uniformRadius, ftyp, varRadTyp, overFlowType, nRadii, radii, useHelpPoint, useTangentHoldLine, cornerType, setbackDistCount, setBackDistances);
	return result;
}

void IModelDoc::InsertDeleteFace()
{
	InvokeHelper(0x10240, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IModelDoc::GetDependencies2(BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10241, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		traverseflag, searchflag, addReadOnlyInfo);
	return result;
}

CString IModelDoc::IGetDependencies2(BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo)
{
	CString result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10242, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		traverseflag, searchflag, addReadOnlyInfo);
	return result;
}

long IModelDoc::IGetNumDependencies2(BOOL traverseflag, BOOL searchflag, BOOL addReadOnlyInfo)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x10243, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		traverseflag, searchflag, addReadOnlyInfo);
	return result;
}

void IModelDoc::InsertScale(double scaleFactor_x, double scaleFactor_y, double scaleFactor_z, BOOL isUniform, long scaleType)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_I4;
	InvokeHelper(0x10244, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 scaleFactor_x, scaleFactor_y, scaleFactor_z, isUniform, scaleType);
}

void IModelDoc::LockAllExternalReferences()
{
	InvokeHelper(0x10245, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::UnlockAllExternalReferences()
{
	InvokeHelper(0x10246, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::BreakAllExternalReferences()
{
	InvokeHelper(0x10247, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::EditDimensionProperties(long tolType, double tolMax, double tolMin, LPCTSTR tolMaxFit, LPCTSTR tolMinFit, BOOL useDocPrec, long precision, long arrowsIn, BOOL useDocArrows, long arrow1, long arrow2)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_BSTR VTS_BSTR VTS_BOOL VTS_I4 VTS_I4 VTS_BOOL VTS_I4 VTS_I4;
	InvokeHelper(0x10248, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		tolType, tolMax, tolMin, tolMaxFit, tolMinFit, useDocPrec, precision, arrowsIn, useDocArrows, arrow1, arrow2);
	return result;
}

BOOL IModelDoc::SketchPolygon(double xCenter, double yCenter, double xEdge, double yEdge, long nSides, BOOL bInscribed)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_BOOL;
	InvokeHelper(0x10249, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		xCenter, yCenter, xEdge, yEdge, nSides, bInscribed);
	return result;
}

long IModelDoc::GetBlockingState()
{
	long result;
	InvokeHelper(0x1024a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::CreateFeatureMgrView3(long* bitmap, LPCTSTR toolTip, long whichPane)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4 VTS_BSTR VTS_I4;
	InvokeHelper(0x1024b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		bitmap, toolTip, whichPane);
	return result;
}

LPDISPATCH IModelDoc::ICreateFeatureMgrView3(long* bitmap, LPCTSTR toolTip, long whichPane)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4 VTS_BSTR VTS_I4;
	InvokeHelper(0x1024c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		bitmap, toolTip, whichPane);
	return result;
}

BOOL IModelDoc::AddFeatureMgrView3(long* bitmap, long* appView, LPCTSTR toolTip, long whichPane)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PI4 VTS_PI4 VTS_BSTR VTS_I4;
	InvokeHelper(0x1024d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bitmap, appView, toolTip, whichPane);
	return result;
}

LPDISPATCH IModelDoc::CreatePlaneAtOffset2(double val, BOOL flipDir)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x1024e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		val, flipDir);
	return result;
}

LPDISPATCH IModelDoc::ICreatePlaneAtOffset2(double val, BOOL flipDir)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x1024f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		val, flipDir);
	return result;
}

LPDISPATCH IModelDoc::CreatePlaneAtAngle2(double val, BOOL flipDir)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x10250, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		val, flipDir);
	return result;
}

LPDISPATCH IModelDoc::ICreatePlaneAtAngle2(double val, BOOL flipDir)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x10251, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		val, flipDir);
	return result;
}

LPDISPATCH IModelDoc::CreatePlaneThru3Points2()
{
	LPDISPATCH result;
	InvokeHelper(0x10252, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::ICreatePlaneThru3Points2()
{
	LPDISPATCH result;
	InvokeHelper(0x10253, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::CreatePlanePerCurveAndPassPoint2(BOOL origAtCurve)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10254, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		origAtCurve);
	return result;
}

LPDISPATCH IModelDoc::ICreatePlanePerCurveAndPassPoint2(BOOL origAtCurve)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10255, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		origAtCurve);
	return result;
}

LPDISPATCH IModelDoc::CreatePlaneAtSurface2(long interIndex, BOOL projOpt, BOOL reverseDir, BOOL normalPlane, double angle)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8;
	InvokeHelper(0x10256, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		interIndex, projOpt, reverseDir, normalPlane, angle);
	return result;
}

LPDISPATCH IModelDoc::ICreatePlaneAtSurface2(long interIndex, BOOL projOpt, BOOL reverseDir, BOOL normalPlane, double angle)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8;
	InvokeHelper(0x10257, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		interIndex, projOpt, reverseDir, normalPlane, angle);
	return result;
}

long IModelDoc::GetUserPreferenceIntegerValue(long userPreferenceValue)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10258, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		userPreferenceValue);
	return result;
}

BOOL IModelDoc::SetUserPreferenceIntegerValue(long userPreferenceValue, long Value)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x10259, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreferenceValue, Value);
	return result;
}

LPDISPATCH IModelDoc::GetUserPreferenceTextFormat(long userPreferenceValue)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1025a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		userPreferenceValue);
	return result;
}

LPDISPATCH IModelDoc::IGetUserPreferenceTextFormat(long userPreferenceValue)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1025b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		userPreferenceValue);
	return result;
}

BOOL IModelDoc::SetUserPreferenceTextFormat(long userPreferenceValue, LPDISPATCH Value)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x1025c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreferenceValue, Value);
	return result;
}

BOOL IModelDoc::ISetUserPreferenceTextFormat(long userPreferenceValue, LPDISPATCH Value)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x1025d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreferenceValue, Value);
	return result;
}

void IModelDoc::InsertRib2(BOOL is2Sided, BOOL reverseThicknessDir, double thickness, long referenceEdgeIndex, BOOL reverseMaterialDir, BOOL isDrafted, BOOL draftOutward, double draftAngle, BOOL isNormToSketch)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_R8 VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_BOOL;
	InvokeHelper(0x1025e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 is2Sided, reverseThicknessDir, thickness, referenceEdgeIndex, reverseMaterialDir, isDrafted, draftOutward, draftAngle, isNormToSketch);
}

BOOL IModelDoc::InsertObjectFromFile(LPCTSTR filePath, BOOL createLink, double xx, double yy, double zz)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1025f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		filePath, createLink, xx, yy, zz);
	return result;
}

void IModelDoc::InspectCurvature()
{
	InvokeHelper(0x10260, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::RemoveInspectCurvature()
{
	InvokeHelper(0x10261, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IModelDoc::InsertDatumTag2()
{
	LPDISPATCH result;
	InvokeHelper(0x10262, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IInsertDatumTag2()
{
	LPDISPATCH result;
	InvokeHelper(0x10263, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IModelDoc::ActivateFeatureMgrView(long* appView)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x10264, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		appView);
	return result;
}

void IModelDoc::FeatureSketchDrivenPattern(BOOL useCentroid)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10265, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 useCentroid);
}

void IModelDoc::HideShowBodies()
{
	InvokeHelper(0x10266, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::HideSolidBody()
{
	InvokeHelper(0x10267, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ShowSolidBody()
{
	InvokeHelper(0x10268, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertFramePoint(double xx, double yy, double zz)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x10269, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 xx, yy, zz);
}

void IModelDoc::LockFramePoint()
{
	InvokeHelper(0x1026a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::UnlockFramePoint()
{
	InvokeHelper(0x1026b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IModelDoc::InsertGtol()
{
	LPDISPATCH result;
	InvokeHelper(0x1026c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IInsertGtol()
{
	LPDISPATCH result;
	InvokeHelper(0x1026d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IModelDoc::DeActivateFeatureMgrView(long* appView)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x1026e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appView);
	return result;
}

LPDISPATCH IModelDoc::InsertNote(LPCTSTR text)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1026f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		text);
	return result;
}

LPDISPATCH IModelDoc::IInsertNote(LPCTSTR text)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10270, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		text);
	return result;
}

void IModelDoc::SetSaveAsFileName(LPCTSTR fileName)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10271, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 fileName);
}

void IModelDoc::ClosePrintPreview()
{
	InvokeHelper(0x10272, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::HideComponent2()
{
	InvokeHelper(0x10273, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::ShowComponent2()
{
	InvokeHelper(0x10274, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::SaveBMP(LPCTSTR filenameIn, long widthIn, long heightIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0x10275, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		filenameIn, widthIn, heightIn);
	return result;
}

void IModelDoc::InsertSketch2(BOOL updateEditRebuild)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10276, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 updateEditRebuild);
}

void IModelDoc::Insert3DSketch2(BOOL updateEditRebuild)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10277, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 updateEditRebuild);
}

void IModelDoc::InsertDeleteHole()
{
	InvokeHelper(0x10278, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::PreTrimSurface(BOOL bMutualTrimIn)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10279, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 bMutualTrimIn);
}

void IModelDoc::PostTrimSurface(BOOL bSewSurfaceIn)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1027a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 bSewSurfaceIn);
}

void IModelDoc::SketchConvertIsoCurves(double percentRatio, BOOL vORuDir, BOOL doConstrain, BOOL skipHoles)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1027b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 percentRatio, vORuDir, doConstrain, skipHoles);
}

void IModelDoc::SelectLoop()
{
	InvokeHelper(0x1027c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertSheetMetalBaseFlange(double thickness, BOOL thickenDir, double radius, double extrudeDist1, double extrudeDist2, BOOL flipExtruDir, long endCondition1, long endCondition2, long dirToUse)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x1027d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 thickness, thickenDir, radius, extrudeDist1, extrudeDist2, flipExtruDir, endCondition1, endCondition2, dirToUse);
}

void IModelDoc::InsertSheetMetalFold()
{
	InvokeHelper(0x1027e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertSheetMetalUnfold()
{
	InvokeHelper(0x1027f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertSheetMetalMiterFlange(BOOL useReliefRatio, BOOL useDefaultGap, BOOL useAutoRelief, double globalRadius, double ripGap, double autoReliefRatio, double autoReliefWidth, double autoReliefDepth, long reliefType, long ripLocation, 
		BOOL trimSideBends)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_BOOL;
	InvokeHelper(0x10280, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 useReliefRatio, useDefaultGap, useAutoRelief, globalRadius, ripGap, autoReliefRatio, autoReliefWidth, autoReliefDepth, reliefType, ripLocation, trimSideBends);
}

void IModelDoc::CreateGroup()
{
	InvokeHelper(0x10281, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::RemoveItemsFromGroup()
{
	InvokeHelper(0x10282, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::RemoveGroups()
{
	InvokeHelper(0x10283, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IModelDoc::InsertBOMBalloon2(long Style, long size, long upperTextStyle, LPCTSTR upperText, long lowerTextStyle, LPCTSTR lowerText)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x10284, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Style, size, upperTextStyle, upperText, lowerTextStyle, lowerText);
	return result;
}

LPDISPATCH IModelDoc::IInsertBOMBalloon2(long Style, long size, long upperTextStyle, LPCTSTR upperText, long lowerTextStyle, LPCTSTR lowerText)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x10285, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Style, size, upperTextStyle, upperText, lowerTextStyle, lowerText);
	return result;
}

void IModelDoc::EditRedo(unsigned long nSteps)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10286, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 nSteps);
}

void IModelDoc::InsertProtrusionBlend4(BOOL closed, BOOL keepTangency, BOOL forceNonRational, double tessToleranceFactor, short startMatchingType, short endMatchingType, BOOL isThinBody, double thickness1, double thickness2, short thinType)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_I2 VTS_I2 VTS_BOOL VTS_R8 VTS_R8 VTS_I2;
	InvokeHelper(0x10287, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 closed, keepTangency, forceNonRational, tessToleranceFactor, startMatchingType, endMatchingType, isThinBody, thickness1, thickness2, thinType);
}

void IModelDoc::InsertCutBlend4(BOOL closed, BOOL keepTangency, BOOL forceNonRational, double tessToleranceFactor, short startMatchingType, short endMatchingType, BOOL isThinBody, double thickness1, double thickness2, short thinType)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_I2 VTS_I2 VTS_BOOL VTS_R8 VTS_R8 VTS_I2;
	InvokeHelper(0x10288, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 closed, keepTangency, forceNonRational, tessToleranceFactor, startMatchingType, endMatchingType, isThinBody, thickness1, thickness2, thinType);
}

void IModelDoc::InsertProtrusionSwept4(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational, short startMatchingType, short endMatchingType, BOOL isThinBody, double thickness1, double thickness2, 
		short thinType)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_I2 VTS_BOOL VTS_BOOL VTS_I2 VTS_I2 VTS_BOOL VTS_R8 VTS_R8 VTS_I2;
	InvokeHelper(0x10289, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 propagate, alignment, twistCtrlOption, keepTangency, forceNonRational, startMatchingType, endMatchingType, isThinBody, thickness1, thickness2, thinType);
}

void IModelDoc::InsertCutSwept4(BOOL propagate, BOOL alignment, short twistCtrlOption, BOOL keepTangency, BOOL forceNonRational, short startMatchingType, short endMatchingType, BOOL isThinBody, double thickness1, double thickness2, short thinType)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_I2 VTS_BOOL VTS_BOOL VTS_I2 VTS_I2 VTS_BOOL VTS_R8 VTS_R8 VTS_I2;
	InvokeHelper(0x1028a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 propagate, alignment, twistCtrlOption, keepTangency, forceNonRational, startMatchingType, endMatchingType, isThinBody, thickness1, thickness2, thinType);
}

void IModelDoc::SelectTangency()
{
	InvokeHelper(0x1028b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::InsertBendTableOpen(LPCTSTR fileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1028c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName);
	return result;
}

BOOL IModelDoc::InsertBendTableNew(LPCTSTR fileName, LPCTSTR units, LPCTSTR type)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1028d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName, units, type);
	return result;
}

void IModelDoc::InsertBendTableEdit()
{
	InvokeHelper(0x1028e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::DeleteBendTable()
{
	InvokeHelper(0x1028f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::InsertSheetMetal3dBend(double angle, double radius, BOOL flipDir, short bendPos)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_BOOL VTS_I2;
	InvokeHelper(0x10290, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 angle, radius, flipDir, bendPos);
}

BOOL IModelDoc::CreateTangentArc2(double p1x, double p1y, double p1z, double p2x, double p2y, double p2z, long arcTypeIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I4;
	InvokeHelper(0x10291, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		p1x, p1y, p1z, p2x, p2y, p2z, arcTypeIn);
	return result;
}

VARIANT IModelDoc::GetMassProperties2(long* status)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x10292, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		status);
	return result;
}

double IModelDoc::IGetMassProperties2(long* status)
{
	double result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x10293, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		status);
	return result;
}

void IModelDoc::SketchChamfer(double angleORdist, double dist1, long options)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_I4;
	InvokeHelper(0x10294, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 angleORdist, dist1, options);
}

void IModelDoc::FeatureCut4(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, long keepPieceIndex, BOOL normalCut)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_I4 VTS_BOOL;
	InvokeHelper(0x10295, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2, keepPieceIndex, normalCut);
}

LPDISPATCH IModelDoc::GetPropertyManagerPage(long dialogId, LPCTSTR title, LPUNKNOWN handler)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_UNKNOWN;
	InvokeHelper(0x10296, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		dialogId, title, handler);
	return result;
}

void IModelDoc::AlignOrdinate()
{
	InvokeHelper(0x10297, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelDoc::EditOrdinate()
{
	InvokeHelper(0x10298, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::ReattachOrdinate()
{
	BOOL result;
	InvokeHelper(0x10299, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::EditBalloonProperties(long Style, long size, long upperTextStyle, LPCTSTR upperText, long lowerTextStyle, LPCTSTR lowerText)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x1029a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Style, size, upperTextStyle, upperText, lowerTextStyle, lowerText);
	return result;
}

BOOL IModelDoc::EditDimensionProperties2(long tolType, double tolMax, double tolMin, LPCTSTR tolMaxFit, LPCTSTR tolMinFit, BOOL useDocPrec, long precision, long arrowsIn, BOOL useDocArrows, long arrow1, long arrow2, LPCTSTR prefixText, 
		LPCTSTR suffixText, BOOL showValue, LPCTSTR calloutText1, LPCTSTR calloutText2, BOOL CenterText)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_BSTR VTS_BSTR VTS_BOOL VTS_I4 VTS_I4 VTS_BOOL VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_BOOL VTS_BSTR VTS_BSTR VTS_BOOL;
	InvokeHelper(0x1029b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		tolType, tolMax, tolMin, tolMaxFit, tolMinFit, useDocPrec, precision, arrowsIn, useDocArrows, arrow1, arrow2, prefixText, suffixText, showValue, calloutText1, calloutText2, CenterText);
	return result;
}

void IModelDoc::InsertSheetMetalClosedCorner()
{
	InvokeHelper(0x1029c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelDoc::SketchUseEdge2(BOOL chain)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1029d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		chain);
	return result;
}

BOOL IModelDoc::SketchOffsetEntities2(double offset, BOOL bothDirections, BOOL chain)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1029e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		offset, bothDirections, chain);
	return result;
}

BOOL IModelDoc::SketchOffset2(double offset, BOOL bothDirections, BOOL chain)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1029f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		offset, bothDirections, chain);
	return result;
}

LPDISPATCH IModelDoc::AddDimension2(double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x102a0, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

LPDISPATCH IModelDoc::IAddDimension2(double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x102a1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

LPDISPATCH IModelDoc::AddHorizontalDimension2(double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x102a2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

LPDISPATCH IModelDoc::IAddHorizontalDimension2(double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x102a3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

LPDISPATCH IModelDoc::AddVerticalDimension2(double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x102a4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

LPDISPATCH IModelDoc::IAddVerticalDimension2(double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x102a5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

LPDISPATCH IModelDoc::AddRadialDimension2(double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x102a6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

LPDISPATCH IModelDoc::IAddRadialDimension2(double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x102a7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

LPDISPATCH IModelDoc::AddDiameterDimension2(double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x102a8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

LPDISPATCH IModelDoc::IAddDiameterDimension2(double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x102a9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

LPDISPATCH IModelDoc::GetUserUnit(long UnitType)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x102aa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		UnitType);
	return result;
}

LPDISPATCH IModelDoc::IGetUserUnit(long UnitType)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x102ab, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		UnitType);
	return result;
}

LPDISPATCH IModelDoc::InsertWeldSymbol3()
{
	LPDISPATCH result;
	InvokeHelper(0x102ac, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelDoc::IInsertWeldSymbol3()
{
	LPDISPATCH result;
	InvokeHelper(0x102ad, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IModelDoc::SaveAs3(LPCTSTR newName, long saveAsVersion, long options)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0x102ae, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		newName, saveAsVersion, options);
	return result;
}

CString IModelDoc::GetUserPreferenceStringValue(long userPreference)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x102af, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		userPreference);
	return result;
}

BOOL IModelDoc::SetUserPreferenceStringValue(long userPreference, LPCTSTR Value)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x102b0, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		userPreference, Value);
	return result;
}

void IModelDoc::DeleteDesignTable()
{
	InvokeHelper(0x102b1, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// ISelectionMgr properties

/////////////////////////////////////////////////////////////////////////////
// ISelectionMgr operations

long ISelectionMgr::GetSelectedObjectCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ISelectionMgr::GetSelectedObjectType(long AtIndex)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		AtIndex);
	return result;
}

LPDISPATCH ISelectionMgr::GetSelectedObject(long AtIndex)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		AtIndex);
	return result;
}

LPUNKNOWN ISelectionMgr::IGetSelectedObject(long AtIndex)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		AtIndex);
	return result;
}

VARIANT ISelectionMgr::GetSelectionPoint(long AtIndex)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		AtIndex);
	return result;
}

VARIANT ISelectionMgr::GetSelectionPointInSketchSpace(long AtIndex)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		AtIndex);
	return result;
}

LPDISPATCH ISelectionMgr::GetSelectedObject2(long AtIndex)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		AtIndex);
	return result;
}

LPUNKNOWN ISelectionMgr::IGetSelectedObject2(long AtIndex)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		AtIndex);
	return result;
}

BOOL ISelectionMgr::IsInEditTarget(long AtIndex)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		AtIndex);
	return result;
}

LPDISPATCH ISelectionMgr::GetSelectedObjectsComponent(long AtIndex)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		AtIndex);
	return result;
}

LPDISPATCH ISelectionMgr::IGetSelectedObjectsComponent(long AtIndex)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		AtIndex);
	return result;
}

LPDISPATCH ISelectionMgr::GetSelectedObject3(long AtIndex)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		AtIndex);
	return result;
}

LPUNKNOWN ISelectionMgr::IGetSelectedObject3(long AtIndex)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		AtIndex);
	return result;
}

long ISelectionMgr::GetSelectedObjectType2(long AtIndex)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		AtIndex);
	return result;
}

double ISelectionMgr::IGetSelectionPoint(long AtIndex)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		AtIndex);
	return result;
}

double ISelectionMgr::IGetSelectionPointInSketchSpace(long AtIndex)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		AtIndex);
	return result;
}

long ISelectionMgr::GetSelectedObjectMark(long AtIndex)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		AtIndex);
	return result;
}

BOOL ISelectionMgr::SetSelectedObjectMark(long AtIndex, long mark, long Action)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		AtIndex, mark, Action);
	return result;
}

long ISelectionMgr::DeSelect(const VARIANT& AtIndex)
{
	long result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		&AtIndex);
	return result;
}

long ISelectionMgr::IDeSelect(long count, long* AtIndex)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		count, AtIndex);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IComponent properties

/////////////////////////////////////////////////////////////////////////////
// IComponent operations

VARIANT IComponent::GetChildren()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IComponent::IGetChildrenCount()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IComponent::GetXform()
{
	VARIANT result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IComponent::IGetXform()
{
	double result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH IComponent::GetBody()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IComponent::IGetBody()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IComponent::GetMaterialPropertyValues()
{
	VARIANT result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IComponent::SetMaterialPropertyValues(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IComponent::GetIMaterialPropertyValues()
{
	double result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IComponent::SetIMaterialPropertyValues(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IComponent::AddPropertyExtension(const VARIANT& PropertyExtension)
{
	long result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		&PropertyExtension);
	return result;
}

VARIANT IComponent::GetPropertyExtension(long Id)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		Id);
	return result;
}

void IComponent::ResetPropertyExtension()
{
	InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString IComponent::GetMaterialIdName()
{
	CString result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IComponent::SetMaterialIdName(LPCTSTR Name)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name);
	return result;
}

CString IComponent::GetMaterialUserName()
{
	CString result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IComponent::SetMaterialUserName(LPCTSTR Name)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name);
	return result;
}

VARIANT IComponent::GetSectionedBodies(LPDISPATCH viewIn)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		viewIn);
	return result;
}

VARIANT IComponent::GetBox(BOOL includeRefPlanes, BOOL includeSketches)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		includeRefPlanes, includeSketches);
	return result;
}

double IComponent::IGetBox(BOOL includeRefPlanes, BOOL includeSketches)
{
	double result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		includeRefPlanes, includeSketches);
	return result;
}

BOOL IComponent::SetXform(const VARIANT& xformIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&xformIn);
	return result;
}

BOOL IComponent::ISetXform(double* xformIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		xformIn);
	return result;
}

LPDISPATCH IComponent::GetModelDoc()
{
	LPDISPATCH result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IComponent::IGetModelDoc()
{
	LPDISPATCH result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IComponent::IsFixed()
{
	BOOL result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IComponent::EnumRelatedBodies()
{
	LPUNKNOWN result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

BOOL IComponent::IsSuppressed()
{
	BOOL result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IComponent::EnumSectionedBodies(LPDISPATCH viewIn)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		viewIn);
	return result;
}

BOOL IComponent::IsHidden(BOOL considerSuppressed)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		considerSuppressed);
	return result;
}

CString IComponent::GetName()
{
	CString result;
	InvokeHelper(0x1d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IComponent::SetReferencedConfiguration(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString IComponent::GetReferencedConfiguration()
{
	CString result;
	InvokeHelper(0x1e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IComponent::GetSuppression()
{
	long result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IComponent::SetSuppression(long state)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		state);
	return result;
}

long IComponent::GetVisible()
{
	long result;
	InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IComponent::SetVisible(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x21, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

CString IComponent::GetPathName()
{
	CString result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IComponent::SetXformAndSolve(const VARIANT& xformIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&xformIn);
	return result;
}

BOOL IComponent::ISetXformAndSolve(double* xformIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		xformIn);
	return result;
}

VARIANT IComponent::GetTessTriangles(BOOL noConversion)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		noConversion);
	return result;
}

float IComponent::IGetTessTriangles(BOOL noConversion)
{
	float result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_R4, (void*)&result, parms,
		noConversion);
	return result;
}

long IComponent::IGetTessTriangleCount()
{
	long result;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IComponent::GetTessNorms()
{
	VARIANT result;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

float IComponent::IGetTessNorms()
{
	float result;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_R4, (void*)&result, NULL);
	return result;
}

VARIANT IComponent::GetTessTriStrips(BOOL noConversion)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		noConversion);
	return result;
}

float IComponent::IGetTessTriStrips(BOOL noConversion)
{
	float result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_R4, (void*)&result, parms,
		noConversion);
	return result;
}

long IComponent::IGetTessTriStripSize()
{
	long result;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IComponent::GetTessTriStripNorms()
{
	VARIANT result;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

float IComponent::IGetTessTriStripNorms()
{
	float result;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_R4, (void*)&result, NULL);
	return result;
}

VARIANT IComponent::GetTessTriStripEdges()
{
	VARIANT result;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IComponent::IGetTessTriStripEdges()
{
	long result;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IComponent::IGetTessTriStripEdgeSize()
{
	long result;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IComponent::IsDisplayDataOutOfDate()
{
	long result;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IComponent::GetConstrainedStatus()
{
	long result;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IComponent::RemoveMaterialProperty()
{
	BOOL result;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IComponent::IGetTemporaryBodyID()
{
	long result;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IComponent::FindAttribute(LPDISPATCH attributeDef, long whichOne)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		attributeDef, whichOne);
	return result;
}

LPDISPATCH IComponent::IFindAttribute(LPDISPATCH attributeDef, long whichOne)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		attributeDef, whichOne);
	return result;
}

BOOL IComponent::Select(BOOL appendFlag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag);
	return result;
}

BOOL IComponent::SelectByMark(BOOL appendFlag, long mark)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag, mark);
	return result;
}

BOOL IComponent::DeSelect()
{
	BOOL result;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString IComponent::GetName2()
{
	CString result;
	InvokeHelper(0x3b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IComponent::SetName2(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

long IComponent::GetSolving()
{
	long result;
	InvokeHelper(0x3c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IBody properties

/////////////////////////////////////////////////////////////////////////////
// IBody operations

LPDISPATCH IBody::GetFirstFace()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::IGetFirstFace()
{
	LPDISPATCH result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IBody::GetFaceCount()
{
	long result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::CreateNewSurface()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::ICreateNewSurface()
{
	LPDISPATCH result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IBody::CreateBodyFromSurfaces()
{
	BOOL result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::CreatePlanarSurface(const VARIANT& vRootPoint, const VARIANT& vNormal)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&vRootPoint, &vNormal);
	return result;
}

LPDISPATCH IBody::ICreatePlanarSurface(const VARIANT& vRootPoint, const VARIANT& vNormal)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&vRootPoint, &vNormal);
	return result;
}

LPDISPATCH IBody::CreateRevolutionSurface(LPDISPATCH profileCurve, const VARIANT& axisPoint, const VARIANT& axisDirection, const VARIANT& profileEndPtParams)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		profileCurve, &axisPoint, &axisDirection, &profileEndPtParams);
	return result;
}

LPDISPATCH IBody::ICreateRevolutionSurface(LPDISPATCH profileCurve, const VARIANT& axisPoint, const VARIANT& axisDirection, const VARIANT& profileEndPtParams)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		profileCurve, &axisPoint, &axisDirection, &profileEndPtParams);
	return result;
}

LPDISPATCH IBody::CreateBsplineSurface(const VARIANT& props, const VARIANT& uKnots, const VARIANT& vKnots, const VARIANT& ctrlPtCoords)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&props, &uKnots, &vKnots, &ctrlPtCoords);
	return result;
}

LPDISPATCH IBody::ICreateBsplineSurface(const VARIANT& props, const VARIANT& uKnots, const VARIANT& vKnots, const VARIANT& ctrlPtCoords)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&props, &uKnots, &vKnots, &ctrlPtCoords);
	return result;
}

BOOL IBody::CreateTrimmedSurface()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::AddProfileLine(const VARIANT& rootPoint, const VARIANT& direction)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&rootPoint, &direction);
	return result;
}

LPDISPATCH IBody::IAddProfileLine(const VARIANT& rootPoint, const VARIANT& direction)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&rootPoint, &direction);
	return result;
}

LPDISPATCH IBody::AddProfileArc(const VARIANT& center, const VARIANT& axis, double radius, const VARIANT& startPoint, const VARIANT& endPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_R8 VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&center, &axis, radius, &startPoint, &endPoint);
	return result;
}

LPDISPATCH IBody::IAddProfileArc(const VARIANT& center, const VARIANT& axis, double radius, const VARIANT& startPoint, const VARIANT& endPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_R8 VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&center, &axis, radius, &startPoint, &endPoint);
	return result;
}

LPDISPATCH IBody::AddProfileBspline(const VARIANT& props, const VARIANT& Knots, const VARIANT& ctrlPtCoords)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&props, &Knots, &ctrlPtCoords);
	return result;
}

LPDISPATCH IBody::IAddProfileBspline(const VARIANT& props, const VARIANT& Knots, const VARIANT& ctrlPtCoords)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&props, &Knots, &ctrlPtCoords);
	return result;
}

LPDISPATCH IBody::CreateExtrusionSurface(LPDISPATCH profileCurve, const VARIANT& axisDirection)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_VARIANT;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		profileCurve, &axisDirection);
	return result;
}

LPDISPATCH IBody::ICreateExtrusionSurface(LPDISPATCH profileCurve, const VARIANT& axisDirection)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_VARIANT;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		profileCurve, &axisDirection);
	return result;
}

LPDISPATCH IBody::GetFirstSelectedFace()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::IGetFirstSelectedFace()
{
	LPDISPATCH result;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::GetNextSelectedFace()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::IGetNextSelectedFace()
{
	LPDISPATCH result;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IBody::GetSelectedFaceCount()
{
	long result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IBody::CreateBoundedSurface(BOOL uOpt, BOOL vOpt, const VARIANT& uvParams)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_VARIANT;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		uOpt, vOpt, &uvParams);
	return result;
}

long IBody::GetIgesErrorCount()
{
	long result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IBody::GetIgesErrorCode(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH IBody::Copy()
{
	LPDISPATCH result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::ICopy()
{
	LPDISPATCH result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IBody::EnumFaces()
{
	LPUNKNOWN result;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::CreateBodyFromFaces(long NumOfFaces, const VARIANT& FaceList)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		NumOfFaces, &FaceList);
	return result;
}

LPDISPATCH IBody::ICreateBodyFromFaces(long NumOfFaces, const VARIANT& FaceList)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		NumOfFaces, &FaceList);
	return result;
}

BOOL IBody::DeleteFaces(long NumOfFaces, const VARIANT& FaceList)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		NumOfFaces, &FaceList);
	return result;
}

void IBody::Display(LPDISPATCH part, long Color)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 part, Color);
}

void IBody::IDisplay(LPDISPATCH part, long Color)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 part, Color);
}

void IBody::Hide(LPDISPATCH part)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 part);
}

void IBody::IHide(LPDISPATCH part)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 part);
}

LPDISPATCH IBody::ICreatePlanarSurfaceDLL(double* rootPoint, double* Normal)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		rootPoint, Normal);
	return result;
}

LPDISPATCH IBody::ICreateRevolutionSurfaceDLL(LPDISPATCH profileCurve, double* axisPoint, double* axisDirection, double* profileEndPtParams)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		profileCurve, axisPoint, axisDirection, profileEndPtParams);
	return result;
}

LPDISPATCH IBody::IAddProfileLineDLL(double* rootPoint, double* direction)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		rootPoint, direction);
	return result;
}

LPDISPATCH IBody::IAddProfileArcDLL(double* center, double* axis, double radius, double* startPoint, double* endPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_R8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		center, axis, radius, startPoint, endPoint);
	return result;
}

LPDISPATCH IBody::ICreateBsplineSurfaceDLL(long* Properties, double* UKnotArray, double* VKnotArray, double* ControlPointCoordArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4 VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Properties, UKnotArray, VKnotArray, ControlPointCoordArray);
	return result;
}

LPDISPATCH IBody::IAddProfileBsplineDLL(long* Properties, double* KnotArray, double* ControlPointCoordArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4 VTS_PR8 VTS_PR8;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Properties, KnotArray, ControlPointCoordArray);
	return result;
}

LPDISPATCH IBody::ICreateExtrusionSurfaceDLL(LPDISPATCH profileCurve, double* axisDirection)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_PR8;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		profileCurve, axisDirection);
	return result;
}

void IBody::ICreateBoundedSurface(BOOL uOpt, BOOL vOpt, double* uvParams)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_PR8;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 uOpt, vOpt, uvParams);
}

void IBody::ICombineVolumes(LPDISPATCH ToolBody)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ToolBody);
}

long IBody::ISectionBySheet(LPDISPATCH sheet, long NumMaxSections, LPDISPATCH* SectionedBodies)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		sheet, NumMaxSections, SectionedBodies);
	return result;
}

void IBody::IGetBodyBox(double* BoxCorners)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 BoxCorners);
}

void IBody::SetIgesInfo(LPCTSTR systemName, double granularity, BOOL attemptKnitting)
{
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_BOOL;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 systemName, granularity, attemptKnitting);
}

void IBody::DisplayWireFrameXOR(LPDISPATCH part, long Color)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 part, Color);
}

void IBody::IDisplayWireFrameXOR(LPDISPATCH part, long Color)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 part, Color);
}

void IBody::Save(LPUNKNOWN streamIn)
{
	static BYTE parms[] =
		VTS_UNKNOWN;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 streamIn);
}

void IBody::ISave(LPUNKNOWN streamIn)
{
	static BYTE parms[] =
		VTS_UNKNOWN;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 streamIn);
}

LPDISPATCH IBody::CreateBlendSurface(LPDISPATCH Surface1, double Range1, LPDISPATCH Surface2, double Range2, const VARIANT& StartVec, const VARIANT& EndVec, long HaveHelpVec, const VARIANT& HelpVec, long HaveHelpBox, const VARIANT& HelpBox)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8 VTS_DISPATCH VTS_R8 VTS_VARIANT VTS_VARIANT VTS_I4 VTS_VARIANT VTS_I4 VTS_VARIANT;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Surface1, Range1, Surface2, Range2, &StartVec, &EndVec, HaveHelpVec, &HelpVec, HaveHelpBox, &HelpBox);
	return result;
}

LPDISPATCH IBody::ICreateBlendSurface(LPDISPATCH Surface1, double Range1, LPDISPATCH Surface2, double Range2, double* StartVec, double* EndVec, long HaveHelpVec, double* HelpVec, long HaveHelpBox, double* HelpBox)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8 VTS_DISPATCH VTS_R8 VTS_PR8 VTS_PR8 VTS_I4 VTS_PR8 VTS_I4 VTS_PR8;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Surface1, Range1, Surface2, Range2, StartVec, EndVec, HaveHelpVec, HelpVec, HaveHelpBox, HelpBox);
	return result;
}

LPDISPATCH IBody::CreateOffsetSurface(LPDISPATCH surfaceIn, double distance)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		surfaceIn, distance);
	return result;
}

LPDISPATCH IBody::ICreateOffsetSurface(LPDISPATCH surfaceIn, double distance)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		surfaceIn, distance);
	return result;
}

BOOL IBody::RemoveRedundantTopology()
{
	BOOL result;
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

VARIANT IBody::GetIntersectionEdges(LPDISPATCH toolBodyIn)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		toolBodyIn);
	return result;
}

long IBody::IGetIntersectionEdgeCount(LPDISPATCH toolBodyIn)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		toolBodyIn);
	return result;
}

void IBody::RemoveFacesFromSheet(long NumOfFaces, const VARIANT& facesToRemove)
{
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 NumOfFaces, &facesToRemove);
}

void IBody::ICreatePlanarTrimSurfaceDLL(long VertexCount, double* Points, double* Normal)
{
	static BYTE parms[] =
		VTS_I4 VTS_PR8 VTS_PR8;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 VertexCount, Points, Normal);
}

VARIANT IBody::GetMaterialPropertyValues()
{
	VARIANT result;
	InvokeHelper(0x43, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IBody::SetMaterialPropertyValues(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x43, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IBody::GetIMaterialPropertyValues()
{
	double result;
	InvokeHelper(0x44, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IBody::SetIMaterialPropertyValues(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x44, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IBody::AddPropertyExtension(const VARIANT& PropertyExtension)
{
	long result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		&PropertyExtension);
	return result;
}

VARIANT IBody::GetPropertyExtension(long Id)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x46, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		Id);
	return result;
}

void IBody::ResetPropertyExtension()
{
	InvokeHelper(0x47, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString IBody::GetMaterialIdName()
{
	CString result;
	InvokeHelper(0x48, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IBody::SetMaterialIdName(LPCTSTR Name)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x49, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name);
	return result;
}

CString IBody::GetMaterialUserName()
{
	CString result;
	InvokeHelper(0x4a, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IBody::SetMaterialUserName(LPCTSTR Name)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name);
	return result;
}

VARIANT IBody::GetMassProperties(double density)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x4c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		density);
	return result;
}

double IBody::IGetMassProperties(double density)
{
	double result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x4d, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		density);
	return result;
}

LPDISPATCH IBody::ICreatePsplineSurfaceDLL(long dim, long uorder, long vOrder, long ncol, long nrow, double* coeffs, long basis, double* xform, double scaleFactor)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PR8 VTS_I4 VTS_PR8 VTS_R8;
	InvokeHelper(0x4e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		dim, uorder, vOrder, ncol, nrow, coeffs, basis, xform, scaleFactor);
	return result;
}

BOOL IBody::SetXform(const VARIANT& xformIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x4f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&xformIn);
	return result;
}

BOOL IBody::ISetXform(double* xformIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x50, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		xformIn);
	return result;
}

LPDISPATCH IBody::CreateTempBodyFromSurfaces()
{
	LPDISPATCH result;
	InvokeHelper(0x51, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::ICreateTempBodyFromSurfaces()
{
	LPDISPATCH result;
	InvokeHelper(0x52, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IBody::Operations(long operationType, LPDISPATCH ToolBody, long NumMaxSections)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH VTS_I4;
	InvokeHelper(0x53, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		operationType, ToolBody, NumMaxSections);
	return result;
}

long IBody::IOperations(long operationType, LPDISPATCH ToolBody, long NumMaxSections, LPDISPATCH* resultingBodies)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x54, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		operationType, ToolBody, NumMaxSections, resultingBodies);
	return result;
}

LPDISPATCH IBody::GetSheetBody(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x55, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH IBody::IGetSheetBody(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x56, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH IBody::GetProcessedBody()
{
	LPDISPATCH result;
	InvokeHelper(0x57, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::IGetProcessedBody()
{
	LPDISPATCH result;
	InvokeHelper(0x58, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::GetProcessedBodyWithSelFace()
{
	LPDISPATCH result;
	InvokeHelper(0x59, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBody::IGetProcessedBodyWithSelFace()
{
	LPDISPATCH result;
	InvokeHelper(0x5a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IBody::Check()
{
	long result;
	InvokeHelper(0x5b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IBody::GetExcessBodyArray()
{
	VARIANT result;
	InvokeHelper(0x5c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IBody::IGetExcessBodyCount()
{
	long result;
	InvokeHelper(0x5e, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IBody::CreateBaseFeature(LPDISPATCH bodyIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x5f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bodyIn);
	return result;
}

BOOL IBody::ICreateBaseFeature(LPDISPATCH bodyIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x60, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bodyIn);
	return result;
}

long IBody::DeleteFaces2(long NumOfFaces, const VARIANT& FaceList, long option)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT VTS_I4;
	InvokeHelper(0x61, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		NumOfFaces, &FaceList, option);
	return result;
}

long IBody::IDeleteFaces2(long NumOfFaces, LPDISPATCH* FaceList, long option)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH VTS_I4;
	InvokeHelper(0x62, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		NumOfFaces, FaceList, option);
	return result;
}

LPDISPATCH IBody::IAddVertexPoint(double* point)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x63, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		point);
	return result;
}

LPDISPATCH IBody::AddVertexPoint(const VARIANT& point)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x64, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&point);
	return result;
}

BOOL IBody::GetExtremePoint(double x, double y, double z, double* outx, double* outy, double* outz)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x65, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z, outx, outy, outz);
	return result;
}

long IBody::GetType()
{
	long result;
	InvokeHelper(0x66, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IBody::IDeleteFaces3(long NumOfFaces, LPDISPATCH* FaceList, long option, BOOL doLocalCheck, BOOL* localCheckResult)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH VTS_I4 VTS_BOOL VTS_PBOOL;
	InvokeHelper(0x67, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 NumOfFaces, FaceList, option, doLocalCheck, localCheckResult);
}

void IBody::SetCurrentSurface(LPDISPATCH surfaceIn)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x68, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 surfaceIn);
}

void IBody::ISetCurrentSurface(LPDISPATCH surfaceIn)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x69, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 surfaceIn);
}

BOOL IBody::DraftBody(long NumOfFaces, const VARIANT& FaceList, const VARIANT& EdgeList, double draftAngle, const VARIANT& dir)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT VTS_VARIANT VTS_R8 VTS_VARIANT;
	InvokeHelper(0x6a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		NumOfFaces, &FaceList, &EdgeList, draftAngle, &dir);
	return result;
}

BOOL IBody::IDraftBody(long NumOfFaces, LPDISPATCH* FaceList, LPDISPATCH* EdgeList, double draftAngle, double* dir)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH VTS_PDISPATCH VTS_R8 VTS_PR8;
	InvokeHelper(0x6b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		NumOfFaces, FaceList, EdgeList, draftAngle, dir);
	return result;
}

BOOL IBody::DeleteBlends(long NumOfFaces, const VARIANT& FaceList)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT;
	InvokeHelper(0x6c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		NumOfFaces, &FaceList);
	return result;
}

BOOL IBody::IDeleteBlends(long NumOfFaces, LPDISPATCH* FaceList)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x6d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		NumOfFaces, FaceList);
	return result;
}

VARIANT IBody::Operations2(long operationType, LPDISPATCH ToolBody, long* errorCode)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH VTS_PI4;
	InvokeHelper(0x6e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		operationType, ToolBody, errorCode);
	return result;
}

LPUNKNOWN IBody::IOperations2(long operationType, LPDISPATCH ToolBody, long* errorCode)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH VTS_PI4;
	InvokeHelper(0x6f, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		operationType, ToolBody, errorCode);
	return result;
}

VARIANT IBody::GetBodyBox()
{
	VARIANT result;
	InvokeHelper(0x70, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

BOOL IBody::DeleteBlends2(long NumOfFaces, const VARIANT& FaceList, BOOL doLocalCheck)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT VTS_BOOL;
	InvokeHelper(0x71, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		NumOfFaces, &FaceList, doLocalCheck);
	return result;
}

BOOL IBody::IDeleteBlends2(long NumOfFaces, LPDISPATCH* FaceList, BOOL doLocalCheck)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH VTS_BOOL;
	InvokeHelper(0x72, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		NumOfFaces, FaceList, doLocalCheck);
	return result;
}

LPDISPATCH IBody::GetTessellation(const VARIANT& FaceList)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x73, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&FaceList);
	return result;
}

LPDISPATCH IBody::IGetTessellation(long NumOfFaces, LPDISPATCH* FaceList)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x74, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		NumOfFaces, FaceList);
	return result;
}

BOOL IBody::GetVisible()
{
	BOOL result;
	InvokeHelper(0x75, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

VARIANT IBody::MatchedBoolean(long operationType, LPDISPATCH ToolBody, long numOfMatchingFaces, const VARIANT& faceList1, const VARIANT& faceList2, long* errorCode)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH VTS_I4 VTS_VARIANT VTS_VARIANT VTS_PI4;
	InvokeHelper(0x76, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		operationType, ToolBody, numOfMatchingFaces, &faceList1, &faceList2, errorCode);
	return result;
}

LPUNKNOWN IBody::IMatchedBoolean(long operationType, LPDISPATCH ToolBody, long numOfMatchingFaces, LPDISPATCH* faceList1, LPDISPATCH* faceList2, long* errorCode)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH VTS_I4 VTS_PDISPATCH VTS_PDISPATCH VTS_PI4;
	InvokeHelper(0x77, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		operationType, ToolBody, numOfMatchingFaces, faceList1, faceList2, errorCode);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IFace properties

/////////////////////////////////////////////////////////////////////////////
// IFace operations

VARIANT IFace::GetNormal()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IFace::SetNormal(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

CString IFace::GetMaterialUserName()
{
	CString result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IFace::SetMaterialUserName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString IFace::GetMaterialIdName()
{
	CString result;
	InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IFace::SetMaterialIdName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

VARIANT IFace::GetMaterialPropertyValues()
{
	VARIANT result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IFace::SetMaterialPropertyValues(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IFace::GetINormal()
{
	double result;
	InvokeHelper(0x28, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IFace::SetINormal(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x28, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH IFace::GetNextFace()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::IGetNextFace()
{
	LPDISPATCH result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::GetSurface()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::IGetSurface()
{
	LPDISPATCH result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IFace::GetEdgeCount()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IFace::GetEdges()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::GetFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::IGetFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IFace::GetFeatureId()
{
	long result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IFace::GetTrimCurves(BOOL wantCubic)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		wantCubic);
	return result;
}

VARIANT IFace::GetUVBounds()
{
	VARIANT result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

BOOL IFace::FaceInSurfaceSense()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IFace::GetLoopCount()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::GetFirstLoop()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::IGetFirstLoop()
{
	LPDISPATCH result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IFace::IsSame(LPDISPATCH faceIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		faceIn);
	return result;
}

BOOL IFace::IIsSame(LPDISPATCH faceIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		faceIn);
	return result;
}

long IFace::AddPropertyExtension(const VARIANT& PropertyExtension)
{
	long result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		&PropertyExtension);
	return result;
}

VARIANT IFace::GetPropertyExtension(long Id)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		Id);
	return result;
}

void IFace::ResetPropertyExtension()
{
	InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IFace::GetTessTriangles(BOOL noConversion)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		noConversion);
	return result;
}

VARIANT IFace::GetTessNorms()
{
	VARIANT result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IFace::GetTessTriStrips(BOOL noConversion)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		noConversion);
	return result;
}

VARIANT IFace::GetTessTriStripNorms()
{
	VARIANT result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IFace::EnumLoops()
{
	LPUNKNOWN result;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IFace::EnumEdges()
{
	LPUNKNOWN result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::GetBody()
{
	LPDISPATCH result;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::IGetBody()
{
	LPDISPATCH result;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

float IFace::IGetTessTriangles(BOOL noConversion)
{
	float result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_R4, (void*)&result, parms,
		noConversion);
	return result;
}

float IFace::IGetTessNorms()
{
	float result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_R4, (void*)&result, NULL);
	return result;
}

long IFace::GetTessTriangleCount()
{
	long result;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

float IFace::IGetTessTriStrips(BOOL noConversion)
{
	float result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_R4, (void*)&result, parms,
		noConversion);
	return result;
}

float IFace::IGetTessTriStripNorms()
{
	float result;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_R4, (void*)&result, NULL);
	return result;
}

long IFace::GetTessTriStripSize()
{
	long result;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

double IFace::IGetUVBounds()
{
	double result;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IFace::GetClosestPointOn(double x, double y, double z)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		x, y, z);
	return result;
}

double IFace::IGetClosestPointOn(double x, double y, double z)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		x, y, z);
	return result;
}

void IFace::Highlight(BOOL state)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 state);
}

void IFace::IHighlight(BOOL state)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 state);
}

VARIANT IFace::GetTrimCurveTopology()
{
	VARIANT result;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IFace::GetTrimCurveTopologyCount()
{
	long result;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IFace::GetTrimCurveTopologyTypes()
{
	VARIANT result;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IFace::IGetTrimCurveTopologyTypes()
{
	long result;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IFace::RemoveRedundantTopology()
{
	BOOL result;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::CreateSheetBodyByFaceExtension(const VARIANT& boxLowIn, const VARIANT& boxHighIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&boxLowIn, &boxHighIn);
	return result;
}

LPDISPATCH IFace::ICreateSheetBodyByFaceExtension(double* boxLowIn, double* boxHighIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		boxLowIn, boxHighIn);
	return result;
}

double IFace::GetArea()
{
	double result;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IFace::GetBox()
{
	VARIANT result;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IFace::IGetBox()
{
	double result;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::RemoveInnerLoops(long numOfLoops, const VARIANT& innerLoopsIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		numOfLoops, &innerLoopsIn);
	return result;
}

LPDISPATCH IFace::IRemoveInnerLoops(long numOfLoops, LPDISPATCH* innerLoopsIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		numOfLoops, innerLoopsIn);
	return result;
}

LPDISPATCH IFace::CreateSheetBody()
{
	LPDISPATCH result;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::ICreateSheetBody()
{
	LPDISPATCH result;
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IFace::GetSilhoutteEdges(double* root, double* Normal)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		root, Normal);
	return result;
}

long IFace::IGetSilhoutteEdgeCount(double* root, double* Normal)
{
	long result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		root, Normal);
	return result;
}

long IFace::IGetTrimCurveSize(BOOL wantCubic)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		wantCubic);
	return result;
}

double IFace::IGetTrimCurve()
{
	double result;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IFace::GetIMaterialPropertyValues()
{
	double result;
	InvokeHelper(0x42, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IFace::SetIMaterialPropertyValues(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x42, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IFace::GetFaceId()
{
	long result;
	InvokeHelper(0x43, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IFace::SetFaceId(long idIn)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 idIn);
}

long IFace::IGetTrimCurveSize2(long wantCubic, long wantNRational)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		wantCubic, wantNRational);
	return result;
}

VARIANT IFace::GetSilhoutteEdgesVB(double xroot, double yroot, double zroot, double xnormal, double ynormal, double znormal)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x46, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		xroot, yroot, zroot, xnormal, ynormal, znormal);
	return result;
}

void IFace::RemoveFaceId(long idIn)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x47, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 idIn);
}

VARIANT IFace::GetTrimCurves2(BOOL wantCubic, BOOL wantNRational)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL;
	InvokeHelper(0x48, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		wantCubic, wantNRational);
	return result;
}

long IFace::GetShellType()
{
	long result;
	InvokeHelper(0x49, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IFace::GetTessTriStripEdges()
{
	VARIANT result;
	InvokeHelper(0x4b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IFace::IGetTessTriStripEdges()
{
	long result;
	InvokeHelper(0x4c, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IFace::IGetTessTriStripEdgeSize()
{
	long result;
	InvokeHelper(0x4d, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IFace::RemoveMaterialProperty()
{
	BOOL result;
	InvokeHelper(0x4e, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::GetPatternSeedFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x4f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFace::IGetPatternSeedFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x50, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISurface properties

/////////////////////////////////////////////////////////////////////////////
// ISurface operations

VARIANT ISurface::GetPlaneParams()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT ISurface::GetCylinderParams()
{
	VARIANT result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT ISurface::GetConeParams()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT ISurface::GetSphereParams()
{
	VARIANT result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT ISurface::GetTorusParams()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsPlane()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsCylinder()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsCone()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long ISurface::Identity()
{
	long result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsSphere()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsTorus()
{
	BOOL result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsParametric()
{
	BOOL result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsBlending()
{
	BOOL result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsOffset()
{
	BOOL result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsSwept()
{
	BOOL result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsRevolved()
{
	BOOL result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISurface::IsForeign()
{
	BOOL result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

VARIANT ISurface::Parameterization()
{
	VARIANT result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT ISurface::Evaluate(double UParam, double VParam, long NumUDeriv, long NumVDeriv)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_I4 VTS_I4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		UParam, VParam, NumUDeriv, NumVDeriv);
	return result;
}

VARIANT ISurface::GetBSurfParams(BOOL wantCubicRational, const VARIANT& vP0)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL VTS_VARIANT;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		wantCubicRational, &vP0);
	return result;
}

BOOL ISurface::AddTrimmingLoop(long nCrvs, const VARIANT& vOrder, const VARIANT& vDim, const VARIANT& vPeriodic, const VARIANT& vNumKnots, const VARIANT& vNumCtrlPoints, const VARIANT& vKnots, const VARIANT& vCtrlPointDbls)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nCrvs, &vOrder, &vDim, &vPeriodic, &vNumKnots, &vNumCtrlPoints, &vKnots, &vCtrlPointDbls);
	return result;
}

LPDISPATCH ISurface::CreateNewCurve()
{
	LPDISPATCH result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISurface::ICreateNewCurve()
{
	LPDISPATCH result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT ISurface::GetRevsurfParams()
{
	VARIANT result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT ISurface::GetExtrusionsurfParams()
{
	VARIANT result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISurface::GetProfileCurve()
{
	LPDISPATCH result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISurface::IGetProfileCurve()
{
	LPDISPATCH result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT ISurface::ReverseEvaluate(double positionX, double positionY, double positionZ)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		positionX, positionY, positionZ);
	return result;
}

double ISurface::GetIPlaneParams()
{
	double result;
	InvokeHelper(0x1d, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

double ISurface::GetICylinderParams()
{
	double result;
	InvokeHelper(0x1e, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

double ISurface::GetIConeParams()
{
	double result;
	InvokeHelper(0x1f, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

double ISurface::GetISphereParams()
{
	double result;
	InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

double ISurface::GetITorusParams()
{
	double result;
	InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

double ISurface::IReverseEvaluate(double positionX, double positionY, double positionZ)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		positionX, positionY, positionZ);
	return result;
}

double ISurface::IGetRevsurfParams()
{
	double result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double ISurface::IGetExtrusionsurfParams()
{
	double result;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double ISurface::IParameterization()
{
	double result;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double ISurface::IEvaluate(double UParam, double VParam, long NumUDeriv, long NumVDeriv)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_I4 VTS_I4;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		UParam, VParam, NumUDeriv, NumVDeriv);
	return result;
}

double ISurface::IGetBSurfParams()
{
	double result;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISurface::IGetBSurfParamsSize(BOOL wantCubicRational, double* Range)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_PR8;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		wantCubicRational, Range);
	return result;
}

void ISurface::IAddTrimmingLoop(long CurveCount, long* order, long* dim, long* Periodic, long* NumKnots, long* NumCtrlPoints, double* Knots, double* CtrlPointDbls)
{
	static BYTE parms[] =
		VTS_I4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PR8 VTS_PR8;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 CurveCount, order, dim, Periodic, NumKnots, NumCtrlPoints, Knots, CtrlPointDbls);
}

VARIANT ISurface::EvaluateAtPoint(double positionX, double positionY, double positionZ)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		positionX, positionY, positionZ);
	return result;
}

double ISurface::IEvaluateAtPoint(double positionX, double positionY, double positionZ)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		positionX, positionY, positionZ);
	return result;
}

double ISurface::GetOffsetSurfParams()
{
	double result;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISurface::IGetBSurfParamsSize2(BOOL wantCubic, BOOL wantNonRational, double* Range)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_PR8;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		wantCubic, wantNonRational, Range);
	return result;
}

LPDISPATCH ISurface::Copy()
{
	LPDISPATCH result;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISurface::ICopy()
{
	LPDISPATCH result;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISurface::CreateTrimmedSheet(const VARIANT& curves)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&curves);
	return result;
}

LPDISPATCH ISurface::ICreateTrimmedSheet(long nCurves, LPDISPATCH* curves)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		nCurves, curves);
	return result;
}

BOOL ISurface::AddTrimmingLoop2(long nCrvs, const VARIANT& vOrder, const VARIANT& vDim, const VARIANT& vPeriodic, const VARIANT& vNumKnots, const VARIANT& vNumCtrlPoints, const VARIANT& vKnots, const VARIANT& vCtrlPointDbls, const VARIANT& uvRange)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nCrvs, &vOrder, &vDim, &vPeriodic, &vNumKnots, &vNumCtrlPoints, &vKnots, &vCtrlPointDbls, &uvRange);
	return result;
}

void ISurface::IAddTrimmingLoop2(long CurveCount, long* order, long* dim, long* Periodic, long* NumKnots, long* NumCtrlPoints, double* Knots, double* CtrlPointDbls, double* uvRange)
{
	static BYTE parms[] =
		VTS_I4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 CurveCount, order, dim, Periodic, NumKnots, NumCtrlPoints, Knots, CtrlPointDbls, uvRange);
}

VARIANT ISurface::GetBSurfParams2(BOOL wantCubic, BOOL wantNonRational, const VARIANT& vP0, double tolerance, BOOL* sense)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_VARIANT VTS_R8 VTS_PBOOL;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		wantCubic, wantNonRational, &vP0, tolerance, sense);
	return result;
}

long ISurface::IGetBSurfParamsSize3(BOOL wantCubic, BOOL wantNonRational, double* Range, double tolerance, BOOL* sense)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_PR8 VTS_R8 VTS_PBOOL;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		wantCubic, wantNonRational, Range, tolerance, sense);
	return result;
}

VARIANT ISurface::GetClosestPointOn(double x, double y, double z)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		x, y, z);
	return result;
}

double ISurface::IGetClosestPointOn(double x, double y, double z)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		x, y, z);
	return result;
}

double ISurface::GetOffsetSurfParams2(LPDISPATCH* baseSurf, BOOL* sense)
{
	double result;
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PBOOL;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		baseSurf, sense);
	return result;
}

double ISurface::IGetOffsetSurfParams2(LPDISPATCH* baseSurf, BOOL* sense)
{
	double result;
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PBOOL;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		baseSurf, sense);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ICurve properties

/////////////////////////////////////////////////////////////////////////////
// ICurve operations

VARIANT ICurve::GetLineParams()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT ICurve::GetCircleParams()
{
	VARIANT result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long ICurve::Identity()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ICurve::IsCircle()
{
	BOOL result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ICurve::IsLine()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ICurve::IsBcurve()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

VARIANT ICurve::GetBCurveParams(BOOL wantCubicIn)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		wantCubicIn);
	return result;
}

VARIANT ICurve::ConvertLineToBcurve(const VARIANT& startPoint, const VARIANT& endPoint)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		&startPoint, &endPoint);
	return result;
}

VARIANT ICurve::ConvertArcToBcurve(const VARIANT& center, const VARIANT& axis, const VARIANT& start, const VARIANT& end)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		&center, &axis, &start, &end);
	return result;
}

LPDISPATCH ICurve::ReverseCurve()
{
	LPDISPATCH result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICurve::IReverseCurve()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT ICurve::GetPCurveParams()
{
	VARIANT result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ICurve::GetILineParams()
{
	double result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

double ICurve::GetICircleParams()
{
	double result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

double ICurve::IGetBCurveParams()
{
	double result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ICurve::IGetBCurveParamsSize(BOOL wantCubicIn)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		wantCubicIn);
	return result;
}

long ICurve::IConvertLineToBcurveSize(double* startPoint, double* endPoint)
{
	long result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		startPoint, endPoint);
	return result;
}

long ICurve::IConvertArcToBcurveSize(double* center, double* axis, double* start, double* end)
{
	long result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		center, axis, start, end);
	return result;
}

double ICurve::IGetPCurveParams()
{
	double result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ICurve::IGetPCurveParamsSize()
{
	long result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

double ICurve::GetLength(double startParam, double endParam)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		startParam, endParam);
	return result;
}

long ICurve::IConvertPcurveToBcurveSize(long dim, long order, long nsegs, double* coeffs, long basis, double* xform, double scaleFactor)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_PR8 VTS_I4 VTS_PR8 VTS_R8;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		dim, order, nsegs, coeffs, basis, xform, scaleFactor);
	return result;
}

VARIANT ICurve::GetSplinePts(const VARIANT& paramsArrayIn)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		&paramsArrayIn);
	return result;
}

double ICurve::IGetSplinePts()
{
	double result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ICurve::IGetSplinePtsSize(long* propArray, double* knotsArray, double* cntrlPntCoordArray)
{
	long result;
	static BYTE parms[] =
		VTS_PI4 VTS_PR8 VTS_PR8;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propArray, knotsArray, cntrlPntCoordArray);
	return result;
}

long ICurve::IGetBCurveParamsSize2(BOOL wantCubic, BOOL wantNRational)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		wantCubic, wantNRational);
	return result;
}

LPDISPATCH ICurve::Copy()
{
	LPDISPATCH result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICurve::ICopy()
{
	LPDISPATCH result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT ICurve::GetTessPts(double chordTolerance, double lengthTolerance, const VARIANT& startPoint, const VARIANT& endPoint)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		chordTolerance, lengthTolerance, &startPoint, &endPoint);
	return result;
}

double ICurve::IGetTessPts(double chordTolerance, double lengthTolerance, double* startPoint, double* endPoint)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		chordTolerance, lengthTolerance, startPoint, endPoint);
	return result;
}

long ICurve::IGetTessPtsSize(double chordTolerance, double lengthTolerance, double* startPoint, double* endPoint)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		chordTolerance, lengthTolerance, startPoint, endPoint);
	return result;
}

VARIANT ICurve::IntersectCurve(LPDISPATCH otherCurve, const VARIANT& thisStartPoint, const VARIANT& thisEndPoint, const VARIANT& otherStartPoint, const VARIANT& otherEndPoint)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		otherCurve, &thisStartPoint, &thisEndPoint, &otherStartPoint, &otherEndPoint);
	return result;
}

double ICurve::IIntersectCurve(LPDISPATCH otherCurve, double* thisStartPoint, double* thisEndPoint, double* otherStartPoint, double* otherEndPoint)
{
	double result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		otherCurve, thisStartPoint, thisEndPoint, otherStartPoint, otherEndPoint);
	return result;
}

long ICurve::IIntersectCurveSize(LPDISPATCH otherCurve, double* thisStartPoint, double* thisEndPoint, double* otherStartPoint, double* otherEndPoint)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		otherCurve, thisStartPoint, thisEndPoint, otherStartPoint, otherEndPoint);
	return result;
}

LPDISPATCH ICurve::CreateTrimmedCurve(double x1, double y1, double z1, double x2, double y2, double z2)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x1, y1, z1, x2, y2, z2);
	return result;
}

LPDISPATCH ICurve::ICreateTrimmedCurve(double* start, double* end)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		start, end);
	return result;
}

BOOL ICurve::IsEllipse()
{
	BOOL result;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

VARIANT ICurve::GetEllipseParams()
{
	VARIANT result;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ICurve::IGetEllipseParams(double* paramArray)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 paramArray);
}

VARIANT ICurve::Evaluate(double Parameter)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		Parameter);
	return result;
}

double ICurve::IEvaluate(double Parameter)
{
	double result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		Parameter);
	return result;
}

VARIANT ICurve::GetClosestPointOn(double x, double y, double z)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		x, y, z);
	return result;
}

double ICurve::IGetClosestPointOn(double x, double y, double z)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		x, y, z);
	return result;
}

BOOL ICurve::GetEndParams(double* start, double* end, BOOL* isClosed, BOOL* isPeriodic)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PBOOL VTS_PBOOL;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		start, end, isClosed, isPeriodic);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IEdge properties

/////////////////////////////////////////////////////////////////////////////
// IEdge operations

LPDISPATCH IEdge::GetCurve()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IEdge::IGetCurve()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IEdge::GetCurveParams()
{
	VARIANT result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IEdge::IGetCurveParams()
{
	double result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IEdge::Evaluate(double Parameter)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		Parameter);
	return result;
}

double IEdge::IEvaluate(double Parameter)
{
	double result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		Parameter);
	return result;
}

VARIANT IEdge::GetParameter(double x, double y, double z)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		x, y, z);
	return result;
}

double IEdge::IGetParameter(double x, double y, double z)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		x, y, z);
	return result;
}

BOOL IEdge::EdgeInFaceSense(LPDISPATCH facedisp)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		facedisp);
	return result;
}

BOOL IEdge::IEdgeInFaceSense(LPDISPATCH facedisp)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		facedisp);
	return result;
}

VARIANT IEdge::GetTwoAdjacentFaces()
{
	VARIANT result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IEdge::IGetTwoAdjacentFaces(LPDISPATCH* face1, LPDISPATCH* face2)
{
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PDISPATCH;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 face1, face2);
}

LPUNKNOWN IEdge::EnumCoEdges()
{
	LPUNKNOWN result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

LPDISPATCH IEdge::GetStartVertex()
{
	LPDISPATCH result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IEdge::IGetStartVertex()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IEdge::GetEndVertex()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IEdge::IGetEndVertex()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IEdge::GetClosestPointOn(double x, double y, double z)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		x, y, z);
	return result;
}

double IEdge::IGetClosestPointOn(double x, double y, double z)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		x, y, z);
	return result;
}

BOOL IEdge::RemoveRedundantTopology()
{
	BOOL result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IEdge::GetId()
{
	long result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IEdge::SetId(long idIn)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 idIn);
}

void IEdge::RemoveId()
{
	InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IEdge::GetCurveParams2()
{
	VARIANT result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IEdge::IGetCurveParams2()
{
	double result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IEdge::IsParamReversed()
{
	BOOL result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IEdge::Highlight(BOOL state)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 state);
}

VARIANT IEdge::GetCoEdges()
{
	VARIANT result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ICoEdge properties

/////////////////////////////////////////////////////////////////////////////
// ICoEdge operations

LPDISPATCH ICoEdge::GetEdge()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICoEdge::IGetEdge()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICoEdge::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICoEdge::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICoEdge::GetLoop()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICoEdge::IGetLoop()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL ICoEdge::GetSense()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICoEdge::GetPartner()
{
	LPDISPATCH result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICoEdge::IGetPartner()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT ICoEdge::GetCurveParams()
{
	VARIANT result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT ICoEdge::Evaluate(double param)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		param);
	return result;
}

double ICoEdge::IGetCurveParams()
{
	double result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double ICoEdge::IEvaluate(double param)
{
	double result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		param);
	return result;
}

LPDISPATCH ICoEdge::GetCurve()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICoEdge::IGetCurve()
{
	LPDISPATCH result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ILoop properties

/////////////////////////////////////////////////////////////////////////////
// ILoop operations

LPDISPATCH ILoop::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILoop::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT ILoop::GetEdges()
{
	VARIANT result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long ILoop::GetEdgeCount()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ILoop::IsOuter()
{
	BOOL result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILoop::GetFirstCoEdge()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILoop::IGetFirstCoEdge()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPUNKNOWN ILoop::EnumEdges()
{
	LPUNKNOWN result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

LPUNKNOWN ILoop::EnumCoEdges()
{
	LPUNKNOWN result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILoop::GetFace()
{
	LPDISPATCH result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILoop::IGetFace()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT ILoop::SweepPlanarLoop(double x, double y, double z, double draftAngle)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		x, y, z, draftAngle);
	return result;
}

LPDISPATCH ILoop::ISweepPlanarLoop(double x, double y, double z, double draftAngle, LPDISPATCH* stopFacesOut)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_PDISPATCH;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z, draftAngle, stopFacesOut);
	return result;
}

VARIANT ILoop::RevolvePlanarLoop(double x, double y, double z, double axisx, double axisy, double axisz, double revAngle)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		x, y, z, axisx, axisy, axisz, revAngle);
	return result;
}

LPDISPATCH ILoop::IRevolvePlanarLoop(double x, double y, double z, double axisx, double axisy, double axisz, double revAngle, LPDISPATCH* stopFacesOut)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_PDISPATCH;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z, axisx, axisy, axisz, revAngle, stopFacesOut);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IVertex properties

/////////////////////////////////////////////////////////////////////////////
// IVertex operations

VARIANT IVertex::GetPoint()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IVertex::IGetPoint()
{
	double result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IVertex::EnumEdges()
{
	LPUNKNOWN result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

VARIANT IVertex::GetClosestPointOn(double x, double y, double z)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		x, y, z);
	return result;
}

double IVertex::IGetClosestPointOn(double x, double y, double z)
{
	double result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		x, y, z);
	return result;
}

LPUNKNOWN IVertex::EnumEdgesOriented()
{
	LPUNKNOWN result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IFeature properties

/////////////////////////////////////////////////////////////////////////////
// IFeature operations

CString IFeature::GetName()
{
	CString result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IFeature::SetName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

LPDISPATCH IFeature::Parameter(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IFeature::IParameter(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IFeature::GetNextFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::IGetNextFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IFeature::GetTypeName()
{
	CString result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::GetSpecificFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IFeature::IGetSpecificFeature()
{
	LPUNKNOWN result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

BOOL IFeature::GetUIState(long stateType)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		stateType);
	return result;
}

void IFeature::SetUIState(long stateType, BOOL flag)
{
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 stateType, flag);
}

CString IFeature::GetMaterialUserName()
{
	CString result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IFeature::SetMaterialUserName(LPCTSTR Name)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name);
	return result;
}

CString IFeature::GetMaterialIdName()
{
	CString result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IFeature::SetMaterialIdName(LPCTSTR Name)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name);
	return result;
}

VARIANT IFeature::GetMaterialPropertyValues()
{
	VARIANT result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

BOOL IFeature::SetMaterialPropertyValues(const VARIANT& MaterialPropertyValues)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&MaterialPropertyValues);
	return result;
}

long IFeature::AddPropertyExtension(const VARIANT& PropertyExtension)
{
	long result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		&PropertyExtension);
	return result;
}

VARIANT IFeature::GetPropertyExtension(long Id)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		Id);
	return result;
}

void IFeature::ResetPropertyExtension()
{
	InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IFeature::GetFirstSubFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::IGetFirstSubFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::GetNextSubFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::IGetNextSubFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

double IFeature::IGetMaterialPropertyValues()
{
	double result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IFeature::ISetMaterialPropertyValues(double* MaterialPropertyValues)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		MaterialPropertyValues);
	return result;
}

BOOL IFeature::IsSuppressed()
{
	BOOL result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IFeature::GetUpdateStamp()
{
	long result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IFeature::SetBody(LPDISPATCH bodyIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bodyIn);
	return result;
}

BOOL IFeature::ISetBody(LPDISPATCH bodyIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bodyIn);
	return result;
}

LPDISPATCH IFeature::GetBody()
{
	LPDISPATCH result;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::IGetBody()
{
	LPDISPATCH result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IFeature::EnumDisplayDimensions()
{
	LPUNKNOWN result;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::GetDefinition()
{
	LPDISPATCH result;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IFeature::IGetDefinition()
{
	LPUNKNOWN result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

BOOL IFeature::ModifyDefinition(LPDISPATCH data, LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		data, topDoc, component);
	return result;
}

BOOL IFeature::IModifyDefinition(LPUNKNOWN data, LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_UNKNOWN VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		data, topDoc, component);
	return result;
}

long IFeature::GetFaceCount()
{
	long result;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IFeature::GetFaces()
{
	VARIANT result;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::IGetFaces(long* faceCount)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		faceCount);
	return result;
}

LPDISPATCH IFeature::GetFirstDisplayDimension()
{
	LPDISPATCH result;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::GetNextDisplayDimension(LPDISPATCH dispIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		dispIn);
	return result;
}

long IFeature::GetErrorCode()
{
	long result;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IFeature::IGetChildCount()
{
	long result;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IFeature::GetChildren()
{
	VARIANT result;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::IGetChildren()
{
	LPDISPATCH result;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IFeature::IGetParentCount()
{
	long result;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IFeature::GetParents()
{
	VARIANT result;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFeature::IGetParents()
{
	LPDISPATCH result;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IFeature::SetBody2(LPDISPATCH bodyIn, BOOL applyUserIds)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BOOL;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bodyIn, applyUserIds);
	return result;
}

BOOL IFeature::ISetBody2(LPDISPATCH bodyIn, BOOL applyUserIds)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BOOL;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bodyIn, applyUserIds);
	return result;
}

CString IFeature::GetImportedFileName()
{
	CString result;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IFeature::SetImportedFileName(LPCTSTR ImpName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ImpName);
	return result;
}

BOOL IFeature::SetSuppression(long suppressState)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		suppressState);
	return result;
}

BOOL IFeature::RemoveMaterialProperty()
{
	BOOL result;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IFeature::Select(BOOL appendFlag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag);
	return result;
}

BOOL IFeature::SelectByMark(BOOL appendFlag, long mark)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag, mark);
	return result;
}

BOOL IFeature::DeSelect()
{
	BOOL result;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IFeature::GetBox(VARIANT* bBox)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PVARIANT;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bBox);
	return result;
}

BOOL IFeature::IGetBox(double* bBox)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bBox);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDimension properties

/////////////////////////////////////////////////////////////////////////////
// IDimension operations

double IDimension::GetValue()
{
	double result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IDimension::SetValue(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IDimension::GetSystemValue()
{
	double result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IDimension::SetSystemValue(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IDimension::GetUserValueIn(LPDISPATCH doc)
{
	double result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		doc);
	return result;
}

double IDimension::IGetUserValueIn(LPDISPATCH doc)
{
	double result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		doc);
	return result;
}

void IDimension::SetUserValueIn(LPDISPATCH doc, double newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 doc, newValue);
}

void IDimension::ISetUserValueIn(LPDISPATCH doc, double newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 doc, newValue);
}

CString IDimension::GetName()
{
	CString result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IDimension::SetName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

long IDimension::GetToleranceType()
{
	long result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IDimension::SetToleranceType(long newType)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		newType);
	return result;
}

VARIANT IDimension::GetToleranceValues()
{
	VARIANT result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IDimension::IGetToleranceValues()
{
	double result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IDimension::SetToleranceValues(double tolMin, double tolMax)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		tolMin, tolMax);
	return result;
}

VARIANT IDimension::GetToleranceFontInfo()
{
	VARIANT result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IDimension::IGetToleranceFontInfo()
{
	double result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IDimension::SetToleranceFontInfo(long useFontScale, double tolScale, double tolHeight)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		useFontScale, tolScale, tolHeight);
	return result;
}

CString IDimension::GetToleranceFitValues()
{
	CString result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IDimension::SetToleranceFitValues(LPCTSTR newLValue, LPCTSTR newUValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		newLValue, newUValue);
	return result;
}

BOOL IDimension::GetReadOnly()
{
	BOOL result;
	InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDimension::SetReadOnly(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

CString IDimension::GetFullName()
{
	CString result;
	InvokeHelper(0x13, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IDimension::GetDrivenState()
{
	long result;
	InvokeHelper(0x14, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IDimension::SetDrivenState(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x14, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IDimension::IsReference()
{
	BOOL result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IDimension::IsAppliedToAllConfigurations()
{
	BOOL result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IDimension::SetSystemValue2(double newValue, long whichConfigurations)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		newValue, whichConfigurations);
	return result;
}

long IDimension::SetValue2(double newValue, long whichConfigurations)
{
	long result;
	static BYTE parms[] =
		VTS_R8 VTS_I4;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		newValue, whichConfigurations);
	return result;
}

long IDimension::SetUserValueIn2(LPDISPATCH doc, double newValue, long whichConfigurations)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8 VTS_I4;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		doc, newValue, whichConfigurations);
	return result;
}

long IDimension::ISetUserValueIn2(LPDISPATCH doc, double newValue, long whichConfigurations)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8 VTS_I4;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		doc, newValue, whichConfigurations);
	return result;
}

long IDimension::GetArcEndCondition(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IDimension::SetArcEndCondition(long index, long condition)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index, condition);
	return result;
}

double IDimension::GetValue2(LPCTSTR configName)
{
	double result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		configName);
	return result;
}

double IDimension::GetSystemValue2(LPCTSTR configName)
{
	double result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		configName);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDisplayDimension properties

/////////////////////////////////////////////////////////////////////////////
// IDisplayDimension operations

LPDISPATCH IDisplayDimension::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDisplayDimension::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDisplayDimension::GetDisplayData()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDisplayDimension::IGetDisplayData()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetWitnessVisibility()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetWitnessVisibility(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IDisplayDimension::GetLeaderVisibility()
{
	long result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetLeaderVisibility(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IDisplayDimension::GetBrokenLeader()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetBrokenLeader(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDisplayDimension::GetSmartWitness()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetSmartWitness(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDisplayDimension::GetShowParenthesis()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetShowParenthesis(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

long IDisplayDimension::GetArrowSide()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetArrowSide(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IDisplayDimension::GetShowDimensionValue()
{
	BOOL result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetShowDimensionValue(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

CString IDisplayDimension::GetText(long whichText)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		whichText);
	return result;
}

void IDisplayDimension::SetText(long whichText, LPCTSTR text)
{
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 whichText, text);
}

LPDISPATCH IDisplayDimension::GetDimension()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDisplayDimension::IGetDimension()
{
	LPDISPATCH result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IDisplayDimension::GetUseDocTextFormat()
{
	BOOL result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDisplayDimension::GetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDisplayDimension::IGetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IDisplayDimension::SetTextFormat(long textFormatType, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		textFormatType, textFormat);
	return result;
}

BOOL IDisplayDimension::ISetTextFormat(long textFormatType, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		textFormatType, textFormat);
	return result;
}

LPDISPATCH IDisplayDimension::GetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDisplayDimension::IGetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IDisplayDimension::GetSolidLeader()
{
	BOOL result;
	InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetSolidLeader(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDisplayDimension::GetDiametric()
{
	BOOL result;
	InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetDiametric(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x18, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDisplayDimension::GetDisplayAsLinear()
{
	BOOL result;
	InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetDisplayAsLinear(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDisplayDimension::GetUseDocSecondArrow()
{
	BOOL result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IDisplayDimension::GetSecondArrow()
{
	BOOL result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetSecondArrow(BOOL useDoc, BOOL secondArrow)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 useDoc, secondArrow);
}

BOOL IDisplayDimension::GetShortenedRadius()
{
	BOOL result;
	InvokeHelper(0x1d, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetShortenedRadius(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDisplayDimension::GetDimensionToInside()
{
	BOOL result;
	InvokeHelper(0x1e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetDimensionToInside(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDisplayDimension::GetUseDocDual()
{
	BOOL result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetDual(BOOL useDoc)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 useDoc);
}

BOOL IDisplayDimension::GetUseDocArrowHeadStyle()
{
	BOOL result;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetArrowHeadStyle()
{
	long result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetArrowHeadStyle(BOOL useDoc, long arrowHeadStyle)
{
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 useDoc, arrowHeadStyle);
}

BOOL IDisplayDimension::GetCenterText()
{
	BOOL result;
	InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetCenterText(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDisplayDimension::GetInspection()
{
	BOOL result;
	InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetInspection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDisplayDimension::GetUseDocPrecision()
{
	BOOL result;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetPrimaryPrecision()
{
	long result;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetAlternatePrecision()
{
	long result;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetPrimaryTolPrecision()
{
	long result;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetAlternateTolPrecision()
{
	long result;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::SetPrecision(BOOL useDoc, long primary, long alternate, long primaryTol, long alternateTol)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4 VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		useDoc, primary, alternate, primaryTol, alternateTol);
	return result;
}

BOOL IDisplayDimension::GetAutoArcLengthLeader()
{
	BOOL result;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetArcLengthLeader()
{
	long result;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::SetArcLengthLeader(BOOL autoLeader, long leaderType)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		autoLeader, leaderType);
	return result;
}

BOOL IDisplayDimension::GetUseDocUnits()
{
	BOOL result;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetUnits()
{
	long result;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetFractionBase()
{
	long result;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetFractionValue()
{
	long result;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IDisplayDimension::GetRoundToFraction()
{
	BOOL result;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::SetUnits(BOOL useDoc, long uType, long fractBase, long fractDenom, BOOL roundToFraction)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4 VTS_I4 VTS_I4 VTS_BOOL;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		useDoc, uType, fractBase, fractDenom, roundToFraction);
	return result;
}

BOOL IDisplayDimension::GetUseDocBrokenLeader()
{
	BOOL result;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetBrokenLeader2()
{
	long result;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::SetBrokenLeader2(BOOL useDoc, long broken)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		useDoc, broken);
	return result;
}

LPDISPATCH IDisplayDimension::GetNext2()
{
	LPDISPATCH result;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDisplayDimension::IGetNext2()
{
	LPDISPATCH result;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IDisplayDimension::GetType()
{
	long result;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

double IDisplayDimension::GetScale2()
{
	double result;
	InvokeHelper(0x3b, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetScale2(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x3b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IDisplayDimension::GetDisplayAsChain()
{
	BOOL result;
	InvokeHelper(0x3c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetDisplayAsChain(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x3c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

LPDISPATCH IDisplayDimension::GetNext3()
{
	LPDISPATCH result;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDisplayDimension::IGetNext3()
{
	LPDISPATCH result;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IDisplayDimension::AddDisplayEnt(long type, const VARIANT& data)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		type, &data);
	return result;
}

BOOL IDisplayDimension::IAddDisplayEnt(long type, double* data)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_PR8;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		type, data);
	return result;
}

BOOL IDisplayDimension::AddDisplayText(LPCTSTR text, const VARIANT& position, LPDISPATCH format, long attachment, double WidthFactor)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_VARIANT VTS_DISPATCH VTS_I4 VTS_R8;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		text, &position, format, attachment, WidthFactor);
	return result;
}

BOOL IDisplayDimension::IAddDisplayText(LPCTSTR text, double* position, LPDISPATCH format, long attachment, double WidthFactor)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_PR8 VTS_DISPATCH VTS_I4 VTS_R8;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		text, position, format, attachment, WidthFactor);
	return result;
}

BOOL IDisplayDimension::GetJogged()
{
	BOOL result;
	InvokeHelper(0x43, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDisplayDimension::SetJogged(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x43, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDisplayDimension::AutoJogOrdinate()
{
	BOOL result;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDisplayData properties

/////////////////////////////////////////////////////////////////////////////
// IDisplayData operations

long IDisplayData::GetTextCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IDisplayData::GetTextAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::GetTextHeightAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDisplayData::GetTextPositionAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetTextPositionAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::GetTextAngleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetTextRefPositionAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetTextInvertAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

CString IDisplayData::GetTextFontAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetLineCount()
{
	long result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDisplayData::GetLineAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetLineAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetArcCount()
{
	long result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDisplayData::GetArcAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetArcAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetArrowHeadCount()
{
	long result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDisplayData::GetArrowHeadAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetArrowHeadAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetTriangleCount()
{
	long result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDisplayData::GetTriangleAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetTriangleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetPolylineCount()
{
	long result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDisplayData::GetPolylineSizeAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDisplayData::GetPolylineAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetPolylineAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetEllipseCount()
{
	long result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDisplayData::GetEllipseAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetEllipseAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::GetTextLineSpacingAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDisplayData::GetLineAtIndex2(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetLineAtIndex2(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDisplayData::GetArcAtIndex2(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetArcAtIndex2(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetPolylineSizeAtIndex2(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDisplayData::GetPolylineAtIndex2(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetPolylineAtIndex2(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDisplayData::GetEllipseAtIndex2(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetEllipseAtIndex2(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetParabolaCount()
{
	long result;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDisplayData::GetParabolaAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetParabolaAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDisplayData::GetPolygonCount()
{
	long result;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDisplayData::GetPolygonSizeAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDisplayData::GetPolygonAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDisplayData::IGetPolygonAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ITextFormat properties

/////////////////////////////////////////////////////////////////////////////
// ITextFormat operations

VARIANT ITextFormat::GetAllValues()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ITextFormat::IGetAllValues(long count)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		count);
	return result;
}

BOOL ITextFormat::GetItalic()
{
	BOOL result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetItalic(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITextFormat::GetUnderline()
{
	BOOL result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetUnderline(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITextFormat::GetStrikeout()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetStrikeout(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITextFormat::GetBold()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetBold(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double ITextFormat::GetEscapement()
{
	double result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetEscapement(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ITextFormat::GetLineSpacing()
{
	double result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetLineSpacing(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ITextFormat::GetCharHeight()
{
	double result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetCharHeight(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ITextFormat::GetCharHeightInPts()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetCharHeightInPts(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL ITextFormat::IsHeightSpecifiedInPts()
{
	BOOL result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString ITextFormat::GetTypeFaceName()
{
	CString result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetTypeFaceName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

double ITextFormat::GetWidthFactor()
{
	double result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetWidthFactor(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ITextFormat::GetObliqueAngle()
{
	double result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetObliqueAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ITextFormat::GetLineLength()
{
	double result;
	InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetLineLength(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL ITextFormat::GetVertical()
{
	BOOL result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetVertical(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITextFormat::GetBackWards()
{
	BOOL result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetBackWards(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITextFormat::GetUpsideDown()
{
	BOOL result;
	InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITextFormat::SetUpsideDown(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// IAnnotation properties

/////////////////////////////////////////////////////////////////////////////
// IAnnotation operations

LPDISPATCH IAnnotation::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAnnotation::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAnnotation::GetDisplayData()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAnnotation::IGetDisplayData()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IAnnotation::GetType()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAnnotation::GetSpecificAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IAnnotation::IGetSpecificAnnotation()
{
	LPUNKNOWN result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

VARIANT IAnnotation::GetPosition()
{
	VARIANT result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IAnnotation::IGetPosition()
{
	double result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IAnnotation::SetPosition(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

long IAnnotation::GetLeaderCount()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IAnnotation::GetLeaderPointsAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IAnnotation::IGetLeaderPointsAtIndex(long index, long pointCount)
{
	double result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index, pointCount);
	return result;
}

long IAnnotation::GetArrowHeadStyleAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IAnnotation::SetArrowHeadStyleAtIndex(long index, long arrowHeadStyle)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index, arrowHeadStyle);
	return result;
}

BOOL IAnnotation::GetLeader()
{
	BOOL result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IAnnotation::GetBentLeader()
{
	BOOL result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IAnnotation::GetLeaderSide()
{
	long result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IAnnotation::GetSmartArrowHeadStyle()
{
	BOOL result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IAnnotation::SetLeader(BOOL leader, long leaderSide, BOOL smartArrowHeadStyle, BOOL bentLeader)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		leader, leaderSide, smartArrowHeadStyle, bentLeader);
	return result;
}

CString IAnnotation::GetName()
{
	CString result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IAnnotation::SetName(LPCTSTR nameIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nameIn);
	return result;
}

VARIANT IAnnotation::GetVisualProperties()
{
	VARIANT result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IAnnotation::IGetVisualProperties()
{
	long result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IAnnotation::GetLayer()
{
	CString result;
	InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IAnnotation::SetLayer(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

long IAnnotation::GetLayerOverride()
{
	long result;
	InvokeHelper(0x1a, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IAnnotation::SetLayerOverride(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IAnnotation::GetColor()
{
	long result;
	InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IAnnotation::SetColor(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IAnnotation::GetStyle()
{
	long result;
	InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IAnnotation::SetStyle(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IAnnotation::GetWidth()
{
	long result;
	InvokeHelper(0x1d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IAnnotation::SetWidth(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IAnnotation::IGetAttachedEntityCount()
{
	long result;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IAnnotation::GetAttachedEntities()
{
	VARIANT result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IAnnotation::IGetAttachedEntities()
{
	LPUNKNOWN result;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

VARIANT IAnnotation::GetAttachedEntityTypes()
{
	VARIANT result;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IAnnotation::IGetAttachedEntityTypes()
{
	long result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAnnotation::GetNext2()
{
	LPDISPATCH result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAnnotation::IGetNext2()
{
	LPDISPATCH result;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IAnnotation::GetVisible()
{
	long result;
	InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IAnnotation::SetVisible(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IAnnotation::GetTextFormatCount()
{
	long result;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IAnnotation::GetUseDocTextFormat(long index)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH IAnnotation::GetTextFormat(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH IAnnotation::IGetTextFormat(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

BOOL IAnnotation::SetTextFormat(long index, BOOL useDoc, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL VTS_DISPATCH;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		index, useDoc, textFormat);
	return result;
}

BOOL IAnnotation::ISetTextFormat(long index, BOOL useDoc, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL VTS_DISPATCH;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		index, useDoc, textFormat);
	return result;
}

BOOL IAnnotation::GetLeaderPerpendicular()
{
	BOOL result;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IAnnotation::GetLeaderAllAround()
{
	BOOL result;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IAnnotation::SetLeader2(BOOL leader, long leaderSide, BOOL smartArrowHeadStyle, BOOL bentLeader, BOOL perpendicular, BOOL allAround)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		leader, leaderSide, smartArrowHeadStyle, bentLeader, perpendicular, allAround);
	return result;
}

BOOL IAnnotation::Select(BOOL appendFlag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag);
	return result;
}

BOOL IAnnotation::SelectByMark(BOOL appendFlag, long mark)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag, mark);
	return result;
}

BOOL IAnnotation::DeSelect()
{
	BOOL result;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IPartDoc properties

/////////////////////////////////////////////////////////////////////////////
// IPartDoc operations

CString IPartDoc::GetMaterialUserName()
{
	CString result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IPartDoc::SetMaterialUserName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString IPartDoc::GetMaterialIdName()
{
	CString result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IPartDoc::SetMaterialIdName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

VARIANT IPartDoc::GetMaterialPropertyValues()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IPartDoc::SetMaterialPropertyValues(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

LPDISPATCH IPartDoc::FirstFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IPartDoc::IFirstFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IPartDoc::FeatureByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IPartDoc::IFeatureByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IPartDoc::FeatureById(long Id)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Id);
	return result;
}

LPDISPATCH IPartDoc::IFeatureById(long Id)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Id);
	return result;
}

LPDISPATCH IPartDoc::Body()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IPartDoc::IBodyObject()
{
	LPDISPATCH result;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IPartDoc::FeatureExtrusion(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2);
}

void IPartDoc::FeatureRevolve(double angle, BOOL reverseDir, double angle2, long revType)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_R8 VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 angle, reverseDir, angle2, revType);
}

void IPartDoc::FeatureRevolveCut(double angle, BOOL reverseDir, double angle2, long revType)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_R8 VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 angle, reverseDir, angle2, revType);
}

VARIANT IPartDoc::GetTessTriangles(BOOL noConversion)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		noConversion);
	return result;
}

VARIANT IPartDoc::GetTessNorms()
{
	VARIANT result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH IPartDoc::GetProcessedBody()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IPartDoc::IGetProcessedBody()
{
	LPDISPATCH result;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IPartDoc::GetPartBox(BOOL noConversion)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		noConversion);
	return result;
}

LPDISPATCH IPartDoc::CreateNewBody()
{
	LPDISPATCH result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IPartDoc::ICreateNewBody()
{
	LPDISPATCH result;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IPartDoc::EditRebuild()
{
	InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::ForceRebuild()
{
	InvokeHelper(0x14, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::EditUnsuppressDependent()
{
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::EditRollforward()
{
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::InsertStockTurned()
{
	InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::EditUnsuppress()
{
	InvokeHelper(0x18, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::MakeSection()
{
	InvokeHelper(0x19, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::Dumpfacets()
{
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::FeatureStock()
{
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::EditRollback()
{
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::EditSuppress()
{
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IPartDoc::AddPropertyExtension(const VARIANT& PropertyExtension)
{
	long result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		&PropertyExtension);
	return result;
}

VARIANT IPartDoc::GetPropertyExtension(long Id)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		Id);
	return result;
}

void IPartDoc::MirrorFeature()
{
	InvokeHelper(0x20, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IPartDoc::ResetPropertyExtension()
{
	InvokeHelper(0x21, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IPartDoc::GetProcessedBodyWithSelFace()
{
	LPDISPATCH result;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IPartDoc::IGetProcessedBodyWithSelFace()
{
	LPDISPATCH result;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IPartDoc::GetTessTriStrips(BOOL noConversion)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		noConversion);
	return result;
}

VARIANT IPartDoc::GetTessTriStripNorms()
{
	VARIANT result;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IPartDoc::MirrorPart()
{
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

float IPartDoc::IGetTessTriangles(BOOL noConversion)
{
	float result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_R4, (void*)&result, parms,
		noConversion);
	return result;
}

float IPartDoc::IGetTessNorms()
{
	float result;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_R4, (void*)&result, NULL);
	return result;
}

long IPartDoc::GetTessTriangleCount()
{
	long result;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

double IPartDoc::IGetPartBox(BOOL noConversion)
{
	double result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		noConversion);
	return result;
}

float IPartDoc::IGetTessTriStrips(BOOL noConversion)
{
	float result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_R4, (void*)&result, parms,
		noConversion);
	return result;
}

float IPartDoc::IGetTessTriStripNorms()
{
	float result;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_R4, (void*)&result, NULL);
	return result;
}

long IPartDoc::GetTessTriStripSize()
{
	long result;
	InvokeHelper(0x43, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IPartDoc::GetEntityByName(LPCTSTR Name, long entityType)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name, entityType);
	return result;
}

LPDISPATCH IPartDoc::IGetEntityByName(LPCTSTR Name, long entityType)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name, entityType);
	return result;
}

CString IPartDoc::GetEntityName(LPDISPATCH entity)
{
	CString result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x46, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		entity);
	return result;
}

CString IPartDoc::IGetEntityName(LPDISPATCH entity)
{
	CString result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x47, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		entity);
	return result;
}

BOOL IPartDoc::SetEntityName(LPDISPATCH entity, LPCTSTR StringValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BSTR;
	InvokeHelper(0x48, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		entity, StringValue);
	return result;
}

BOOL IPartDoc::ISetEntityName(LPDISPATCH entity, LPCTSTR StringValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BSTR;
	InvokeHelper(0x49, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		entity, StringValue);
	return result;
}

BOOL IPartDoc::ReorderFeature(LPCTSTR featureToMove, LPCTSTR moveAfterFeature)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x4a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		featureToMove, moveAfterFeature);
	return result;
}

LPUNKNOWN IPartDoc::EnumRelatedBodies()
{
	LPUNKNOWN result;
	InvokeHelper(0x4b, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

LPDISPATCH IPartDoc::GetSectionedBody(LPDISPATCH viewIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		viewIn);
	return result;
}

LPDISPATCH IPartDoc::IGetSectionedBody(LPDISPATCH viewIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		viewIn);
	return result;
}

LPUNKNOWN IPartDoc::EnumRelatedSectionedBodies(LPDISPATCH viewIn)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4e, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		viewIn);
	return result;
}

double IPartDoc::GetIMaterialPropertyValues()
{
	double result;
	InvokeHelper(0x4f, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IPartDoc::SetIMaterialPropertyValues(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x4f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

void IPartDoc::FeatureRevolveThin(double angle, BOOL reverseDir, double angle2, long revType, double thickness1, double thickness2, long reverseThinDir)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_R8 VTS_I4 VTS_R8 VTS_R8 VTS_I4;
	InvokeHelper(0x50, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 angle, reverseDir, angle2, revType, thickness1, thickness2, reverseThinDir);
}

void IPartDoc::FeatureRevolveThinCut(double angle, BOOL reverseDir, double angle2, long revType, double thickness1, double thickness2, long reverseThinDir)
{
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_R8 VTS_I4 VTS_R8 VTS_R8 VTS_I4;
	InvokeHelper(0x51, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 angle, reverseDir, angle2, revType, thickness1, thickness2, reverseThinDir);
}

void IPartDoc::FeatureExtrusionThin(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, double thk1, double thk2, 
		double endThk, long revThinDir, long capEnds, BOOL addBends, double bendRad)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_BOOL VTS_R8;
	InvokeHelper(0x52, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2, thk1, thk2, endThk, revThinDir, capEnds, addBends, bendRad);
}

BOOL IPartDoc::InsertBends(double radius, LPCTSTR useBendTable, double useKfactor, double useBendAllowance, BOOL useAutoRelief, double offsetRatio)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_BOOL VTS_R8;
	InvokeHelper(0x53, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		radius, useBendTable, useKfactor, useBendAllowance, useAutoRelief, offsetRatio);
	return result;
}

LPDISPATCH IPartDoc::CreateFeatureFromBody(LPDISPATCH Body)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x54, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Body);
	return result;
}

LPDISPATCH IPartDoc::ICreateFeatureFromBody(LPDISPATCH Body)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x55, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Body);
	return result;
}

LPDISPATCH IPartDoc::CreateFeatureFromBody2(LPDISPATCH Body, BOOL makeRefSurface)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BOOL;
	InvokeHelper(0x56, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Body, makeRefSurface);
	return result;
}

LPDISPATCH IPartDoc::ICreateFeatureFromBody2(LPDISPATCH Body, BOOL makeRefSurface)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BOOL;
	InvokeHelper(0x57, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Body, makeRefSurface);
	return result;
}

long IPartDoc::ImportDiagnosis(BOOL closeAllGaps, BOOL removeFaces, BOOL fixFaces, long options)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x58, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		closeAllGaps, removeFaces, fixFaces, options);
	return result;
}

BOOL IPartDoc::DeleteEntityName(LPDISPATCH entity)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x59, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		entity);
	return result;
}

BOOL IPartDoc::IDeleteEntityName(LPDISPATCH entity)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x5a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		entity);
	return result;
}

VARIANT IPartDoc::GetTessTriStripEdges()
{
	VARIANT result;
	InvokeHelper(0x5b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IPartDoc::IGetTessTriStripEdges()
{
	long result;
	InvokeHelper(0x5c, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IPartDoc::IGetTessTriStripEdgeSize()
{
	long result;
	InvokeHelper(0x5d, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IPartDoc::EnumBodies(long bodyType)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5e, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		bodyType);
	return result;
}

VARIANT IPartDoc::GetBodies(long bodyType)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5f, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		bodyType);
	return result;
}

VARIANT IPartDoc::GetRelatedBodies()
{
	VARIANT result;
	InvokeHelper(0x60, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IPartDoc::GetRelatedSectionedBodies(LPDISPATCH viewIn)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x61, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		viewIn);
	return result;
}

BOOL IPartDoc::InsertBends2(double radius, LPCTSTR useBendTable, double useKfactor, double useBendAllowance, BOOL useAutoRelief, double offsetRatio, BOOL doFlatten)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_BOOL VTS_R8 VTS_BOOL;
	InvokeHelper(0x62, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		radius, useBendTable, useKfactor, useBendAllowance, useAutoRelief, offsetRatio, doFlatten);
	return result;
}

LPDISPATCH IPartDoc::CreateFeatureFromBody3(LPDISPATCH Body, BOOL makeRefSurface, long options)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BOOL VTS_I4;
	InvokeHelper(0x63, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Body, makeRefSurface, options);
	return result;
}

LPDISPATCH IPartDoc::ICreateFeatureFromBody3(LPDISPATCH Body, BOOL makeRefSurface, long options)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BOOL VTS_I4;
	InvokeHelper(0x64, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Body, makeRefSurface, options);
	return result;
}

LPDISPATCH IPartDoc::GetMateReferenceEntity()
{
	LPDISPATCH result;
	InvokeHelper(0x65, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IPartDoc::FeatureExtrusion2(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, BOOL merge)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x66, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2, merge);
}

void IPartDoc::FeatureExtrusionThin2(BOOL sd, BOOL flip, BOOL dir, long t1, long t2, double d1, double d2, BOOL dchk1, BOOL dchk2, BOOL ddir1, BOOL ddir2, double dang1, double dang2, BOOL offsetReverse1, BOOL offsetReverse2, BOOL merge, double thk1, 
		double thk2, double endThk, long revThinDir, long capEnds, BOOL addBends, double bendRad)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_BOOL VTS_R8;
	InvokeHelper(0x67, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 sd, flip, dir, t1, t2, d1, d2, dchk1, dchk2, ddir1, ddir2, dang1, dang2, offsetReverse1, offsetReverse2, merge, thk1, thk2, endThk, revThinDir, capEnds, addBends, bendRad);
}

VARIANT IPartDoc::CreateSurfaceFeatureFromBody(LPDISPATCH Body, long options)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x68, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		Body, options);
	return result;
}

long IPartDoc::ICreateSurfaceFeatureFromBodyCount(LPDISPATCH Body, long options)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x69, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		Body, options);
	return result;
}

LPDISPATCH IPartDoc::ICreateSurfaceFeatureFromBody()
{
	LPDISPATCH result;
	InvokeHelper(0x6a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IEntity properties

/////////////////////////////////////////////////////////////////////////////
// IEntity operations

long IEntity::CreateStringAttributeDefinition(LPCTSTR identifierString)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10001, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		identifierString);
	return result;
}

BOOL IEntity::CreateStringAttribute(long definitionTag, LPCTSTR StringValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x10002, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		definitionTag, StringValue);
	return result;
}

CString IEntity::FindStringAttribute(long definitionTag)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10003, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		definitionTag);
	return result;
}

BOOL IEntity::RemoveStringAttribute(long definitionTag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10004, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		definitionTag);
	return result;
}

LPDISPATCH IEntity::FindAttribute(LPDISPATCH attributeDef, long whichOne)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x10005, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		attributeDef, whichOne);
	return result;
}

LPDISPATCH IEntity::IFindAttribute(LPDISPATCH attributeDef, long whichOne)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x10006, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		attributeDef, whichOne);
	return result;
}

BOOL IEntity::Select(BOOL appendFlag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10007, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag);
	return result;
}

long IEntity::GetType()
{
	long result;
	InvokeHelper(0x10008, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IEntity::GetComponent()
{
	LPDISPATCH result;
	InvokeHelper(0x10009, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IEntity::IGetComponent()
{
	LPDISPATCH result;
	InvokeHelper(0x1000a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IEntity::SelectByMark(BOOL appendFlag, long mark)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x1000b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag, mark);
	return result;
}

CString IEntity::GetModelName()
{
	CString result;
	InvokeHelper(0x1000c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IEntity::SetModelName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1000c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

void IEntity::DeleteModelName()
{
	InvokeHelper(0x1000d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IEntity::DeSelect()
{
	BOOL result;
	InvokeHelper(0x1000e, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IAttributeDef properties

/////////////////////////////////////////////////////////////////////////////
// IAttributeDef operations

LPDISPATCH IAttributeDef::CreateInstance(LPDISPATCH ownerDoc, LPDISPATCH ownerEntity, LPCTSTR nameIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ownerDoc, ownerEntity, nameIn);
	return result;
}

LPDISPATCH IAttributeDef::ICreateInstance(LPDISPATCH ownerDoc, LPDISPATCH ownerEntity, LPCTSTR nameIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ownerDoc, ownerEntity, nameIn);
	return result;
}

BOOL IAttributeDef::AddParameter(LPCTSTR nameIn, long type, double defaultValue, long options)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_R8 VTS_I4;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nameIn, type, defaultValue, options);
	return result;
}

BOOL IAttributeDef::SetOption(long whichOption, long optionValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		whichOption, optionValue);
	return result;
}

long IAttributeDef::GetOption(long whichOption)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		whichOption);
	return result;
}

BOOL IAttributeDef::AddCallback(long whichCallback, LPCTSTR CallbackFcnAndModule, long whichOption)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		whichCallback, CallbackFcnAndModule, whichOption);
	return result;
}

BOOL IAttributeDef::Register()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAttributeDef::CreateInstance2(LPDISPATCH ownerDoc, LPDISPATCH ownerEntity, LPCTSTR nameIn, long options)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_BSTR VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ownerDoc, ownerEntity, nameIn, options);
	return result;
}

LPDISPATCH IAttributeDef::ICreateInstance2(LPDISPATCH ownerDoc, LPDISPATCH ownerEntity, LPCTSTR nameIn, long options)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_BSTR VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ownerDoc, ownerEntity, nameIn, options);
	return result;
}

LPDISPATCH IAttributeDef::CreateInstance3(LPDISPATCH ownerDoc, LPDISPATCH ownerComp, LPDISPATCH ownerEntity, LPCTSTR nameIn, long options, long configurationOption)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_DISPATCH VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ownerDoc, ownerComp, ownerEntity, nameIn, options, configurationOption);
	return result;
}

LPDISPATCH IAttributeDef::ICreateInstance3(LPDISPATCH ownerDoc, LPDISPATCH ownerComp, LPDISPATCH ownerEntity, LPCTSTR nameIn, long options, long configurationOption)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_DISPATCH VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ownerDoc, ownerComp, ownerEntity, nameIn, options, configurationOption);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IAttribute properties

/////////////////////////////////////////////////////////////////////////////
// IAttribute operations

LPDISPATCH IAttribute::GetParameter(LPCTSTR nameIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		nameIn);
	return result;
}

LPDISPATCH IAttribute::IGetParameter(LPCTSTR nameIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		nameIn);
	return result;
}

LPDISPATCH IAttribute::GetEntity()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAttribute::IGetEntity()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAttribute::GetDefinition()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAttribute::IGetDefinition()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IAttribute::GetName()
{
	CString result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IAttribute::GetEntityState(long whichState)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		whichState);
	return result;
}

LPDISPATCH IAttribute::GetComponent()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAttribute::IGetComponent()
{
	LPDISPATCH result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IParameter properties

/////////////////////////////////////////////////////////////////////////////
// IParameter operations

double IParameter::GetDoubleValue()
{
	double result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IParameter::SetDoubleValue(double Value)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Value);
	return result;
}

long IParameter::GetType()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IParameter::GetName()
{
	CString result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IParameter::GetOption(long whichOption)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		whichOption);
	return result;
}

BOOL IParameter::SetOption(long whichOption, long optionValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		whichOption, optionValue);
	return result;
}

CString IParameter::GetStringValue()
{
	CString result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IParameter::SetStringValue(LPCTSTR StringValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		StringValue);
	return result;
}

void IParameter::GetVector(double* x, double* y, double* z)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x, y, z);
}

BOOL IParameter::SetVector(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

VARIANT IParameter::GetVectorVB()
{
	VARIANT result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

BOOL IParameter::SetDoubleValue2(double Value, long configurationOption, LPCTSTR configurationName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_I4 VTS_BSTR;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Value, configurationOption, configurationName);
	return result;
}

BOOL IParameter::SetStringValue2(LPCTSTR StringValue, long configurationOption, LPCTSTR configurationName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		StringValue, configurationOption, configurationName);
	return result;
}

BOOL IParameter::SetVector2(double x, double y, double z, long configurationOption, LPCTSTR configurationName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_BSTR;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z, configurationOption, configurationName);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IModelView properties

/////////////////////////////////////////////////////////////////////////////
// IModelView operations

VARIANT IModelView::GetXform()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IModelView::SetXform(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT IModelView::GetOrientation()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IModelView::SetOrientation(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT IModelView::GetTranslation()
{
	VARIANT result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IModelView::SetTranslation(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IModelView::GetScale()
{
	double result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelView::SetScale(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IModelView::GetIXform()
{
	double result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelView::SetIXform(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IModelView::GetIOrientation()
{
	double result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelView::SetIOrientation(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IModelView::GetITranslation()
{
	double result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelView::SetITranslation(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IModelView::GetViewHWnd()
{
	long result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelView::StartDynamics()
{
	InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelView::StopDynamics()
{
	InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelView::AddPerspective()
{
	InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IModelView::RemovePerspective()
{
	InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IModelView::HasPerspective()
{
	BOOL result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

VARIANT IModelView::GetEyePoint()
{
	VARIANT result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IModelView::IGetEyePoint(double* eyept)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 eyept);
}

double IModelView::GetViewPlaneDistance()
{
	double result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelView::InitializeShading()
{
	InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IModelView::GetOrientation2()
{
	VARIANT result;
	InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IModelView::SetOrientation2(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT IModelView::GetTranslation2()
{
	VARIANT result;
	InvokeHelper(0x13, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IModelView::SetTranslation2(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x13, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IModelView::GetIOrientation2()
{
	double result;
	InvokeHelper(0x14, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelView::SetIOrientation2(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x14, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IModelView::GetITranslation2()
{
	double result;
	InvokeHelper(0x15, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelView::SetITranslation2(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x15, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IModelView::GetDisplayState(long displayType)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		displayType);
	return result;
}

long IModelView::GetViewDIB()
{
	long result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelView::ZoomByFactor(double factor)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 factor);
}

void IModelView::TranslateBy(double x, double y)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x, y);
}

void IModelView::RotateAboutCenter(double xAngle, double yAngle)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 xAngle, yAngle);
}

void IModelView::RotateAboutPoint(double xAngle, double yAngle, double Ptx, double Pty, double Ptz)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 xAngle, yAngle, Ptx, Pty, Ptz);
}

void IModelView::RotateAboutAxis(double angle, double Ptx, double Pty, double Ptz, double AxisVecX, double AxisVecY, double AxisVecZ)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 angle, Ptx, Pty, Ptz, AxisVecX, AxisVecY, AxisVecZ);
}

BOOL IModelView::SetEyePoint(const VARIANT& eyept)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&eyept);
	return result;
}

BOOL IModelView::ISetEyePoint(double* eyept)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		eyept);
	return result;
}

BOOL IModelView::SetStereoSeparation(const VARIANT& dfSeparation)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&dfSeparation);
	return result;
}

BOOL IModelView::ISetStereoSeparation(double* dfSeparation)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		dfSeparation);
	return result;
}

VARIANT IModelView::GetStereoSeparation()
{
	VARIANT result;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IModelView::IGetStereoSeparation()
{
	double result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelView::GetDIBHeader(long ldib)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ldib);
}

void IModelView::GetStripsOfDIB(long ldib, long nScanLinesPerStrip, long stripIndex)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ldib, nScanLinesPerStrip, stripIndex);
}

void IModelView::SetFrameLeft(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

void IModelView::SetFrameTop(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x26, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

void IModelView::SetFrameWidth(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x27, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

void IModelView::SetFrameHeight(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x28, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

void IModelView::SetFrameState(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x29, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

double IModelView::GetObjectSizesAway()
{
	double result;
	InvokeHelper(0x2a, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelView::SetObjectSizesAway(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x2a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IModelView::GetDynamicMode()
{
	long result;
	InvokeHelper(0x2b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelView::DrawHighlightedItems()
{
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IModelView::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModelView::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

double IModelView::GetScale2()
{
	double result;
	InvokeHelper(0x2f, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IModelView::SetScale2(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x2f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IModelView::GetHlrQuality()
{
	long result;
	InvokeHelper(0x30, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IModelView::SetHlrQuality(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x30, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IModelView::GetXorHighlight()
{
	BOOL result;
	InvokeHelper(0x31, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IModelView::SetXorHighlight(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x31, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// ITessellation properties

/////////////////////////////////////////////////////////////////////////////
// ITessellation operations

double ITessellation::GetMaxFacetWidth()
{
	double result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITessellation::SetMaxFacetWidth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ITessellation::GetCurveChordTolerance()
{
	double result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITessellation::SetCurveChordTolerance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ITessellation::GetCurveChordAngleTolerance()
{
	double result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITessellation::SetCurveChordAngleTolerance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ITessellation::GetSurfacePlaneTolerance()
{
	double result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITessellation::SetSurfacePlaneTolerance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ITessellation::GetSurfacePlaneAngleTolerance()
{
	double result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ITessellation::SetSurfacePlaneAngleTolerance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL ITessellation::GetNeedVertexNormal()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITessellation::SetNeedVertexNormal(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITessellation::GetNeedVertexParams()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITessellation::SetNeedVertexParams(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITessellation::GetNeedFaceFacetMap()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITessellation::SetNeedFaceFacetMap(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITessellation::GetNeedEdgeFinMap()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITessellation::SetNeedEdgeFinMap(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITessellation::GetNeedErrorList()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITessellation::SetNeedErrorList(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITessellation::Tessellate()
{
	BOOL result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long ITessellation::GetFacetCount()
{
	long result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ITessellation::GetFinCount()
{
	long result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ITessellation::GetVertexCount()
{
	long result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ITessellation::GetFacetFins(long facetId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		facetId);
	return result;
}

long ITessellation::IGetFacetFinsCount(long facetId)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		facetId);
	return result;
}

long ITessellation::IGetFacetFins(long facetId)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		facetId);
	return result;
}

long ITessellation::GetFinCoFin(long finId)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		finId);
	return result;
}

VARIANT ITessellation::GetFinVertices(long finId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		finId);
	return result;
}

long ITessellation::IGetFinVertices(long finId)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		finId);
	return result;
}

VARIANT ITessellation::GetVertexPoint(long vertexId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		vertexId);
	return result;
}

double ITessellation::IGetVertexPoint(long vertexId)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		vertexId);
	return result;
}

VARIANT ITessellation::GetVertexNormal(long vertexId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		vertexId);
	return result;
}

double ITessellation::IGetVertexNormal(long vertexId)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		vertexId);
	return result;
}

VARIANT ITessellation::GetVertexParams(long vertexId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		vertexId);
	return result;
}

double ITessellation::IGetVertexParams(long vertexId)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		vertexId);
	return result;
}

VARIANT ITessellation::GetFaceFacets(LPDISPATCH facedisp)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		facedisp);
	return result;
}

long ITessellation::IGetFaceFacetsCount(LPDISPATCH faceObj)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		faceObj);
	return result;
}

long ITessellation::IGetFaceFacets(LPDISPATCH faceObj)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		faceObj);
	return result;
}

LPDISPATCH ITessellation::GetFacetFace(long facetId)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		facetId);
	return result;
}

LPDISPATCH ITessellation::IGetFacetFace(long facetId)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		facetId);
	return result;
}

VARIANT ITessellation::GetEdgeFins(LPDISPATCH edgeDisp)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		edgeDisp);
	return result;
}

long ITessellation::IGetEdgeFinsCount(LPDISPATCH edgeObj)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		edgeObj);
	return result;
}

long ITessellation::IGetEdgeFins(LPDISPATCH edgeObj)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		edgeObj);
	return result;
}

LPDISPATCH ITessellation::GetFinEdge(long finId)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		finId);
	return result;
}

LPDISPATCH ITessellation::IGetFinEdge(long finId)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		finId);
	return result;
}

void ITessellation::GetErrorList(VARIANT* faceErrArray, VARIANT* facetErrArray, VARIANT* vertexPointErrArray, VARIANT* vertexNormalErrArray, VARIANT* vertexParamsErrArray)
{
	static BYTE parms[] =
		VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 faceErrArray, facetErrArray, vertexPointErrArray, vertexNormalErrArray, vertexParamsErrArray);
}

void ITessellation::IGetErrorListCount(long* faceErrArrayCount, long* facetErrArrayCount, long* vertexPointErrArrayCount, long* vertexNormalErrArrayCount, long* vertexParamsErrArrayCount)
{
	static BYTE parms[] =
		VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 faceErrArrayCount, facetErrArrayCount, vertexPointErrArrayCount, vertexNormalErrArrayCount, vertexParamsErrArrayCount);
}

void ITessellation::IGetErrorList(LPDISPATCH* faceErrArray, long* facetErrArray, long* vertexPointErrArray, long* vertexNormalErrArray, long* vertexParamsErrArray)
{
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 faceErrArray, facetErrArray, vertexPointErrArray, vertexNormalErrArray, vertexParamsErrArray);
}


/////////////////////////////////////////////////////////////////////////////
// ISketch properties

/////////////////////////////////////////////////////////////////////////////
// ISketch operations

VARIANT ISketch::GetModelToSketchXform()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ISketch::SetModelToSketchXform(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

VARIANT ISketch::GetLines()
{
	VARIANT result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetLines()
{
	double result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketch::GetLineCount()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISketch::GetArcs()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetArcs()
{
	double result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketch::GetArcCount()
{
	long result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISketch::GetPolylines()
{
	VARIANT result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetPolylines()
{
	double result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketch::GetPolylineCount(long* pointCount)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		pointCount);
	return result;
}

VARIANT ISketch::GetSplines()
{
	VARIANT result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetSplines()
{
	double result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketch::GetSplineCount(long* pointCount)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		pointCount);
	return result;
}

VARIANT ISketch::GetEllipses()
{
	VARIANT result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetEllipses()
{
	double result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketch::GetEllipseCount()
{
	long result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

double ISketch::GetIModelToSketchXform()
{
	double result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketch::GetUserPoints()
{
	VARIANT result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetUserPoints()
{
	double result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketch::GetUserPointsCount()
{
	long result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISketch::GetSplinesInterpolate()
{
	VARIANT result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetSplinesInterpolate()
{
	double result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketch::GetSplineInterpolateCount(long* pointCount)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		pointCount);
	return result;
}

BOOL ISketch::GetAutomaticSolve()
{
	BOOL result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISketch::SetAutomaticSolve(BOOL solveFlag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		solveFlag);
	return result;
}

long ISketch::ConstrainAll()
{
	long result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISketch::GetSplineParams()
{
	VARIANT result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetSplineParams()
{
	double result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketch::GetSplineParamsCount(long* size)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		size);
	return result;
}

VARIANT ISketch::GetParabolas()
{
	VARIANT result;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetParabolas()
{
	double result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketch::GetParabolaCount()
{
	long result;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISketch::GetEllipses2()
{
	VARIANT result;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetEllipses2()
{
	double result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketch::GetSketchPoints()
{
	VARIANT result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN ISketch::IEnumSketchPoints()
{
	LPUNKNOWN result;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

VARIANT ISketch::GetSketchSegments()
{
	VARIANT result;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN ISketch::IEnumSketchSegments()
{
	LPUNKNOWN result;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

BOOL ISketch::Is3D()
{
	BOOL result;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long ISketch::GetSketchPointsCount()
{
	long result;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISketch::GetSketchHatches()
{
	VARIANT result;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN ISketch::IEnumSketchHatches()
{
	LPUNKNOWN result;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

VARIANT ISketch::GetSplineParams2()
{
	VARIANT result;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketch::IGetSplineParams2()
{
	double result;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketch::GetSplineParamsCount2(long* size)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		size);
	return result;
}

VARIANT ISketch::GetSketchTextSegments()
{
	VARIANT result;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN ISketch::IEnumSketchTextSegments()
{
	LPUNKNOWN result;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

long ISketch::GetConstrainedStatus()
{
	long result;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ISketch::SetEntityCount(long entityType, long entityCount)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		entityType, entityCount);
	return result;
}

long ISketch::CheckFeatureUse(long featureType, long* openCount, long* closedCount)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4 VTS_PI4;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		featureType, openCount, closedCount);
	return result;
}

BOOL ISketch::MergePoints(double distance)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		distance);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISketchPoint properties

/////////////////////////////////////////////////////////////////////////////
// ISketchPoint operations

double ISketchPoint::GetX()
{
	double result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

double ISketchPoint::GetY()
{
	double result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

double ISketchPoint::GetZ()
{
	double result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

double ISketchPoint::GetCoords()
{
	double result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchPoint::GetId()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long ISketchPoint::IGetID()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ISketchPoint::Select(BOOL appendFlag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag);
	return result;
}

BOOL ISketchPoint::SelectByMark(BOOL appendFlag, long mark)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag, mark);
	return result;
}

BOOL ISketchPoint::DeSelect()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString ISketchPoint::GetLayer()
{
	CString result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ISketchPoint::SetLayer(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

long ISketchPoint::GetLayerOverride()
{
	long result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISketchPoint::SetLayerOverride(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long ISketchPoint::GetColor()
{
	long result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISketchPoint::SetColor(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL ISketchPoint::SetCoords(double xx, double yy, double zz)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		xx, yy, zz);
	return result;
}

VARIANT ISketchPoint::GetFramePointTangent(BOOL* status)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_PBOOL;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		status);
	return result;
}

double ISketchPoint::IGetFramePointTangent(BOOL* status)
{
	double result;
	static BYTE parms[] =
		VTS_PBOOL;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		status);
	return result;
}

BOOL ISketchPoint::SetFramePointTangent(const VARIANT& toVector)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&toVector);
	return result;
}

BOOL ISketchPoint::ISetFramePointTangent(double* toVector)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		toVector);
	return result;
}

LPDISPATCH ISketchPoint::GetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISketchSegment properties

/////////////////////////////////////////////////////////////////////////////
// ISketchSegment operations

CString ISketchSegment::GetLayer()
{
	CString result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ISketchSegment::SetLayer(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

long ISketchSegment::GetLayerOverride()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISketchSegment::SetLayerOverride(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long ISketchSegment::GetColor()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISketchSegment::SetColor(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long ISketchSegment::GetStyle()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISketchSegment::SetStyle(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long ISketchSegment::GetWidth()
{
	long result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISketchSegment::SetWidth(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long ISketchSegment::GetType()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISketchSegment::GetId()
{
	VARIANT result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long ISketchSegment::IGetID()
{
	long result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ISketchSegment::Select(BOOL appendFlag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag);
	return result;
}

BOOL ISketchSegment::SelectByMark(BOOL appendFlag, long mark)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag, mark);
	return result;
}

BOOL ISketchSegment::DeSelect()
{
	BOOL result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISketchSegment::GetConstructionGeometry()
{
	BOOL result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISketchSegment::SetConstructionGeometry(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

LPDISPATCH ISketchSegment::GetCurve()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchSegment::IGetCurve()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchSegment::GetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT ISketchSegment::GetConstraints()
{
	VARIANT result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long ISketchSegment::IGetConstraintsCount()
{
	long result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString ISketchSegment::IGetConstraints()
{
	CString result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISketchHatch properties

/////////////////////////////////////////////////////////////////////////////
// ISketchHatch operations

VARIANT ISketchHatch::GetId()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long ISketchHatch::IGetID()
{
	long result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ISketchHatch::Select(BOOL appendFlag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag);
	return result;
}

BOOL ISketchHatch::SelectByMark(BOOL appendFlag, long mark)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		appendFlag, mark);
	return result;
}

BOOL ISketchHatch::DeSelect()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString ISketchHatch::GetLayer()
{
	CString result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ISketchHatch::SetLayer(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

long ISketchHatch::GetLayerOverride()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISketchHatch::SetLayerOverride(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long ISketchHatch::GetColor()
{
	long result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISketchHatch::SetColor(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

CString ISketchHatch::GetPattern()
{
	CString result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ISketchHatch::SetPattern(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

double ISketchHatch::GetScale()
{
	double result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISketchHatch::SetScale(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ISketchHatch::GetAngle()
{
	double result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISketchHatch::SetAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH ISketchHatch::GetFace()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchHatch::IGetFace()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchHatch::GetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

double ISketchHatch::GetScale2()
{
	double result;
	InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISketchHatch::SetScale2(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// IMidSurface properties

/////////////////////////////////////////////////////////////////////////////
// IMidSurface operations

long IMidSurface::GetFacePairCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMidSurface::GetFirstFacePair(double* thickness, LPDISPATCH* partnerFaceDisp)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PDISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		thickness, partnerFaceDisp);
	return result;
}

LPDISPATCH IMidSurface::IGetFirstFacePair(double* thickness, LPDISPATCH* partnerFaceDisp)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PDISPATCH;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		thickness, partnerFaceDisp);
	return result;
}

LPDISPATCH IMidSurface::GetNextFacePair(double* thickness, LPDISPATCH* partnerFaceDisp)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PDISPATCH;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		thickness, partnerFaceDisp);
	return result;
}

LPDISPATCH IMidSurface::IGetNextFacePair(double* thickness, LPDISPATCH* partnerFaceDisp)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PDISPATCH;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		thickness, partnerFaceDisp);
	return result;
}

LPDISPATCH IMidSurface::GetFirstNeutralSheet()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMidSurface::IGetFirstNeutralSheet()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMidSurface::GetNextNeutralSheet()
{
	LPDISPATCH result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMidSurface::IGetNextNeutralSheet()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IMidSurface::GetNeutralSheetCount()
{
	long result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IMidSurface::GetFaceCount()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMidSurface::GetFirstFace(LPDISPATCH* fromFace1Disp, LPDISPATCH* fromFace2Disp, double* thickness)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PDISPATCH VTS_PR8;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fromFace1Disp, fromFace2Disp, thickness);
	return result;
}

LPDISPATCH IMidSurface::IGetFirstFace(LPDISPATCH* fromFace1Disp, LPDISPATCH* fromFace2Disp, double* thickness)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PDISPATCH VTS_PR8;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fromFace1Disp, fromFace2Disp, thickness);
	return result;
}

LPDISPATCH IMidSurface::GetNextFace(LPDISPATCH* fromFace1Disp, LPDISPATCH* fromFace2Disp, double* thickness)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PDISPATCH VTS_PR8;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fromFace1Disp, fromFace2Disp, thickness);
	return result;
}

LPDISPATCH IMidSurface::IGetNextFace(LPDISPATCH* fromFace1Disp, LPDISPATCH* fromFace2Disp, double* thickness)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PDISPATCH VTS_PR8;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		fromFace1Disp, fromFace2Disp, thickness);
	return result;
}

LPDISPATCH IMidSurface::EdgeGetFace(LPDISPATCH edgeInDisp)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		edgeInDisp);
	return result;
}

LPDISPATCH IMidSurface::IEdgeGetFace(LPDISPATCH edgeInDisp)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		edgeInDisp);
	return result;
}

VARIANT IMidSurface::GetFirstFaceArray(double* thickness)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		thickness);
	return result;
}

VARIANT IMidSurface::GetNextFaceArray(double* thickness)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		thickness);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IFeatMgrView properties

/////////////////////////////////////////////////////////////////////////////
// IFeatMgrView operations

long IFeatMgrView::GetFeatMgrViewWnd()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IConfiguration properties

/////////////////////////////////////////////////////////////////////////////
// IConfiguration operations

LPDISPATCH IConfiguration::GetRootComponent()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IConfiguration::IGetRootComponent()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IConfiguration::GetName()
{
	CString result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IConfiguration::SetName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString IConfiguration::GetComment()
{
	CString result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IConfiguration::SetComment(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString IConfiguration::GetAlternateName()
{
	CString result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IConfiguration::SetAlternateName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

BOOL IConfiguration::GetUseAlternateNameInBOM()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IConfiguration::SetUseAlternateNameInBOM(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IConfiguration::GetSuppressNewFeatures()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IConfiguration::SetSuppressNewFeatures(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IConfiguration::GetHideNewComponentModels()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IConfiguration::SetHideNewComponentModels(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IConfiguration::GetSuppressNewComponentModels()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IConfiguration::SetSuppressNewComponentModels(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IConfiguration::GetShowChildComponentsInBOM()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IConfiguration::SetShowChildComponentsInBOM(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

long IConfiguration::GetNumberOfExplodeSteps()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IConfiguration::GetExplodeStep(long explodeStepIndex)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		explodeStepIndex);
	return result;
}

LPDISPATCH IConfiguration::IGetExplodeStep(long explodeStepIndex)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		explodeStepIndex);
	return result;
}

LPDISPATCH IConfiguration::AddExplodeStep(double explDist, BOOL reverseDir, BOOL rigidSubassembly, BOOL explodeRelated)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		explDist, reverseDir, rigidSubassembly, explodeRelated);
	return result;
}

LPDISPATCH IConfiguration::IAddExplodeStep(double explDist, BOOL reverseDir, BOOL rigidSubassembly, BOOL explodeRelated)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		explDist, reverseDir, rigidSubassembly, explodeRelated);
	return result;
}

BOOL IConfiguration::DeleteExplodeStep(LPCTSTR explodeStepName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		explodeStepName);
	return result;
}

CString IConfiguration::GetStreamName()
{
	CString result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IExplodeStep properties

/////////////////////////////////////////////////////////////////////////////
// IExplodeStep operations

long IExplodeStep::GetNumOfComponents()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IExplodeStep::GetComponentName(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

VARIANT IExplodeStep::GetComponentXform()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IExplodeStep::IGetComponentXform()
{
	double result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH IExplodeStep::GetComponent(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH IExplodeStep::IGetComponent(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

CString IExplodeStep::GetName()
{
	CString result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IExplodeStep::SetName(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// IReferenceCurve properties

/////////////////////////////////////////////////////////////////////////////
// IReferenceCurve operations

long IReferenceCurve::GetSegmentCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IReferenceCurve::GetFirstSegment()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IReferenceCurve::IGetFirstSegment()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IReferenceCurve::GetNextSegment()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IReferenceCurve::IGetNextSegment()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IReferenceCurve::SetColor(long colorIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		colorIn);
	return result;
}

BOOL IReferenceCurve::SetVisible(BOOL Visible)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Visible);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IColorTable properties

/////////////////////////////////////////////////////////////////////////////
// IColorTable operations

long IColorTable::GetCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IColorTable::GetNameAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

long IColorTable::GetColorRefAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IColorTable::GetStandardCount()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IColorTable::SetColorRefAtIndex(long index, long colorRef, long applyTo)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 index, colorRef, applyTo);
}

long IColorTable::GetBasicColorCount()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IColorTable::GetBasicColors()
{
	VARIANT result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IColorTable::IGetBasicColors(long ColorCount)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ColorCount);
	return result;
}

long IColorTable::GetCustomColorCount()
{
	long result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IColorTable::GetCustomColors()
{
	VARIANT result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IColorTable::IGetCustomColors(long ColorCount)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ColorCount);
	return result;
}

BOOL IColorTable::SetCustomColor(long index, long colorRef)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		index, colorRef);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDetailingDefaults properties

/////////////////////////////////////////////////////////////////////////////
// IDetailingDefaults operations

VARIANT IDetailingDefaults::GetAllValues()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IDetailingDefaults::IGetAllValues(long count)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		count);
	return result;
}

LPDISPATCH IDetailingDefaults::GetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDetailingDefaults::IGetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IDetailingDefaults::SetTextFormat(LPDISPATCH textFormat)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 textFormat);
}

void IDetailingDefaults::ISetTextFormat(LPDISPATCH textFormat)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 textFormat);
}

LPDISPATCH IDetailingDefaults::GetDimensionTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDetailingDefaults::IGetDimensionTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IDetailingDefaults::SetDimensionTextFormat(LPDISPATCH textFormat)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 textFormat);
}

void IDetailingDefaults::ISetDimensionTextFormat(LPDISPATCH textFormat)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 textFormat);
}

LPDISPATCH IDetailingDefaults::GetSectionTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDetailingDefaults::IGetSectionTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IDetailingDefaults::SetSectionTextFormat(LPDISPATCH textFormat)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 textFormat);
}

void IDetailingDefaults::ISetSectionTextFormat(LPDISPATCH textFormat)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 textFormat);
}

LPDISPATCH IDetailingDefaults::GetDetailTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDetailingDefaults::IGetDetailTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IDetailingDefaults::SetDetailTextFormat(LPDISPATCH textFormat)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 textFormat);
}

void IDetailingDefaults::ISetDetailTextFormat(LPDISPATCH textFormat)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 textFormat);
}


/////////////////////////////////////////////////////////////////////////////
// ILayerMgr properties

/////////////////////////////////////////////////////////////////////////////
// ILayerMgr operations

long ILayerMgr::AddLayer(LPCTSTR nameIn, LPCTSTR descIn, long colorIn, long styleIn, long widthIn)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		nameIn, descIn, colorIn, styleIn, widthIn);
	return result;
}

long ILayerMgr::SetCurrentLayer(LPCTSTR nameIn)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		nameIn);
	return result;
}

CString ILayerMgr::GetCurrentLayer()
{
	CString result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILayerMgr::GetLayer(LPCTSTR nameIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		nameIn);
	return result;
}

LPDISPATCH ILayerMgr::IGetLayer(LPCTSTR nameIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		nameIn);
	return result;
}

long ILayerMgr::GetCount()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ILayerMgr::GetLayerList()
{
	VARIANT result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString ILayerMgr::IGetLayerList()
{
	CString result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILayerMgr::GetLayerById(short layerId)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		layerId);
	return result;
}

LPDISPATCH ILayerMgr::IGetLayerById(short layerId)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		layerId);
	return result;
}

BOOL ILayerMgr::DeleteLayer(LPCTSTR Name)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ILayer properties

/////////////////////////////////////////////////////////////////////////////
// ILayer operations

CString ILayer::GetName()
{
	CString result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long ILayer::GetColor()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long ILayer::GetStyle()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long ILayer::GetWidth()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ILayer::GetVisible()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ILayer::SetVisible(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

CString ILayer::GetDescription()
{
	CString result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ILayer::SetDescription(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

long ILayer::GetId()
{
	long result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDesignTable properties

/////////////////////////////////////////////////////////////////////////////
// IDesignTable operations

long IDesignTable::GetRowCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDesignTable::GetColumnCount()
{
	long result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IDesignTable::GetHeaderText(long col)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		col);
	return result;
}

CString IDesignTable::GetEntryText(long row, long col)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		row, col);
	return result;
}

BOOL IDesignTable::Attach()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDesignTable::Detach()
{
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IDesignTable::GetEntryValue(long row, long col)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		row, col);
	return result;
}

CString IDesignTable::GetTitle()
{
	CString result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IDesignTable::GetTotalRowCount()
{
	long result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDesignTable::GetTotalColumnCount()
{
	long result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDesignTable::GetVisibleRowCount()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDesignTable::GetVisibleColumnCount()
{
	long result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IDesignTable::SetEntryText(long row, long col, LPCTSTR textIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 row, col, textIn);
}

BOOL IDesignTable::AddRow(const VARIANT& cellValues)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&cellValues);
	return result;
}

BOOL IDesignTable::UpdateModel()
{
	BOOL result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IRefPlane properties

/////////////////////////////////////////////////////////////////////////////
// IRefPlane operations

VARIANT IRefPlane::GetRefPlaneParams()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IRefPlane::IGetRefPlaneParams()
{
	double result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDatumTag properties

/////////////////////////////////////////////////////////////////////////////
// IDatumTag operations

LPDISPATCH IDatumTag::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDatumTag::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IDatumTag::GetTextCount()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IDatumTag::GetTextAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

double IDatumTag::GetTextHeightAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDatumTag::GetTextPositionAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDatumTag::IGetTextPositionAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

double IDatumTag::GetTextAngleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDatumTag::GetTextRefPositionAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IDatumTag::GetTextInvertAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IDatumTag::GetLineCount()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDatumTag::GetLineAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDatumTag::IGetLineAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDatumTag::GetArcCount()
{
	long result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDatumTag::GetArcAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDatumTag::IGetArcAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDatumTag::GetArrowHeadCount()
{
	long result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDatumTag::GetArrowHeadAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDatumTag::IGetArrowHeadAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDatumTag::GetTriangleCount()
{
	long result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDatumTag::GetTriangleAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDatumTag::IGetTriangleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH IDatumTag::GetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDatumTag::IGetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IDatumTag::GetLabel()
{
	CString result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IDatumTag::SetLabel(LPCTSTR label)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		label);
	return result;
}

BOOL IDatumTag::GetFilledTriangle()
{
	BOOL result;
	InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDatumTag::SetFilledTriangle(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDatumTag::GetShoulder()
{
	BOOL result;
	InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDatumTag::SetShoulder(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// IGtol properties

/////////////////////////////////////////////////////////////////////////////
// IGtol operations

void IGtol::SetFrameValues(short frameNumber, LPCTSTR tol1, LPCTSTR tol2, LPCTSTR datum1, LPCTSTR datum2, LPCTSTR datum3)
{
	static BYTE parms[] =
		VTS_I2 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 frameNumber, tol1, tol2, datum1, datum2, datum3);
}

void IGtol::SetFrameSymbols(short frameNumber, short GCS, BOOL tolDia1, short tolMC1, BOOL tolDia2, short tolMC2, short datumMC1, short datumMC2, short datumMC3)
{
	static BYTE parms[] =
		VTS_I2 VTS_I2 VTS_BOOL VTS_I2 VTS_BOOL VTS_I2 VTS_I2 VTS_I2 VTS_I2;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 frameNumber, GCS, tolDia1, tolMC1, tolDia2, tolMC2, datumMC1, datumMC2, datumMC3);
}

void IGtol::SetPTZHeight(LPCTSTR ptzHt, BOOL displayIn)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ptzHt, displayIn);
}

void IGtol::SetDatumIdentifier(LPCTSTR datumIdentifier)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 datumIdentifier);
}

LPDISPATCH IGtol::GetNextGTOL()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IGtol::IGetNextGTOL()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetFrameValues(short frameNumber)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		frameNumber);
	return result;
}

VARIANT IGtol::GetFrameSymbols(short frameNumber)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		frameNumber);
	return result;
}

CString IGtol::GetPTZHeight()
{
	CString result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

CString IGtol::GetDatumIdentifier()
{
	CString result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

double IGtol::GetHeight()
{
	double result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetAttachPos()
{
	VARIANT result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetFontInfo()
{
	VARIANT result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetLeaderInfo()
{
	VARIANT result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

BOOL IGtol::IsAttached()
{
	BOOL result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IGtol::HasExtraLeader()
{
	BOOL result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetTextPoint()
{
	VARIANT result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetArrowHeadInfo()
{
	VARIANT result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IGtol::IGetAttachPos()
{
	double result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IGtol::IGetFontInfo()
{
	double result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IGtol::IGetLeaderInfo(long* pointCount)
{
	double result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		pointCount);
	return result;
}

double IGtol::IGetTextPoint()
{
	double result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IGtol::IGetArrowHeadInfo()
{
	double result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

CString IGtol::GetSymName(short symIdx)
{
	CString result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		symIdx);
	return result;
}

CString IGtol::GetSymDesc(short symIdx)
{
	CString result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		symIdx);
	return result;
}

VARIANT IGtol::GetSymText(short symIdx)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symIdx);
	return result;
}

VARIANT IGtol::GetSymEdgeCounts(short symIdx)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symIdx);
	return result;
}

VARIANT IGtol::GetSymLines(short symIdx)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symIdx);
	return result;
}

VARIANT IGtol::GetSymArcs(short symIdx)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symIdx);
	return result;
}

VARIANT IGtol::GetSymCircles(short symIdx)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symIdx);
	return result;
}

VARIANT IGtol::GetSymTextPoints(short symIdx)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symIdx);
	return result;
}

short IGtol::IGetSymEdgeCounts(short symIdx)
{
	short result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		symIdx);
	return result;
}

double IGtol::IGetSymLines(short symIdx)
{
	double result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		symIdx);
	return result;
}

double IGtol::IGetSymArcs(short symIdx)
{
	double result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		symIdx);
	return result;
}

double IGtol::IGetSymCircles(short symIdx)
{
	double result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		symIdx);
	return result;
}

double IGtol::IGetSymTextPoints(short symIdx)
{
	double result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		symIdx);
	return result;
}

long IGtol::GetTextCount()
{
	long result;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IGtol::GetTextAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

double IGtol::GetTextHeightAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT IGtol::GetTextPositionAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IGtol::IGetTextPositionAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

double IGtol::GetTextAngleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IGtol::GetTextRefPositionAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IGtol::GetTextInvertAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IGtol::GetLineCount()
{
	long result;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetLineAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IGtol::IGetLineAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IGtol::GetArcCount()
{
	long result;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetArcAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IGtol::IGetArcAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IGtol::GetArrowHeadCount()
{
	long result;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetArrowHeadAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IGtol::IGetArrowHeadAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IGtol::GetTriangleCount()
{
	long result;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetTriangleAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IGtol::IGetTriangleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

CString IGtol::IGetFrameValues(short frameNumber)
{
	CString result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		frameNumber);
	return result;
}

short IGtol::IGetFrameSymbols(short frameNumber)
{
	short result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		frameNumber);
	return result;
}

CString IGtol::IGetSymText(short symIdx)
{
	CString result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		symIdx);
	return result;
}

void IGtol::SetFrameSymbols2(short frameNumber, LPCTSTR GCS, BOOL tolDia1, LPCTSTR tolMC1, BOOL tolDia2, LPCTSTR tolMC2, LPCTSTR datumMC1, LPCTSTR datumMC2, LPCTSTR datumMC3)
{
	static BYTE parms[] =
		VTS_I2 VTS_BSTR VTS_BOOL VTS_BSTR VTS_BOOL VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 frameNumber, GCS, tolDia1, tolMC1, tolDia2, tolMC2, datumMC1, datumMC2, datumMC3);
}

CString IGtol::GetTextFont()
{
	CString result;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IGtol::GetLeaderCount()
{
	long result;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetLeaderAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IGtol::IGetLeaderAtIndex(long index, long* pointCount)
{
	double result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x43, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index, pointCount);
	return result;
}

void IGtol::SetPosition(double x, double y, double z)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x, y, z);
}

BOOL IGtol::GetUseDocTextFormat()
{
	BOOL result;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IGtol::GetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x46, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IGtol::IGetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x47, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IGtol::SetTextFormat(long textFormatType, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x48, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		textFormatType, textFormat);
	return result;
}

BOOL IGtol::ISetTextFormat(long textFormatType, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x49, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		textFormatType, textFormat);
	return result;
}

BOOL IGtol::GetCompositeFrame()
{
	BOOL result;
	InvokeHelper(0x4a, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IGtol::SetCompositeFrame(BOOL composite)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x4b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 composite);
}

BOOL IGtol::GetBetweenTwoPoints()
{
	BOOL result;
	InvokeHelper(0x4c, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString IGtol::GetBetweenTwoPointsText(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4d, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

void IGtol::SetBetweenTwoPoints(BOOL between, LPCTSTR textFrom, LPCTSTR textTo)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BSTR VTS_BSTR;
	InvokeHelper(0x4e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 between, textFrom, textTo);
}

BOOL IGtol::GetAllAround()
{
	BOOL result;
	InvokeHelper(0x4f, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IGtol::GetLeaderSide()
{
	long result;
	InvokeHelper(0x50, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IGtol::SetLeader(BOOL leader, long leaderSide, BOOL bentLeader, BOOL allAround)
{
	static BYTE parms[] =
		VTS_BOOL VTS_I4 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x51, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 leader, leaderSide, bentLeader, allAround);
}

LPDISPATCH IGtol::GetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x52, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IGtol::IGetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x53, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IGtol::GetFrameSymbols2(short frameNumber)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x54, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		frameNumber);
	return result;
}

CString IGtol::IGetFrameSymbols2(short frameNumber)
{
	CString result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x55, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		frameNumber);
	return result;
}

VARIANT IGtol::GetFrameDiameterSymbols(short frameNumber)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x56, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		frameNumber);
	return result;
}

BOOL IGtol::IGetFrameDiameterSymbols(short frameNumber)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x57, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		frameNumber);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// INote properties

/////////////////////////////////////////////////////////////////////////////
// INote operations

LPDISPATCH INote::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH INote::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString INote::GetText()
{
	CString result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

double INote::GetHeight()
{
	double result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetAttachPos()
{
	VARIANT result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetFontInfo()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetLeaderInfo()
{
	VARIANT result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

BOOL INote::IsAttached()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL INote::HasExtraLeader()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL INote::HasBalloon()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetBalloonInfo()
{
	VARIANT result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetTextPoint()
{
	VARIANT result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetArrowHeadInfo()
{
	VARIANT result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double INote::IGetAttachPos()
{
	double result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double INote::IGetFontInfo()
{
	double result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double INote::IGetLeaderInfo(long* pointCount)
{
	double result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		pointCount);
	return result;
}

double INote::IGetBalloonInfo()
{
	double result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double INote::IGetTextPoint()
{
	double result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double INote::IGetArrowHeadInfo()
{
	double result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetUpperRight()
{
	VARIANT result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double INote::IGetUpperRight()
{
	double result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetExtent()
{
	VARIANT result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double INote::IGetExtent()
{
	double result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL INote::IsCompoundNote()
{
	BOOL result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL INote::AddText(LPCTSTR txt, double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		txt, x, y, z);
	return result;
}

long INote::GetTextCount()
{
	long result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString INote::GetTextAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

double INote::GetHeightAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT INote::GetTextOffsetAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double INote::IGetTextOffsetAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

void INote::BeginSketchEdit()
{
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void INote::EndSketchEdit()
{
	InvokeHelper(0x20, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT INote::GetExtentAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double INote::IGetExtentAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

void INote::SetTextOffsetAtIndex(long index, double x, double y, double z)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 index, x, y, z);
}

LPDISPATCH INote::GetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH INote::IGetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL INote::SetTextAtIndex(long index, LPCTSTR txt)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		index, txt);
	return result;
}

BOOL INote::SetText(LPCTSTR txt)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		txt);
	return result;
}

BOOL INote::SetZeroLengthLeader(BOOL flag)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		flag);
	return result;
}

void INote::SetHeight(double heightIn)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 heightIn);
}

long INote::GetHeightInPoints()
{
	long result;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void INote::SetHeightInPoints(long heightIn)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 heightIn);
}

CString INote::GetName()
{
	CString result;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL INote::SetName(LPCTSTR nameIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nameIn);
	return result;
}

double INote::GetTextHeightAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT INote::GetTextPositionAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double INote::IGetTextPositionAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

double INote::GetTextAngleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long INote::GetTextRefPositionAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long INote::GetTextInvertAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long INote::GetLineCount()
{
	long result;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetLineAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double INote::IGetLineAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long INote::GetArcCount()
{
	long result;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetArcAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double INote::IGetArcAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long INote::GetArrowHeadCount()
{
	long result;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetArrowHeadAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double INote::IGetArrowHeadAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long INote::GetTriangleCount()
{
	long result;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetTriangleAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double INote::IGetTriangleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

CString INote::GetTextFontAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

void INote::SetTextPoint(double x, double y, double z)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x, y, z);
}

double INote::GetTextLineSpacingAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long INote::GetLeaderCount()
{
	long result;
	InvokeHelper(0x43, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetLeaderAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double INote::IGetLeaderAtIndex(long index, long* pointCount)
{
	double result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index, pointCount);
	return result;
}

LPDISPATCH INote::GetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x46, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH INote::IGetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x47, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL INote::SetTextFormat(long textFormatType, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x48, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		textFormatType, textFormat);
	return result;
}

BOOL INote::ISetTextFormat(long textFormatType, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x49, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		textFormatType, textFormat);
	return result;
}

LPDISPATCH INote::GetTextFormatAtIndex(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH INote::IGetTextFormatAtIndex(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

void INote::SetTextFormatAtIndex(long index, LPDISPATCH textFormat)
{
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x4c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 index, textFormat);
}

void INote::ISetTextFormatAtIndex(long index, LPDISPATCH textFormat)
{
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x4d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 index, textFormat);
}

long INote::GetTextJustification()
{
	long result;
	InvokeHelper(0x4e, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void INote::SetTextJustification(long justification)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 justification);
}

long INote::GetTextJustificationAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x50, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

void INote::SetTextJustificationAtIndex(long index, long justification)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x51, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 index, justification);
}

BOOL INote::GetUseDocTextFormat()
{
	BOOL result;
	InvokeHelper(0x52, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString INote::GetHyperlinkText()
{
	CString result;
	InvokeHelper(0x53, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL INote::SetHyperlinkText(LPCTSTR text)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x54, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		text);
	return result;
}

LPDISPATCH INote::GetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x55, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH INote::IGetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x56, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

double INote::GetAngle()
{
	double result;
	InvokeHelper(0x57, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void INote::SetAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x57, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long INote::GetBalloonStyle()
{
	long result;
	InvokeHelper(0x58, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long INote::GetBalloonSize()
{
	long result;
	InvokeHelper(0x59, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL INote::SetBalloon(long Style, long size)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x5a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Style, size);
	return result;
}

long INote::IGetTextTokenCount()
{
	long result;
	InvokeHelper(0x5b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT INote::GetTextTokens(VARIANT* positions, VARIANT* widths, VARIANT* heights)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT;
	InvokeHelper(0x5c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		positions, widths, heights);
	return result;
}

CString INote::IGetTextTokens(double* positions, double* widths, double* heights)
{
	CString result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x5d, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		positions, widths, heights);
	return result;
}

BOOL INote::IsBomBalloon()
{
	BOOL result;
	InvokeHelper(0x5e, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long INote::GetBomBalloonTextStyle(BOOL whichOne)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x5f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		whichOne);
	return result;
}

CString INote::GetBomBalloonText(BOOL whichOne)
{
	CString result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x60, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		whichOne);
	return result;
}

BOOL INote::SetBomBalloonText(long upperTextStyle, LPCTSTR upperText, long lowerTextStyle, LPCTSTR lowerText)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x61, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		upperTextStyle, upperText, lowerTextStyle, lowerText);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IPropertyManagerPage properties

/////////////////////////////////////////////////////////////////////////////
// IPropertyManagerPage operations

long IPropertyManagerPage::SetButtons(long ButtonTypes)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		ButtonTypes);
	return result;
}

long IPropertyManagerPage::SetGroupRange(long FirstGroupId, long FirstCheckId, long GroupCount)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		FirstGroupId, FirstCheckId, GroupCount);
	return result;
}

long IPropertyManagerPage::Show()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IPropertyManagerPage::GetDialogWindow(long* status)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		status);
	return result;
}

BOOL IPropertyManagerPage::GetGroupVisible(long GroupId, long* status)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		GroupId, status);
	return result;
}

long IPropertyManagerPage::SetGroupVisible(long GroupId, BOOL Visible)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		GroupId, Visible);
	return result;
}

BOOL IPropertyManagerPage::GetGroupExpanded(long GroupId, long* status)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		GroupId, status);
	return result;
}

long IPropertyManagerPage::SetGroupExpanded(long GroupId, BOOL Expanded)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		GroupId, Expanded);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IUserUnit properties

/////////////////////////////////////////////////////////////////////////////
// IUserUnit operations

long IUserUnit::GetFractionBase()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IUserUnit::SetFractionBase(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IUserUnit::GetFractionValue()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IUserUnit::SetFractionValue(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IUserUnit::GetRoundToFraction()
{
	BOOL result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IUserUnit::SetRoundToFraction(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

long IUserUnit::GetSignificantDigits()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IUserUnit::SetSignificantDigits(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IUserUnit::GetDisplayLeadingZero()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IUserUnit::SetDisplayLeadingZero(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IUserUnit::GetPadZero()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IUserUnit::SetPadZero(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

CString IUserUnit::GetSeparatorCharacter()
{
	CString result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IUserUnit::SetSeparatorCharacter(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

double IUserUnit::GetConversionFactor()
{
	double result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IUserUnit::IsMetric()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString IUserUnit::GetFullUnitName(BOOL plural)
{
	CString result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		plural);
	return result;
}

CString IUserUnit::ConvertToUserUnit(double valueIn, BOOL showUsernames, BOOL nameInEnglish)
{
	CString result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		valueIn, showUsernames, nameInEnglish);
	return result;
}

BOOL IUserUnit::ConvertToSystemValue(LPCTSTR unitText, double* computedValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_PR8;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		unitText, computedValue);
	return result;
}

double IUserUnit::ConvertDoubleToSystemValue(double userValue)
{
	double result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		userValue);
	return result;
}

CString IUserUnit::GetUnitsString(BOOL inEnglish)
{
	CString result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		inEnglish);
	return result;
}

long IUserUnit::GetUnitType()
{
	long result;
	InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

long IUserUnit::GetSpecificUnitType()
{
	long result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IUserUnit::SetSpecificUnitType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

double IUserUnit::GetUserAngleTolerance()
{
	double result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IWeldSymbol properties

/////////////////////////////////////////////////////////////////////////////
// IWeldSymbol operations

LPDISPATCH IWeldSymbol::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IWeldSymbol::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IWeldSymbol::GetTextCount()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IWeldSymbol::GetTextAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

double IWeldSymbol::GetTextHeightAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT IWeldSymbol::GetTextPositionAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IWeldSymbol::IGetTextPositionAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

double IWeldSymbol::GetTextAngleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IWeldSymbol::GetTextRefPositionAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IWeldSymbol::GetTextInvertAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IWeldSymbol::GetLineCount()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IWeldSymbol::GetLineAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IWeldSymbol::IGetLineAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IWeldSymbol::GetArcCount()
{
	long result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IWeldSymbol::GetArcAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IWeldSymbol::IGetArcAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IWeldSymbol::GetArrowHeadCount()
{
	long result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IWeldSymbol::GetArrowHeadAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IWeldSymbol::IGetArrowHeadAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IWeldSymbol::GetTriangleCount()
{
	long result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IWeldSymbol::GetTriangleAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IWeldSymbol::IGetTriangleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

BOOL IWeldSymbol::IsAttached()
{
	BOOL result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IWeldSymbol::HasExtraLeader()
{
	BOOL result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IWeldSymbol::GetLeaderCount()
{
	long result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IWeldSymbol::GetLeaderAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IWeldSymbol::IGetLeaderAtIndex(long index, long* pointCount)
{
	double result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index, pointCount);
	return result;
}

double IWeldSymbol::IGetArrowHeadInfo()
{
	double result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IWeldSymbol::GetArrowHeadInfo()
{
	VARIANT result;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH IWeldSymbol::GetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IWeldSymbol::IGetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IWeldSymbol::GetText(long whichText)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		whichText);
	return result;
}

long IWeldSymbol::GetContour(BOOL top)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		top);
	return result;
}

long IWeldSymbol::GetSymmetric()
{
	long result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IWeldSymbol::GetPeripheral()
{
	BOOL result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IWeldSymbol::GetFieldWeld()
{
	long result;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IWeldSymbol::GetProcess()
{
	BOOL result;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IWeldSymbol::GetProcessReference()
{
	BOOL result;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IWeldSymbol::GetStagger()
{
	BOOL result;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IWeldSymbol::SetText(BOOL top, LPCTSTR left, LPCTSTR symbol, LPCTSTR right, LPCTSTR stagger, long contour)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		top, left, symbol, right, stagger, contour);
	return result;
}

BOOL IWeldSymbol::SetSymmetric(long symmetric)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		symmetric);
	return result;
}

BOOL IWeldSymbol::SetPeripheral(BOOL peripheral)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		peripheral);
	return result;
}

BOOL IWeldSymbol::SetFieldWeld(long fieldWeld)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fieldWeld);
	return result;
}

BOOL IWeldSymbol::SetProcess(BOOL process, LPCTSTR text, BOOL reference)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_BSTR VTS_BOOL;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		process, text, reference);
	return result;
}

BOOL IWeldSymbol::SetStagger(BOOL stagger)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		stagger);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IFrame properties

/////////////////////////////////////////////////////////////////////////////
// IFrame operations

BOOL IFrame::AddMenuItem(LPCTSTR Menu, LPCTSTR Item, long position, LPCTSTR CallbackFcnAndModule)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Menu, Item, position, CallbackFcnAndModule);
	return result;
}

BOOL IFrame::AddMenu(LPCTSTR Menu, long position)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Menu, position);
	return result;
}

long IFrame::GetMenuState(LPCTSTR MenuItemString)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		MenuItemString);
	return result;
}

void IFrame::RenameMenu(LPCTSTR MenuItemString, LPCTSTR newName)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 MenuItemString, newName);
}

void IFrame::RemoveMenu(LPCTSTR MenuItemString)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 MenuItemString);
}

BOOL IFrame::AddMenuPopupItem(long DocType, long SelectType, LPCTSTR Item, LPCTSTR CallbackFcnAndModule, LPCTSTR CustomNames, long Unused)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		DocType, SelectType, Item, CallbackFcnAndModule, CustomNames, Unused);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IAssemblyDoc properties

/////////////////////////////////////////////////////////////////////////////
// IAssemblyDoc operations

BOOL IAssemblyDoc::AddComponent(LPCTSTR compName, double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		compName, x, y, z);
	return result;
}

void IAssemblyDoc::FeatureExtrusion()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::EditRebuild()
{
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::ToolsCheckInterference()
{
	InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IAssemblyDoc::GetFirstMember()
{
	LPDISPATCH result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAssemblyDoc::GetSelectedMember()
{
	LPDISPATCH result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAssemblyDoc::IGetFirstMember()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAssemblyDoc::IGetSelectedMember()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IAssemblyDoc::ViewCollapseAssembly()
{
	InvokeHelper(0xf, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::ViewExplodeAssembly()
{
	InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::TranslateComponent()
{
	InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::InsertNewPart()
{
	InvokeHelper(0x12, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::RotateComponent()
{
	InvokeHelper(0x14, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::FileDeriveComponentPart()
{
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::InsertCavity()
{
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::HideComponent()
{
	InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::ShowComponent()
{
	InvokeHelper(0x18, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::FixComponent()
{
	InvokeHelper(0x19, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::UnfixComponent()
{
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::EditAssembly()
{
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::EditPart()
{
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::OpenCompFile()
{
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::UpdateFeatureScope()
{
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::AddMate(long mateType, long align, BOOL flip, double dist, double angle)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BOOL VTS_R8 VTS_R8;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 mateType, align, flip, dist, angle);
}

void IAssemblyDoc::CompConfigProperties(BOOL m_suppressed, BOOL m_show_component, BOOL m_fdetail)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 m_suppressed, m_show_component, m_fdetail);
}

BOOL IAssemblyDoc::AddToFeatureScope(LPCTSTR compName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		compName);
	return result;
}

BOOL IAssemblyDoc::RemoveFromFeatureScope(LPCTSTR compName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		compName);
	return result;
}

void IAssemblyDoc::EditExplodeParameters()
{
	InvokeHelper(0x23, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::RotateComponentAxis()
{
	InvokeHelper(0x24, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::ViewFeatureManagerByFeatures()
{
	InvokeHelper(0x25, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::ComponentReload()
{
	InvokeHelper(0x26, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::ViewFeatureManagerByDependencies()
{
	InvokeHelper(0x27, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::AssemblyPartToggle()
{
	InvokeHelper(0x28, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IAssemblyDoc::FeatureByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IAssemblyDoc::IFeatureByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

void IAssemblyDoc::InsertJoin()
{
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::UpdateBox()
{
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IAssemblyDoc::InsertWeld(LPCTSTR type, LPCTSTR shape, double topDelta, double bottomDelta, double radius, LPCTSTR part)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_BSTR;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 type, shape, topDelta, bottomDelta, radius, part);
}

void IAssemblyDoc::ForceRebuild()
{
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IAssemblyDoc::GetEditTarget()
{
	LPDISPATCH result;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IAssemblyDoc::IGetEditTarget()
{
	LPDISPATCH result;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IAssemblyDoc::InsertCavity2(double scaleFactor, long scaleType)
{
	static BYTE parms[] =
		VTS_R8 VTS_I4;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 scaleFactor, scaleType);
}

BOOL IAssemblyDoc::AutoExplode()
{
	BOOL result;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IAssemblyDoc::ShowExploded(BOOL showIt)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		showIt);
	return result;
}

LPDISPATCH IAssemblyDoc::AddComponent2(LPCTSTR compName, double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		compName, x, y, z);
	return result;
}

LPDISPATCH IAssemblyDoc::IAddComponent2(LPCTSTR compName, double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		compName, x, y, z);
	return result;
}

void IAssemblyDoc::EditMate(long mateType, long align, BOOL flip, double dist, double angle)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BOOL VTS_R8 VTS_R8;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 mateType, align, flip, dist, angle);
}

BOOL IAssemblyDoc::InsertDerivedPattern()
{
	BOOL result;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IAssemblyDoc::ResolveAllLightWeightComponents(BOOL warnUser)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		warnUser);
	return result;
}

long IAssemblyDoc::GetLightWeightComponentCount()
{
	long result;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IAssemblyDoc::InsertCavity3(double scaleFactor, long scaleType, long keepPieceIndex)
{
	static BYTE parms[] =
		VTS_R8 VTS_I4 VTS_I4;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 scaleFactor, scaleType, keepPieceIndex);
}

long IAssemblyDoc::ComponentReload2(LPDISPATCH component, BOOL ReadOnly, long options)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BOOL VTS_I4;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		component, ReadOnly, options);
	return result;
}

long IAssemblyDoc::IComponentReload2(LPDISPATCH component, BOOL ReadOnly, long options)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BOOL VTS_I4;
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		component, ReadOnly, options);
	return result;
}

BOOL IAssemblyDoc::CompConfigProperties2(long suppression, BOOL visibility, BOOL featureDetails)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		suppression, visibility, featureDetails);
	return result;
}

long IAssemblyDoc::AddPipePenetration()
{
	long result;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IAssemblyDoc::AddPipingFitting(LPCTSTR pathname, LPCTSTR configName, short alignmentIndex)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I2;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		pathname, configName, alignmentIndex);
	return result;
}

BOOL IAssemblyDoc::IsComponentTreeValid()
{
	BOOL result;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IAssemblyDoc::ForceRebuild2(BOOL topOnly)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 topOnly);
}

BOOL IAssemblyDoc::SetDroppedFileConfigName(LPCTSTR configName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		configName);
	return result;
}

VARIANT IAssemblyDoc::AddComponents(const VARIANT& names, const VARIANT& transforms)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x43, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		&names, &transforms);
	return result;
}

LPDISPATCH IAssemblyDoc::IAddComponents(long* count, BSTR* names, double* transforms)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4 VTS_PBSTR VTS_PR8;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		count, names, transforms);
	return result;
}

long IAssemblyDoc::EditPart2(BOOL silent, BOOL allowReadOnly, long* information)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_PI4;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		silent, allowReadOnly, information);
	return result;
}

void IAssemblyDoc::InsertCavity4(double scaleFactor_x, double scaleFactor_y, double scaleFactor_z, BOOL isUniform, long scaleType, long keepPieceIndex)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_I4 VTS_I4;
	InvokeHelper(0x46, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 scaleFactor_x, scaleFactor_y, scaleFactor_z, isUniform, scaleType, keepPieceIndex);
}

void IAssemblyDoc::ToolsCheckInterference2(long numComponents, const VARIANT& lpComponents, BOOL coincidentInterference, VARIANT* pComp, VARIANT* pFace)
{
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT VTS_BOOL VTS_PVARIANT VTS_PVARIANT;
	InvokeHelper(0x47, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 numComponents, &lpComponents, coincidentInterference, pComp, pFace);
}

void IAssemblyDoc::IToolsCheckInterference2(long numComponents, LPDISPATCH* lpComponents, BOOL coincidentInterference, VARIANT* pComp, VARIANT* pFace)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH VTS_BOOL VTS_PVARIANT VTS_PVARIANT;
	InvokeHelper(0x48, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 numComponents, lpComponents, coincidentInterference, pComp, pFace);
}

LPDISPATCH IAssemblyDoc::GetDroppedAtEntity()
{
	LPDISPATCH result;
	InvokeHelper(0x49, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IAssemblyDoc::CompConfigProperties3(long suppression, long Solving, BOOL visibility, BOOL featureDetails)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x4a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		suppression, Solving, visibility, featureDetails);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IMember properties

/////////////////////////////////////////////////////////////////////////////
// IMember operations


/////////////////////////////////////////////////////////////////////////////
// IDrawingDoc properties

/////////////////////////////////////////////////////////////////////////////
// IDrawingDoc operations

void IDrawingDoc::NewNote(LPCTSTR text, double height)
{
	static BYTE parms[] =
		VTS_BSTR VTS_R8;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 text, height);
}

void IDrawingDoc::NewSheet(LPCTSTR Name, short paperSize, short templateIn, double scale1, double Scale2)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I2 VTS_I2 VTS_R8 VTS_R8;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Name, paperSize, templateIn, scale1, Scale2);
}

void IDrawingDoc::SetupSheet(LPCTSTR Name, short paperSize, short templateIn, double scale1, double Scale2)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I2 VTS_I2 VTS_R8 VTS_R8;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Name, paperSize, templateIn, scale1, Scale2);
}

LPDISPATCH IDrawingDoc::NewGtol()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrawingDoc::INewGtol()
{
	LPDISPATCH result;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrawingDoc::EditSelectedGtol()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrawingDoc::IEditSelectedGtol()
{
	LPDISPATCH result;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IDrawingDoc::CreateLinearDim(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&p0, &P1, &P2, &P3, &p4, angle, arrowSize, text, textHeight, witnessGap, witnessOvershoot);
	return result;
}

BOOL IDrawingDoc::CreateAngDim(const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, const VARIANT& vP4, const VARIANT& vP5, const VARIANT& vP6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, 
		double witnessOvershoot)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&vP0, &vP1, &vP2, &vP3, &vP4, &vP5, &vP6, arrowSize, text, textHeight, witnessGap, witnessOvershoot);
	return result;
}

BOOL IDrawingDoc::CreateDiamDim(double dimVal, const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		dimVal, &vP0, &vP1, &vP2, &vP3, arrowSize, text, textHeight, witnessGap, witnessOvershoot);
	return result;
}

CString IDrawingDoc::CreateViewport(double LowerLeftX, double LowerLeftY, double UpperRightX, double UpperRightY, short sketchSize)
{
	CString result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I2;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LowerLeftX, LowerLeftY, UpperRightX, UpperRightY, sketchSize);
	return result;
}

BOOL IDrawingDoc::ActivateView(LPCTSTR viewname)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		viewname);
	return result;
}

BOOL IDrawingDoc::Create1stAngleViews(LPCTSTR ModelName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ModelName);
	return result;
}

BOOL IDrawingDoc::Create3rdAngleViews(LPCTSTR ModelName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ModelName);
	return result;
}

BOOL IDrawingDoc::CreateDrawViewFromModelView(LPCTSTR ModelName, LPCTSTR viewname, double locX, double locY, double locZ)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ModelName, viewname, locX, locY, locZ);
	return result;
}

BOOL IDrawingDoc::CreateUnfoldedViewAt(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

BOOL IDrawingDoc::CreateText(LPCTSTR textString, double textX, double textY, double textZ, double textHeight, double textAngle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		textString, textX, textY, textZ, textHeight, textAngle);
	return result;
}

void IDrawingDoc::EditRebuild()
{
	InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::ViewFullPage()
{
	InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::CreateSectionView()
{
	InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::SheetNext()
{
	InvokeHelper(0x14, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::Dimensions()
{
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::InsertGroup()
{
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::SheetPrevious()
{
	InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::AlignVert()
{
	InvokeHelper(0x18, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::AlignHorz()
{
	InvokeHelper(0x19, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::InsertRefDim()
{
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::MakeSectionLine()
{
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::InsertBaseDim()
{
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::EditSketch()
{
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::CreateDetailView()
{
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::CreateAuxiliaryView()
{
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::StartDrawing()
{
	InvokeHelper(0x20, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::EndDrawing()
{
	InvokeHelper(0x21, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IDrawingDoc::GetFirstView()
{
	LPDISPATCH result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrawingDoc::IGetFirstView()
{
	LPDISPATCH result;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IDrawingDoc::GetInsertionPoint()
{
	VARIANT result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IDrawingDoc::AttachDimensions()
{
	InvokeHelper(0x24, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::InsertModelDimensions(long option)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 option);
}

void IDrawingDoc::EditTemplate()
{
	InvokeHelper(0x29, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::InsertOrdinate()
{
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::UnsuppressView()
{
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::HideShowDrawingViews()
{
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::SuppressView()
{
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::AlignOrdinate()
{
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::SketchDim()
{
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::CenterMark()
{
	InvokeHelper(0x30, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::EditSheet()
{
	InvokeHelper(0x31, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::InsertHorizontalOrdinate()
{
	InvokeHelper(0x32, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::EditOrdinate()
{
	InvokeHelper(0x33, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::InsertVerticalOrdinate()
{
	InvokeHelper(0x34, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::ChangeOrdDir()
{
	InvokeHelper(0x35, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IDrawingDoc::GetLineFontCount()
{
	long result;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IDrawingDoc::GetLineFontName(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDrawingDoc::GetLineFontInfo(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

void IDrawingDoc::ICreateLinearDim(double* p0, double* P1, double* P2, double* P3, double* p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 p0, P1, P2, P3, p4, angle, arrowSize, text, textHeight, witnessGap, witnessOvershoot);
}

void IDrawingDoc::ICreateAngDim(double* p0, double* P1, double* P2, double* P3, double* p4, double* P5, double* P6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 p0, P1, P2, P3, p4, P5, P6, arrowSize, text, textHeight, witnessGap, witnessOvershoot);
}

void IDrawingDoc::ICreateDiamDim(double DimValue, double* p0, double* P1, double* P2, double* P3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot)
{
	static BYTE parms[] =
		VTS_R8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 DimValue, p0, P1, P2, P3, arrowSize, text, textHeight, witnessGap, witnessOvershoot);
}

double IDrawingDoc::IGetInsertionPoint()
{
	double result;
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrawingDoc::CreateCompoundNote(double height, double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		height, x, y, z);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateCompoundNote(double height, double x, double y, double z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		height, x, y, z);
	return result;
}

BOOL IDrawingDoc::CreateOrdinateDim(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&p0, &P1, &P2, &P3, &p4, angle, arrowSize, text, textHeight, witnessGap, witnessOvershoot);
	return result;
}

void IDrawingDoc::ICreateOrdinateDim(double* p0, double* P1, double* P2, double* P3, double* p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 p0, P1, P2, P3, p4, angle, arrowSize, text, textHeight, witnessGap, witnessOvershoot);
}

void IDrawingDoc::InsertNewNote(LPCTSTR text, BOOL noLeader, BOOL balloonNote, BOOL bentLeader, short arrowStyle, short leaderSide)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_I2 VTS_I2;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 text, noLeader, balloonNote, bentLeader, arrowStyle, leaderSide);
}

BOOL IDrawingDoc::AddCenterMark(double cmSize, BOOL cmShowLines)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_BOOL;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		cmSize, cmShowLines);
	return result;
}

void IDrawingDoc::InsertWeldSymbol(LPCTSTR dim1, LPCTSTR symbol, LPCTSTR dim2, BOOL symmetric, BOOL fieldWeld, BOOL showOtherSide, BOOL dashOnTop, BOOL peripheral, BOOL hasProcess, LPCTSTR processValue)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BSTR;
	InvokeHelper(0x43, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 dim1, symbol, dim2, symmetric, fieldWeld, showOtherSide, dashOnTop, peripheral, hasProcess, processValue);
}

BOOL IDrawingDoc::InsertSurfaceFinishSymbol(long symType, long leaderType, double locX, double locY, double locZ, long laySymbol, long arrowType, LPCTSTR machAllowance, LPCTSTR otherVals, LPCTSTR prodMethod, LPCTSTR sampleLen, LPCTSTR maxRoughness, 
		LPCTSTR minRoughness, LPCTSTR roughnessSpacing)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		symType, leaderType, locX, locY, locZ, laySymbol, arrowType, machAllowance, otherVals, prodMethod, sampleLen, maxRoughness, minRoughness, roughnessSpacing);
	return result;
}

BOOL IDrawingDoc::ModifySurfaceFinishSymbol(long symType, long leaderType, double locX, double locY, double locZ, long laySymbol, long arrowType, LPCTSTR machAllowance, LPCTSTR otherVals, LPCTSTR prodMethod, LPCTSTR sampleLen, LPCTSTR maxRoughness, 
		LPCTSTR minRoughness, LPCTSTR roughnessSpacing)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		symType, leaderType, locX, locY, locZ, laySymbol, arrowType, machAllowance, otherVals, prodMethod, sampleLen, maxRoughness, minRoughness, roughnessSpacing);
	return result;
}

LPDISPATCH IDrawingDoc::GetCurrentSheet()
{
	LPDISPATCH result;
	InvokeHelper(0x46, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrawingDoc::IGetCurrentSheet()
{
	LPDISPATCH result;
	InvokeHelper(0x47, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IDrawingDoc::CreateConstructionGeometry()
{
	InvokeHelper(0x48, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::ViewDisplayHidden()
{
	InvokeHelper(0x49, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::InsertBreakHorizontal()
{
	InvokeHelper(0x4a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::ViewDisplayWireframe()
{
	InvokeHelper(0x4b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::BreakLineZigZagCut()
{
	InvokeHelper(0x4c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::BreakView()
{
	InvokeHelper(0x4d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::ViewDisplayHiddengreyed()
{
	InvokeHelper(0x4e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::ViewTangentEdges()
{
	InvokeHelper(0x4f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::BreakLineSplineCut()
{
	InvokeHelper(0x50, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::InsertBreakVertical()
{
	InvokeHelper(0x51, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::UnBreakView()
{
	InvokeHelper(0x52, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::BreakLineStraightCut()
{
	InvokeHelper(0x53, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::InsertDatumTag()
{
	InvokeHelper(0x54, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::ToggleGrid()
{
	InvokeHelper(0x55, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::FlipSectionLine()
{
	InvokeHelper(0x56, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IDrawingDoc::FeatureByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x57, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IDrawingDoc::IFeatureByName(LPCTSTR Name)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x58, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Name);
	return result;
}

BOOL IDrawingDoc::NewSheet2(LPCTSTR Name, long paperSize, long templateIn, double scale1, double Scale2, BOOL firstAngle, LPCTSTR templateName, double Width, double height)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BSTR VTS_R8 VTS_R8;
	InvokeHelper(0x59, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, paperSize, templateIn, scale1, Scale2, firstAngle, templateName, Width, height);
	return result;
}

BOOL IDrawingDoc::CreateLinearDim2(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double val, long primPrec, LPCTSTR text, const VARIANT& TextPoint, double angle, double textHeight, LPCTSTR prefix, 
		LPCTSTR suffix, LPCTSTR callout1, LPCTSTR callout2, long tolType, LPCTSTR tolMin, LPCTSTR tolMax, long tolPrec, double arrowSize, long arrowStyle, long arrowDir, double witnessGap, double witnessOvershoot, BOOL dualDisplay, long dualPrec)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_I4 VTS_BSTR VTS_VARIANT VTS_R8 VTS_R8 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_R8 VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_I4;
	InvokeHelper(0x5a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&p0, &P1, &P2, &P3, &p4, val, primPrec, text, &TextPoint, angle, textHeight, prefix, suffix, callout1, callout2, tolType, tolMin, tolMax, tolPrec, arrowSize, arrowStyle, arrowDir, witnessGap, witnessOvershoot, dualDisplay, dualPrec);
	return result;
}

void IDrawingDoc::ICreateLinearDim2(double* p0, double* P1, double* P2, double* P3, double* p4, double val, long primPrec, LPCTSTR text, double* TextPoint, double angle, double textHeight, LPCTSTR prefix, LPCTSTR suffix, LPCTSTR callout1, 
		LPCTSTR callout2, long tolType, LPCTSTR tolMin, LPCTSTR tolMax, long tolPrec, double arrowSize, long arrowStyle, long arrowDir, double witnessGap, double witnessOvershoot, BOOL dualDisplay, long dualPrecision)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_I4 VTS_BSTR VTS_PR8 VTS_R8 VTS_R8 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_R8 VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_I4;
	InvokeHelper(0x5b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 p0, P1, P2, P3, p4, val, primPrec, text, TextPoint, angle, textHeight, prefix, suffix, callout1, callout2, tolType, tolMin, tolMax, tolPrec, arrowSize, arrowStyle, arrowDir, witnessGap, witnessOvershoot, dualDisplay, dualPrecision);
}

BOOL IDrawingDoc::CreateAngDim2(const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, const VARIANT& vP4, const VARIANT& vP5, const VARIANT& vP6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, 
		double witnessOvershoot, const VARIANT& vTextPoint)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_VARIANT;
	InvokeHelper(0x5c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&vP0, &vP1, &vP2, &vP3, &vP4, &vP5, &vP6, arrowSize, text, textHeight, witnessGap, witnessOvershoot, &vTextPoint);
	return result;
}

void IDrawingDoc::ICreateAngDim2(double* p0, double* P1, double* P2, double* P3, double* p4, double* P5, double* P6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* TextPoint)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_PR8;
	InvokeHelper(0x5d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 p0, P1, P2, P3, p4, P5, P6, arrowSize, text, textHeight, witnessGap, witnessOvershoot, TextPoint);
}

BOOL IDrawingDoc::CreateDiamDim2(double dimVal, const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, const VARIANT& vTextPoint)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_VARIANT;
	InvokeHelper(0x5e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		dimVal, &vP0, &vP1, &vP2, &vP3, arrowSize, text, textHeight, witnessGap, witnessOvershoot, &vTextPoint);
	return result;
}

void IDrawingDoc::ICreateDiamDim2(double DimValue, double* p0, double* P1, double* P2, double* P3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* TextPoint)
{
	static BYTE parms[] =
		VTS_R8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_PR8;
	InvokeHelper(0x5f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 DimValue, p0, P1, P2, P3, arrowSize, text, textHeight, witnessGap, witnessOvershoot, TextPoint);
}

BOOL IDrawingDoc::CreateOrdinateDim2(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, 
		const VARIANT& P5)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_VARIANT;
	InvokeHelper(0x60, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&p0, &P1, &P2, &P3, &p4, angle, arrowSize, text, textHeight, witnessGap, witnessOvershoot, &P5);
	return result;
}

void IDrawingDoc::ICreateOrdinateDim2(double* p0, double* P1, double* P2, double* P3, double* p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* P5)
{
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_PR8;
	InvokeHelper(0x61, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 p0, P1, P2, P3, p4, angle, arrowSize, text, textHeight, witnessGap, witnessOvershoot, P5);
}

void IDrawingDoc::InsertNewNote2(LPCTSTR upperText, LPCTSTR lowerText, BOOL noLeader, BOOL bentLeader, short arrowStyle, short leaderSide, double angle, short balloonStyle, short balloonFit, short upperNoteContent, short lowerNoteContent)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_I2 VTS_I2 VTS_R8 VTS_I2 VTS_I2 VTS_I2 VTS_I2;
	InvokeHelper(0x62, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 upperText, lowerText, noLeader, bentLeader, arrowStyle, leaderSide, angle, balloonStyle, balloonFit, upperNoteContent, lowerNoteContent);
}

void IDrawingDoc::DragModelDimension(LPCTSTR viewname, short dropEffect, double x, double y, double z)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I2 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x63, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 viewname, dropEffect, x, y, z);
}

CString IDrawingDoc::CreateViewport2(double LowerLeftX, double LowerLeftY, double UpperRightX, double UpperRightY, short sketchSize, double Scale)
{
	CString result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_I2 VTS_R8;
	InvokeHelper(0x64, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		LowerLeftX, LowerLeftY, UpperRightX, UpperRightY, sketchSize, Scale);
	return result;
}

void IDrawingDoc::SetupSheet2(LPCTSTR Name, short paperSize, short templateIn, double scale1, double Scale2, long skPointsFlag)
{
	static BYTE parms[] =
		VTS_BSTR VTS_I2 VTS_I2 VTS_R8 VTS_R8 VTS_I4;
	InvokeHelper(0x65, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Name, paperSize, templateIn, scale1, Scale2, skPointsFlag);
}

void IDrawingDoc::OnComponentProperties()
{
	InvokeHelper(0x66, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IDrawingDoc::GetLineFontCount2()
{
	long result;
	InvokeHelper(0x67, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IDrawingDoc::GetLineFontName2(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x68, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDrawingDoc::GetLineFontInfo2(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x69, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

void IDrawingDoc::SetLineStyle(LPCTSTR styleName)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 styleName);
}

void IDrawingDoc::SetLineWidth(long Width)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Width);
}

void IDrawingDoc::SetLineColor(long Color)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Color);
}

void IDrawingDoc::ShowEdge()
{
	InvokeHelper(0x6d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::HideEdge()
{
	InvokeHelper(0x6e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::AddHoleCallout()
{
	InvokeHelper(0x6f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IDrawingDoc::GetPenCount()
{
	long result;
	InvokeHelper(0x70, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDrawingDoc::GetPenInfo()
{
	VARIANT result;
	InvokeHelper(0x71, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

long IDrawingDoc::IGetPenInfo()
{
	long result;
	InvokeHelper(0x72, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDrawingDoc::GetLineFontId(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x73, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

BOOL IDrawingDoc::CreateAuxiliaryViewAt(double x, double y, double z, BOOL notAligned)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_BOOL;
	InvokeHelper(0x74, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z, notAligned);
	return result;
}

BOOL IDrawingDoc::CreateDetailViewAt(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x75, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

BOOL IDrawingDoc::CreateSectionViewAt(double x, double y, double z, BOOL notAligned, BOOL isOffsetSection)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x76, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z, notAligned, isOffsetSection);
	return result;
}

BOOL IDrawingDoc::CreateUnfoldedViewAt2(double x, double y, double z, BOOL notAligned)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_BOOL;
	InvokeHelper(0x77, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z, notAligned);
	return result;
}

BOOL IDrawingDoc::SetupSheet3(LPCTSTR Name, long paperSize, long templateIn, double scale1, double Scale2, BOOL firstAngle, LPCTSTR templateName, double Width, double height)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BSTR VTS_R8 VTS_R8;
	InvokeHelper(0x78, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, paperSize, templateIn, scale1, Scale2, firstAngle, templateName, Width, height);
	return result;
}

BOOL IDrawingDoc::InsertModelAnnotations(long option, BOOL allTypes, long types, BOOL allViews)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BOOL VTS_I4 VTS_BOOL;
	InvokeHelper(0x79, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		option, allTypes, types, allViews);
	return result;
}

void IDrawingDoc::InsertCustomSymbol(LPCTSTR symbolPath)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 symbolPath);
}

long IDrawingDoc::GetSheetCount()
{
	long result;
	InvokeHelper(0x7b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDrawingDoc::GetSheetNames()
{
	VARIANT result;
	InvokeHelper(0x7c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IDrawingDoc::IGetSheetNames(long* count)
{
	CString result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x7d, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		count);
	return result;
}

BOOL IDrawingDoc::ActivateSheet(LPCTSTR Name)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name);
	return result;
}

LPDISPATCH IDrawingDoc::CreateText2(LPCTSTR textString, double textX, double textY, double textZ, double textHeight, double textAngle)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x7f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		textString, textX, textY, textZ, textHeight, textAngle);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateText2(LPCTSTR textString, double textX, double textY, double textZ, double textHeight, double textAngle)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x80, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		textString, textX, textY, textZ, textHeight, textAngle);
	return result;
}

BOOL IDrawingDoc::NewSheet3(LPCTSTR Name, long paperSize, long templateIn, double scale1, double Scale2, BOOL firstAngle, LPCTSTR templateName, double Width, double height, LPCTSTR propertyViewName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BSTR VTS_R8 VTS_R8 VTS_BSTR;
	InvokeHelper(0x81, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, paperSize, templateIn, scale1, Scale2, firstAngle, templateName, Width, height, propertyViewName);
	return result;
}

BOOL IDrawingDoc::SetupSheet4(LPCTSTR Name, long paperSize, long templateIn, double scale1, double Scale2, BOOL firstAngle, LPCTSTR templateName, double Width, double height, LPCTSTR propertyViewName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_BSTR VTS_R8 VTS_R8 VTS_BSTR;
	InvokeHelper(0x82, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Name, paperSize, templateIn, scale1, Scale2, firstAngle, templateName, Width, height, propertyViewName);
	return result;
}

LPDISPATCH IDrawingDoc::CreateLinearDim3(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double val, long primPrec, LPCTSTR text, const VARIANT& TextPoint, double angle, double textHeight, LPCTSTR prefix, 
		LPCTSTR suffix, LPCTSTR callout1, LPCTSTR callout2, long tolType, LPCTSTR tolMin, LPCTSTR tolMax, long tolPrec, double arrowSize, long arrowStyle, long arrowDir, double witnessGap, double witnessOvershoot, BOOL dualDisplay, long dualPrec)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_I4 VTS_BSTR VTS_VARIANT VTS_R8 VTS_R8 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_R8 VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_I4;
	InvokeHelper(0x83, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&p0, &P1, &P2, &P3, &p4, val, primPrec, text, &TextPoint, angle, textHeight, prefix, suffix, callout1, callout2, tolType, tolMin, tolMax, tolPrec, arrowSize, arrowStyle, arrowDir, witnessGap, witnessOvershoot, dualDisplay, dualPrec);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateLinearDim3(double* p0, double* P1, double* P2, double* P3, double* p4, double val, long primPrec, LPCTSTR text, double* TextPoint, double angle, double textHeight, LPCTSTR prefix, LPCTSTR suffix, LPCTSTR callout1, 
		LPCTSTR callout2, long tolType, LPCTSTR tolMin, LPCTSTR tolMax, long tolPrec, double arrowSize, long arrowStyle, long arrowDir, double witnessGap, double witnessOvershoot, BOOL dualDisplay, long dualPrecision)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_I4 VTS_BSTR VTS_PR8 VTS_R8 VTS_R8 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR VTS_BSTR VTS_I4 VTS_R8 VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_I4;
	InvokeHelper(0x84, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		p0, P1, P2, P3, p4, val, primPrec, text, TextPoint, angle, textHeight, prefix, suffix, callout1, callout2, tolType, tolMin, tolMax, tolPrec, arrowSize, arrowStyle, arrowDir, witnessGap, witnessOvershoot, dualDisplay, dualPrecision);
	return result;
}

LPDISPATCH IDrawingDoc::CreateAngDim3(const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, const VARIANT& vP4, const VARIANT& vP5, const VARIANT& vP6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, 
		double witnessOvershoot, const VARIANT& vTextPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_VARIANT;
	InvokeHelper(0x85, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&vP0, &vP1, &vP2, &vP3, &vP4, &vP5, &vP6, arrowSize, text, textHeight, witnessGap, witnessOvershoot, &vTextPoint);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateAngDim3(double* p0, double* P1, double* P2, double* P3, double* p4, double* P5, double* P6, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* TextPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_PR8;
	InvokeHelper(0x86, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		p0, P1, P2, P3, p4, P5, P6, arrowSize, text, textHeight, witnessGap, witnessOvershoot, TextPoint);
	return result;
}

LPDISPATCH IDrawingDoc::CreateDiamDim3(double dimVal, const VARIANT& vP0, const VARIANT& vP1, const VARIANT& vP2, const VARIANT& vP3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, const VARIANT& vTextPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_VARIANT;
	InvokeHelper(0x87, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		dimVal, &vP0, &vP1, &vP2, &vP3, arrowSize, text, textHeight, witnessGap, witnessOvershoot, &vTextPoint);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateDiamDim3(double DimValue, double* p0, double* P1, double* P2, double* P3, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* TextPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_PR8;
	InvokeHelper(0x88, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		DimValue, p0, P1, P2, P3, arrowSize, text, textHeight, witnessGap, witnessOvershoot, TextPoint);
	return result;
}

LPDISPATCH IDrawingDoc::CreateOrdinateDim3(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, 
		const VARIANT& P5)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_VARIANT;
	InvokeHelper(0x89, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&p0, &P1, &P2, &P3, &p4, angle, arrowSize, text, textHeight, witnessGap, witnessOvershoot, &P5);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateOrdinateDim3(double* p0, double* P1, double* P2, double* P3, double* p4, double angle, double arrowSize, LPCTSTR text, double textHeight, double witnessGap, double witnessOvershoot, double* P5)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_R8 VTS_BSTR VTS_R8 VTS_R8 VTS_R8 VTS_PR8;
	InvokeHelper(0x8a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		p0, P1, P2, P3, p4, angle, arrowSize, text, textHeight, witnessGap, witnessOvershoot, P5);
	return result;
}

void IDrawingDoc::ForceRebuild()
{
	InvokeHelper(0x8b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IDrawingDoc::AddOrdinateDimension(long DimType, double locX, double locY, double locZ)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x8c, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		DimType, locX, locY, locZ);
	return result;
}

BOOL IDrawingDoc::CreateLayer(LPCTSTR layerName, LPCTSTR layerDesc, long layerColor, long layerStyle, long layerWidth, BOOL bOn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_BOOL;
	InvokeHelper(0x8d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		layerName, layerDesc, layerColor, layerStyle, layerWidth, bOn);
	return result;
}

BOOL IDrawingDoc::SetCurrentLayer(LPCTSTR layerName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		layerName);
	return result;
}

BOOL IDrawingDoc::DrawingViewRotate(double newAngle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x8f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		newAngle);
	return result;
}

BOOL IDrawingDoc::CreateDetailViewAt2(double x, double y, double z)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x90, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		x, y, z);
	return result;
}

void IDrawingDoc::RestoreRotation()
{
	InvokeHelper(0x91, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IDrawingDoc::GetEditSheet()
{
	BOOL result;
	InvokeHelper(0x92, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDrawingDoc::TranslateDrawing(double deltaX, double deltaY)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8;
	InvokeHelper(0x93, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 deltaX, deltaY);
}

BOOL IDrawingDoc::Create1stAngleViews2(LPCTSTR ModelName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x94, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ModelName);
	return result;
}

BOOL IDrawingDoc::Create3rdAngleViews2(LPCTSTR ModelName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x95, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ModelName);
	return result;
}

void IDrawingDoc::HideShowDimensions()
{
	InvokeHelper(0x96, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IDrawingDoc::CreateLinearDim4(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, const VARIANT& TextPoint, double val, double angle, double textHeight)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x97, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&p0, &P1, &P2, &P3, &p4, &TextPoint, val, angle, textHeight);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateLinearDim4(double* p0, double* P1, double* P2, double* P3, double* p4, double* TextPoint, double val, double angle, double textHeight)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x98, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		p0, P1, P2, P3, p4, TextPoint, val, angle, textHeight);
	return result;
}

LPDISPATCH IDrawingDoc::CreateDiamDim4(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& TextPoint, double val, double textHeight)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_R8;
	InvokeHelper(0x99, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&p0, &P1, &P2, &P3, &TextPoint, val, textHeight);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateDiamDim4(double* p0, double* P1, double* P2, double* P3, double* TextPoint, double val, double textHeight)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_R8;
	InvokeHelper(0x9a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		p0, P1, P2, P3, TextPoint, val, textHeight);
	return result;
}

LPDISPATCH IDrawingDoc::CreateOrdinateDim4(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, const VARIANT& P5, double val, double angle, double textHeight)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x9b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&p0, &P1, &P2, &P3, &p4, &P5, val, angle, textHeight);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateOrdinateDim4(double* p0, double* P1, double* P2, double* P3, double* p4, double* P5, double val, double angle, double textHeight)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x9c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		p0, P1, P2, P3, p4, P5, val, angle, textHeight);
	return result;
}

LPDISPATCH IDrawingDoc::CreateAngDim4(const VARIANT& p0, const VARIANT& P1, const VARIANT& P2, const VARIANT& P3, const VARIANT& p4, const VARIANT& P5, const VARIANT& P6, const VARIANT& TextPoint, double textHeight)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8;
	InvokeHelper(0x9d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&p0, &P1, &P2, &P3, &p4, &P5, &P6, &TextPoint, textHeight);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateAngDim4(double* p0, double* P1, double* P2, double* P3, double* p4, double* P5, double* P6, double* TextPoint, double textHeight)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8;
	InvokeHelper(0x9e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		p0, P1, P2, P3, p4, P5, P6, TextPoint, textHeight);
	return result;
}

LPDISPATCH IDrawingDoc::CreateDetailViewAt3(double x, double y, double z, long Style, double scale1, double Scale2, LPCTSTR labelIn, long showtype, BOOL fulloutline)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_R8 VTS_R8 VTS_BSTR VTS_I4 VTS_BOOL;
	InvokeHelper(0x9f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z, Style, scale1, Scale2, labelIn, showtype, fulloutline);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateDetailViewAt3(double x, double y, double z, long Style, double scale1, double Scale2, LPCTSTR labelIn, long showtype, BOOL fulloutline)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_I4 VTS_R8 VTS_R8 VTS_BSTR VTS_I4 VTS_BOOL;
	InvokeHelper(0xa0, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z, Style, scale1, Scale2, labelIn, showtype, fulloutline);
	return result;
}

LPDISPATCH IDrawingDoc::CreateSectionViewAt2(double x, double y, double z, BOOL notAligned, BOOL isOffsetSection, LPCTSTR label, BOOL chgdirection, BOOL scwithmodel, BOOL partial, BOOL dispsurfcut)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0xa1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z, notAligned, isOffsetSection, label, chgdirection, scwithmodel, partial, dispsurfcut);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateSectionViewAt2(double x, double y, double z, BOOL notAligned, BOOL isOffsetSection, LPCTSTR label, BOOL chgdirection, BOOL scwithmodel, BOOL partial, BOOL dispsurfcut)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0xa2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z, notAligned, isOffsetSection, label, chgdirection, scwithmodel, partial, dispsurfcut);
	return result;
}

LPDISPATCH IDrawingDoc::CreateAuxiliaryViewAt2(double x, double y, double z, BOOL notAligned, LPCTSTR label, BOOL showarrow, BOOL flip)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_BSTR VTS_BOOL VTS_BOOL;
	InvokeHelper(0xa3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z, notAligned, label, showarrow, flip);
	return result;
}

LPDISPATCH IDrawingDoc::ICreateAuxiliaryViewAt2(double x, double y, double z, BOOL notAligned, LPCTSTR label, BOOL showarrow, BOOL flip)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_BOOL VTS_BSTR VTS_BOOL VTS_BOOL;
	InvokeHelper(0xa4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z, notAligned, label, showarrow, flip);
	return result;
}

void IDrawingDoc::MakeCustomSymbol()
{
	InvokeHelper(0xa5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::ExplodeCustomSymbol()
{
	InvokeHelper(0xa6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IDrawingDoc::SaveCustomSymbol(LPCTSTR filenameIn)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xa7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 filenameIn);
}

BOOL IDrawingDoc::CreateBreakOutSection(double depth)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xa8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		depth);
	return result;
}

void IDrawingDoc::InsertThreadCallout()
{
	InvokeHelper(0xa9, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IDrawingDoc::CreateFlatPatternViewFromModelView(LPCTSTR ModelName, LPCTSTR configName, double locX, double locY, double locZ)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0xaa, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ModelName, configName, locX, locY, locZ);
	return result;
}

BOOL IDrawingDoc::ChangeRefConfigurationOfFlatPatternView(LPCTSTR ModelName, LPCTSTR configName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0xab, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ModelName, configName);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IView properties

/////////////////////////////////////////////////////////////////////////////
// IView operations

long IView::GetSuppressState()
{
	long result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IView::SetSuppressState(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

LPDISPATCH IView::GetNextView()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetNextView()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetLines()
{
	VARIANT result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetOutline()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetXform()
{
	VARIANT result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetArcs()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetUserPoints()
{
	VARIANT result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetPolylines()
{
	VARIANT result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetSplines()
{
	VARIANT result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionInfo()
{
	VARIANT result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionString()
{
	VARIANT result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstNote()
{
	LPDISPATCH result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstNote()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstGTOL()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstGTOL()
{
	LPDISPATCH result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

double IView::IGetLines()
{
	double result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IView::IGetOutline()
{
	double result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IView::IGetXform()
{
	double result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IView::IGetArcs()
{
	double result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IView::IGetUserPoints()
{
	double result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IView::IGetPolylines()
{
	double result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IView::IGetSplines()
{
	double result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IView::IGetDimensionInfo()
{
	double result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

CString IView::IGetDimensionString()
{
	CString result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IView::GetLineCount()
{
	long result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IView::GetArcCount()
{
	long result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IView::GetUserPointsCount()
{
	long result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IView::GetPolylineCount(long* pointCount)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		pointCount);
	return result;
}

long IView::GetSplineCount(long* pointCount)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		pointCount);
	return result;
}

long IView::GetDimensionCount()
{
	long result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetEllipses()
{
	VARIANT result;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetEllipses()
{
	double result;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long IView::GetEllipseCount()
{
	long result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IView::GetDisplayMode()
{
	long result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IView::SetDisplayMode(long displayIn)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 displayIn);
}

BOOL IView::GetDisplayTangentEdges()
{
	BOOL result;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IView::SetDisplayTangentEdges(BOOL displayIn)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 displayIn);
}

long IView::GetCenterMarkCount()
{
	long result;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetCenterMarkInfo()
{
	VARIANT result;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetCenterMarkInfo()
{
	double result;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long IView::GetSectionLineCount(long* size)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		size);
	return result;
}

VARIANT IView::GetSectionLineInfo()
{
	VARIANT result;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetSectionLineStrings()
{
	VARIANT result;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetSectionLineInfo()
{
	double result;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

CString IView::IGetSectionLineStrings()
{
	CString result;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IView::UpdateViewDisplayGeometry()
{
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IView::GetDetailCircleCount()
{
	long result;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDetailCircleInfo()
{
	VARIANT result;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDetailCircleInfo()
{
	double result;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDetailCircleStrings()
{
	VARIANT result;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IView::IGetDetailCircleStrings()
{
	CString result;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionIds()
{
	VARIANT result;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IView::IGetDimensionIds()
{
	CString result;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionDisplayInfo()
{
	VARIANT result;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDimensionDisplayInfo()
{
	double result;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionDisplayString()
{
	VARIANT result;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IView::IGetDimensionDisplayString()
{
	CString result;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetArcs2()
{
	VARIANT result;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetArcs2()
{
	double result;
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetEllipses2()
{
	VARIANT result;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetEllipses2()
{
	double result;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetViewXform()
{
	VARIANT result;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetViewXform()
{
	double result;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstSFSymbol()
{
	LPDISPATCH result;
	InvokeHelper(0x41, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstSFSymbol()
{
	LPDISPATCH result;
	InvokeHelper(0x42, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstDatumTag()
{
	LPDISPATCH result;
	InvokeHelper(0x43, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstDatumTag()
{
	LPDISPATCH result;
	InvokeHelper(0x44, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstDatumTargetSym()
{
	LPDISPATCH result;
	InvokeHelper(0x45, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstDatumTargetSym()
{
	LPDISPATCH result;
	InvokeHelper(0x46, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstWeldSymbol()
{
	LPDISPATCH result;
	InvokeHelper(0x47, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstWeldSymbol()
{
	LPDISPATCH result;
	InvokeHelper(0x48, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IView::GetDatumPointsCount()
{
	long result;
	InvokeHelper(0x49, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDatumPoints()
{
	VARIANT result;
	InvokeHelper(0x4a, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDatumPoints()
{
	double result;
	InvokeHelper(0x4b, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

void IView::UseDefaultAlignment()
{
	InvokeHelper(0x4c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IView::RemoveAlignment()
{
	InvokeHelper(0x4d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IView::AlignVerticalTo(LPCTSTR viewNameIn)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 viewNameIn);
}

void IView::AlignHorizontalTo(LPCTSTR viewNameIn)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 viewNameIn);
}

LPDISPATCH IView::GetDisplayData()
{
	LPDISPATCH result;
	InvokeHelper(0x50, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetDisplayData()
{
	LPDISPATCH result;
	InvokeHelper(0x51, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IView::GetPolyLineCount2(long* pointCount)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x52, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		pointCount);
	return result;
}

VARIANT IView::GetPolylines2()
{
	VARIANT result;
	InvokeHelper(0x53, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetPolylines2()
{
	double result;
	InvokeHelper(0x54, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IView::ShowExploded(BOOL showIt)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x55, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		showIt);
	return result;
}

BOOL IView::IsExploded()
{
	BOOL result;
	InvokeHelper(0x56, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IView::SetReferencedConfiguration(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x57, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString IView::GetReferencedConfiguration()
{
	CString result;
	InvokeHelper(0x57, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstCThread()
{
	LPDISPATCH result;
	InvokeHelper(0x58, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstCThread()
{
	LPDISPATCH result;
	InvokeHelper(0x59, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IView::CreateViewArrow(LPCTSTR drawingViewNameIn, LPCTSTR arrowNameIn)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x5a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		drawingViewNameIn, arrowNameIn);
	return result;
}

void IView::ModifyViewArrow(LPCTSTR drawingViewNameIn, LPCTSTR arrowNameIn)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x5b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 drawingViewNameIn, arrowNameIn);
}

void IView::MoveViewArrow(LPCTSTR drawingViewNameIn, double dx, double dy, double dz)
{
	static BYTE parms[] =
		VTS_BSTR VTS_R8 VTS_R8 VTS_R8;
	InvokeHelper(0x5c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 drawingViewNameIn, dx, dy, dz);
}

VARIANT IView::GetPolylines3()
{
	VARIANT result;
	InvokeHelper(0x5d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetPolylines3()
{
	double result;
	InvokeHelper(0x5e, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long IView::GetPolyLineCount3(long* pointCount)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0x5f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		pointCount);
	return result;
}

LPDISPATCH IView::GetFirstDisplayDimension()
{
	LPDISPATCH result;
	InvokeHelper(0x60, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstDisplayDimension()
{
	LPDISPATCH result;
	InvokeHelper(0x61, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x62, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x63, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetLines2()
{
	VARIANT result;
	InvokeHelper(0x64, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetLines2()
{
	double result;
	InvokeHelper(0x65, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetSplines2()
{
	VARIANT result;
	InvokeHelper(0x66, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetSplines2()
{
	double result;
	InvokeHelper(0x67, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetArcs3()
{
	VARIANT result;
	InvokeHelper(0x68, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetArcs3()
{
	double result;
	InvokeHelper(0x69, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetEllipses3()
{
	VARIANT result;
	InvokeHelper(0x6a, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetEllipses3()
{
	double result;
	InvokeHelper(0x6b, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetBomTable()
{
	LPDISPATCH result;
	InvokeHelper(0x6c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetBomTable()
{
	LPDISPATCH result;
	InvokeHelper(0x6d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IView::GetUseSheetScale()
{
	long result;
	InvokeHelper(0x6e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IView::SetUseSheetScale(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

double IView::GetScaleDecimal()
{
	double result;
	InvokeHelper(0x6f, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IView::SetScaleDecimal(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x6f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

VARIANT IView::GetScaleRatio()
{
	VARIANT result;
	InvokeHelper(0x70, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IView::SetScaleRatio(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x70, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IView::GetIScaleRatio()
{
	double result;
	InvokeHelper(0x71, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IView::SetIScaleRatio(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x71, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

VARIANT IView::GetPosition()
{
	VARIANT result;
	InvokeHelper(0x72, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IView::SetPosition(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x72, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IView::GetIPosition()
{
	double result;
	InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IView::SetIPosition(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IView::SetXform(const VARIANT& transform)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x74, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&transform);
	return result;
}

BOOL IView::ISetXform(double* transform)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x75, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		transform);
	return result;
}

long IView::GetAlignment()
{
	long result;
	InvokeHelper(0x76, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IView::GetReferencedModelName()
{
	CString result;
	InvokeHelper(0x77, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x78, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x79, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstCustomSymbol()
{
	LPDISPATCH result;
	InvokeHelper(0x7a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstCustomSymbol()
{
	LPDISPATCH result;
	InvokeHelper(0x7b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionInfo2()
{
	VARIANT result;
	InvokeHelper(0x7c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDimensionInfo2()
{
	double result;
	InvokeHelper(0x7d, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetParabolas()
{
	VARIANT result;
	InvokeHelper(0x7e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetParabolas()
{
	double result;
	InvokeHelper(0x7f, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long IView::GetParabolaCount()
{
	long result;
	InvokeHelper(0x80, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetEllipses4()
{
	VARIANT result;
	InvokeHelper(0x81, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetEllipses4()
{
	double result;
	InvokeHelper(0x82, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetLines3()
{
	VARIANT result;
	InvokeHelper(0x83, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetLines3()
{
	double result;
	InvokeHelper(0x84, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetArcs4()
{
	VARIANT result;
	InvokeHelper(0x85, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetArcs4()
{
	double result;
	InvokeHelper(0x86, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetSplines3()
{
	VARIANT result;
	InvokeHelper(0x87, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetSplines3()
{
	double result;
	InvokeHelper(0x88, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetEllipses5()
{
	VARIANT result;
	InvokeHelper(0x89, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetEllipses5()
{
	double result;
	InvokeHelper(0x8a, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetParabolas2()
{
	VARIANT result;
	InvokeHelper(0x8b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetParabolas2()
{
	double result;
	InvokeHelper(0x8c, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetUserPoints2()
{
	VARIANT result;
	InvokeHelper(0x8d, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetUserPoints2()
{
	double result;
	InvokeHelper(0x8e, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionInfo3()
{
	VARIANT result;
	InvokeHelper(0x8f, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDimensionInfo3()
{
	double result;
	InvokeHelper(0x90, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDatumPoints2()
{
	VARIANT result;
	InvokeHelper(0x91, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDatumPoints2()
{
	double result;
	InvokeHelper(0x92, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

CString IView::GetName()
{
	CString result;
	InvokeHelper(0x93, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

long IView::GetType()
{
	long result;
	InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IView::GetProjectedDimensions()
{
	BOOL result;
	InvokeHelper(0x95, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IView::SetProjectedDimensions(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x95, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IView::HasDesignTable()
{
	BOOL result;
	InvokeHelper(0x96, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDesignTableExtent()
{
	VARIANT result;
	InvokeHelper(0x97, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDesignTableExtent()
{
	double result;
	InvokeHelper(0x98, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetDisplayData2()
{
	LPDISPATCH result;
	InvokeHelper(0x99, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetDisplayData2()
{
	LPDISPATCH result;
	InvokeHelper(0x9a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstAnnotation2()
{
	LPDISPATCH result;
	InvokeHelper(0x9b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstAnnotation2()
{
	LPDISPATCH result;
	InvokeHelper(0x9c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IView::GetDimensionCount2()
{
	long result;
	InvokeHelper(0x9d, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionInfo4()
{
	VARIANT result;
	InvokeHelper(0x9e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDimensionInfo4()
{
	double result;
	InvokeHelper(0x9f, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionString2()
{
	VARIANT result;
	InvokeHelper(0xa0, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IView::IGetDimensionString2()
{
	CString result;
	InvokeHelper(0xa1, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionIds2()
{
	VARIANT result;
	InvokeHelper(0xa2, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IView::IGetDimensionIds2()
{
	CString result;
	InvokeHelper(0xa3, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionDisplayInfo2()
{
	VARIANT result;
	InvokeHelper(0xa4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDimensionDisplayInfo2()
{
	double result;
	InvokeHelper(0xa5, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionDisplayString2()
{
	VARIANT result;
	InvokeHelper(0xa6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IView::IGetDimensionDisplayString2()
{
	CString result;
	InvokeHelper(0xa7, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstDisplayDimension2()
{
	LPDISPATCH result;
	InvokeHelper(0xa8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstDisplayDimension2()
{
	LPDISPATCH result;
	InvokeHelper(0xa9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetPolylines4()
{
	VARIANT result;
	InvokeHelper(0xaa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetPolylines4()
{
	double result;
	InvokeHelper(0xab, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long IView::GetPolyLineCount4(long* pointCount)
{
	long result;
	static BYTE parms[] =
		VTS_PI4;
	InvokeHelper(0xac, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		pointCount);
	return result;
}

BOOL IView::IsModelLoaded()
{
	BOOL result;
	InvokeHelper(0xad, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IView::LoadModel()
{
	long result;
	InvokeHelper(0xae, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IView::GetDisplayTangentEdges2()
{
	long result;
	InvokeHelper(0xaf, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IView::SetDisplayTangentEdges2(long displayIn)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xb0, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 displayIn);
}

VARIANT IView::GetSectionLines()
{
	VARIANT result;
	InvokeHelper(0xb1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetSectionLines()
{
	LPDISPATCH result;
	InvokeHelper(0xb2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetSection()
{
	LPDISPATCH result;
	InvokeHelper(0xb3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetSection()
{
	LPDISPATCH result;
	InvokeHelper(0xb4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetHiddenComponents()
{
	VARIANT result;
	InvokeHelper(0xb5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IView::EnumHiddenComponents()
{
	LPUNKNOWN result;
	InvokeHelper(0xb6, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IView::EnumSectionLines()
{
	LPUNKNOWN result;
	InvokeHelper(0xb7, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetDisplayData3()
{
	LPDISPATCH result;
	InvokeHelper(0xb8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetDisplayData3()
{
	LPDISPATCH result;
	InvokeHelper(0xb9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IView::GetDimensionCount3()
{
	long result;
	InvokeHelper(0xba, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionInfo5()
{
	VARIANT result;
	InvokeHelper(0xbb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDimensionInfo5()
{
	double result;
	InvokeHelper(0xbc, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionString3()
{
	VARIANT result;
	InvokeHelper(0xbd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IView::IGetDimensionString3()
{
	CString result;
	InvokeHelper(0xbe, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionIds3()
{
	VARIANT result;
	InvokeHelper(0xbf, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IView::IGetDimensionIds3()
{
	CString result;
	InvokeHelper(0xc0, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionDisplayInfo3()
{
	VARIANT result;
	InvokeHelper(0xc1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IView::IGetDimensionDisplayInfo3()
{
	double result;
	InvokeHelper(0xc2, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IView::GetDimensionDisplayString3()
{
	VARIANT result;
	InvokeHelper(0xc3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

CString IView::IGetDimensionDisplayString3()
{
	CString result;
	InvokeHelper(0xc4, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetFirstDisplayDimension3()
{
	LPDISPATCH result;
	InvokeHelper(0xc5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetFirstDisplayDimension3()
{
	LPDISPATCH result;
	InvokeHelper(0xc6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IView::IsCropped()
{
	BOOL result;
	InvokeHelper(0xc7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

double IView::GetAngle()
{
	double result;
	InvokeHelper(0xc8, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IView::SetAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xc8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

VARIANT IView::GetDetailCircles()
{
	VARIANT result;
	InvokeHelper(0xc9, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetDetailCircles(long NumDetailCircles)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xca, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		NumDetailCircles);
	return result;
}

LPDISPATCH IView::GetProjectionArrow()
{
	LPDISPATCH result;
	InvokeHelper(0xcb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetProjectionArrow()
{
	LPDISPATCH result;
	InvokeHelper(0xcc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetDetail()
{
	LPDISPATCH result;
	InvokeHelper(0xcd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetDetail()
{
	LPDISPATCH result;
	InvokeHelper(0xce, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::GetBaseView()
{
	LPDISPATCH result;
	InvokeHelper(0xcf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IView::IGetBaseView()
{
	LPDISPATCH result;
	InvokeHelper(0xd0, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISFSymbol properties

/////////////////////////////////////////////////////////////////////////////
// ISFSymbol operations

LPDISPATCH ISFSymbol::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISFSymbol::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long ISFSymbol::GetTextCount()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString ISFSymbol::GetTextAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

double ISFSymbol::GetTextHeightAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT ISFSymbol::GetTextPositionAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ISFSymbol::IGetTextPositionAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

double ISFSymbol::GetTextAngleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long ISFSymbol::GetLineCount()
{
	long result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISFSymbol::GetLineAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ISFSymbol::IGetLineAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long ISFSymbol::GetArcCount()
{
	long result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISFSymbol::GetArcAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ISFSymbol::IGetArcAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long ISFSymbol::GetArrowHeadCount()
{
	long result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISFSymbol::GetArrowHeadAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ISFSymbol::IGetArrowHeadAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long ISFSymbol::GetTextRefPositionAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long ISFSymbol::GetTextInvertAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

CString ISFSymbol::GetTextFontAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

long ISFSymbol::GetTriangleCount()
{
	long result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISFSymbol::GetTriangleAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ISFSymbol::IGetTriangleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

BOOL ISFSymbol::IsAttached()
{
	BOOL result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ISFSymbol::HasExtraLeader()
{
	BOOL result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long ISFSymbol::GetLeaderCount()
{
	long result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISFSymbol::GetLeaderAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ISFSymbol::IGetLeaderAtIndex(long index, long* pointCount)
{
	double result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index, pointCount);
	return result;
}

double ISFSymbol::IGetArrowHeadInfo()
{
	double result;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISFSymbol::GetArrowHeadInfo()
{
	VARIANT result;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISFSymbol::GetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISFSymbol::IGetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long ISFSymbol::GetSymbolType()
{
	long result;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ISFSymbol::SetSymbolType(long symbolType)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		symbolType);
	return result;
}

long ISFSymbol::GetDirectionOfLay()
{
	long result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ISFSymbol::SetDirectionOfLay(long direction)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		direction);
	return result;
}

CString ISFSymbol::GetText(long whichOne)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		whichOne);
	return result;
}

BOOL ISFSymbol::SetText(long whichOne, LPCTSTR text)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		whichOne, text);
	return result;
}

BOOL ISFSymbol::GetRotated()
{
	BOOL result;
	InvokeHelper(0x27, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISFSymbol::SetRotated(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x27, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ISFSymbol::GetGrinding()
{
	BOOL result;
	InvokeHelper(0x28, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISFSymbol::SetGrinding(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x28, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// IDatumTargetSym properties

/////////////////////////////////////////////////////////////////////////////
// IDatumTargetSym operations

LPDISPATCH IDatumTargetSym::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDatumTargetSym::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IDatumTargetSym::GetTextCount()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IDatumTargetSym::GetTextAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

double IDatumTargetSym::GetTextHeightAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT IDatumTargetSym::GetTextPositionAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDatumTargetSym::IGetTextPositionAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

double IDatumTargetSym::GetTextAngleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDatumTargetSym::GetTextRefPositionAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IDatumTargetSym::GetTextInvertAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long IDatumTargetSym::GetLineCount()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDatumTargetSym::GetLineAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDatumTargetSym::IGetLineAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDatumTargetSym::GetArcCount()
{
	long result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDatumTargetSym::GetArcAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDatumTargetSym::IGetArcAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDatumTargetSym::GetArrowHeadCount()
{
	long result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDatumTargetSym::GetArrowHeadAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDatumTargetSym::IGetArrowHeadAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long IDatumTargetSym::GetTriangleCount()
{
	long result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDatumTargetSym::GetTriangleAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double IDatumTargetSym::IGetTriangleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH IDatumTargetSym::GetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDatumTargetSym::IGetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IDatumTargetSym::GetTargetShape()
{
	long result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IDatumTargetSym::GetTargetAreaSize(long whichOne)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		whichOne);
	return result;
}

BOOL IDatumTargetSym::SetTargetArea(long shape, LPCTSTR size1, LPCTSTR size2)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		shape, size1, size2);
	return result;
}

BOOL IDatumTargetSym::GetDisplaySymbol()
{
	BOOL result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IDatumTargetSym::GetDisplayTargetArea()
{
	BOOL result;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IDatumTargetSym::GetDisplaySizeOutside()
{
	BOOL result;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IDatumTargetSym::SetDisplay(BOOL symbol, BOOL targetArea, BOOL sizeOutside)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		symbol, targetArea, sizeOutside);
	return result;
}

CString IDatumTargetSym::GetDatumReferenceLabel(long whichOne)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		whichOne);
	return result;
}

BOOL IDatumTargetSym::SetDatumReferenceLabel(long whichOne, LPCTSTR text)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		whichOne, text);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ICThread properties

/////////////////////////////////////////////////////////////////////////////
// ICThread operations

LPDISPATCH ICThread::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICThread::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICThread::GetDisplayData()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICThread::IGetDisplayData()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICThread::GetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICThread::IGetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IBomTable properties

/////////////////////////////////////////////////////////////////////////////
// IBomTable operations

VARIANT IBomTable::GetExtent()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IBomTable::IGetExtent()
{
	double result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long IBomTable::GetRowCount()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IBomTable::GetColumnCount()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString IBomTable::GetHeaderText(long col)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		col);
	return result;
}

CString IBomTable::GetEntryText(long row, long col)
{
	CString result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		row, col);
	return result;
}

void IBomTable::Attach()
{
	InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IBomTable::Detach()
{
	InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

double IBomTable::GetColumnWidth(long col)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		col);
	return result;
}

double IBomTable::GetRowHeight(long row)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		row);
	return result;
}

VARIANT IBomTable::GetEntryValue(long row, long col)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		row, col);
	return result;
}

BOOL IBomTable::Attach2()
{
	BOOL result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBomTable::GetDisplayData()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBomTable::IGetDisplayData()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IBomTable::IsVisible()
{
	BOOL result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ICustomSymbol properties

/////////////////////////////////////////////////////////////////////////////
// ICustomSymbol operations

LPDISPATCH ICustomSymbol::GetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICustomSymbol::IGetNext()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long ICustomSymbol::GetTextCount()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString ICustomSymbol::GetTextAtIndex(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

double ICustomSymbol::GetTextHeightAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

VARIANT ICustomSymbol::GetTextPositionAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ICustomSymbol::IGetTextPositionAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

double ICustomSymbol::GetTextAngleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long ICustomSymbol::GetTextRefPositionAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long ICustomSymbol::GetTextInvertAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

long ICustomSymbol::GetLineCount()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ICustomSymbol::GetLineAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ICustomSymbol::IGetLineAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long ICustomSymbol::GetArcCount()
{
	long result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ICustomSymbol::GetArcAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ICustomSymbol::IGetArcAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long ICustomSymbol::GetArrowHeadCount()
{
	long result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ICustomSymbol::GetArrowHeadAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ICustomSymbol::IGetArrowHeadAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

long ICustomSymbol::GetTriangleCount()
{
	long result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ICustomSymbol::GetTriangleAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ICustomSymbol::IGetTriangleAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH ICustomSymbol::GetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICustomSymbol::IGetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT ICustomSymbol::GetSketchPosition()
{
	VARIANT result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ICustomSymbol::IGetSketchPosition()
{
	double result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL ICustomSymbol::IsAttached()
{
	BOOL result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL ICustomSymbol::HasExtraLeader()
{
	BOOL result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long ICustomSymbol::GetLeaderCount()
{
	long result;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ICustomSymbol::GetLeaderAtIndex(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

double ICustomSymbol::IGetLeaderAtIndex(long index, long* pointCount)
{
	double result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index, pointCount);
	return result;
}

double ICustomSymbol::IGetArrowHeadInfo()
{
	double result;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ICustomSymbol::GetArrowHeadInfo()
{
	VARIANT result;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICustomSymbol::GetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICustomSymbol::IGetAnnotation()
{
	LPDISPATCH result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

double ICustomSymbol::GetAngle()
{
	double result;
	InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ICustomSymbol::SetAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ICustomSymbol::GetScale2()
{
	double result;
	InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ICustomSymbol::SetScale2(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

CString ICustomSymbol::GetText(long index)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		index);
	return result;
}

BOOL ICustomSymbol::SetText(long index, LPCTSTR text)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		index, text);
	return result;
}

long ICustomSymbol::GetTextJustificationAtIndex(long index)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		index);
	return result;
}

void ICustomSymbol::SetTextJustificationAtIndex(long index, long justification)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 index, justification);
}

double ICustomSymbol::GetTextLineSpacingAtIndex(long index)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		index);
	return result;
}

BOOL ICustomSymbol::GetTextVisible()
{
	BOOL result;
	InvokeHelper(0x2b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ICustomSymbol::SetTextVisible(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// IDrSection properties

/////////////////////////////////////////////////////////////////////////////
// IDrSection operations

LPDISPATCH IDrSection::GetView()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrSection::IGetView()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrSection::GetSectionView()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrSection::IGetSectionView()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IDrSection::GetLabel()
{
	CString result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IDrSection::SetLabel(LPCTSTR label)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		label);
	return result;
}

BOOL IDrSection::GetUseDocTextFormat()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrSection::GetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDrSection::IGetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IDrSection::SetTextFormat(BOOL useDoc, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_DISPATCH;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		useDoc, textFormat);
	return result;
}

BOOL IDrSection::ISetTextFormat(BOOL useDoc, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_DISPATCH;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		useDoc, textFormat);
	return result;
}

BOOL IDrSection::GetReversedCutDirection()
{
	BOOL result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDrSection::SetReversedCutDirection(BOOL reversed)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 reversed);
}

BOOL IDrSection::GetScaleWithModelChanges()
{
	BOOL result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDrSection::SetScaleWithModelChanges(BOOL scaleWithChanges)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 scaleWithChanges);
}

BOOL IDrSection::GetPartialSection()
{
	BOOL result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDrSection::SetPartialSection(BOOL partial)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 partial);
}

BOOL IDrSection::GetDisplayOnlySurfaceCut()
{
	BOOL result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDrSection::SetDisplayOnlySurfaceCut(BOOL Display)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 Display);
}

BOOL IDrSection::IsAligned()
{
	BOOL result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString IDrSection::GetName()
{
	CString result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

VARIANT IDrSection::GetTextInfo()
{
	VARIANT result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IDrSection::IGetTextInfo()
{
	double result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IDrSection::GetArrowInfo()
{
	VARIANT result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IDrSection::IGetArrowInfo()
{
	double result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IDrSection::GetExcludedComponents()
{
	VARIANT result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN IDrSection::EnumExcludedComponents()
{
	LPUNKNOWN result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}

BOOL IDrSection::GetAutoHatch()
{
	BOOL result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDrSection::SetAutoHatch(BOOL autoHatch)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 autoHatch);
}

long IDrSection::IGetLineSegmentCount()
{
	long result;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDrSection::GetLineInfo()
{
	VARIANT result;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IDrSection::IGetLineInfo()
{
	double result;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IDetailCircle properties

/////////////////////////////////////////////////////////////////////////////
// IDetailCircle operations

LPDISPATCH IDetailCircle::GetView()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDetailCircle::GetDetailView()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IDetailCircle::GetLabel()
{
	CString result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDetailCircle::GetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IDetailCircle::GetName()
{
	CString result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

VARIANT IDetailCircle::GetArrowInfo()
{
	VARIANT result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IDetailCircle::IGetArrowInfo()
{
	double result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long IDetailCircle::GetStyle()
{
	long result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDetailCircle::GetDisplay()
{
	long result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IDetailCircle::GetConnectingLine()
{
	VARIANT result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IDetailCircle::IGetConnectingLine()
{
	double result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IDetailCircle::HasFullOutline()
{
	BOOL result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

VARIANT IDetailCircle::GetLeaderInfo()
{
	VARIANT result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IDetailCircle::IGetLeaderInfo()
{
	double result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL IDetailCircle::GetUseDocTextFormat()
{
	BOOL result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IDetailCircle::SetTextFormat(BOOL useDoc, LPDISPATCH textFormat)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL VTS_DISPATCH;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		useDoc, textFormat);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IProjectionArrow properties

/////////////////////////////////////////////////////////////////////////////
// IProjectionArrow operations

LPDISPATCH IProjectionArrow::GetView()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IProjectionArrow::IGetView()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IProjectionArrow::GetProjectedView()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IProjectionArrow::IGetProjectedView()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IProjectionArrow::GetLabel()
{
	CString result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL IProjectionArrow::SetLabel(LPCTSTR label)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		label);
	return result;
}

BOOL IProjectionArrow::GetUseDocTextFormat()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH IProjectionArrow::GetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IProjectionArrow::IGetTextFormat()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

VARIANT IProjectionArrow::GetCoordinates()
{
	VARIANT result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IProjectionArrow::IGetCoordinates()
{
	double result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISheet properties

/////////////////////////////////////////////////////////////////////////////
// ISheet operations

LPDISPATCH ISheet::GetBomTable()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISheet::IGetBomTable()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString ISheet::GetName()
{
	CString result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ISheet::SetName(LPCTSTR nameIn)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 nameIn);
}

CString ISheet::GetTemplateName()
{
	CString result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ISheet::SetTemplateName(LPCTSTR nameIn)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 nameIn);
}

VARIANT ISheet::GetProperties()
{
	VARIANT result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISheet::IGetProperties()
{
	double result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

void ISheet::SetProperties(long paperSz, long templ, double scale1, double Scale2, BOOL firstAngle, double Width, double height)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_BOOL VTS_R8 VTS_R8;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 paperSz, templ, scale1, Scale2, firstAngle, Width, height);
}

long ISheet::GetCustomColorsCount()
{
	long result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ISheet::IGetCustomColors()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ISheet::GetOLEObjectCount()
{
	long result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISheet::GetOLEObjectSettings(long index, long* byteCount, long* aspect)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4 VTS_PI4;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index, byteCount, aspect);
	return result;
}

BOOL ISheet::IGetOLEObjectSettings(long index, long* byteCount, long* aspect, double* position)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_PI4 VTS_PI4 VTS_PR8;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		index, byteCount, aspect, position);
	return result;
}

VARIANT ISheet::GetOLEObjectData(long index)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		index);
	return result;
}

BOOL ISheet::CreateOLEObject(long aspect, const VARIANT& position, const VARIANT& buffer)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		aspect, &position, &buffer);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IModeler properties

/////////////////////////////////////////////////////////////////////////////
// IModeler operations

LPDISPATCH IModeler::CreateBodyFromBox(const VARIANT& boxDimArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&boxDimArray);
	return result;
}

LPDISPATCH IModeler::ICreateBodyFromBox(double* boxDimArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		boxDimArray);
	return result;
}

BOOL IModeler::SetTolerances(long* ToleranceIDArray, double* ToleranceValueArray, long NumTol)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PI4 VTS_PR8 VTS_I4;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ToleranceIDArray, ToleranceValueArray, NumTol);
	return result;
}

BOOL IModeler::UnsetTolerances(long* ToleranceIDArray, long NumTol)
{
	BOOL result;
	static BYTE parms[] =
		VTS_PI4 VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ToleranceIDArray, NumTol);
	return result;
}

LPDISPATCH IModeler::Restore(LPUNKNOWN streamIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_UNKNOWN;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		streamIn);
	return result;
}

LPDISPATCH IModeler::IRestore(LPUNKNOWN streamIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_UNKNOWN;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		streamIn);
	return result;
}

BOOL IModeler::SetInitKnitGapWidth(double InitGapWidth)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		InitGapWidth);
	return result;
}

double IModeler::GetInitKnitGapWidth()
{
	double result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH IModeler::CreateBodyFromCyl(const VARIANT& cylDimArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&cylDimArray);
	return result;
}

LPDISPATCH IModeler::ICreateBodyFromCyl(double* cylDimArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		cylDimArray);
	return result;
}

LPDISPATCH IModeler::CreateBodyFromCone(const VARIANT& coneDimArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&coneDimArray);
	return result;
}

LPDISPATCH IModeler::ICreateBodyFromCone(double* coneDimArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		coneDimArray);
	return result;
}

LPDISPATCH IModeler::CreateBrepBody(long type, long nTopologies, const VARIANT& topologies, const VARIANT& edgeToleranceArray, const VARIANT& vertexToleranceArray, const VARIANT& pointArray, const VARIANT& curveArray, const VARIANT& surfaceArray, 
		long nRelations, const VARIANT& parents, const VARIANT& children, const VARIANT& senses)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		type, nTopologies, &topologies, &edgeToleranceArray, &vertexToleranceArray, &pointArray, &curveArray, &surfaceArray, nRelations, &parents, &children, &senses);
	return result;
}

LPDISPATCH IModeler::ICreateBrepBody(long type, long nTopologies, long* topologies, double* edgeTolArray, double* vertexTolArray, double* pointArray, LPDISPATCH* curveArray, LPDISPATCH* surfaceArray, long nRelations, long* parents, long* children, 
		long* senses)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_PI4 VTS_PR8 VTS_PR8 VTS_PR8 VTS_PDISPATCH VTS_PDISPATCH VTS_I4 VTS_PI4 VTS_PI4 VTS_PI4;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		type, nTopologies, topologies, edgeTolArray, vertexTolArray, pointArray, curveArray, surfaceArray, nRelations, parents, children, senses);
	return result;
}

LPDISPATCH IModeler::CreatePlanarSurface(const VARIANT& vRootPoint, const VARIANT& vNormal)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&vRootPoint, &vNormal);
	return result;
}

LPDISPATCH IModeler::ICreatePlanarSurface(double* rootPoint, double* Normal)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		rootPoint, Normal);
	return result;
}

LPDISPATCH IModeler::CreateExtrusionSurface(LPDISPATCH profileCurve, const VARIANT& axisDirection)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_VARIANT;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		profileCurve, &axisDirection);
	return result;
}

LPDISPATCH IModeler::ICreateExtrusionSurface(LPDISPATCH profileCurve, double* axisDirection)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_PR8;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		profileCurve, axisDirection);
	return result;
}

LPDISPATCH IModeler::CreateRevolutionSurface(LPDISPATCH profileCurve, const VARIANT& axisPoint, const VARIANT& axisDirection, const VARIANT& profileEndPtParams)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		profileCurve, &axisPoint, &axisDirection, &profileEndPtParams);
	return result;
}

LPDISPATCH IModeler::ICreateRevolutionSurface(LPDISPATCH profileCurve, double* axisPoint, double* axisDirection, double* profileEndPtParams)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		profileCurve, axisPoint, axisDirection, profileEndPtParams);
	return result;
}

LPDISPATCH IModeler::CreateBsplineSurface(const VARIANT& props, const VARIANT& uKnots, const VARIANT& vKnots, const VARIANT& ctrlPtCoords)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&props, &uKnots, &vKnots, &ctrlPtCoords);
	return result;
}

LPDISPATCH IModeler::ICreateBsplineSurface(long* Properties, double* UKnotArray, double* VKnotArray, double* ControlPointCoordArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4 VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Properties, UKnotArray, VKnotArray, ControlPointCoordArray);
	return result;
}

LPDISPATCH IModeler::CreateOffsetSurface(LPDISPATCH surfaceIn, double distance)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		surfaceIn, distance);
	return result;
}

LPDISPATCH IModeler::ICreateOffsetSurface(LPDISPATCH surfaceIn, double distance)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		surfaceIn, distance);
	return result;
}

LPDISPATCH IModeler::CreateLine(const VARIANT& rootPoint, const VARIANT& direction)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&rootPoint, &direction);
	return result;
}

LPDISPATCH IModeler::ICreateLine(double* rootPoint, double* direction)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		rootPoint, direction);
	return result;
}

LPDISPATCH IModeler::CreateArc(const VARIANT& center, const VARIANT& axis, double radius, const VARIANT& startPoint, const VARIANT& endPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_R8 VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&center, &axis, radius, &startPoint, &endPoint);
	return result;
}

LPDISPATCH IModeler::ICreateArc(double* center, double* axis, double radius, double* startPoint, double* endPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_R8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		center, axis, radius, startPoint, endPoint);
	return result;
}

LPDISPATCH IModeler::CreateBsplineCurve(const VARIANT& props, const VARIANT& Knots, const VARIANT& ctrlPtCoords)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&props, &Knots, &ctrlPtCoords);
	return result;
}

LPDISPATCH IModeler::ICreateBsplineCurve(long* Properties, double* KnotArray, double* ControlPointCoordArray)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PI4 VTS_PR8 VTS_PR8;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		Properties, KnotArray, ControlPointCoordArray);
	return result;
}

LPDISPATCH IModeler::CreatePCurve(LPDISPATCH surface, const VARIANT& props, const VARIANT& Knots, const VARIANT& ctrlPtCoords)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_VARIANT VTS_VARIANT VTS_VARIANT;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		surface, &props, &Knots, &ctrlPtCoords);
	return result;
}

LPDISPATCH IModeler::ICreatePCurve(LPDISPATCH surface, long* props, double* Knots, double* ctrlPtCoords)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_PI4 VTS_PR8 VTS_PR8;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		surface, props, Knots, ctrlPtCoords);
	return result;
}

VARIANT IModeler::CreateBodiesFromSheets(const VARIANT& sheets, long options, long* error)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_VARIANT VTS_I4 VTS_PI4;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		&sheets, options, error);
	return result;
}

long IModeler::ICreateBodiesFromSheets(long nSheets, LPUNKNOWN* sheets, long options, long* nResults, LPUNKNOWN* results)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_PUNKNOWN VTS_I4 VTS_PI4 VTS_PUNKNOWN;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		nSheets, sheets, options, nResults, results);
	return result;
}

LPDISPATCH IModeler::ICreateBodyFromFaces(long NumOfFaces, LPDISPATCH* faces, BOOL doLocalCheck, BOOL* localCheckResult)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH VTS_BOOL VTS_PBOOL;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		NumOfFaces, faces, doLocalCheck, localCheckResult);
	return result;
}

double IModeler::FindTwoEdgeMaxDeviation(LPDISPATCH lpEdge1, LPDISPATCH lpEdge2)
{
	double result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		lpEdge1, lpEdge2);
	return result;
}

double IModeler::IFindTwoEdgeMaxDeviation(LPDISPATCH pEdge1, LPDISPATCH pEdge2)
{
	double result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		pEdge1, pEdge2);
	return result;
}

LPDISPATCH IModeler::CreateConicalSurface(const VARIANT& center, const VARIANT& direction, double radius, double semiAngle)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_R8 VTS_R8;
	InvokeHelper(0x26, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&center, &direction, radius, semiAngle);
	return result;
}

LPDISPATCH IModeler::ICreateConicalSurface(double* center, double* direction, double radius, double semiAngle)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_R8 VTS_R8;
	InvokeHelper(0x27, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		center, direction, radius, semiAngle);
	return result;
}

LPDISPATCH IModeler::CreateCylindricalSurface(const VARIANT& center, const VARIANT& direction, double radius)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_R8;
	InvokeHelper(0x28, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&center, &direction, radius);
	return result;
}

LPDISPATCH IModeler::ICreateCylindricalSurface(double* center, double* direction, double radius)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_R8;
	InvokeHelper(0x29, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		center, direction, radius);
	return result;
}

LPDISPATCH IModeler::CreateSphericalSurface(const VARIANT& center, double radius)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_R8;
	InvokeHelper(0x2a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&center, radius);
	return result;
}

LPDISPATCH IModeler::ICreateSphericalSurface(double* center, double radius)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_R8;
	InvokeHelper(0x2b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		center, radius);
	return result;
}

LPDISPATCH IModeler::CreateToroidalSurface(const VARIANT& center, const VARIANT& axis, const VARIANT& refDirection, double majorRadius, double minorRadius)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8 VTS_R8;
	InvokeHelper(0x2c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&center, &axis, &refDirection, majorRadius, minorRadius);
	return result;
}

LPDISPATCH IModeler::ICreateToroidalSurface(double* center, double* axis, double* refDirection, double majorRadius, double minorRadius)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8 VTS_R8 VTS_R8;
	InvokeHelper(0x2d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		center, axis, refDirection, majorRadius, minorRadius);
	return result;
}

LPDISPATCH IModeler::CreateBodyFromFaces2(long NumOfFaces, const VARIANT& faces, long actionType, BOOL doLocalCheck, BOOL* localCheckResult)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT VTS_I4 VTS_BOOL VTS_PBOOL;
	InvokeHelper(0x2e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		NumOfFaces, &faces, actionType, doLocalCheck, localCheckResult);
	return result;
}

LPDISPATCH IModeler::ICreateBodyFromFaces2(long NumOfFaces, LPDISPATCH* faces, long actionType, BOOL doLocalCheck, BOOL* localCheckResult)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH VTS_I4 VTS_BOOL VTS_PBOOL;
	InvokeHelper(0x2f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		NumOfFaces, faces, actionType, doLocalCheck, localCheckResult);
	return result;
}

double IModeler::SetToleranceValue(long ToleranceID, double NewToleranceValue)
{
	double result;
	static BYTE parms[] =
		VTS_I4 VTS_R8;
	InvokeHelper(0x30, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		ToleranceID, NewToleranceValue);
	return result;
}

double IModeler::GetToleranceValue(long ToleranceID)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x31, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		ToleranceID);
	return result;
}

LPDISPATCH IModeler::CreateSheetFromSurface(LPDISPATCH surfaceIn, const VARIANT& uvRange)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_VARIANT;
	InvokeHelper(0x32, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		surfaceIn, &uvRange);
	return result;
}

LPDISPATCH IModeler::ICreateSheetFromSurface(LPDISPATCH surfaceIn, double* uvRange)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_PR8;
	InvokeHelper(0x33, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		surfaceIn, uvRange);
	return result;
}

BOOL IModeler::ImprintingFaces(const VARIANT& targetFaceArray, const VARIANT& toolFaceArray, long options, VARIANT* targetEdges, VARIANT* toolEdges, VARIANT* targetVertices, VARIANT* toolVertices)
{
	BOOL result;
	static BYTE parms[] =
		VTS_VARIANT VTS_VARIANT VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT;
	InvokeHelper(0x34, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		&targetFaceArray, &toolFaceArray, options, targetEdges, toolEdges, targetVertices, toolVertices);
	return result;
}

void IModeler::IImprintingFaces(LPDISPATCH* targetEdges, LPDISPATCH* toolEdges, LPDISPATCH* targetVertices, LPDISPATCH* toolVertices)
{
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PDISPATCH VTS_PDISPATCH VTS_PDISPATCH;
	InvokeHelper(0x35, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 targetEdges, toolEdges, targetVertices, toolVertices);
}

BOOL IModeler::IImprintingFacesCount(long nTargetFaces, LPDISPATCH* targetFaceArray, long nToolFaces, LPDISPATCH* toolFaceArray, long options, long* nTargetEdges, long* ntoolEdges, long* ntargetVertices, long* toolVertices)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH VTS_I4 VTS_PDISPATCH VTS_I4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4;
	InvokeHelper(0x36, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nTargetFaces, targetFaceArray, nToolFaces, toolFaceArray, options, nTargetEdges, ntoolEdges, ntargetVertices, toolVertices);
	return result;
}

LPDISPATCH IModeler::CreateSweptSurface(LPDISPATCH curveIn, const VARIANT& dir)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_VARIANT;
	InvokeHelper(0x37, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		curveIn, &dir);
	return result;
}

LPDISPATCH IModeler::ICreateSweptSurface(LPDISPATCH curveIn, double* dir)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_PR8;
	InvokeHelper(0x38, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		curveIn, dir);
	return result;
}

BOOL IModeler::ReplaceSurfaces(long nFaces, const VARIANT& faceArray, const VARIANT& newSurfArray, const VARIANT& senseArray, double tolerance)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R8;
	InvokeHelper(0x39, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nFaces, &faceArray, &newSurfArray, &senseArray, tolerance);
	return result;
}

BOOL IModeler::IReplaceSurfaces(long nFaces, LPDISPATCH* faceArray, LPDISPATCH* newSurfArray, long* senseArray, double tolerance)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH VTS_PDISPATCH VTS_PI4 VTS_R8;
	InvokeHelper(0x3a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nFaces, faceArray, newSurfArray, senseArray, tolerance);
	return result;
}

VARIANT IModeler::SplitFaceOnParam(LPDISPATCH facedisp, long UVFlag, double Parameter, BOOL* status)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4 VTS_R8 VTS_PBOOL;
	InvokeHelper(0x3b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		facedisp, UVFlag, Parameter, status);
	return result;
}

long IModeler::ISplitFaceOnParamCount(LPDISPATCH facedisp, long UVFlag, double Parameter, BOOL* status)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4 VTS_R8 VTS_PBOOL;
	InvokeHelper(0x3c, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		facedisp, UVFlag, Parameter, status);
	return result;
}

LPDISPATCH IModeler::ISplitFaceOnParam()
{
	LPDISPATCH result;
	InvokeHelper(0x3d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL IModeler::CheckInterference(LPDISPATCH body1, LPDISPATCH body2, BOOL coincidentInterference, VARIANT* body1InterferedFaceArray, VARIANT* body2InterferedFaceArray, VARIANT* intersectedBodyArray)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_BOOL VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT;
	InvokeHelper(0x3e, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		body1, body2, coincidentInterference, body1InterferedFaceArray, body2InterferedFaceArray, intersectedBodyArray);
	return result;
}

BOOL IModeler::ICheckInterferenceCount(LPDISPATCH body1, LPDISPATCH body2, BOOL coincidentInterference, long* body1InterferedFaceCount, long* body2InterferedFaceCount, long* intersectedBodyCount)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_BOOL VTS_PI4 VTS_PI4 VTS_PI4;
	InvokeHelper(0x3f, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		body1, body2, coincidentInterference, body1InterferedFaceCount, body2InterferedFaceCount, intersectedBodyCount);
	return result;
}

void IModeler::ICheckInterference(LPDISPATCH* body1InterferedFaceArray, LPDISPATCH* body2InterferedFaceArray, LPDISPATCH* intersectedBodyArray)
{
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PDISPATCH VTS_PDISPATCH;
	InvokeHelper(0x40, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 body1InterferedFaceArray, body2InterferedFaceArray, intersectedBodyArray);
}


/////////////////////////////////////////////////////////////////////////////
// IEnvironment properties

/////////////////////////////////////////////////////////////////////////////
// IEnvironment operations

VARIANT IEnvironment::GetSymEdgeCounts(LPCTSTR symId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symId);
	return result;
}

short IEnvironment::IGetSymEdgeCounts(LPCTSTR symId)
{
	short result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		symId);
	return result;
}

VARIANT IEnvironment::GetSymLines(LPCTSTR symId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symId);
	return result;
}

double IEnvironment::IGetSymLines(LPCTSTR symId)
{
	double result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		symId);
	return result;
}

VARIANT IEnvironment::GetSymArcs(LPCTSTR symId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symId);
	return result;
}

double IEnvironment::IGetSymArcs(LPCTSTR symId)
{
	double result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		symId);
	return result;
}

VARIANT IEnvironment::GetSymCircles(LPCTSTR symId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symId);
	return result;
}

double IEnvironment::IGetSymCircles(LPCTSTR symId)
{
	double result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		symId);
	return result;
}

VARIANT IEnvironment::GetSymTextPoints(LPCTSTR symId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symId);
	return result;
}

double IEnvironment::IGetSymTextPoints(LPCTSTR symId)
{
	double result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		symId);
	return result;
}

VARIANT IEnvironment::GetSymText(LPCTSTR symId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symId);
	return result;
}

VARIANT IEnvironment::GetSymTriangles(LPCTSTR symId)
{
	VARIANT result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
		symId);
	return result;
}

double IEnvironment::IGetSymTriangles(LPCTSTR symId)
{
	double result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		symId);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IMathUtility properties

/////////////////////////////////////////////////////////////////////////////
// IMathUtility operations

LPDISPATCH IMathUtility::CreateTransform(const VARIANT& ArrayDataIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&ArrayDataIn);
	return result;
}

LPDISPATCH IMathUtility::ICreateTransform(double* ArrayDataIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ArrayDataIn);
	return result;
}

LPDISPATCH IMathUtility::CreateTransformRotateAxis(LPDISPATCH pointObjIn, LPDISPATCH vectorObjIn, double angle)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_R8;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		pointObjIn, vectorObjIn, angle);
	return result;
}

LPDISPATCH IMathUtility::ICreateTransformRotateAxis(LPDISPATCH pointObjIn, LPDISPATCH vectorObjIn, double angle)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_R8;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		pointObjIn, vectorObjIn, angle);
	return result;
}

LPDISPATCH IMathUtility::CreatePoint(const VARIANT& ArrayDataIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&ArrayDataIn);
	return result;
}

LPDISPATCH IMathUtility::ICreatePoint(double* ArrayDataIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ArrayDataIn);
	return result;
}

LPDISPATCH IMathUtility::CreateVector(const VARIANT& ArrayDataIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		&ArrayDataIn);
	return result;
}

LPDISPATCH IMathUtility::ICreateVector(double* ArrayDataIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		ArrayDataIn);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IMathTransform properties

/////////////////////////////////////////////////////////////////////////////
// IMathTransform operations

LPDISPATCH IMathTransform::Multiply(LPDISPATCH TransformObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		TransformObjIn);
	return result;
}

LPDISPATCH IMathTransform::IMultiply(LPDISPATCH TransformObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		TransformObjIn);
	return result;
}

VARIANT IMathTransform::GetArrayData()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IMathTransform::SetArrayData(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IMathTransform::GetIArrayData()
{
	double result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IMathTransform::SetIArrayData(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

void IMathTransform::GetData(LPDISPATCH* xAxisObjOut, LPDISPATCH* yAxisObjOut, LPDISPATCH* zAxisObjOut, LPDISPATCH* TransformObjOut, double* scaleOut)
{
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PDISPATCH VTS_PDISPATCH VTS_PDISPATCH VTS_PR8;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 xAxisObjOut, yAxisObjOut, zAxisObjOut, TransformObjOut, scaleOut);
}

void IMathTransform::IGetData(LPDISPATCH* xAxisObjOut, LPDISPATCH* yAxisObjOut, LPDISPATCH* zAxisObjOut, LPDISPATCH* TransformObjOut, double* scaleOut)
{
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PDISPATCH VTS_PDISPATCH VTS_PDISPATCH VTS_PR8;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 xAxisObjOut, yAxisObjOut, zAxisObjOut, TransformObjOut, scaleOut);
}

void IMathTransform::SetData(LPDISPATCH xAxisObjIn, LPDISPATCH yAxisObjIn, LPDISPATCH zAxisObjIn, LPDISPATCH TransformObjIn, double scaleValIn)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_DISPATCH VTS_DISPATCH VTS_R8;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 xAxisObjIn, yAxisObjIn, zAxisObjIn, TransformObjIn, scaleValIn);
}

void IMathTransform::ISetData(LPDISPATCH xAxisObjIn, LPDISPATCH yAxisObjIn, LPDISPATCH zAxisObjIn, LPDISPATCH TransformObjIn, double scaleValIn)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH VTS_DISPATCH VTS_DISPATCH VTS_R8;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 xAxisObjIn, yAxisObjIn, zAxisObjIn, TransformObjIn, scaleValIn);
}

LPDISPATCH IMathTransform::Inverse()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMathTransform::IInverse()
{
	LPDISPATCH result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IMathVector properties

/////////////////////////////////////////////////////////////////////////////
// IMathVector operations

LPDISPATCH IMathVector::MultiplyTransform(LPDISPATCH TransformObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		TransformObjIn);
	return result;
}

LPDISPATCH IMathVector::IMultiplyTransform(LPDISPATCH TransformObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		TransformObjIn);
	return result;
}

VARIANT IMathVector::GetArrayData()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IMathVector::SetArrayData(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IMathVector::GetIArrayData()
{
	double result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IMathVector::SetIArrayData(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH IMathVector::Add(LPDISPATCH vectorObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathVector::IAdd(LPDISPATCH vectorObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathVector::Subtract(LPDISPATCH vectorObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathVector::ISubtract(LPDISPATCH vectorObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathVector::Scale(double valueIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		valueIn);
	return result;
}

LPDISPATCH IMathVector::IScale(double valueIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		valueIn);
	return result;
}

double IMathVector::GetLength()
{
	double result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

double IMathVector::Dot(LPDISPATCH vectorObjIn)
{
	double result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		vectorObjIn);
	return result;
}

double IMathVector::IDot(LPDISPATCH vectorObjIn)
{
	double result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathVector::Cross(LPDISPATCH vectorObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathVector::ICross(LPDISPATCH vectorObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathVector::ConvertToPoint()
{
	LPDISPATCH result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMathVector::IConvertToPoint()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IMathPoint properties

/////////////////////////////////////////////////////////////////////////////
// IMathPoint operations

LPDISPATCH IMathPoint::MultiplyTransform(LPDISPATCH TransformObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		TransformObjIn);
	return result;
}

LPDISPATCH IMathPoint::IMultiplyTransform(LPDISPATCH TransformObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		TransformObjIn);
	return result;
}

VARIANT IMathPoint::GetArrayData()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IMathPoint::SetArrayData(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

double IMathPoint::GetIArrayData()
{
	double result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IMathPoint::SetIArrayData(double* newValue)
{
	static BYTE parms[] =
		VTS_PR8;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH IMathPoint::AddVector(LPDISPATCH vectorObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathPoint::IAddVector(LPDISPATCH vectorObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathPoint::SubtractVector(LPDISPATCH vectorObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathPoint::ISubtractVector(LPDISPATCH vectorObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		vectorObjIn);
	return result;
}

LPDISPATCH IMathPoint::Subtract(LPDISPATCH pointObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		pointObjIn);
	return result;
}

LPDISPATCH IMathPoint::ISubtract(LPDISPATCH pointObjIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		pointObjIn);
	return result;
}

LPDISPATCH IMathPoint::Scale(double valueIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		valueIn);
	return result;
}

LPDISPATCH IMathPoint::IScale(double valueIn)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		valueIn);
	return result;
}

LPDISPATCH IMathPoint::ConvertToVector()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMathPoint::IConvertToVector()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IRefAxis properties

/////////////////////////////////////////////////////////////////////////////
// IRefAxis operations

VARIANT IRefAxis::GetRefAxisParams()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IRefAxis::IGetRefAxisParams()
{
	double result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IMate properties

/////////////////////////////////////////////////////////////////////////////
// IMate operations

VARIANT IMate::GetMateParams()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IMate::IGetMateParams(long* mateType, long* alignFlag, long* canBeFlipped)
{
	static BYTE parms[] =
		VTS_PI4 VTS_PI4 VTS_PI4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 mateType, alignFlag, canBeFlipped);
}

VARIANT IMate::GetMateDimensionValue()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IMate::IGetMateDimensionValue()
{
	double result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT IMate::GetMateEntities()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IMate::IGetMateEntities(LPDISPATCH* entity1, LPDISPATCH* entity2)
{
	static BYTE parms[] =
		VTS_PDISPATCH VTS_PDISPATCH;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 entity1, entity2);
}

LPDISPATCH IMate::GetEntity(long whichOne)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		whichOne);
	return result;
}

LPDISPATCH IMate::IGetEntity(long whichOne)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		whichOne);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IMateEntity properties

/////////////////////////////////////////////////////////////////////////////
// IMateEntity operations

LPDISPATCH IMateEntity::GetMember()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMateEntity::IGetMember()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long IMateEntity::GetEntityType()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IMateEntity::GetEntityParams()
{
	VARIANT result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double IMateEntity::IGetEntityParams()
{
	double result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMateEntity::GetComponent()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMateEntity::IGetComponent()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString IMateEntity::GetComponentName()
{
	CString result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISWPropertySheet properties

/////////////////////////////////////////////////////////////////////////////
// ISWPropertySheet operations

long ISWPropertySheet::AddPage(long page)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		page);
	return result;
}

long ISWPropertySheet::RemovePage(long page)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		page);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IRibFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IRibFeatureData operations

BOOL IRibFeatureData::GetIsTwoSided()
{
	BOOL result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IRibFeatureData::SetIsTwoSided(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IRibFeatureData::GetReverseThicknessDir()
{
	BOOL result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IRibFeatureData::SetReverseThicknessDir(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double IRibFeatureData::GetThickness()
{
	double result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IRibFeatureData::SetThickness(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IRibFeatureData::GetRefSketchIndex()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IRibFeatureData::SetRefSketchIndex(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IRibFeatureData::NextReference()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL IRibFeatureData::GetFlipSide()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IRibFeatureData::SetFlipSide(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IRibFeatureData::GetEnableDraft()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IRibFeatureData::SetEnableDraft(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IRibFeatureData::GetDraftOutward()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IRibFeatureData::SetDraftOutward(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double IRibFeatureData::GetDraftAngle()
{
	double result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IRibFeatureData::SetDraftAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// IDomeFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IDomeFeatureData operations

double IDomeFeatureData::GetHeight()
{
	double result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IDomeFeatureData::SetHeight(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IDomeFeatureData::GetReverseDir()
{
	BOOL result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDomeFeatureData::SetReverseDir(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDomeFeatureData::GetElliptical()
{
	BOOL result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDomeFeatureData::SetElliptical(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IDomeFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IDomeFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IDomeFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IDomeFeatureData::GetFace()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IDomeFeatureData::SetFace(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH IDomeFeatureData::GetIFace()
{
	LPDISPATCH result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IDomeFeatureData::SetIFace(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// ISketchLine properties

/////////////////////////////////////////////////////////////////////////////
// ISketchLine operations

VARIANT ISketchLine::GetStartPoint()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchLine::IGetStartPoint()
{
	double result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchLine::GetEndPoint()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchLine::IGetEndPoint()
{
	double result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchLine::GetStartPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchLine::IGetStartPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchLine::GetEndPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchLine::IGetEndPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISketchArc properties

/////////////////////////////////////////////////////////////////////////////
// ISketchArc operations

VARIANT ISketchArc::GetStartPoint()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchArc::IGetStartPoint()
{
	double result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchArc::GetEndPoint()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchArc::IGetEndPoint()
{
	double result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchArc::GetCenterPoint()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchArc::IGetCenterPoint()
{
	double result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

long ISketchArc::IsCircle()
{
	long result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ISketchArc::GetRotationDir()
{
	long result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchArc::GetStartPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchArc::IGetStartPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchArc::GetEndPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchArc::IGetEndPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchArc::GetCenterPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchArc::IGetCenterPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

double ISketchArc::GetRadius()
{
	double result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL ISketchArc::SetRadius(double radius)
{
	BOOL result;
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		radius);
	return result;
}

VARIANT ISketchArc::GetNormalVector()
{
	VARIANT result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchArc::IGetNormalVector()
{
	double result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISketchText properties

/////////////////////////////////////////////////////////////////////////////
// ISketchText operations

VARIANT ISketchText::GetEdges()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN ISketchText::EnumEdges()
{
	LPUNKNOWN result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISketchEllipse properties

/////////////////////////////////////////////////////////////////////////////
// ISketchEllipse operations

VARIANT ISketchEllipse::GetStartPoint()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchEllipse::IGetStartPoint()
{
	double result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchEllipse::GetEndPoint()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchEllipse::IGetEndPoint()
{
	double result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchEllipse::GetCenterPoint()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchEllipse::IGetCenterPoint()
{
	double result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchEllipse::GetMajorPoint()
{
	VARIANT result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchEllipse::IGetMajorPoint()
{
	double result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchEllipse::GetMinorPoint()
{
	VARIANT result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchEllipse::IGetMinorPoint()
{
	double result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchEllipse::GetStartPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchEllipse::IGetStartPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchEllipse::GetEndPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchEllipse::IGetEndPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchEllipse::GetCenterPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchEllipse::IGetCenterPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchEllipse::GetMajorPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchEllipse::IGetMajorPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchEllipse::GetMinorPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchEllipse::IGetMinorPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISketchParabola properties

/////////////////////////////////////////////////////////////////////////////
// ISketchParabola operations

VARIANT ISketchParabola::GetStartPoint()
{
	VARIANT result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchParabola::IGetStartPoint()
{
	double result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchParabola::GetEndPoint()
{
	VARIANT result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchParabola::IGetEndPoint()
{
	double result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchParabola::GetFocalPoint()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchParabola::IGetFocalPoint()
{
	double result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchParabola::GetApexPoint()
{
	VARIANT result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchParabola::IGetApexPoint()
{
	double result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchParabola::GetStartPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchParabola::IGetStartPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchParabola::GetEndPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchParabola::IGetEndPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchParabola::GetFocalPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchParabola::IGetFocalPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchParabola::GetApexPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchParabola::IGetApexPoint2()
{
	LPDISPATCH result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISketchSpline properties

/////////////////////////////////////////////////////////////////////////////
// ISketchSpline operations

long ISketchSpline::GetPointCount()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT ISketchSpline::GetPoints()
{
	VARIANT result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchSpline::IGetPoints()
{
	double result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

VARIANT ISketchSpline::GetPoints2()
{
	VARIANT result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

LPUNKNOWN ISketchSpline::IEnumPoints()
{
	LPUNKNOWN result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ILightDialog properties

/////////////////////////////////////////////////////////////////////////////
// ILightDialog operations

BOOL ILightDialog::AddSubDialog(long page)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		page);
	return result;
}

long ILightDialog::GetLightId()
{
	long result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISimpleHoleFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// ISimpleHoleFeatureData operations

BOOL ISimpleHoleFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL ISimpleHoleFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void ISimpleHoleFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL ISimpleHoleFeatureData::GetReverseDirection()
{
	BOOL result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ISimpleHoleFeatureData::GetDraftWhileExtruding()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetDraftWhileExtruding(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ISimpleHoleFeatureData::GetDraftOutward()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetDraftOutward(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ISimpleHoleFeatureData::GetReverseOffset()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetReverseOffset(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

long ISimpleHoleFeatureData::GetType()
{
	long result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

double ISimpleHoleFeatureData::GetDiameter()
{
	double result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ISimpleHoleFeatureData::GetDepth()
{
	double result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ISimpleHoleFeatureData::GetDraftAngle()
{
	double result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetDraftAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ISimpleHoleFeatureData::GetSurfaceOffset()
{
	double result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetSurfaceOffset(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH ISimpleHoleFeatureData::GetFace()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetFace(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH ISimpleHoleFeatureData::GetIFace()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetIFace(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH ISimpleHoleFeatureData::GetVertex()
{
	LPDISPATCH result;
	InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetVertex(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH ISimpleHoleFeatureData::GetIVertex()
{
	LPDISPATCH result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ISimpleHoleFeatureData::SetIVertex(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// IWizardHoleFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IWizardHoleFeatureData operations

BOOL IWizardHoleFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IWizardHoleFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IWizardHoleFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IWizardHoleFeatureData::GetType()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

double IWizardHoleFeatureData::GetDiameter()
{
	double result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetCounterBoreDiameter()
{
	double result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetCounterBoreDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetCounterDrillDiameter()
{
	double result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetCounterDrillDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetCounterSinkDiameter()
{
	double result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetCounterSinkDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetMinorDiameter()
{
	double result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetMinorDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetMajorDiameter()
{
	double result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetMajorDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetHoleDiameter()
{
	double result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetHoleDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetThruHoleDiameter()
{
	double result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetThruHoleDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetTapDrillDiameter()
{
	double result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetTapDrillDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetThruTapDrillDiameter()
{
	double result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetThruTapDrillDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetNearCounterSinkDiameter()
{
	double result;
	InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetNearCounterSinkDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetMidCounterSinkDiameter()
{
	double result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetMidCounterSinkDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetFarCounterSinkDiameter()
{
	double result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetFarCounterSinkDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetThreadDiameter()
{
	double result;
	InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetThreadDiameter(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetDepth()
{
	double result;
	InvokeHelper(0x13, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x13, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetCounterBoreDepth()
{
	double result;
	InvokeHelper(0x14, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetCounterBoreDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x14, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetCounterDrillDepth()
{
	double result;
	InvokeHelper(0x15, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetCounterDrillDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x15, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetHoleDepth()
{
	double result;
	InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetHoleDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetThruHoleDepth()
{
	double result;
	InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetThruHoleDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetTapDrillDepth()
{
	double result;
	InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetTapDrillDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x18, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetThruTapDrillDepth()
{
	double result;
	InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetThruTapDrillDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetThreadDepth()
{
	double result;
	InvokeHelper(0x1a, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetThreadDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetCounterDrillAngle()
{
	double result;
	InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetCounterDrillAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetCounterSinkAngle()
{
	double result;
	InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetCounterSinkAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetDrillAngle()
{
	double result;
	InvokeHelper(0x1d, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetDrillAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetNearCounterSinkAngle()
{
	double result;
	InvokeHelper(0x1e, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetNearCounterSinkAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetMidCounterSinkAngle()
{
	double result;
	InvokeHelper(0x1f, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetMidCounterSinkAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetFarCounterSinkAngle()
{
	double result;
	InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetFarCounterSinkAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x20, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetThreadAngle()
{
	double result;
	InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetThreadAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x21, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IWizardHoleFeatureData::GetHeadClearance()
{
	double result;
	InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetHeadClearance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x22, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH IWizardHoleFeatureData::GetFace()
{
	LPDISPATCH result;
	InvokeHelper(0x23, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetFace(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x23, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH IWizardHoleFeatureData::GetIFace()
{
	LPDISPATCH result;
	InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetIFace(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH IWizardHoleFeatureData::GetVertex()
{
	LPDISPATCH result;
	InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetVertex(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH IWizardHoleFeatureData::GetIVertex()
{
	LPDISPATCH result;
	InvokeHelper(0x26, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetIVertex(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x26, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IWizardHoleFeatureData::GetEndCondition()
{
	long result;
	InvokeHelper(0x27, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetEndCondition(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x27, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

CString IWizardHoleFeatureData::GetStandard()
{
	CString result;
	InvokeHelper(0x28, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetStandard(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x28, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString IWizardHoleFeatureData::GetFastenerType()
{
	CString result;
	InvokeHelper(0x29, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetFastenerType(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x29, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

CString IWizardHoleFeatureData::GetFastenerSize()
{
	CString result;
	InvokeHelper(0x2a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IWizardHoleFeatureData::SetFastenerSize(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// IChamferFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IChamferFeatureData operations

double IChamferFeatureData::GetEdgeChamferDistance(long side)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		side);
	return result;
}

void IChamferFeatureData::SetEdgeChamferDistance(long side, double distance)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 side, distance);
}

double IChamferFeatureData::GetVertexChamferDistance(long side)
{
	double result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		side);
	return result;
}

void IChamferFeatureData::SetVertexChamferDistance(long side, double distance)
{
	static BYTE parms[] =
		VTS_I4 VTS_R8;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 side, distance);
}

long IChamferFeatureData::GetType()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IChamferFeatureData::SetType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

double IChamferFeatureData::GetEdgeChamferAngle()
{
	double result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IChamferFeatureData::SetEdgeChamferAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// IDraftFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IDraftFeatureData operations

long IDraftFeatureData::GetType()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

double IDraftFeatureData::GetAngle()
{
	double result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IDraftFeatureData::SetAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IDraftFeatureData::GetReverseDirection()
{
	BOOL result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IDraftFeatureData::SetReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// ISimpleFilletFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// ISimpleFilletFeatureData operations

long ISimpleFilletFeatureData::GetType()
{
	long result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

double ISimpleFilletFeatureData::GetDefaultRadius()
{
	double result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISimpleFilletFeatureData::SetDefaultRadius(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ISimpleFilletFeatureData::GetOverFlowType()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISimpleFilletFeatureData::SetOverFlowType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL ISimpleFilletFeatureData::GetIsMultipleRadius()
{
	BOOL result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISimpleFilletFeatureData::SetIsMultipleRadius(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ISimpleFilletFeatureData::GetRoundCorners()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISimpleFilletFeatureData::SetRoundCorners(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ISimpleFilletFeatureData::GetPropagateToTangentFaces()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISimpleFilletFeatureData::SetPropagateToTangentFaces(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

long ISimpleFilletFeatureData::GetFilletItemsCount()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISimpleFilletFeatureData::GetFilletItemAtIndex(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

LPUNKNOWN ISimpleFilletFeatureData::IGetFilletItemAtIndex(long index)
{
	LPUNKNOWN result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, parms,
		index);
	return result;
}

double ISimpleFilletFeatureData::GetRadius(LPDISPATCH pFilletItem)
{
	double result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		pFilletItem);
	return result;
}

double ISimpleFilletFeatureData::IGetRadius(LPUNKNOWN pFilletItem)
{
	double result;
	static BYTE parms[] =
		VTS_UNKNOWN;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		pFilletItem);
	return result;
}

void ISimpleFilletFeatureData::SetRadius(LPDISPATCH pFilletItem, double radius)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 pFilletItem, radius);
}

void ISimpleFilletFeatureData::ISetRadius(LPUNKNOWN pFilletItem, double radius)
{
	static BYTE parms[] =
		VTS_UNKNOWN VTS_R8;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 pFilletItem, radius);
}

BOOL ISimpleFilletFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL ISimpleFilletFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void ISimpleFilletFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// IVariableFilletFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IVariableFilletFeatureData operations

double IVariableFilletFeatureData::GetDefaultRadius()
{
	double result;
	InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IVariableFilletFeatureData::SetDefaultRadius(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IVariableFilletFeatureData::GetOverFlowType()
{
	long result;
	InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IVariableFilletFeatureData::SetOverFlowType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IVariableFilletFeatureData::GetTransitionType()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IVariableFilletFeatureData::SetTransitionType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IVariableFilletFeatureData::GetPropagateToTangentFaces()
{
	BOOL result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IVariableFilletFeatureData::SetPropagateToTangentFaces(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

long IVariableFilletFeatureData::GetFilletEdgeCount()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IVariableFilletFeatureData::GetFilletEdgeAtIndex(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

LPDISPATCH IVariableFilletFeatureData::IGetFilletEdgeAtIndex(long index)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		index);
	return result;
}

double IVariableFilletFeatureData::GetRadius(LPDISPATCH pFilletItem)
{
	double result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		pFilletItem);
	return result;
}

double IVariableFilletFeatureData::IGetRadius(LPDISPATCH pFilletItem)
{
	double result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		pFilletItem);
	return result;
}

void IVariableFilletFeatureData::SetRadius(LPDISPATCH pFilletItem, double radius)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 pFilletItem, radius);
}

void IVariableFilletFeatureData::ISetRadius(LPDISPATCH pFilletItem, double radius)
{
	static BYTE parms[] =
		VTS_DISPATCH VTS_R8;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 pFilletItem, radius);
}

BOOL IVariableFilletFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IVariableFilletFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IVariableFilletFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0xe, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// IExtrudeFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IExtrudeFeatureData operations

long IExtrudeFeatureData::GetEndCondition(BOOL forward)
{
	long result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		forward);
	return result;
}

void IExtrudeFeatureData::SetEndCondition(BOOL forward, long endCondition)
{
	static BYTE parms[] =
		VTS_BOOL VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, endCondition);
}

double IExtrudeFeatureData::GetDepth(BOOL forward)
{
	double result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		forward);
	return result;
}

void IExtrudeFeatureData::SetDepth(BOOL forward, double depth)
{
	static BYTE parms[] =
		VTS_BOOL VTS_R8;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, depth);
}

double IExtrudeFeatureData::GetWallThickness(BOOL forward)
{
	double result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		forward);
	return result;
}

void IExtrudeFeatureData::SetWallThickness(BOOL forward, double wallThickness)
{
	static BYTE parms[] =
		VTS_BOOL VTS_R8;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, wallThickness);
}

BOOL IExtrudeFeatureData::GetDraftWhileExtruding(BOOL forward)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		forward);
	return result;
}

void IExtrudeFeatureData::SetDraftWhileExtruding(BOOL forward, BOOL draftWhileExtrude)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, draftWhileExtrude);
}

BOOL IExtrudeFeatureData::GetDraftOutward(BOOL forward)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		forward);
	return result;
}

void IExtrudeFeatureData::SetDraftOutward(BOOL forward, BOOL draftOutward)
{
	static BYTE parms[] =
		VTS_BOOL VTS_BOOL;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, draftOutward);
}

double IExtrudeFeatureData::GetDraftAngle(BOOL forward)
{
	double result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		forward);
	return result;
}

void IExtrudeFeatureData::SetDraftAngle(BOOL forward, double draftAngle)
{
	static BYTE parms[] =
		VTS_BOOL VTS_R8;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, draftAngle);
}

BOOL IExtrudeFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IExtrudeFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IExtrudeFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0xf, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IExtrudeFeatureData::GetFace(BOOL forward)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		forward);
	return result;
}

void IExtrudeFeatureData::SetFace(BOOL forward, LPDISPATCH face)
{
	static BYTE parms[] =
		VTS_BOOL VTS_DISPATCH;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, face);
}

LPDISPATCH IExtrudeFeatureData::IGetFace(BOOL forward)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		forward);
	return result;
}

void IExtrudeFeatureData::ISetFace(BOOL forward, LPDISPATCH face)
{
	static BYTE parms[] =
		VTS_BOOL VTS_DISPATCH;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, face);
}

LPDISPATCH IExtrudeFeatureData::GetVertex(BOOL forward)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		forward);
	return result;
}

void IExtrudeFeatureData::SetVertex(BOOL forward, LPDISPATCH face)
{
	static BYTE parms[] =
		VTS_BOOL VTS_DISPATCH;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, face);
}

LPDISPATCH IExtrudeFeatureData::IGetVertex(BOOL forward)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		forward);
	return result;
}

void IExtrudeFeatureData::ISetVertex(BOOL forward, LPDISPATCH face)
{
	static BYTE parms[] =
		VTS_BOOL VTS_DISPATCH;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, face);
}

BOOL IExtrudeFeatureData::GetReverseDirection()
{
	BOOL result;
	InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IExtrudeFeatureData::SetReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x18, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IExtrudeFeatureData::GetBothDirections()
{
	BOOL result;
	InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IExtrudeFeatureData::SetBothDirections(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IExtrudeFeatureData::GetFlipSideToCut()
{
	BOOL result;
	InvokeHelper(0x1a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IExtrudeFeatureData::SetFlipSideToCut(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IExtrudeFeatureData::IsBossFeature()
{
	BOOL result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IExtrudeFeatureData::IsThinFeature()
{
	BOOL result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IExtrudeFeatureData::IsBaseExtrude()
{
	BOOL result;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long IExtrudeFeatureData::GetThinWallType()
{
	long result;
	InvokeHelper(0x1e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IExtrudeFeatureData::SetThinWallType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IExtrudeFeatureData::GetCapEnds()
{
	BOOL result;
	InvokeHelper(0x1f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IExtrudeFeatureData::SetCapEnds(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double IExtrudeFeatureData::GetCapThickness()
{
	double result;
	InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IExtrudeFeatureData::SetCapThickness(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x20, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// IRevolveFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IRevolveFeatureData operations

double IRevolveFeatureData::GetRevolutionAngle(BOOL forward)
{
	double result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		forward);
	return result;
}

void IRevolveFeatureData::SetRevolutionAngle(BOOL forward, double angle)
{
	static BYTE parms[] =
		VTS_BOOL VTS_R8;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, angle);
}

double IRevolveFeatureData::GetWallThickness(BOOL forward)
{
	double result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
		forward);
	return result;
}

void IRevolveFeatureData::SetWallThickness(BOOL forward, double wallThickness)
{
	static BYTE parms[] =
		VTS_BOOL VTS_R8;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 forward, wallThickness);
}

long IRevolveFeatureData::GetType()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IRevolveFeatureData::SetType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IRevolveFeatureData::GetReverseDirection()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IRevolveFeatureData::SetReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IRevolveFeatureData::IsBossFeature()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL IRevolveFeatureData::IsThinFeature()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// IMirrorPatternFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IMirrorPatternFeatureData operations

BOOL IMirrorPatternFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IMirrorPatternFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IMirrorPatternFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IMirrorPatternFeatureData::GetPlane()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IMirrorPatternFeatureData::SetPlane(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IMirrorPatternFeatureData::GetMirrorPlaneType()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

VARIANT IMirrorPatternFeatureData::GetPatternFeatureArray()
{
	VARIANT result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IMirrorPatternFeatureData::SetPatternFeatureArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long IMirrorPatternFeatureData::GetPatternFeatureCount()
{
	long result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMirrorPatternFeatureData::IGetPatternFeatureArray()
{
	LPDISPATCH result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IMirrorPatternFeatureData::ISetPatternFeatureArray(long featCount, LPDISPATCH* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}

BOOL IMirrorPatternFeatureData::GetGeometryPattern()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IMirrorPatternFeatureData::SetGeometryPattern(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// ICircularPatternFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// ICircularPatternFeatureData operations

BOOL ICircularPatternFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL ICircularPatternFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void ICircularPatternFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH ICircularPatternFeatureData::GetAxis()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::SetAxis(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ICircularPatternFeatureData::GetAxisType()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ICircularPatternFeatureData::GetReverseDirection()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::SetReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double ICircularPatternFeatureData::GetSpacing()
{
	double result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::SetSpacing(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ICircularPatternFeatureData::GetTotalInstances()
{
	long result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::SetTotalInstances(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL ICircularPatternFeatureData::GetEqualSpacing()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::SetEqualSpacing(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ICircularPatternFeatureData::GetVarySketch()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::SetVarySketch(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ICircularPatternFeatureData::GetGeometryPattern()
{
	BOOL result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::SetGeometryPattern(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

VARIANT ICircularPatternFeatureData::GetPatternFeatureArray()
{
	VARIANT result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::SetPatternFeatureArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ICircularPatternFeatureData::GetPatternFeatureCount()
{
	long result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH ICircularPatternFeatureData::IGetPatternFeatureArray()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::ISetPatternFeatureArray(long featCount, LPDISPATCH* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}

VARIANT ICircularPatternFeatureData::GetSkippedItemArray()
{
	VARIANT result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::SetSkippedItemArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ICircularPatternFeatureData::GetSkippedItemCount()
{
	long result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ICircularPatternFeatureData::IGetSkippedItemArray()
{
	long result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void ICircularPatternFeatureData::ISetSkippedItemArray(long featCount, long* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}


/////////////////////////////////////////////////////////////////////////////
// ILinearPatternFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// ILinearPatternFeatureData operations

BOOL ILinearPatternFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL ILinearPatternFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void ILinearPatternFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH ILinearPatternFeatureData::GetD1Axis()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetD1Axis(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH ILinearPatternFeatureData::GetD2Axis()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetD2Axis(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ILinearPatternFeatureData::GetD1AxisType()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ILinearPatternFeatureData::GetD2AxisType()
{
	long result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ILinearPatternFeatureData::GetD1ReverseDirection()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetD1ReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ILinearPatternFeatureData::GetD2ReverseDirection()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetD2ReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double ILinearPatternFeatureData::GetD1Spacing()
{
	double result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetD1Spacing(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ILinearPatternFeatureData::GetD2Spacing()
{
	double result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetD2Spacing(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ILinearPatternFeatureData::GetD1TotalInstances()
{
	long result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetD1TotalInstances(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long ILinearPatternFeatureData::GetD2TotalInstances()
{
	long result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetD2TotalInstances(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL ILinearPatternFeatureData::GetVarySketch()
{
	BOOL result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetVarySketch(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ILinearPatternFeatureData::GetGeometryPattern()
{
	BOOL result;
	InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetGeometryPattern(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

VARIANT ILinearPatternFeatureData::GetPatternFeatureArray()
{
	VARIANT result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetPatternFeatureArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ILinearPatternFeatureData::GetPatternFeatureCount()
{
	long result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILinearPatternFeatureData::IGetPatternFeatureArray()
{
	LPDISPATCH result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::ISetPatternFeatureArray(long featCount, LPDISPATCH* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}

VARIANT ILinearPatternFeatureData::GetSkippedItemArray()
{
	VARIANT result;
	InvokeHelper(0x14, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::SetSkippedItemArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x14, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ILinearPatternFeatureData::GetSkippedItemCount()
{
	long result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ILinearPatternFeatureData::IGetSkippedItemArray()
{
	long result;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void ILinearPatternFeatureData::ISetSkippedItemArray(long featCount, long* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}


/////////////////////////////////////////////////////////////////////////////
// ITablePatternFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// ITablePatternFeatureData operations

BOOL ITablePatternFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL ITablePatternFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void ITablePatternFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH ITablePatternFeatureData::GetCoordinateSystem()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ITablePatternFeatureData::SetCoordinateSystem(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH ITablePatternFeatureData::GetReferencePoint()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ITablePatternFeatureData::SetReferencePoint(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ITablePatternFeatureData::GetReferencePointType()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ITablePatternFeatureData::GetUseCentroid()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITablePatternFeatureData::SetUseCentroid(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ITablePatternFeatureData::SavePointsToFile(LPCTSTR fileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName);
	return result;
}

BOOL ITablePatternFeatureData::LoadPointsFromFile(LPCTSTR fileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName);
	return result;
}

VARIANT ITablePatternFeatureData::GetBasePoint()
{
	VARIANT result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ITablePatternFeatureData::IGetBasePoint()
{
	double result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL ITablePatternFeatureData::GetGeometryPattern()
{
	BOOL result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ITablePatternFeatureData::SetGeometryPattern(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

VARIANT ITablePatternFeatureData::GetPatternFeatureArray()
{
	VARIANT result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ITablePatternFeatureData::SetPatternFeatureArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ITablePatternFeatureData::GetPatternFeatureCount()
{
	long result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH ITablePatternFeatureData::IGetPatternFeatureArray()
{
	LPDISPATCH result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ITablePatternFeatureData::ISetPatternFeatureArray(long featCount, LPDISPATCH* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}

VARIANT ITablePatternFeatureData::GetPointArray()
{
	VARIANT result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ITablePatternFeatureData::SetPointArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ITablePatternFeatureData::GetPointCount()
{
	long result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

double ITablePatternFeatureData::IGetPointArray()
{
	double result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

void ITablePatternFeatureData::ISetPointArray(long featCount, double* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PR8;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}


/////////////////////////////////////////////////////////////////////////////
// ISketchPatternFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// ISketchPatternFeatureData operations

BOOL ISketchPatternFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL ISketchPatternFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void ISketchPatternFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH ISketchPatternFeatureData::GetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ISketchPatternFeatureData::SetSketch(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH ISketchPatternFeatureData::GetReferencePoint()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ISketchPatternFeatureData::SetReferencePoint(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ISketchPatternFeatureData::GetReferencePointType()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ISketchPatternFeatureData::GetUseCentroid()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISketchPatternFeatureData::SetUseCentroid(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

VARIANT ISketchPatternFeatureData::GetBasePoint()
{
	VARIANT result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
	return result;
}

double ISketchPatternFeatureData::IGetBasePoint()
{
	double result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_R8, (void*)&result, NULL);
	return result;
}

BOOL ISketchPatternFeatureData::GetGeometryPattern()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISketchPatternFeatureData::SetGeometryPattern(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

VARIANT ISketchPatternFeatureData::GetPatternFeatureArray()
{
	VARIANT result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ISketchPatternFeatureData::SetPatternFeatureArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ISketchPatternFeatureData::GetPatternFeatureCount()
{
	long result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH ISketchPatternFeatureData::IGetPatternFeatureArray()
{
	LPDISPATCH result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ISketchPatternFeatureData::ISetPatternFeatureArray(long featCount, LPDISPATCH* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}


/////////////////////////////////////////////////////////////////////////////
// IMirrorSolidFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IMirrorSolidFeatureData operations

BOOL IMirrorSolidFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IMirrorSolidFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IMirrorSolidFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IMirrorSolidFeatureData::GetFace()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IMirrorSolidFeatureData::SetFace(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// ISheetMetalFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// ISheetMetalFeatureData operations

BOOL ISheetMetalFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL ISheetMetalFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void ISheetMetalFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

double ISheetMetalFeatureData::GetBendRadius()
{
	double result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISheetMetalFeatureData::SetBendRadius(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ISheetMetalFeatureData::GetThickness()
{
	double result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISheetMetalFeatureData::SetThickness(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH ISheetMetalFeatureData::GetFixedReference()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ISheetMetalFeatureData::SetFixedReference(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ISheetMetalFeatureData::GetBendAllowanceType()
{
	long result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISheetMetalFeatureData::SetBendAllowanceType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

CString ISheetMetalFeatureData::GetBendTableFile()
{
	CString result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void ISheetMetalFeatureData::SetBendTableFile(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

double ISheetMetalFeatureData::GetKFactor()
{
	double result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISheetMetalFeatureData::SetKFactor(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ISheetMetalFeatureData::GetBendAllowance()
{
	double result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISheetMetalFeatureData::SetBendAllowance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL ISheetMetalFeatureData::GetUseAutoRelief()
{
	BOOL result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISheetMetalFeatureData::SetUseAutoRelief(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

long ISheetMetalFeatureData::GetAutoReliefType()
{
	long result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISheetMetalFeatureData::SetAutoReliefType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

double ISheetMetalFeatureData::GetReliefRatio()
{
	double result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISheetMetalFeatureData::SetReliefRatio(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// IOneBendFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IOneBendFeatureData operations

BOOL IOneBendFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IOneBendFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IOneBendFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

double IOneBendFeatureData::GetBendRadius()
{
	double result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetBendRadius(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IOneBendFeatureData::GetBendAllowanceType()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetBendAllowanceType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

CString IOneBendFeatureData::GetBendTableFile()
{
	CString result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetBendTableFile(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

double IOneBendFeatureData::GetKFactor()
{
	double result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetKFactor(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IOneBendFeatureData::GetBendAllowance()
{
	double result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetBendAllowance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IOneBendFeatureData::GetUseAutoRelief()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetUseAutoRelief(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

long IOneBendFeatureData::GetAutoReliefType()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetAutoReliefType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IOneBendFeatureData::GetUseDefaultBendRadius()
{
	BOOL result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetUseDefaultBendRadius(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IOneBendFeatureData::GetUseDefaultBendAllowance()
{
	BOOL result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetUseDefaultBendAllowance(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IOneBendFeatureData::GetUseDefaultBendRelief()
{
	BOOL result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetUseDefaultBendRelief(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IOneBendFeatureData::GetBendDown()
{
	BOOL result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetBendDown(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double IOneBendFeatureData::GetBendAngle()
{
	double result;
	InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetBendAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IOneBendFeatureData::GetBendOrder()
{
	long result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetBendOrder(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

double IOneBendFeatureData::GetReliefWidth()
{
	double result;
	InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetReliefWidth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IOneBendFeatureData::GetReliefDepth()
{
	double result;
	InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IOneBendFeatureData::SetReliefDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// IBendsFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IBendsFeatureData operations

BOOL IBendsFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IBendsFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IBendsFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

double IBendsFeatureData::GetBendRadius()
{
	double result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IBendsFeatureData::SetBendRadius(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IBendsFeatureData::GetBendAllowanceType()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IBendsFeatureData::SetBendAllowanceType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

CString IBendsFeatureData::GetBendTableFile()
{
	CString result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IBendsFeatureData::SetBendTableFile(LPCTSTR lpszNewValue)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 lpszNewValue);
}

double IBendsFeatureData::GetKFactor()
{
	double result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IBendsFeatureData::SetKFactor(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IBendsFeatureData::GetBendAllowance()
{
	double result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IBendsFeatureData::SetBendAllowance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IBendsFeatureData::GetUseDefaultBendRadius()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IBendsFeatureData::SetUseDefaultBendRadius(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IBendsFeatureData::GetUseDefaultBendAllowance()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IBendsFeatureData::SetUseDefaultBendAllowance(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// IBaseFlangeFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IBaseFlangeFeatureData operations

BOOL IBaseFlangeFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IBaseFlangeFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IBaseFlangeFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IBaseFlangeFeatureData::GetOffsetDirections()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetOffsetDirections(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IBaseFlangeFeatureData::GetD1OffsetType()
{
	long result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetD1OffsetType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IBaseFlangeFeatureData::GetD2OffsetType()
{
	long result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetD2OffsetType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IBaseFlangeFeatureData::GetD1OffsetReferenceType()
{
	long result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IBaseFlangeFeatureData::GetD2OffsetReferenceType()
{
	long result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IBaseFlangeFeatureData::GetD1OffsetReference()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetD1OffsetReference(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH IBaseFlangeFeatureData::GetD2OffsetReference()
{
	LPDISPATCH result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetD2OffsetReference(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IBaseFlangeFeatureData::GetD1OffsetDistance()
{
	double result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetD1OffsetDistance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IBaseFlangeFeatureData::GetD2OffsetDistance()
{
	double result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetD2OffsetDistance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IBaseFlangeFeatureData::GetThickness()
{
	double result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetThickness(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IBaseFlangeFeatureData::GetReverseThickness()
{
	BOOL result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetReverseThickness(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double IBaseFlangeFeatureData::GetBendRadius()
{
	double result;
	InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetBendRadius(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IBaseFlangeFeatureData::GetReverseDirection()
{
	BOOL result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IBaseFlangeFeatureData::SetReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// IEdgeFlangeFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IEdgeFlangeFeatureData operations

BOOL IEdgeFlangeFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IEdgeFlangeFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IEdgeFlangeFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IEdgeFlangeFeatureData::GetEdge()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetEdge(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IEdgeFlangeFeatureData::GetUseDefaultBendRadius()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetUseDefaultBendRadius(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double IEdgeFlangeFeatureData::GetBendRadius()
{
	double result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetBendRadius(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IEdgeFlangeFeatureData::GetBendAngle()
{
	double result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetBendAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IEdgeFlangeFeatureData::GetOffsetType()
{
	long result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetOffsetType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

LPDISPATCH IEdgeFlangeFeatureData::GetOffsetReference()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetOffsetReference(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IEdgeFlangeFeatureData::GetReverseOffset()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetReverseOffset(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double IEdgeFlangeFeatureData::GetOffsetDistance()
{
	double result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetOffsetDistance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IEdgeFlangeFeatureData::GetOffsetDimType()
{
	long result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetOffsetDimType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IEdgeFlangeFeatureData::GetPositionType()
{
	long result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetPositionType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IEdgeFlangeFeatureData::GetUsePositionTrimSideBends()
{
	BOOL result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetUsePositionTrimSideBends(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL IEdgeFlangeFeatureData::GetUsePositionOffset()
{
	BOOL result;
	InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetUsePositionOffset(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

long IEdgeFlangeFeatureData::GetPositionOffsetType()
{
	long result;
	InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetPositionOffsetType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IEdgeFlangeFeatureData::GetPositionReferenceType()
{
	long result;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IEdgeFlangeFeatureData::GetPositionOffsetReference()
{
	LPDISPATCH result;
	InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetPositionOffsetReference(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IEdgeFlangeFeatureData::GetPositionOffsetDistance()
{
	double result;
	InvokeHelper(0x13, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetPositionOffsetDistance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x13, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IEdgeFlangeFeatureData::GetReversePositionOffset()
{
	BOOL result;
	InvokeHelper(0x14, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetReversePositionOffset(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x14, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

long IEdgeFlangeFeatureData::GetAutoReliefType()
{
	long result;
	InvokeHelper(0x15, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetAutoReliefType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IEdgeFlangeFeatureData::GetUseReliefRatio()
{
	BOOL result;
	InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetUseReliefRatio(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double IEdgeFlangeFeatureData::GetReliefRatio()
{
	double result;
	InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetReliefRatio(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IEdgeFlangeFeatureData::GetReliefWidth()
{
	double result;
	InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetReliefWidth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x18, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double IEdgeFlangeFeatureData::GetReliefDepth()
{
	double result;
	InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetReliefDepth(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IEdgeFlangeFeatureData::GetReliefTearType()
{
	long result;
	InvokeHelper(0x1a, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IEdgeFlangeFeatureData::SetReliefTearType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// IMiterFlangeFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IMiterFlangeFeatureData operations

BOOL IMiterFlangeFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IMiterFlangeFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IMiterFlangeFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IMiterFlangeFeatureData::GetEdges()
{
	VARIANT result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IMiterFlangeFeatureData::SetEdges(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long IMiterFlangeFeatureData::IGetEdgesCount()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IMiterFlangeFeatureData::IGetEdges()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IMiterFlangeFeatureData::ISetEdges(long edgeCount, LPDISPATCH* edgeArray)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 edgeCount, edgeArray);
}

BOOL IMiterFlangeFeatureData::GetUseDefaultBendRadius()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IMiterFlangeFeatureData::SetUseDefaultBendRadius(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double IMiterFlangeFeatureData::GetBendRadius()
{
	double result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IMiterFlangeFeatureData::SetBendRadius(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long IMiterFlangeFeatureData::GetPositionType()
{
	long result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IMiterFlangeFeatureData::SetPositionType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL IMiterFlangeFeatureData::GetUsePositionTrimSideBends()
{
	BOOL result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IMiterFlangeFeatureData::SetUsePositionTrimSideBends(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double IMiterFlangeFeatureData::GetGapDistance()
{
	double result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void IMiterFlangeFeatureData::SetGapDistance(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// ISketchedBendFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// ISketchedBendFeatureData operations

BOOL ISketchedBendFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL ISketchedBendFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void ISketchedBendFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH ISketchedBendFeatureData::GetFixedFace(double* x, double* y, double* z)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_PR8 VTS_PR8 VTS_PR8;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		x, y, z);
	return result;
}

void ISketchedBendFeatureData::SetFixedFace(double x, double y, double z, LPDISPATCH edgeArray)
{
	static BYTE parms[] =
		VTS_R8 VTS_R8 VTS_R8 VTS_DISPATCH;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 x, y, z, edgeArray);
}

long ISketchedBendFeatureData::GetPositionType()
{
	long result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ISketchedBendFeatureData::SetPositionType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

BOOL ISketchedBendFeatureData::GetReverseDirection()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISketchedBendFeatureData::SetReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double ISketchedBendFeatureData::GetBendAngle()
{
	double result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISketchedBendFeatureData::SetBendAngle(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL ISketchedBendFeatureData::GetUseDefaultBendRadius()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ISketchedBendFeatureData::SetUseDefaultBendRadius(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double ISketchedBendFeatureData::GetBendRadius()
{
	double result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ISketchedBendFeatureData::SetBendRadius(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// IClosedCornerFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IClosedCornerFeatureData operations

BOOL IClosedCornerFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IClosedCornerFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IClosedCornerFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT IClosedCornerFeatureData::GetFaces()
{
	VARIANT result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IClosedCornerFeatureData::SetFaces(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long IClosedCornerFeatureData::IGetFacesCount()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IClosedCornerFeatureData::IGetFaces()
{
	LPDISPATCH result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IClosedCornerFeatureData::ISetFaces(long faceCount, LPDISPATCH* faceArray)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 faceCount, faceArray);
}

long IClosedCornerFeatureData::GetCornerType()
{
	long result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IClosedCornerFeatureData::SetCornerType(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// IFoldsFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IFoldsFeatureData operations

BOOL IFoldsFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IFoldsFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IFoldsFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IFoldsFeatureData::GetFixedFace()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IFoldsFeatureData::SetFixedFace(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

VARIANT IFoldsFeatureData::GetBends()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IFoldsFeatureData::SetBends(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long IFoldsFeatureData::IGetBendsCount()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IFoldsFeatureData::IGetBends()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IFoldsFeatureData::ISetBends(long faceCount, LPDISPATCH* faceArray)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 faceCount, faceArray);
}


/////////////////////////////////////////////////////////////////////////////
// IFlatPatternFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IFlatPatternFeatureData operations

BOOL IFlatPatternFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IFlatPatternFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IFlatPatternFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IFlatPatternFeatureData::GetFixedFace()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IFlatPatternFeatureData::SetFixedFace(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

BOOL IFlatPatternFeatureData::GetMergeFace()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IFlatPatternFeatureData::SetMergeFace(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// ILocalLinearPatternFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// ILocalLinearPatternFeatureData operations

BOOL ILocalLinearPatternFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL ILocalLinearPatternFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void ILocalLinearPatternFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH ILocalLinearPatternFeatureData::GetD1Axis()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::SetD1Axis(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

LPDISPATCH ILocalLinearPatternFeatureData::GetD2Axis()
{
	LPDISPATCH result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::SetD2Axis(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ILocalLinearPatternFeatureData::GetD1AxisType()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ILocalLinearPatternFeatureData::GetD2AxisType()
{
	long result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ILocalLinearPatternFeatureData::GetD1ReverseDirection()
{
	BOOL result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::SetD1ReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

BOOL ILocalLinearPatternFeatureData::GetD2ReverseDirection()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::SetD2ReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double ILocalLinearPatternFeatureData::GetD1Spacing()
{
	double result;
	InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::SetD1Spacing(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

double ILocalLinearPatternFeatureData::GetD2Spacing()
{
	double result;
	InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::SetD2Spacing(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ILocalLinearPatternFeatureData::GetD1TotalInstances()
{
	long result;
	InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::SetD1TotalInstances(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long ILocalLinearPatternFeatureData::GetD2TotalInstances()
{
	long result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::SetD2TotalInstances(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

VARIANT ILocalLinearPatternFeatureData::GetSeedComponentArray()
{
	VARIANT result;
	InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::SetSeedComponentArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ILocalLinearPatternFeatureData::GetSeedComponentCount()
{
	long result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILocalLinearPatternFeatureData::IGetSeedComponentArray()
{
	LPDISPATCH result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::ISetSeedComponentArray(long featCount, LPDISPATCH* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}

VARIANT ILocalLinearPatternFeatureData::GetSkippedItemArray()
{
	VARIANT result;
	InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::SetSkippedItemArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ILocalLinearPatternFeatureData::GetSkippedItemCount()
{
	long result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ILocalLinearPatternFeatureData::IGetSkippedItemArray()
{
	long result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void ILocalLinearPatternFeatureData::ISetSkippedItemArray(long featCount, long* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}


/////////////////////////////////////////////////////////////////////////////
// ILocalCircularPatternFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// ILocalCircularPatternFeatureData operations

BOOL ILocalCircularPatternFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL ILocalCircularPatternFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void ILocalCircularPatternFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH ILocalCircularPatternFeatureData::GetAxis()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ILocalCircularPatternFeatureData::SetAxis(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ILocalCircularPatternFeatureData::GetAxisType()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL ILocalCircularPatternFeatureData::GetReverseDirection()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void ILocalCircularPatternFeatureData::SetReverseDirection(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

double ILocalCircularPatternFeatureData::GetSpacing()
{
	double result;
	InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_R8, (void*)&result, NULL);
	return result;
}

void ILocalCircularPatternFeatureData::SetSpacing(double newValue)
{
	static BYTE parms[] =
		VTS_R8;
	InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

long ILocalCircularPatternFeatureData::GetTotalInstances()
{
	long result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void ILocalCircularPatternFeatureData::SetTotalInstances(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

VARIANT ILocalCircularPatternFeatureData::GetSeedComponentArray()
{
	VARIANT result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ILocalCircularPatternFeatureData::SetSeedComponentArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ILocalCircularPatternFeatureData::GetSeedComponentCount()
{
	long result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH ILocalCircularPatternFeatureData::IGetSeedComponentArray()
{
	LPDISPATCH result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void ILocalCircularPatternFeatureData::ISetSeedComponentArray(long featCount, LPDISPATCH* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}

VARIANT ILocalCircularPatternFeatureData::GetSkippedItemArray()
{
	VARIANT result;
	InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void ILocalCircularPatternFeatureData::SetSkippedItemArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long ILocalCircularPatternFeatureData::GetSkippedItemCount()
{
	long result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long ILocalCircularPatternFeatureData::IGetSkippedItemArray()
{
	long result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void ILocalCircularPatternFeatureData::ISetSkippedItemArray(long featCount, long* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}


/////////////////////////////////////////////////////////////////////////////
// IDerivedPatternFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IDerivedPatternFeatureData operations

BOOL IDerivedPatternFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IDerivedPatternFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IDerivedPatternFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH IDerivedPatternFeatureData::GetPatternFeature()
{
	LPDISPATCH result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IDerivedPatternFeatureData::SetPatternFeature(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}

VARIANT IDerivedPatternFeatureData::GetSeedComponentArray()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IDerivedPatternFeatureData::SetSeedComponentArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long IDerivedPatternFeatureData::GetSeedComponentCount()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IDerivedPatternFeatureData::IGetSeedComponentArray()
{
	LPDISPATCH result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IDerivedPatternFeatureData::ISetSeedComponentArray(long featCount, LPDISPATCH* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}

VARIANT IDerivedPatternFeatureData::GetSkippedItemArray()
{
	VARIANT result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IDerivedPatternFeatureData::SetSkippedItemArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long IDerivedPatternFeatureData::GetSkippedItemCount()
{
	long result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long IDerivedPatternFeatureData::IGetSkippedItemArray()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

void IDerivedPatternFeatureData::ISetSkippedItemArray(long featCount, long* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PI4;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 featCount, ArrayDataIn);
}


/////////////////////////////////////////////////////////////////////////////
// IProjectionCurveFeatureData properties

/////////////////////////////////////////////////////////////////////////////
// IProjectionCurveFeatureData operations

BOOL IProjectionCurveFeatureData::AccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

BOOL IProjectionCurveFeatureData::IAccessSelections(LPDISPATCH topDoc, LPDISPATCH component)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		topDoc, component);
	return result;
}

void IProjectionCurveFeatureData::ReleaseSelectionAccess()
{
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

BOOL IProjectionCurveFeatureData::GetReverse()
{
	BOOL result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
	return result;
}

void IProjectionCurveFeatureData::SetReverse(BOOL bNewValue)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 bNewValue);
}

VARIANT IProjectionCurveFeatureData::GetFaceArray()
{
	VARIANT result;
	InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
	return result;
}

void IProjectionCurveFeatureData::SetFaceArray(const VARIANT& newValue)
{
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 &newValue);
}

long IProjectionCurveFeatureData::GetFaceArrayCount()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH IProjectionCurveFeatureData::IGetFaceArray(long faceCount)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		faceCount);
	return result;
}

void IProjectionCurveFeatureData::ISetFaceArray(long faceCount, LPDISPATCH* ArrayDataIn)
{
	static BYTE parms[] =
		VTS_I4 VTS_PDISPATCH;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 faceCount, ArrayDataIn);
}

LPDISPATCH IProjectionCurveFeatureData::GetSketch()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

void IProjectionCurveFeatureData::SetSketch(LPDISPATCH newValue)
{
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 newValue);
}


/////////////////////////////////////////////////////////////////////////////
// DSldWorksEvents properties

/////////////////////////////////////////////////////////////////////////////
// DSldWorksEvents operations

long DSldWorksEvents::FileOpenNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DSldWorksEvents::FileNewNotify(LPDISPATCH newDoc, long DocType)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		newDoc, DocType);
	return result;
}

long DSldWorksEvents::DestroyNotify()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DSldWorksEvents::ActiveDocChangeNotify()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DSldWorksEvents::ActiveModelDocChangeNotify()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DSldWorksEvents::PropertySheetCreateNotify(LPDISPATCH sheet, long sheetType)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		sheet, sheetType);
	return result;
}

long DSldWorksEvents::NonNativeFileOpenNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DSldWorksEvents::LightSheetCreateNotify(LPDISPATCH NewSheet, long sheetType, long lightId)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4 VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		NewSheet, sheetType, lightId);
	return result;
}

long DSldWorksEvents::DocumentConversionNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DSldWorksEvents::DocumentLoadNotify(LPCTSTR docTitle, LPCTSTR docPath)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		docTitle, docPath);
	return result;
}

long DSldWorksEvents::FileNewNotify2(LPDISPATCH newDoc, long DocType, LPCTSTR templateName)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4 VTS_BSTR;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		newDoc, DocType, templateName);
	return result;
}

long DSldWorksEvents::FileOpenNotify2(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DSldWorksEvents::ReferenceNotFoundNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DSldWorksEvents::PromptForFilenameNotify(long openOrSave, LPCTSTR suggestedFileName, long DocType, long Unused)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_I4 VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		openOrSave, suggestedFileName, DocType, Unused);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// DPartDocEvents properties

/////////////////////////////////////////////////////////////////////////////
// DPartDocEvents operations

long DPartDocEvents::RegenNotify()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::DestroyNotify()
{
	long result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::RegenPostNotify()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::ViewNewNotify()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::NewSelectionNotify()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::FileSaveNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DPartDocEvents::FileSaveAsNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DPartDocEvents::LoadFromStorageNotify()
{
	long result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::SaveToStorageNotify()
{
	long result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::ActiveConfigChangeNotify()
{
	long result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::ActiveConfigChangePostNotify()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::ViewNewNotify2(LPDISPATCH viewBeingAdded)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		viewBeingAdded);
	return result;
}

long DPartDocEvents::LightingDialogCreateNotify(LPDISPATCH dialog)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		dialog);
	return result;
}

long DPartDocEvents::AddItemNotify(long entityType, LPCTSTR itemName)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		entityType, itemName);
	return result;
}

long DPartDocEvents::RenameItemNotify(long entityType, LPCTSTR oldName, LPCTSTR newName)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		entityType, oldName, newName);
	return result;
}

long DPartDocEvents::DeleteItemNotify(long entityType, LPCTSTR itemName)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		entityType, itemName);
	return result;
}

long DPartDocEvents::ModifyNotify()
{
	long result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::FileReloadNotify()
{
	long result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::AddCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propName, configuration, Value, valueType);
	return result;
}

long DPartDocEvents::ChangeCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR oldValue, LPCTSTR newValue, long valueType)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propName, configuration, oldValue, newValue, valueType);
	return result;
}

long DPartDocEvents::DeleteCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propName, configuration, Value, valueType);
	return result;
}

long DPartDocEvents::FeatureEditPreNotify(LPDISPATCH editFeature)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		editFeature);
	return result;
}

long DPartDocEvents::FeatureSketchEditPreNotify(LPDISPATCH editFeature, LPDISPATCH featureSketch)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		editFeature, featureSketch);
	return result;
}

long DPartDocEvents::FileSaveAsNotify2(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DPartDocEvents::DeleteSelectionPreNotify()
{
	long result;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::FileReloadPreNotify()
{
	long result;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DPartDocEvents::BodyVisibleChangeNotify()
{
	long result;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// DDrawingDocEvents properties

/////////////////////////////////////////////////////////////////////////////
// DDrawingDocEvents operations

long DDrawingDocEvents::RegenNotify()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::DestroyNotify()
{
	long result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::RegenPostNotify()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::ViewNewNotify()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::NewSelectionNotify()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::FileSaveNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DDrawingDocEvents::FileSaveAsNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DDrawingDocEvents::LoadFromStorageNotify()
{
	long result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::SaveToStorageNotify()
{
	long result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::ActiveConfigChangeNotify()
{
	long result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::ActiveConfigChangePostNotify()
{
	long result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::ViewNewNotify2(LPDISPATCH viewBeingAdded)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		viewBeingAdded);
	return result;
}

long DDrawingDocEvents::AddItemNotify(long entityType, LPCTSTR itemName)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		entityType, itemName);
	return result;
}

long DDrawingDocEvents::RenameItemNotify(long entityType, LPCTSTR oldName, LPCTSTR newName)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		entityType, oldName, newName);
	return result;
}

long DDrawingDocEvents::DeleteItemNotify(long entityType, LPCTSTR itemName)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		entityType, itemName);
	return result;
}

long DDrawingDocEvents::ModifyNotify()
{
	long result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::FileReloadNotify()
{
	long result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::AddCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propName, configuration, Value, valueType);
	return result;
}

long DDrawingDocEvents::ChangeCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR oldValue, LPCTSTR newValue, long valueType)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propName, configuration, oldValue, newValue, valueType);
	return result;
}

long DDrawingDocEvents::DeleteCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propName, configuration, Value, valueType);
	return result;
}

long DDrawingDocEvents::FileSaveAsNotify2(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DDrawingDocEvents::DeleteSelectionPreNotify()
{
	long result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DDrawingDocEvents::FileReloadPreNotify()
{
	long result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// DAssemblyDocEvents properties

/////////////////////////////////////////////////////////////////////////////
// DAssemblyDocEvents operations

long DAssemblyDocEvents::RegenNotify()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::DestroyNotify()
{
	long result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::RegenPostNotify()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::ViewNewNotify()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::NewSelectionNotify()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::FileSaveNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DAssemblyDocEvents::FileSaveAsNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DAssemblyDocEvents::LoadFromStorageNotify()
{
	long result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::SaveToStorageNotify()
{
	long result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::ActiveConfigChangeNotify()
{
	long result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::ActiveConfigChangePostNotify()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::BeginInContextEditNotify(LPDISPATCH docBeingEdited, long DocType)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		docBeingEdited, DocType);
	return result;
}

long DAssemblyDocEvents::EndInContextEditNotify(LPDISPATCH docBeingEdited, long DocType)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		docBeingEdited, DocType);
	return result;
}

long DAssemblyDocEvents::ViewNewNotify2(LPDISPATCH viewBeingAdded)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		viewBeingAdded);
	return result;
}

long DAssemblyDocEvents::LightingDialogCreateNotify(LPDISPATCH dialog)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		dialog);
	return result;
}

long DAssemblyDocEvents::AddItemNotify(long entityType, LPCTSTR itemName)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		entityType, itemName);
	return result;
}

long DAssemblyDocEvents::RenameItemNotify(long entityType, LPCTSTR oldName, LPCTSTR newName)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_BSTR;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		entityType, oldName, newName);
	return result;
}

long DAssemblyDocEvents::DeleteItemNotify(long entityType, LPCTSTR itemName)
{
	long result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		entityType, itemName);
	return result;
}

long DAssemblyDocEvents::ModifyNotify()
{
	long result;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::ComponentStateChangeNotify(LPDISPATCH componentModel, short oldCompState, short newCompState)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I2 VTS_I2;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		componentModel, oldCompState, newCompState);
	return result;
}

long DAssemblyDocEvents::FileDropNotify(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DAssemblyDocEvents::FileReloadNotify()
{
	long result;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::ComponentStateChangeNotify2(LPDISPATCH componentModel, LPCTSTR compName, short oldCompState, short newCompState)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BSTR VTS_I2 VTS_I2;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		componentModel, compName, oldCompState, newCompState);
	return result;
}

long DAssemblyDocEvents::AddCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propName, configuration, Value, valueType);
	return result;
}

long DAssemblyDocEvents::ChangeCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR oldValue, LPCTSTR newValue, long valueType)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propName, configuration, oldValue, newValue, valueType);
	return result;
}

long DAssemblyDocEvents::DeleteCustomPropertyNotify(LPCTSTR propName, LPCTSTR configuration, LPCTSTR Value, long valueType)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		propName, configuration, Value, valueType);
	return result;
}

long DAssemblyDocEvents::FeatureEditPreNotify(LPDISPATCH editFeature)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		editFeature);
	return result;
}

long DAssemblyDocEvents::FeatureSketchEditPreNotify(LPDISPATCH editFeature, LPDISPATCH featureSketch)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_DISPATCH;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		editFeature, featureSketch);
	return result;
}

long DAssemblyDocEvents::FileSaveAsNotify2(LPCTSTR fileName)
{
	long result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		fileName);
	return result;
}

long DAssemblyDocEvents::InterferenceNotify(VARIANT* pComp, VARIANT* pFace)
{
	long result;
	static BYTE parms[] =
		VTS_PVARIANT VTS_PVARIANT;
	InvokeHelper(0x20, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		pComp, pFace);
	return result;
}

long DAssemblyDocEvents::DeleteSelectionPreNotify()
{
	long result;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::FileReloadPreNotify()
{
	long result;
	InvokeHelper(0x22, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::ComponentMoveNotify()
{
	long result;
	InvokeHelper(0x23, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::ComponentVisibleChangeNotify()
{
	long result;
	InvokeHelper(0x24, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DAssemblyDocEvents::BodyVisibleChangeNotify()
{
	long result;
	InvokeHelper(0x25, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// DModelViewEvents properties

/////////////////////////////////////////////////////////////////////////////
// DModelViewEvents operations

long DModelViewEvents::RepaintNotify(long paintType)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		paintType);
	return result;
}

long DModelViewEvents::ViewChangeNotify(const VARIANT& view)
{
	long result;
	static BYTE parms[] =
		VTS_VARIANT;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		&view);
	return result;
}

long DModelViewEvents::DestroyNotify()
{
	long result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DModelViewEvents::RepaintPostNotify()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DModelViewEvents::BufferSwapNotify()
{
	long result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DModelViewEvents::DestroyNotify2(long destroyType)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		destroyType);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// DFeatMgrViewEvents properties

/////////////////////////////////////////////////////////////////////////////
// DFeatMgrViewEvents operations

long DFeatMgrViewEvents::ActivateNotify(VARIANT* view)
{
	long result;
	static BYTE parms[] =
		VTS_PVARIANT;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		view);
	return result;
}

long DFeatMgrViewEvents::DeactivateNotify(VARIANT* view)
{
	long result;
	static BYTE parms[] =
		VTS_PVARIANT;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		view);
	return result;
}

long DFeatMgrViewEvents::DestroyNotify(VARIANT* view)
{
	long result;
	static BYTE parms[] =
		VTS_PVARIANT;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		view);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// DSWPropertySheetEvents properties

/////////////////////////////////////////////////////////////////////////////
// DSWPropertySheetEvents operations

long DSWPropertySheetEvents::DestroyNotify()
{
	long result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

long DSWPropertySheetEvents::HelpNotify(long page)
{
	long result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		page);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// ISdmDoc properties

/////////////////////////////////////////////////////////////////////////////
// ISdmDoc operations
