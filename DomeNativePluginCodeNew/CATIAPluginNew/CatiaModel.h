// CATIAModel.h: interface for the CATIAModel class.
//
//////////////////////////////////////////////////////////////////////

#ifndef CATIA_MODEL_H
#define CATIA_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector.h>


namespace DOME {
namespace CATIAPlugin {

//**static CATSession *m_pSession = NULL;
class CATIAModel;

class UserLibraryManager
{
public:
	UserLibraryManager (char *name, unsigned numParameters, CATIAModel *pmodel);
	~UserLibraryManager ();

	const
	char*			getName () const;
	void			addParameter (int index, CATIAData *data);
	void			setValue (const CATIAData *param, CATVariant va);
	void*			getValue (const CATIAData *param) const;
	void			setValueDoubleArray (const CATIAData *param, vector<double>);
	vector<double>	getValueDoubleArray (const CATIAData *param) const;
	void			run();

	typedef void			(*FUNC_RUN)(void);
	typedef void			(*FUNC_SET_ITEM)(int, void*);
	typedef void*			(*FUNC_GET_ITEM)(int);
	typedef void			(*FUNC_SET_ITEM_DOUBLEARRAY)(int, vector<double>);
	typedef vector<double>	(*FUNC_GET_ITEM_DOUBLEARRAY)(int);

private:
	char *m_LibraryName;
	CATIAData **m_Parameters;
	int m_NumParameters;

	//*** each UserLibraryManager corresponds to a third-party dll.
	//*** all dlls will use their own CATIADomeInterface to initialize different components in 
	//*** CATIA to do different task.
	CATBaseDispatch * m_CBD;
	CATIADomeInterface * m_CATIADomeInterface;
	CATIAModel *m_model;

	HINSTANCE m_hDLL;               // Handle to DLL
	FUNC_RUN m_funcRun;				// handle to the Run function
	FUNC_SET_ITEM m_funcSetItem;	// handle to the SetItem function
	FUNC_GET_ITEM m_funcGetItem;	// handle to the GetItem function
	FUNC_SET_ITEM_DOUBLEARRAY 
		m_funcSetItemDoubleArray;	// handle to the SetItem_DoubleArray function
	FUNC_GET_ITEM_DOUBLEARRAY 
		m_funcGetItemDoubleArray;	// handle to the GetItem_DoubleArray function
};


class CATIAModel// : public DomeModel
{
public:
	CATIAModel(char* fileName, bool bIsVisible=false);
	virtual ~CATIAModel();

	CATIAReal*		createReal(char *name);
	CATIAInteger*	createInteger(char *name);
	CATIAString*	createString(char *name);
	CATIABoolean*	createBoolean(char *name);
	CATIAFile*		createFile(char *fileName, char *fileType);
	CATIAVector*	createVector(char *name);

	CATIAVector*	createVectorUserLib(char *name,
										char *LibraryName, unsigned int argIndex);
	CATIAString*	createStringUserLib(char *name, char *LibraryName, int argIndex);
	CATIAReal*		createRealUserLib  (char *name, char *LibraryName, int argIndex);
	CATIAInteger*	createIntegerUserLib (char *name, char *LibraryName, int argIndex);
	CATIABoolean*	createBooleanUserLib (char *name, char *LibraryName, int argIndex);


	bool isModelLoaded();
	int loadModel();
	void unloadModel(int numModels);

	int executeBeforeInput() { return 0; };
	int execute();
    int executeAfterOutput() { return 0; };

	void setupUserLibrary (char *name, unsigned int numParameters);

	void setUserLibraryParameterValue (CATIAData *pParam, 
									   UserLibraryManager *pLibraryMgr,
									   void *pValue);
    CATIAApplication* m_pCatiaApp;
	CATIADocument* m_pDoc;
    CATIAPart * m_pPart;  
	//** For now, CATIAPlugin assumes all documents are part documents. It can support more kinds of documents by some changes

private:
	bool m_bIsVisible;
	bool m_bModelLoaded;
	char* m_Filename;
	vector <CATIAData*> m_Data;
	vector <CATIAFile*> m_Files;

	CATSession *m_pSession;
	char m_pSessionName[_MAX_PATH];


	//CATDocument* m_pDoc;
	//CATIPrtPart_var m_pPart;
	CATISpecObject_var m_pPartSpec;
    CATVisManager* m_pVisManager;
    CAT3DViewpoint* m_pVP;

	UserLibraryManager **m_UserLibraryManagers;
	int m_NumUserLibraries;

	void _createConnections();
	void _destroyConnections();
	int  VisualizePart ();

};

} // namespace CATIAPlugin
} // DOME

#endif // CATIA_MODEL_H
