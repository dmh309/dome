
// Util_ExcelPlugin_2010Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "Util_ExcelPlugin_2010.h"
#include "Util_ExcelPlugin_2010Dlg.h"
#include "DlgProxy.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "CApplication.h"

// CUtil_ExcelPlugin_2010Dlg dialog


IMPLEMENT_DYNAMIC(CUtil_ExcelPlugin_2010Dlg, CDialogEx);

CUtil_ExcelPlugin_2010Dlg::CUtil_ExcelPlugin_2010Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CUtil_ExcelPlugin_2010Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pAutoProxy = NULL;
}

CUtil_ExcelPlugin_2010Dlg::~CUtil_ExcelPlugin_2010Dlg()
{
	// If there is an automation proxy for this dialog, set
	//  its back pointer to this dialog to NULL, so it knows
	//  the dialog has been deleted.
	if (m_pAutoProxy != NULL)
		m_pAutoProxy->m_pDialog = NULL;
}

void CUtil_ExcelPlugin_2010Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CUtil_ExcelPlugin_2010Dlg, CDialogEx)
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CUtil_ExcelPlugin_2010Dlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CUtil_ExcelPlugin_2010Dlg message handlers

BOOL CUtil_ExcelPlugin_2010Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CUtil_ExcelPlugin_2010Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CUtil_ExcelPlugin_2010Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// Automation servers should not exit when a user closes the UI
//  if a controller still holds on to one of its objects.  These
//  message handlers make sure that if the proxy is still in use,
//  then the UI is hidden but the dialog remains around if it
//  is dismissed.

void CUtil_ExcelPlugin_2010Dlg::OnClose()
{
	if (CanExit())
		CDialogEx::OnClose();
}

void CUtil_ExcelPlugin_2010Dlg::OnOK()
{
	if (CanExit())
		CDialogEx::OnOK();
}

void CUtil_ExcelPlugin_2010Dlg::OnCancel()
{
	if (CanExit())
		CDialogEx::OnCancel();
}

BOOL CUtil_ExcelPlugin_2010Dlg::CanExit()
{
	// If the proxy object is still around, then the automation
	//  controller is still holding on to this application.  Leave
	//  the dialog around, but hide its UI.
	if (m_pAutoProxy != NULL)
	{
		ShowWindow(SW_HIDE);
		return FALSE;
	}

	return TRUE;
}



void CUtil_ExcelPlugin_2010Dlg::OnBnClickedButton1()
{
	CApplication app;  // app is the Excel _Application object

	// Start Excel and get Application object.
	//CreateDispatch
	if (!app.CreateDispatch(_T("Excel.Application"))) {
		AfxMessageBox(_T("Cannot start Excel and get Application object."));
		return;
	}
	else
	{
		//Make the application visible and give the user control of
		//Microsoft Excel.
		app.put_Visible(TRUE);
		app.put_UserControl(TRUE);
	}
}
