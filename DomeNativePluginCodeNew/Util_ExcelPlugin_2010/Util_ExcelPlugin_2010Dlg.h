
// Util_ExcelPlugin_2010Dlg.h : header file
//

#pragma once

class CUtil_ExcelPlugin_2010DlgAutoProxy;


// CUtil_ExcelPlugin_2010Dlg dialog
class CUtil_ExcelPlugin_2010Dlg : public CDialogEx
{
	DECLARE_DYNAMIC(CUtil_ExcelPlugin_2010Dlg);
	friend class CUtil_ExcelPlugin_2010DlgAutoProxy;

// Construction
public:
	CUtil_ExcelPlugin_2010Dlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CUtil_ExcelPlugin_2010Dlg();

// Dialog Data
	enum { IDD = IDD_UTIL_EXCELPLUGIN_2010_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	CUtil_ExcelPlugin_2010DlgAutoProxy* m_pAutoProxy;
	HICON m_hIcon;

	BOOL CanExit();

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
};
