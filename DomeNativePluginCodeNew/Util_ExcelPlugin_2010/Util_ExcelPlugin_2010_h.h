

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Thu Jul 30 09:57:25 2015
 */
/* Compiler settings for Util_ExcelPlugin_2010.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __Util_ExcelPlugin_2010_h_h__
#define __Util_ExcelPlugin_2010_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IUtil_ExcelPlugin_2010_FWD_DEFINED__
#define __IUtil_ExcelPlugin_2010_FWD_DEFINED__
typedef interface IUtil_ExcelPlugin_2010 IUtil_ExcelPlugin_2010;

#endif 	/* __IUtil_ExcelPlugin_2010_FWD_DEFINED__ */


#ifndef __Util_ExcelPlugin_2010_FWD_DEFINED__
#define __Util_ExcelPlugin_2010_FWD_DEFINED__

#ifdef __cplusplus
typedef class Util_ExcelPlugin_2010 Util_ExcelPlugin_2010;
#else
typedef struct Util_ExcelPlugin_2010 Util_ExcelPlugin_2010;
#endif /* __cplusplus */

#endif 	/* __Util_ExcelPlugin_2010_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __Util_ExcelPlugin_2010_LIBRARY_DEFINED__
#define __Util_ExcelPlugin_2010_LIBRARY_DEFINED__

/* library Util_ExcelPlugin_2010 */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_Util_ExcelPlugin_2010;

#ifndef __IUtil_ExcelPlugin_2010_DISPINTERFACE_DEFINED__
#define __IUtil_ExcelPlugin_2010_DISPINTERFACE_DEFINED__

/* dispinterface IUtil_ExcelPlugin_2010 */
/* [uuid] */ 


EXTERN_C const IID DIID_IUtil_ExcelPlugin_2010;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("F88ACF61-481B-4843-9DD1-22267D6865BA")
    IUtil_ExcelPlugin_2010 : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct IUtil_ExcelPlugin_2010Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IUtil_ExcelPlugin_2010 * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IUtil_ExcelPlugin_2010 * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IUtil_ExcelPlugin_2010 * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IUtil_ExcelPlugin_2010 * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IUtil_ExcelPlugin_2010 * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IUtil_ExcelPlugin_2010 * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IUtil_ExcelPlugin_2010 * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } IUtil_ExcelPlugin_2010Vtbl;

    interface IUtil_ExcelPlugin_2010
    {
        CONST_VTBL struct IUtil_ExcelPlugin_2010Vtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IUtil_ExcelPlugin_2010_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IUtil_ExcelPlugin_2010_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IUtil_ExcelPlugin_2010_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IUtil_ExcelPlugin_2010_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IUtil_ExcelPlugin_2010_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IUtil_ExcelPlugin_2010_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IUtil_ExcelPlugin_2010_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* __IUtil_ExcelPlugin_2010_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_Util_ExcelPlugin_2010;

#ifdef __cplusplus

class DECLSPEC_UUID("9BE399C3-E55B-48E1-8302-8491287C72E9")
Util_ExcelPlugin_2010;
#endif
#endif /* __Util_ExcelPlugin_2010_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


