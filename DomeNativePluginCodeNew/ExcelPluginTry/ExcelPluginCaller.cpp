#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "StdAfx.h"

#ifdef _WINDOWS
#pragma warning(disable:4786)	// stl warnings
#endif
#include <iostream>   // std::cout, std::endl
#include <vector>
using namespace std;


//------------------------------------------------------------------
// Create the delegate object
//
// *********** PLUGIN AUTHOR MODIFIES THIS SECTION ONLY (begin) ************
//

#include "ExcelPluginCaller.h"
#include "ExcelDelegate.h"
ExcelDelegate delegate;
// *********** PLUGIN AUTHOR MODIFIES THIS SECTION ONLY (end)   ************
//------------------------------------------------------------------


//------------------------------------------------------------------
// function prototypes
jstring JNU_NewStringNative(JNIEnv *env, const char *str);
//------------------------------------------------------------------



/*
 * Class:     mit_cadlab_dome3_plugin_ExcelPluginCaller
 * Method:    callVoidFunc
 */
JNIEXPORT void JNICALL Java_mit_cadlab_dome3_plugin_excel_ExcelPluginCaller_callVoidFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}

	try {
		delegate.callVoidFunctions (env, args, iNumArgs, iObjectPtr, iFunction);
	}
	catch(...) {
		cout<<"Error calling VoidFunc"<<endl;
	}

}


/*
 * Class:     mit_cadlab_dome3_plugin_ExcelPluginCaller
 * Method:    callBoolFunc
 */
JNIEXPORT jboolean JNICALL Java_mit_cadlab_dome3_plugin_excel_ExcelPluginCaller_callBoolFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	jboolean value;
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}

	try {
		value = delegate.callBooleanFunctions (env, args, iNumArgs, iObjectPtr, iFunction);
	}
	catch(...) {
		value = false;
		cout<<"Error calling BoolFunc"<<endl;
	}

	return value;
}


/*
 * Class:     mit_cadlab_dome3_plugin_ExcelPluginCaller
 * Method:    callIntFunc
 */
JNIEXPORT jint JNICALL Java_mit_cadlab_dome3_plugin_excel_ExcelPluginCaller_callIntFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	jint value;
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}

	try {
		value = delegate.callIntegerFunctions (env, args, iNumArgs, iObjectPtr, iFunction);
	}
	catch(...) {
		value = 0;
		cout<<"Error calling IntFunc"<<endl;
	}

	return value;
}


/*
 * Class:     mit_cadlab_dome3_plugin_ExcelPluginCaller
 * Method:    callDoubleFunc
 */
JNIEXPORT jdouble JNICALL Java_mit_cadlab_dome3_plugin_excel_ExcelPluginCaller_callDoubleFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	jdouble value;
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	
	//for debug
	//cout << "Begin to call delegate.callDoubleFunctions" << endl;

	try {
		value = delegate.callDoubleFunctions (env, args, iNumArgs, iObjectPtr, iFunction);
	}
	catch(...) {
		value = 0;
		cout<<"Error calling DoubleFunc"<<endl;
	}

	//for debug
	//cout << "Finish calling delegate.callDoubleFunctions" << endl;

	return value;
}


/*
 * Class:     mit_cadlab_dome3_plugin_ExcelPluginCaller
 * Method:    callStringFunc
 */
JNIEXPORT jstring JNICALL Java_mit_cadlab_dome3_plugin_excel_ExcelPluginCaller_callStringFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	const char *value;
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}

	try {
		value = delegate.callStringFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

		jstring returnStr = JNU_NewStringNative(env, value);
		return returnStr;
	}
	catch(...) {
		cout<<"Error calling StringFunc"<<endl;
		return 0;
	}

}


/*
 * Class:     mit_cadlab_dome3_plugin_ExcelPluginCaller
 * Method:    callObjectFunc
 */
JNIEXPORT jlong JNICALL Java_mit_cadlab_dome3_plugin_excel_ExcelPluginCaller_callObjectFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	
	try {
		iObjectPtr = delegate.callObjectFunctions (env, args, iNumArgs, iObjectPtr, iFunction);

		return iObjectPtr;
	}
	catch(...) {
		cout<<"Error calling ObjectFunc"<<endl;
		return 0;
	}

}


/*
 * Class:     mit_cadlab_dome3_plugin_ExcelPluginCaller
 * Method:    callDoubleArrayFunc
 */
JNIEXPORT jdoubleArray JNICALL Java_mit_cadlab_dome3_plugin_excel_ExcelPluginCaller_callDoubleArrayFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	
	vector<double> array;
	try {
		array = delegate.callDoubleArrayFunctions (env, args, iNumArgs, iObjectPtr, iFunction);
	}
	catch(...) {
		cout<<"Error calling DoubleArrayFunc"<<endl;
	}

	int arrlength = array.size();
	jdoubleArray darr = env->NewDoubleArray(arrlength);
	if(darr == 0) {
		return 0; //out of memory
	}
	double* arr = new double[arrlength];
	for(int i = 0; i < arrlength; i++) {
		arr[i] = array[i];
	}
	env->SetDoubleArrayRegion(darr, 0, arrlength, arr);
	delete arr;

	return darr;
}


/*
 * Class:     mit_cadlab_dome3_plugin_ExcelPluginCaller
 * Method:    callIntArrayFunc
 */
JNIEXPORT jintArray JNICALL Java_mit_cadlab_dome3_plugin_excel_ExcelPluginCaller_callIntArrayFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}
	
	vector<int> array;
	try {
		array =	delegate.callIntegerArrayFunctions (env, args, iNumArgs, iObjectPtr, iFunction);
	}
	catch(...) {
		cout<<"Error calling IntArrayFunc"<<endl;
	}

	int arrlength = array.size();
	jintArray iarr = env->NewIntArray(arrlength);
	if(iarr == 0) {
		return 0; //out of memory
	}
#ifdef _WINDOWS
	long* arr = new long[arrlength];
#else
	int* arr = new int[arrlength];
#endif
	for(int i = 0; i < arrlength; i++) {
		arr[i] = array[i];
	}
	env->SetIntArrayRegion(iarr, 0, arrlength, arr);
	delete arr;
	return iarr;
}


/*
 * Class:     mit_cadlab_dome3_plugin_ExcelPluginCaller
 * Method:    call2DimDoubleArrayFunc
 */
JNIEXPORT jobjectArray JNICALL Java_mit_cadlab_dome3_plugin_excel_ExcelPluginCaller_call2DimDoubleArrayFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}

	vector< vector<double> > array;
	try {
		array = delegate.call2DDoubleArrayFunctions (env, args, iNumArgs, iObjectPtr, iFunction);
	}
	catch(...) {
		cout<<"Error calling 2DimDoubleArrayFunc"<<endl;
	}

	int rows = array.size();
	jclass dubArrCls =  env->FindClass("[D");
	if (dubArrCls == 0) {
		return 0;
	}
	jobjectArray objarr = env->NewObjectArray(rows, dubArrCls, 0);
	for(int i = 0; i < rows; i++) {
		vector<double> vec = array[i];
		int cols = vec.size();
		double* arr = new double[cols];
		jdoubleArray darr = env->NewDoubleArray(cols);
		if(darr == 0) {
		return 0; //out of memory
		}
		for(int j = 0; j < cols; j++) {
			arr[j] = vec[j];
		}
		env->SetDoubleArrayRegion(darr, 0, cols, arr);
		env->SetObjectArrayElement(objarr, i, darr);
        env->DeleteLocalRef(darr);
		delete arr;
	}
	return objarr;
}


/*
 * Class:     mit_cadlab_dome3_plugin_ExcelPluginCaller
 * Method:    call2DimIntArrayFunc
 */
JNIEXPORT jobjectArray JNICALL Java_mit_cadlab_dome3_plugin_excel_ExcelPluginCaller_call2DimIntArrayFunc
  (JNIEnv *env, jobject thisobj, jlong iObjectPtr, jint iFunction, jobjectArray args)
{
	int iNumArgs = 0;
	if (args != NULL) {
		iNumArgs = env->GetArrayLength(args);
	}

	vector< vector<int> > array;
	try {
		array = delegate.call2DIntegerArrayFunctions (env, args, iNumArgs, iObjectPtr, iFunction);
	}
	catch(...) {
		cout<<"Error calling 2DimIntArrayFunc"<<endl;
	}

	int rows = array.size();
	jclass intArrCls =  env->FindClass("[I");
	if (intArrCls == 0) {
		return 0;
	}
	jobjectArray objarr = env->NewObjectArray(rows, intArrCls, 0);
	for(int i = 0; i < rows; i++) {
		vector<int> vec = array[i];
		int cols = vec.size();
#ifdef _WINDOWS
		long* arr = new long[cols];
#else
		int* arr = new int[cols];
#endif
		jintArray darr = env->NewIntArray(cols);
		if(darr == 0) {
		return 0; //out of memory
		}
		for(int j = 0; j < cols; j++) {
			arr[j] = vec[j];
		}
		env->SetIntArrayRegion(darr, 0, cols, arr);
		env->SetObjectArrayElement(objarr, i, darr);
        env->DeleteLocalRef(darr);
		delete arr;
	}
	return objarr;
}



jstring JNU_NewStringNative(JNIEnv *env, const char *str)
{
	jstring result;
	jbyteArray bytes = 0;
	int len;
	if (env->EnsureLocalCapacity(2) < 0) {
		return 0; /* out of memory error */
	}
	len = strlen(str);
	bytes = env->NewByteArray(len);
	if (bytes != 0) {
		env->SetByteArrayRegion(bytes, 0, len,
			(jbyte *)str);
		jclass cls =  env->FindClass("java/lang/String");
		if (cls == 0) {
			return 0;
		}
		jmethodID mid = env->GetMethodID(cls, "<init>", "([B)V");
		if (mid == 0) {
			return 0;
		}
		result = (jstring)env->NewObject(cls, mid, bytes);
		env->DeleteLocalRef(bytes);
		return result;
	} /* else fall through */
	return 0;
}
