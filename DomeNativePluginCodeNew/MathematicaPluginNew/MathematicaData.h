// MathematicaData.h: interface for the MathematicaData class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_MATHEMATICADATA_H
#define DOME_MATHEMATICADATA_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MathematicaIncludes.h"
#include "MathematicaModel.h"

namespace DOME {
namespace MathematicaPlugin {
class MathematicaModel;

class MathematicaData
{
friend class MathematicaModel;
private:
	virtual void connect(MLINK lp) = 0; 
	virtual void disconnect() = 0; 
};

class MathematicaReal : public DomeReal, public MathematicaData
{
friend class MathematicaModel;

public:
	MathematicaReal(string name);
	virtual ~MathematicaReal();

	double getValue() throw(DomeException);
	void setValue(double value) throw(DomeException);

private:
	void connect(MLINK lp) throw(DomeException); 
	void disconnect();

	string _name;
	MLINK _lp;
};

class MathematicaInteger : public DomeInteger, public MathematicaData
{
friend class MathematicaModel;

public:
	MathematicaInteger(string name);
	virtual ~MathematicaInteger();

	int getValue() throw(DomeException);
	void setValue(int value) throw(DomeException);

private:
	void connect(MLINK lp) throw(DomeException); 
	void disconnect();

	string _name;
	MLINK _lp;
};

class MathematicaMatrix : public DomeMatrix, public MathematicaData
{
friend class MathematicaModel;
public:
	MathematicaMatrix(string name, int rows, int columns);
	virtual ~MathematicaMatrix();

	vector<int> getDimension() throw(DomeException);
    void setDimension(int rows, int cols);
    
    int getRows() throw(DomeException);
    void setRows(int rows);

    int getColumns() throw(DomeException);
    void setColumns(int cols);

    vector <vector <double> > getValues() throw(DomeException);
    void setValues(vector <vector <double> > values) throw(DomeException);
    double getElement(int row, int column) throw(DomeException);
    void setElement(int row, int column, double value) throw(DomeException);

private:
	void connect(MLINK lp) throw(DomeException); 
	void disconnect();
	int _rows;
	int _columns;
	string _name;
	MLINK _lp;
};

class MathematicaVector : public DomeVector, public MathematicaData
{
friend class MathematicaModel;
public:
	MathematicaVector(string name, int columns);
	virtual ~MathematicaVector();

	int getDimension() throw(DomeException);
    void setDimension(int columns);
    
    int getLength() throw(DomeException);
    void setLength(int columns);

    vector <double> getValues() throw(DomeException);
    void setValues(vector <double> values) throw(DomeException);
    double getElement(int row) throw(DomeException);
    void setElement(int row, double value) throw(DomeException);

private:
	void connect(MLINK lp) throw(DomeException); 
	void disconnect();
	int _rows;
	int _columns;
	string _name;
	MLINK _lp;
};

} // namespace MathematicaPlugin
} // DOME

#endif // DOME_MATHEMATICADATA_H
