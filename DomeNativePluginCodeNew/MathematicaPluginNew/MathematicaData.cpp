// MathematicaData.cpp: implementation of the MathematicaData class.
//
//////////////////////////////////////////////////////////////////////

#include "MathematicaData.h"

namespace DOME {
namespace MathematicaPlugin {

MathematicaReal::MathematicaReal(string name)
{
	_name = name;
	_lp = NULL;
}

MathematicaReal::~MathematicaReal()
{
	disconnect();
}

double MathematicaReal::getValue() throw(DomeException)
{
	if (_lp == NULL) {
		MATHEMATICA_ERROR("MathematicaReal::getValue: MLink pointer is null. Unable to getValue.");
	}

	// API calls to get value of variable
	MLPutFunction(_lp, "EvaluatePacket", 1);
	MLPutFunction(_lp, "Get", 1);
	MLPutSymbol(_lp, _name.c_str());
	MLEndPacket(_lp);

	//MLFlush() ensures that the packet is sent immediately
	MLFlush(_lp);

	int pkt;

	int i = 0;
	while ( (pkt = MLNextPacket(_lp)) && pkt != RETURNPKT)
	{
		MLNewPacket(_lp);
		i++;
		if (i>100)
			MATHEMATICA_ERROR("MathematicaReal::getValue: No return packet. Unable to getValue.");
	}
	if (!pkt) {
	    string message = MLErrorMessage(_lp);
		MLClearError(_lp);
		MATHEMATICA_ERROR("MathematicaReal::getValue: "+message);
	} else {
		double result;
		// Ignore all expressions up to a REAL number
		// Ignoring the FUNCTION head and symbols
		while (MLGetNext(_lp) != MLTKREAL) ;

		MLGetReal(_lp, &result);
		MLNewPacket(_lp);
		return result;
	}
}

void MathematicaReal::setValue(double value) throw(DomeException)
{

	if (_lp == NULL) {
		MATHEMATICA_ERROR("MathematicaReal::setValue: MLink pointer is null. Unable to setValue.");	
	}

	// API calls to set value of variable
	MLPutFunction(_lp, "EvaluatePacket", 1);
		MLPutFunction(_lp, "Set", 2);
			MLPutSymbol(_lp, _name.c_str());
			MLPutReal(_lp, value);
	MLEndPacket(_lp);

	//MLFlush() ensures that the packet is sent immediately
	MLFlush(_lp);
	int pkt;

	// discarding all packets before a RETURNPKT
	int i = 0;
	while ( (pkt = MLNextPacket(_lp)) && pkt != RETURNPKT)
	{
		MLNewPacket(_lp);
		i++;
		if (i>100)
			MATHEMATICA_ERROR("MathematicaReal::setValue: No return packet. Unable to setValue.");
	}

	if (!pkt) {
		string message = MLErrorMessage(_lp);
		MLClearError(_lp);
		MATHEMATICA_ERROR("MathematicaReal::setValue: "+message);
	}

	MLNewPacket(_lp);
}

void MathematicaReal::connect(MLINK lp) throw(DomeException)
{
	if (lp != NULL) {
		_lp = lp;
	} else {
		MATHEMATICA_ERROR("MathematicaReal::connect: MLink pointer is null. Unable to connect.");
	}
}

void MathematicaReal::disconnect()
{
	_lp = NULL;
}

//********************************************************************
MathematicaInteger::MathematicaInteger(string name)
{
	_name = name;
	_lp = NULL;
}

MathematicaInteger::~MathematicaInteger()
{
	disconnect();
}

int MathematicaInteger::getValue() throw(DomeException)
{
	if (_lp == NULL) {
		MATHEMATICA_ERROR("MathematicaInteger::getValue: MLink pointer is null. Unable to getValue.");
	}


	// API calls to get value of variable
	MLPutFunction(_lp, "EvaluatePacket", 1);
	MLPutFunction(_lp, "Get", 1);
	MLPutSymbol(_lp, _name.c_str());
	MLEndPacket(_lp);
	//MLFlush() ensures that the packet is sent immediately
	MLFlush(_lp);

	int pkt;

	int i = 0;
	while ( (pkt = MLNextPacket(_lp)) && pkt != RETURNPKT)
	{
		MLNewPacket(_lp);
		i++;
		if (i>100)
			MATHEMATICA_ERROR("MathematicaInteger::getValue: No return packet. Unable to getValue.");
	}
	if (!pkt) {
	    string message = MLErrorMessage(_lp);
		MLClearError(_lp);
		MATHEMATICA_ERROR("MathematicaInteger::getValue: "+message);
	} else {
		int result;
		// Ignore all expressions up to a REAL number
		// Ignoring the FUNCTION head and symbols
		while (MLGetNext(_lp) != MLTKINT) ;

		MLGetInteger(_lp, &result);
		MLNewPacket(_lp);
		return result;
	}
}

void MathematicaInteger::setValue(int value) throw(DomeException)
{
	if (_lp == NULL) {
		MATHEMATICA_ERROR("MathematicaInteger::setValue: MLink pointer is null. Unable to setValue.");	
	}

	// API calls to set value of variable
	MLPutFunction(_lp, "EvaluatePacket", 1);
		MLPutFunction(_lp, "Set", 2);
			MLPutSymbol(_lp, _name.c_str());
			MLPutInteger(_lp, value);
	MLEndPacket(_lp);

	//MLFlush() ensures that the packet is sent immediately
	MLFlush(_lp);
	int pkt;

	// discarding all packets before a RETURNPKT
	int i = 0;
	while ( (pkt = MLNextPacket(_lp)) && pkt != RETURNPKT)
	{
		MLNewPacket(_lp);
		i++;
		if (i>100)
			MATHEMATICA_ERROR("MathematicaInteger::setValue: No return packet. Unable to setValue.");
	}

	if (!pkt) {
		string message = MLErrorMessage(_lp);
		MLClearError(_lp);
		MATHEMATICA_ERROR("MathematicaInteger::setValue1: "+message);
	}

	MLNewPacket(_lp);
}

void MathematicaInteger::connect(MLINK lp) throw(DomeException)
{
	if (lp != NULL) {
		_lp = lp;
	} else {
		MATHEMATICA_ERROR("MathematicaInteger::connect: MLink pointer is null. Unable to connect.");
	}
}

void MathematicaInteger::disconnect()
{
	_lp = NULL;
}
//********************************************************************

MathematicaMatrix::MathematicaMatrix(string name, int rows, int columns)
{	
	_rows = rows;
	_columns = columns;
	_name = name;
	_lp = NULL;
}

MathematicaMatrix::~MathematicaMatrix()
{
	disconnect();
}

vector<int> MathematicaMatrix::getDimension() throw(DomeException)
{
	try{
		vector<int> dim;
        dim.push_back(getRows());
        dim.push_back(getColumns());
        return dim;
	} catch (DomeException) {
	  throw;
	}
}

void MathematicaMatrix::setDimension(int rows, int columns)
{
	DOME_NOT_SUPPORTED("MathematicaMatrix::setDimension");
}
   
int MathematicaMatrix::getRows() throw(DomeException)
{
	try {
		return _rows;
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaMatrix::getRows");
	}
}

void MathematicaMatrix::setRows(int rows)
{
	DOME_NOT_SUPPORTED("MathematicaMatrix::setRows");
}

int MathematicaMatrix::getColumns() throw(DomeException)
{
	try {
		return _columns;
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaMatrix::getColumns");
	}
}

void MathematicaMatrix::setColumns(int columns)
{
	DOME_NOT_SUPPORTED("MathematicaMatrix::setColumns");
}

vector <vector <double> > MathematicaMatrix::getValues() throw(DomeException)
{
	try {
		// initialize vector
		vector <vector <double> > values;
		for (int i=0; i<_rows; i++){
			vector <double> row;
			for (int j=0; j<_columns; j++){
				row.push_back(0.0);
			}
			values.push_back(row);
		}

		// API calls to get value of variable
		MLPutFunction(_lp, "EvaluatePacket", 1);
		MLPutFunction(_lp, "Get", 1);
		MLPutSymbol(_lp, _name.c_str());
		MLEndPacket(_lp);

		//MLFlush() ensures that the packet is sent immediately
		MLFlush(_lp);

		int pkt;
		while ((pkt = MLNextPacket(_lp)) && pkt != RETURNPKT)
			MLNewPacket(_lp);

		if (!pkt) {
			string message = MLErrorMessage(_lp);
			MLClearError(_lp);
			MATHEMATICA_ERROR("MathematicaMatrix::getValues: "+message);		
		}	

		// Ignore all expressions up to a REAL number
		// Ignoring the FUNCTION head and symbols
		double result;
		for (i = 0; i < _rows; i++)
		{
			for (int j = 0; j < _columns; j++)
			{
				while (MLGetNext(_lp) != MLTKREAL) ;
				MLGetReal(_lp, &result);
				values[i][j] = result;
			}
		}
		
		MLNewPacket(_lp);
		return values;


	} catch (DomeException) {
		throw;
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaMatrix::getValues");
	}
}

void MathematicaMatrix::setValues(vector <vector <double> > values) throw(DomeException)
{
//cout<<"_lp in MathematicaMatrix::setValues = ";
//cout<<_lp<<endl;
	if (values.size() != _rows)
	{
		cout<<"should not be in here\n";
		MATHEMATICA_ERROR("MathematicaMatrix::setValues: invalid row dimension "+str((int)values.size()));
	}

	for (int i=0; i<values.size(); i++)
	{
		if (values[i].size() != _columns)
		{
			cout<<"should not be in here either\n";
			MATHEMATICA_ERROR("MathematicaMatrix::setValues: invalid column dimension "+str((int)values[i].size()));
		}
	}

	try {

		MLPutFunction(_lp, "EvaluatePacket", 1);
		MLPutFunction(_lp, "Set", 2);
		MLPutSymbol(_lp, _name.c_str());

		if (_rows == 1)
			MLPutFunction (_lp, "List", _columns);
		else if (_rows > 1)
			MLPutFunction(_lp, "List", _rows);
		
		for (i = 0; i < _rows; i++)
		{
			if (_rows > 1)
				MLPutFunction(_lp, "List", _columns);

			for (int j = 0; j < _columns; j++)
			{
				MLPutReal(_lp, values[i][j]);
			}
		}

		MLEndPacket(_lp);
		MLFlush(_lp);



		int pkt;

		// discarding all packets before a RETURNPKT
		int i = 0;
		while ( (pkt = MLNextPacket(_lp)) && pkt != RETURNPKT)
		{
			MLNewPacket(_lp);
			i++;
			if (i>100) throw DomeException(__FILE__, __LINE__, "No return packet. Unable to setValue.");
		}

		if (!pkt) {
			cout<<"MathematicaMatrix::setValues: in if\n ";
			if(_lp == 0) {
				cout<<"_lp is null\n";
			}
			string message = MLErrorMessage(_lp);
			cout<<"MathematicaMatrix::setValues: after error msg func\n ";
			MLClearError(_lp);
			MATHEMATICA_ERROR("MathematicaMatrix::setValues: "+message);
			cout<<"MathematicaMatrix::setValues: "+message;
		}

		MLNewPacket(_lp);

	} catch (DomeException) {
		throw;
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaMatrix::setValues");
	}
}

double MathematicaMatrix::getElement(int row, int column) throw(DomeException)
{
	if (row < 0 || column < 0 || row >= getRows() || column >= getColumns())
	{
		MATHEMATICA_ERROR("MathematicaMatrix::getElement: invalid row or column");
	}

	try {
		vector <vector <double> > values;
		values = getValues();
		return values[row][column];
		// implemented, use getValues() and return individual value at [row][column]
	} catch (DomeException) {
		throw;
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaMatrix::getElement: ["+str(row)+str(column)+"].");
	}
}

void MathematicaMatrix::setElement(int row, int column, double value) throw(DomeException)
{
	if (row < 0 || column < 0 || row >= getRows() || column >= getColumns())
	{
		MATHEMATICA_ERROR("MathematicaMatrix::setElement: invalid row or column");
	}

	try {
		vector <vector <double> > values;
		values = getValues();
		values[row][column] = value;
		setValues(values);
		// implemented, use getValues(), then change individual value and then use setValues
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaMatrix::setElement: ["+str(row)+str(column)+"] to "+str(value)+".");
	}
}

void MathematicaMatrix::connect(MLINK lp) throw(DomeException)
{
	if (lp != NULL) {
		_lp = lp;
	} else {
		MATHEMATICA_ERROR("MathematicaMatrix::connect: MLink pointer is null. Unable to connect.");
	}
}

void MathematicaMatrix::disconnect()
{
	_lp = NULL;
}


//********************************************************************

MathematicaVector::MathematicaVector(string name, int columns)
{	
	_columns = columns;
	_name = name;
	_lp = NULL;
}

MathematicaVector::~MathematicaVector()
{
	disconnect();
}

int MathematicaVector::getDimension() throw(DomeException)
{
	try{
        return _columns;
	} catch (DomeException) {
	  throw;
	}
}

void MathematicaVector::setDimension(int columns)
{
	DOME_NOT_SUPPORTED("MathematicaMatrix::setDimension");
}
   

int MathematicaVector::getLength() throw(DomeException)
{
	try {
		return _columns;
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaMatrix::getColumns");
	}
}

void MathematicaVector::setLength(int columns)
{
	DOME_NOT_SUPPORTED("MathematicaVector::setLength");
}

vector <double> MathematicaVector::getValues() throw(DomeException)
{
	try {
		// initialize vector

		vector <double>values;
			for (int j=0; j<_columns; j++){
				values.push_back(0.0);
			}
		/*
		vector <vector <double> > values;
		for (int i=0; i<_rows; i++){
			vector <double> row;
			for (int j=0; j<_columns; j++){
				row.push_back(0.0);
			}
			values.push_back(row);
		}*/

		// API calls to get value of variable
		MLPutFunction(_lp, "EvaluatePacket", 1);
		MLPutFunction(_lp, "Get", 1);
		MLPutSymbol(_lp, _name.c_str());
		MLEndPacket(_lp);
		
		//MLFlush() ensures that the packet is sent immediately
		MLFlush(_lp);

		int pkt;
		while ((pkt = MLNextPacket(_lp)) && pkt != RETURNPKT)
			MLNewPacket(_lp);

		if (!pkt) {
			string message = MLErrorMessage(_lp);
			MLClearError(_lp);
			MATHEMATICA_ERROR("MathematicaVector::getValues1: "+message);		
		}	
		// Ignore all expressions up to a REAL number
		// Ignoring the FUNCTION head and symbols
		double result;
			for (j = 0; j < _columns; j++)
			{
				while (MLGetNext(_lp) != MLTKREAL) ;
				MLGetReal(_lp, &result);
				values[j] = result;
			}


		/*for (i = 0; i < _rows; i++)
		{
			for (int j = 0; j < _columns; j++)
			{
				while (MLGetNext(_lp) != MLTKREAL) ;
				MLGetReal(_lp, &result);
				values[i][j] = result;
			}
		} */


		MLNewPacket(_lp);

/*		vector <double> returnval;
		for (i = 0; i < _columns; i++)
		{
			returnval.push_back(values[0][i]);
		}

		return returnval; */
		return values;


	} catch (DomeException) {
		throw;
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaVector2::getValues");
	}
}

void MathematicaVector::setValues(vector <double> values) throw(DomeException)
//void MathematicaVector::setValues(vector <double> invalues) throw(DomeException)
{
//cout<<"_lp in MathematicaMatrix::setValues = ";
//cout<<_lp<<endl;
	if (values.size() != _columns)
//	if (invalues.size() != _columns)
	{

		MATHEMATICA_ERROR("MathematicaVector::setValues: invalid dimension "+str((int)values.size()));
//		MATHEMATICA_ERROR("MathematicaVector::setValues: invalid dimension "+str((int)invalues.size()));
	}

/*	vector <vector <double> > values;
		for (int i=0; i<_rows; i++){
			vector <double> row;
			for (int j=0; j<_columns; j++){
				row.push_back(invalues[j]);
			}
			values.push_back(row);
		} */

	try {

		MLPutFunction(_lp, "EvaluatePacket", 1);
		MLPutFunction(_lp, "Set", 2);
		MLPutSymbol(_lp, _name.c_str());


		MLPutFunction (_lp, "List", _columns);
			for (int j = 0; j < _columns; j++)
			{
				MLPutReal(_lp, values[j]);
			}


/*		if (_rows == 1)
			MLPutFunction (_lp, "List", _columns);
		else if (_rows > 1)
			MLPutFunction(_lp, "List", _rows);
		
		for (i = 0; i < _rows; i++)
		{
			if (_rows > 1)
				MLPutFunction(_lp, "List", _columns);

			for (int j = 0; j < _columns; j++)
			{
				MLPutReal(_lp, values[i][j]);
			}
		} */

		MLEndPacket(_lp);
		MLFlush(_lp);



		int pkt;

		// discarding all packets before a RETURNPKT
		int i = 0;
		while ( (pkt = MLNextPacket(_lp)) && pkt != RETURNPKT)
		{
			MLNewPacket(_lp);
			i++;
			if (i>100) throw DomeException(__FILE__, __LINE__, "No return packet. Unable to setValue.");
		}

		if (!pkt) {
			cout<<"MathematicaMatrix::setValues: in if\n ";
			if(_lp == 0) {
				cout<<"_lp is null\n";
			}
			string message = MLErrorMessage(_lp);
			cout<<"MathematicaMatrix::setValues: after error msg func\n ";
			MLClearError(_lp);
			MATHEMATICA_ERROR("MathematicaVector::setValues: "+message);
			cout<<"MathematicaVector::setValues: "+message;
		}

		MLNewPacket(_lp);

	} catch (DomeException) {
		throw;
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaVector::setValues");
	}
}

double MathematicaVector::getElement(int column) throw(DomeException)
{
	if (column < 0 ||  column >= getLength())
	{
		MATHEMATICA_ERROR("MathematicaVector::getElement: invalid position");
	}

	try {
		vector <double>values;
		values = getValues();
		return values[column];
		// implemented, use getValues() and return individual value at [row][column]
	} catch (DomeException) {
		throw;
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaVector::getElement: ["+str(column)+"].");
	}
}

void MathematicaVector::setElement(int column, double value) throw(DomeException)
{
	if (column < 0 || column >= getLength())
	{
		MATHEMATICA_ERROR("MathematicaVector::setElement: invalid position");
	}

	try {
		vector <double> values;
		values = getValues();
		values[column] = value;
		setValues(values);
		// implemented, use getValues(), then change individual value and then use setValues
	} catch (...) {
		MATHEMATICA_ERROR("MathematicaVector::setElement: ["+str(column)+"] to "+str(value)+".");
	}
}

void MathematicaVector::connect(MLINK lp) throw(DomeException)
{
	if (lp != NULL) {
		_lp = lp;
	} else {
		MATHEMATICA_ERROR("MathematicaVector::connect: MLink pointer is null. Unable to connect.");
	}
}

void MathematicaVector::disconnect()
{
	_lp = NULL;
}

} // namespace MathematicaPlugin
} // DOME
