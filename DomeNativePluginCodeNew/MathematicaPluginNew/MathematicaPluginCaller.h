/* DO NOT EDIT THIS FILE - it is machine generated */
#ifdef _WINDOWS
#include <jni.h>
#else
#include "../jni_linux/jni.h"
#endif
/* Header for class mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller */

#ifndef _Included_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
#define _Included_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
#ifdef __cplusplus
extern "C" {
#endif
#undef MathematicaPluginCaller_MODEL_INIT
#define MathematicaPluginCaller_MODEL_INIT 0L
#undef MathematicaPluginCaller_MODEL_DESTROY
#define MathematicaPluginCaller_MODEL_DESTROY 1L
#undef MathematicaPluginCaller_MODEL_LOAD
#define MathematicaPluginCaller_MODEL_LOAD 2L
#undef MathematicaPluginCaller_MODEL_UNLOAD
#define MathematicaPluginCaller_MODEL_UNLOAD 3L
#undef MathematicaPluginCaller_MODEL_IS_LOADED
#define MathematicaPluginCaller_MODEL_IS_LOADED 4L
#undef MathematicaPluginCaller_MODEL_EXECUTE
#define MathematicaPluginCaller_MODEL_EXECUTE 5L
#undef MathematicaPluginCaller_MODEL_EXEC_BF_INPUT
#define MathematicaPluginCaller_MODEL_EXEC_BF_INPUT 6L
#undef MathematicaPluginCaller_MODEL_EXEC_AF_INPUT
#define MathematicaPluginCaller_MODEL_EXEC_AF_INPUT 7L
#undef MathematicaPluginCaller_MODEL_CREATE_REAL
#define MathematicaPluginCaller_MODEL_CREATE_REAL 8L
#undef MathematicaPluginCaller_MODEL_CREATE_MATRIX
#define MathematicaPluginCaller_MODEL_CREATE_MATRIX 9L
#undef MathematicaPluginCaller_MODEL_CREATE_INTEGER
#define MathematicaPluginCaller_MODEL_CREATE_INTEGER 10L
#undef MathematicaPluginCaller_MODEL_CREATE_VECTOR
#define MathematicaPluginCaller_MODEL_CREATE_VECTOR 11L
#undef MathematicaPluginCaller_REAL_GET_VALUE
#define MathematicaPluginCaller_REAL_GET_VALUE 12L
#undef MathematicaPluginCaller_REAL_SET_VALUE
#define MathematicaPluginCaller_REAL_SET_VALUE 13L
#undef MathematicaPluginCaller_MATRIX_SET_VALUES
#define MathematicaPluginCaller_MATRIX_SET_VALUES 14L
#undef MathematicaPluginCaller_MATRIX_SET_ELEMENT
#define MathematicaPluginCaller_MATRIX_SET_ELEMENT 15L
#undef MathematicaPluginCaller_MATRIX_SET_ROWS
#define MathematicaPluginCaller_MATRIX_SET_ROWS 16L
#undef MathematicaPluginCaller_MATRIX_SET_COLS
#define MathematicaPluginCaller_MATRIX_SET_COLS 17L
#undef MathematicaPluginCaller_MATRIX_SET_DIM
#define MathematicaPluginCaller_MATRIX_SET_DIM 18L
#undef MathematicaPluginCaller_MATRIX_GET_VALUES
#define MathematicaPluginCaller_MATRIX_GET_VALUES 19L
#undef MathematicaPluginCaller_MATRIX_GET_ELEMENT
#define MathematicaPluginCaller_MATRIX_GET_ELEMENT 20L
#undef MathematicaPluginCaller_MATRIX_GET_ROWS
#define MathematicaPluginCaller_MATRIX_GET_ROWS 21L
#undef MathematicaPluginCaller_MATRIX_GET_COLS
#define MathematicaPluginCaller_MATRIX_GET_COLS 22L
#undef MathematicaPluginCaller_MATRIX_GET_DIM
#define MathematicaPluginCaller_MATRIX_GET_DIM 23L
#undef MathematicaPluginCaller_INTEGER_GET_VALUE
#define MathematicaPluginCaller_INTEGER_GET_VALUE 24L
#undef MathematicaPluginCaller_INTEGER_SET_VALUE
#define MathematicaPluginCaller_INTEGER_SET_VALUE 25L
#undef MathematicaPluginCaller_VECTOR_GET_LENGTH
#define MathematicaPluginCaller_VECTOR_GET_LENGTH 26L
#undef MathematicaPluginCaller_VECTOR_GET_ELEMENT
#define MathematicaPluginCaller_VECTOR_GET_ELEMENT 27L
#undef MathematicaPluginCaller_VECTOR_GET_VALUES
#define MathematicaPluginCaller_VECTOR_GET_VALUES 28L
#undef MathematicaPluginCaller_VECTOR_SET_ELEMENT
#define MathematicaPluginCaller_VECTOR_SET_ELEMENT 29L
#undef MathematicaPluginCaller_VECTOR_SET_VALUES
#define MathematicaPluginCaller_VECTOR_SET_VALUES 30L
/*
 * Class:     mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
 * Method:    callIntFunc
 * Signature: (JI[Ljava/lang/Object;)I
 */
JNIEXPORT jint JNICALL Java_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller_callIntFunc
  (JNIEnv *, jobject, jlong, jint, jobjectArray);

/*
 * Class:     mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
 * Method:    callVoidFunc
 * Signature: (JI[Ljava/lang/Object;)V
 */
JNIEXPORT void JNICALL Java_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller_callVoidFunc
  (JNIEnv *, jobject, jlong, jint, jobjectArray);

/*
 * Class:     mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
 * Method:    callObjectFunc
 * Signature: (JI[Ljava/lang/Object;)J
 */
JNIEXPORT jlong JNICALL Java_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller_callObjectFunc
  (JNIEnv *, jobject, jlong, jint, jobjectArray);

/*
 * Class:     mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
 * Method:    callBoolFunc
 * Signature: (JI[Ljava/lang/Object;)Z
 */
JNIEXPORT jboolean JNICALL Java_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller_callBoolFunc
  (JNIEnv *, jobject, jlong, jint, jobjectArray);

/*
 * Class:     mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
 * Method:    callDoubleFunc
 * Signature: (JI[Ljava/lang/Object;)D
 */
JNIEXPORT jdouble JNICALL Java_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller_callDoubleFunc
  (JNIEnv *, jobject, jlong, jint, jobjectArray);

/*
 * Class:     mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
 * Method:    callStringFunc
 * Signature: (JI[Ljava/lang/Object;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller_callStringFunc
  (JNIEnv *, jobject, jlong, jint, jobjectArray);

/*
 * Class:     mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
 * Method:    callIntArrayFunc
 * Signature: (JI[Ljava/lang/Object;)[I
 */
JNIEXPORT jintArray JNICALL Java_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller_callIntArrayFunc
  (JNIEnv *, jobject, jlong, jint, jobjectArray);

/*
 * Class:     mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
 * Method:    call2DimIntArrayFunc
 * Signature: (JI[Ljava/lang/Object;)[[I
 */
JNIEXPORT jobjectArray JNICALL Java_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller_call2DimIntArrayFunc
  (JNIEnv *, jobject, jlong, jint, jobjectArray);

/*
 * Class:     mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
 * Method:    callDoubleArrayFunc
 * Signature: (JI[Ljava/lang/Object;)[D
 */
JNIEXPORT jdoubleArray JNICALL Java_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller_callDoubleArrayFunc
  (JNIEnv *, jobject, jlong, jint, jobjectArray);

/*
 * Class:     mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller
 * Method:    call2DimDoubleArrayFunc
 * Signature: (JI[Ljava/lang/Object;)[[D
 */
JNIEXPORT jobjectArray JNICALL Java_mit_cadlab_dome3_plugin_mathematica_MathematicaPluginCaller_call2DimDoubleArrayFunc
  (JNIEnv *, jobject, jlong, jint, jobjectArray);

#ifdef __cplusplus
}
#endif
#endif
