// MathematicaIncludes.h: interface for the MathematicaIncludes class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_MATHEMATICAINCLUDES_H
#define DOME_MATHEMATICAINCLUDES_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DomePlugin.h"
using namespace DOME::DomePlugin;

#include "TypeConversions.h" // str
using namespace DOME::Utilities::TypeConversions;

#define MATHEMATICA_DEBUG(msg) cout << "MATHEMATICA DEBUG: " << msg << endl;
#define MATHEMATICA_ERROR(msg) throw DomeException("Mathematica Error",msg)
#define MATHEMATICA_ERROR2(msg) throw DomeException("Mathematica Error",__FILE__,__LINE__,msg)
//#define MATHEMATICA_ERROR MATHEMATICA_ERROR2

#include "mathlink.h"

#endif // DOME_MATHEMATICAINCLUDES_H
