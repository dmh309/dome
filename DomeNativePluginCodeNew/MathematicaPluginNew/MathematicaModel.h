// MathematicaModel.h: interface for the MathematicaModel class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_MATHEMATICAMODEL_H
#define DOME_MATHEMATICAMODEL_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MathematicaData.h"
#include <cstdio>
//using std::sprintf;

namespace DOME {
namespace MathematicaPlugin {
class MathematicaData;
class MathematicaReal;
class MathematicaMatrix;
class MathematicaInteger;
class MathematicaVector;

class MathematicaModel : public DomeModel  
{
public:
  MathematicaModel(string fileName) throw(DomeException);
	virtual ~MathematicaModel();

	MathematicaReal* createReal(string name) throw(DomeException);
	MathematicaMatrix* createMatrix(string name, int rows, int columns) throw(DomeException);
	MathematicaInteger* createInteger(string name) throw(DomeException);
	MathematicaVector* createVector(string name, int rows) throw(DomeException);

	bool isModelLoaded();
	void loadModel() throw(DomeException);
	void unloadModel() throw(DomeException);

	void executeBeforeInput() throw(DomeException) {};
    void execute() throw(DomeException);
    void executeAfterOutput() throw(DomeException) {};

private:
	string _filename;
	vector <MathematicaData*> _data;

	void* _ep;
	MLINK _lp;
	
	void _createConnections(); // throw(DomeException);
	void _destroyConnections(); // throw(DomeException);

	void _readReturnPacket(MLINK);
};

} // namespace MathematicaPlugin
} // DOME

#endif // DATA_MATHEMATICAMODEL_H
