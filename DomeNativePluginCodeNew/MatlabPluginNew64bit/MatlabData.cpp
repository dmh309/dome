// MatlabData.cpp: implementation of the MatlabData class.
//
//////////////////////////////////////////////////////////////////////

#include <string>
#include "MatlabData.h"


namespace DOME {
namespace MatlabPlugin {


MatlabData::MatlabData()
{
	m_array = NULL;
	m_engine = NULL;
	m_name = "";
}

MatlabData::~MatlabData()
{
}



////////////////////////////////////////////////////////////////////////
//
// MatlabReal
//
////////////////////////////////////////////////////////////////////////

MatlabReal::MatlabReal(string name)
{
	m_name = name;
}

MatlabReal::~MatlabReal()
{
	cout << "deleting " << m_name << endl;
	disconnect ();
}

double MatlabReal::getValue() throw(DomeException)
{
	//for debug
	//cout << "Get into MatlabReal::getValue!" << endl;

	if (m_engine == NULL) {
		MATLAB_ERROR("Matlab engine pointer is null. Unable to getValue.");
	}
#ifdef _MATLAB_R2014
	mxArray* array = engGetVariable(m_engine, m_name.c_str());
#endif


	if (array == NULL) {
		MATLAB_ERROR("Variable (" + m_name
					 + "): not found. Unable to getValue.");
	}

	double value = mxGetScalar(array);

	mxDestroyArray(array);

	//for debug
	//cout << "MatlabReal::getValue is done!" << endl;
	return value;
}

void MatlabReal::setValue(double value) throw(DomeException)
{
	//cout << "in setValue" << endl;

	if (m_engine == NULL) {
		MATLAB_ERROR("Matlab engine pointer is null. Unable to setValue.");
	}

	if (m_array == NULL) {
		m_array = mxCreateDoubleMatrix(1, 1, mxREAL);
#ifdef _MATLAB_R12
		mxSetName(m_array, m_name.c_str());
#endif
	}

	mxGetPr(m_array)[0] = value;

#ifdef _MATLAB_R2014
	int result = engPutVariable(m_engine, m_name.c_str(), m_array);
#endif


	if (result) {
		MATLAB_ERROR("Error in setValue (" + m_name + "=" + str(value)
					 + "): engPutVariable/engPutArray failed.");
	}
}

void MatlabReal::connect(Engine* engine) throw(DomeException)
{
	//for debug
	cout << "in connect: " << m_name << endl;

	try {
		m_engine = engine;
		//m_array = mxCreateDoubleMatrix(1, 1, mxREAL);
		//mxSetName(m_array, m_name.c_str());
		//engPutVariable(m_engine, m_name.c_str(), m_array);
	} catch (...) {
		MATLAB_ERROR("MatlabReal::connect");
	}
}


void MatlabReal::disconnect() throw(DomeException)
{
	if (m_array != NULL)
		mxDestroyArray(m_array);
	m_array = NULL;
	m_engine = NULL;
}



////////////////////////////////////////////////////////////////////////
//
// MatlabMatrix
//
////////////////////////////////////////////////////////////////////////

MatlabMatrix::MatlabMatrix(string name, int rows, int columns)
{	
	m_array = NULL;
	m_name = name;
	m_rows = rows;
	m_columns = columns;
}

MatlabMatrix::~MatlabMatrix()
{
	cout << "deleting " << m_name << endl;
	disconnect ();
}

vector<int> MatlabMatrix::getDimension() throw(DomeException)
{
	try{
		vector<int> dim;
        dim.push_back(getRows());
        dim.push_back(getColumns());
        return dim;
	} catch (DomeException) {
	  throw;
	}
}

void MatlabMatrix::setDimension(int rows, int columns) throw(DomeException)
{
	DOME_NOT_SUPPORTED("MatlabMatrix::setDimension");
}
   
int MatlabMatrix::getRows() throw(DomeException)
{
	try {
		return m_rows;
	} catch (...) {
		MATLAB_ERROR("MatlabMatrix::getRows");
	}
}

void MatlabMatrix::setRows(int rows) throw(DomeException)
{
	DOME_NOT_SUPPORTED("MatlabMatrix::setRows");
}

int MatlabMatrix::getColumns() throw(DomeException)
{
	try {
		return m_columns;
	} catch (...) {
		MATLAB_ERROR("MatlabMatrix::getColumns");
	}
}

void MatlabMatrix::setColumns(int columns) throw(DomeException)
{
	DOME_NOT_SUPPORTED("MatlabMatrix::setColumns");
}

vector <vector <double> > MatlabMatrix::getValues() throw(DomeException)
{
	if (m_engine == NULL) {
		MATLAB_ERROR("Matlab engine pointer is null. Unable to getValues.");
	}
#ifdef _MATLAB_R2014
	mxArray* array = engGetVariable(m_engine, m_name.c_str());
#endif

	
	if (array == NULL) {
		MATLAB_ERROR("Variable (" 
					 + m_name + "): not found. Unable to getValues.");
	}
	
	try {
		m_rows = mxGetM(array);
		m_columns = mxGetN(array);
		
		vector <vector <double> > values;
		double* val = mxGetPr(array);
		for (int i=0; i<m_rows; ++i) { // column major to row major
			vector <double> a_row;
			for (int j=0; j<m_columns; ++j) {
				a_row.push_back(val[i+(j*m_rows)]);
			}
			values.push_back(a_row);
		}

		mxDestroyArray(array);

		return values;
	} 
	catch (DomeException) {
		mxDestroyArray(array);
		throw;
	}
	catch (...) {
		mxDestroyArray(array);
		MATLAB_ERROR("MatlabMatrix::getValues (" 
					 + m_name + "): failed to read and return data.");
	}
}

void MatlabMatrix::setValues(std::vector <vector <double> > values) throw(DomeException)
{
	//for debug
	//cout<<"Get into MatlabMatrix::setValues!" << endl;

	//	cout << "in setValues" << endl;

	if (m_engine == NULL) {
		MATLAB_ERROR("Matlab engine pointer is null. Unable to setValues.");
	}

	int nRows = values.size();
	int nColumns = values[0].size();
	for (int i=1; i<nRows; i++)
	{
		if (values[i].size() != nColumns)
		{
			MATLAB_ERROR("MatlabMatrix::setValues: mismatched column dimensions "+str((int)values[i].size()));
		}
	}

	if (nRows!=m_rows || nColumns!=m_columns || m_array==NULL) { // destroy old matrix and create new one
		if (m_array != NULL)
			mxDestroyArray(m_array);
		m_rows = nRows;
		m_columns = nColumns;
		m_array = mxCreateDoubleMatrix(m_rows, m_columns, mxREAL);
#ifdef _MATLAB_R12
		mxSetName(m_array, m_name.c_str());
#endif
	}

	try {
		double* arr = mxGetPr(m_array);
	    for (int i=0; i<m_rows; ++i){ // row major to column major
		    for (int j=0; j<m_columns; ++j){
			    arr[(j*m_rows)+i] = values[i][j];
		    }
	    }
#ifdef _MATLAB_R2014
	    int result = engPutVariable(m_engine, m_name.c_str(), m_array);
#endif


		//for debug
		//cout<<"MatlabMatrix::setValues is done!" << endl;

		if(result)
		{
			MATLAB_ERROR("MatlabMatrix::setValues (" 
						 + m_name + "): engPutVariable/engPutArray failed.");
		}
	}
	catch (DomeException) {
		throw;
	}
	catch (...) {
		MATLAB_ERROR("MatlabMatrix::setValues");
	}
}

double MatlabMatrix::getElement(int row, int column) throw(DomeException)
{
	if (row < 0 || column < 0 || row >= getRows() || column >= getColumns())
	{
		MATLAB_ERROR("MatlabMatrix::setElement: invalid row or column");
	}

	if (m_engine == NULL) {
		MATLAB_ERROR("Matlab engine pointer is null. Unable to getElement.");
	}
#ifdef _MATLAB_R2014
	mxArray* array = engGetVariable(m_engine, m_name.c_str());
#endif


	if (array == NULL) {
		MATLAB_ERROR("Variable (" 
					 + m_name + ") not found. Unable to getElement.");
	}

	try 
	{		
		double* arr = mxGetPr(array);

		int rows = getRows();
		double value = arr[row+rows*column];

		mxDestroyArray (array);
		return value;
	} 
	catch (DomeException) {
		mxDestroyArray(array);
		throw;
	}
	catch (...) {
		mxDestroyArray(array);
		MATLAB_ERROR("MatlabMatrix::getElement: ["+str(row)+str(column)+"].");
	}
}

void MatlabMatrix::setElement(int row, int column, double value) throw(DomeException)
{
//	cout << "in setElement" << endl;

	if (row < 0 || column < 0 || row >= getRows() || column >= getColumns())
	{
		MATLAB_ERROR("MatlabMatrix::setElement: invalid row or column");
	}
	
	if (m_array == NULL) {
		MATLAB_ERROR("Stored value not found. Unable to setElement."); // should retrieve from engine in this case
	}

	if (m_engine == NULL) {
		MATLAB_ERROR("Matlab engine pointer is null. Unable to setElement.");
	}

	try 
	{
		int rows = getRows();

#ifdef _MATLAB_R2014
		mxArray* array = engGetVariable(m_engine, m_name.c_str());
		mxGetPr(array)[row+rows*column] = value;
		int result = engPutVariable(m_engine, m_name.c_str(), m_array);
#endif



		if (result)
		{
			MATLAB_ERROR("MatlabMatrix::setElement (" 
						 + m_name + "): engPutVariable/engPutArray failed.");
		}

		mxDestroyArray (array);
	}
	catch (...) {
		MATLAB_ERROR("MatlabMatrix::setElement: ["+str(row)+str(column)+"] to "+str(value)+".");
	}
}

void MatlabMatrix::connect(Engine* engine) throw(DomeException)
{
	try {
		m_engine = engine;
		//m_array = mxCreateDoubleMatrix(m_rows, m_columns, mxREAL);
		//mxSetName(m_array, m_name.c_str());
		//engPutVariable(m_engine, m_name.c_str(), m_array);
	} catch (...) {
		MATLAB_ERROR("MatlabMatrix::connect");
	}
}


void MatlabMatrix::disconnect() throw(DomeException)
{
	cout << "disconnecting from " << m_name << endl;
	if (m_array != NULL)
		mxDestroyArray(m_array);
	m_array = NULL;
	m_engine = NULL;
}

} // namespace MatlabPlugin
} // DOME
