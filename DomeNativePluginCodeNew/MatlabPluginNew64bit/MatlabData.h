// MatlabData.h: interface for the MatlabData class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_MATLABDATA_H
#define DOME_MATLABDATA_H

//to supress STL related warnings in MSVC
#ifdef _WINDOWS
#pragma warning(disable:4786)
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MatlabIncludes.h"

namespace DOME {
namespace MatlabPlugin {


class MatlabData
{
public:
	MatlabData();
	virtual ~MatlabData();

	virtual void connect(Engine* _engine) throw(DomeException)=0;
	virtual void disconnect () = 0;

	string getName ()
	{
		return m_name;
	}

	mxArray* m_array;
	Engine* m_engine;
	string m_name;
};

class MatlabReal : public DomeReal, public MatlabData
{

public:
	MatlabReal(string name);
	virtual ~MatlabReal();

	double getValue() throw(DomeException);
	void setValue(double value) throw(DomeException);

	void connect(Engine* _engine) throw(DomeException); 
	void disconnect () throw(DomeException); 
};


class MatlabMatrix : public DomeMatrix, public MatlabData
{

public:
	MatlabMatrix(string name, int rows, int columns);
	virtual ~MatlabMatrix();

	vector<int> getDimension() throw(DomeException);
	void setDimension(int rows, int cols) throw(DomeException);
    
	int getRows() throw(DomeException);
	void setRows(int rows) throw(DomeException);

	int getColumns() throw(DomeException);
	void setColumns(int cols) throw(DomeException);

	vector <vector <double> > getValues() throw(DomeException);
	void setValues(vector <vector <double> > values) throw(DomeException);
	double getElement(int row, int column) throw(DomeException);
	void setElement(int row, int column, double value) throw(DomeException);

protected:
	void connect(Engine* _engine) throw(DomeException); 
	void disconnect() throw(DomeException); 

private:
	int m_rows;
	int m_columns;
};

} // namespace MatlabPlugin
} // DOME

#endif // DOME_MATLABDATA_H
