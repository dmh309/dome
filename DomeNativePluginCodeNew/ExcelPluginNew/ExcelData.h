// ExcelData.h: interface for the ExcelData class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DOME_EXCELDATA_H
#define DOME_EXCELDATA_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifdef _EXCEL_2010
#include "excel_2010.h"
#endif

#include "ExcelIncludes.h"
#include "ExcelModel.h"




namespace DOME {
namespace ExcelPlugin {
class ExcelModel;

class ExcelData
{
public:
	ExcelData(string sheetName, string rangeName);
	virtual ~ExcelData();

    friend class ExcelModel;
#ifndef _EXCEL_2010
	virtual void connect(Worksheets &sheets) throw(DomeException); 
#else
	virtual void connect(CWorksheets &sheets) throw(DomeException); 
#endif

	virtual void disconnect();

protected:
	string m_sheetName;
	string m_rangeName;
#ifndef _EXCEL_2010
	Range m_range;
#else
	CRange m_range;
#endif

};


class ExcelReal : public DomeReal, public ExcelData
{
friend class ExcelModel;
public:
	ExcelReal(string sheetName, string rangeName);

	double getValue() throw(DomeException);
	void setValue(double value) throw(DomeException);
};


class ExcelString : public DomeString, public ExcelData
{
friend class ExcelModel;
public:
	ExcelString(string sheetName, string rangeName);

	string getValue() throw(DomeException);
	void setValue(string value) throw(DomeException);
};


class ExcelMatrix : public DomeMatrix, public ExcelData
{
friend class ExcelModel;
public:
	ExcelMatrix(string sheetName, string rangeName);

	vector<int> getDimension() throw(DomeException);
    void setDimension(int rows, int cols);
    
    int getRows() throw(DomeException);
    void setRows(int rows) throw(DomeException);

    int getColumns() throw(DomeException);
    void setColumns(int cols) throw(DomeException);

    vector < vector<double> > getValues() throw(DomeException);
    void setValues(vector < vector<double> > values) throw(DomeException);
    double getElement(int row, int column) throw(DomeException);
    void setElement(int row, int column, double value) throw(DomeException);
};

/*class ExcelVisible : public DomeBoolean
{
friend class ExcelModel;
public:
	ExcelVisible();
	ExcelVisible(bool value);

	bool getValue() throw(DomeException);
	void setValue(bool value) throw(DomeException);

protected:
	void connect(_Application* xlApp) throw(DomeException); 

	bool _isVisible;
    _Application* _xlApp;
};
*/

} // namespace ExcelPlugin
} // DOME

#endif // DOME_EXCELDATA_H