//----------------------------------------------------------------------------
// ExcelDelegate.cpp
//
// Purpose: unwraps command, data objects and data values supplied by
// the caller. Instantiates model and data objects. Delegates work
// to the model and data objects.
//
// *********** PLUGIN AUTHOR MODIFIES EVERYTHING BELOW THIS LINE ************
// ***********					  AS NEEDED                      ************
//----------------------------------------------------------------------------

#ifdef _WINDOWS
#pragma warning(disable:4786)	// stl warnings
#endif
#include <vector>
#include <string>
#include <iostream>
using namespace std;

// java native interface stuff
#ifdef _WINDOWS
#include <jni.h>
#else
#include "../jni_linux/jni.h"
#endif
#include "JNIHelper.h"

// Excel function prototypes
#include "ExcelIncludes.h"

// data and model classes
#include "ExcelData.h"
#include "ExcelModel.h"
using namespace DOME::ExcelPlugin;

// definitions for this class
#include "ExcelPluginCaller.h"
#include "ExcelDelegate.h"



/////////////////////////////////////////////////////////////////////
// invoke all functions that return an object (casted to a long)
/////////////////////////////////////////////////////////////////////
long ExcelDelegate::callObjectFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case ExcelPluginCaller_MODEL_INIT:
		iObjectPtr = (long) initializeModel (env, args, iNumArgs);
		break;

	case ExcelPluginCaller_MODEL_CREATE_REAL:
		iObjectPtr = (long) createModelObject (env, args, iNumArgs, iObjectPtr);
		break;

	case ExcelPluginCaller_MODEL_CREATE_STRING:
		iObjectPtr = (long) createStringObject (env, args, iNumArgs, iObjectPtr);
		break;

	case ExcelPluginCaller_MODEL_CREATE_MATRIX:
		iObjectPtr = (long) createMatrix (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		EXCEL_DEBUG ("callObjectFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return iObjectPtr;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return void
/////////////////////////////////////////////////////////////////////
void ExcelDelegate::callVoidFunctions (JNIEnv* env, jobjectArray args, 
									   unsigned int iNumArgs,
									   long iObjectPtr,
									   unsigned int iFunctionIndex)
{
	// call the appropriate function
	switch (iFunctionIndex)
	{
	case ExcelPluginCaller_MODEL_LOAD:
		loadModel (iObjectPtr);
		break;

	case ExcelPluginCaller_MODEL_EXECUTE:
		executeModel (iObjectPtr);
		break;

	case ExcelPluginCaller_MODEL_EXEC_BF_INPUT:
		break;

	case ExcelPluginCaller_MODEL_EXEC_AF_INPUT:
		break;

	case ExcelPluginCaller_MODEL_UNLOAD:
		unloadModel (iObjectPtr);
		break;

	case ExcelPluginCaller_MODEL_DESTROY:
		destroyModel (iObjectPtr);
		break;

	case ExcelPluginCaller_REAL_SET_VALUE:
		setRealValue (env, args, iNumArgs, iObjectPtr);
		break;

	case ExcelPluginCaller_STRING_SET_VALUE:
		setStringValue (env, args, iNumArgs, iObjectPtr);
		break;

	case ExcelPluginCaller_MATRIX_SET_ELEMENT:
		setMatrixElement (env, args, iNumArgs, iObjectPtr);
		break;

	case ExcelPluginCaller_MATRIX_SET_VALUES:
		setMatrixValues (env, args, iNumArgs, iObjectPtr);
		break;

	case ExcelPluginCaller_MATRIX_SET_ROWS:
	case ExcelPluginCaller_MATRIX_SET_COLS:
		setMatrixDimension (env, args, iNumArgs, iObjectPtr, iFunctionIndex);
		break;

	case ExcelPluginCaller_MATRIX_SET_DIM:
		setMatrixDimensions (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		EXCEL_DEBUG ("callVoidFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return an integer
/////////////////////////////////////////////////////////////////////
int ExcelDelegate::callIntegerFunctions (JNIEnv* env, jobjectArray args, 
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	int value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case ExcelPluginCaller_MATRIX_GET_ROWS:
	case ExcelPluginCaller_MATRIX_GET_COLS:
		value = getIntegerValue (iObjectPtr, iFunctionIndex);
		break;

	default:
		EXCEL_DEBUG ("callIntegerFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a boolean
/////////////////////////////////////////////////////////////////////
bool ExcelDelegate::callBooleanFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	bool value = false;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case ExcelPluginCaller_MODEL_IS_LOADED:
		value = isModelLoaded (iObjectPtr);
		break;
		
	default:
		EXCEL_DEBUG ("callBooleanFunctions: Undefined function index ("
					<< iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a double
/////////////////////////////////////////////////////////////////////
double ExcelDelegate::callDoubleFunctions (JNIEnv* env, jobjectArray args, 
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	//for debug
	//cout << "Get into ExcelDelegate::callDoubleFunctions!" << endl;
	//cout << "iFunctionIndex = " << iFunctionIndex << ", ExcelPluginCaller_REAL_GET_VALUE = "<< ExcelPluginCaller_REAL_GET_VALUE << endl;
	
	double value = 0;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case ExcelPluginCaller_REAL_GET_VALUE:
		value = getDoubleValue (iObjectPtr);
		break;

	case ExcelPluginCaller_MATRIX_GET_ELEMENT:
		value = getMatrixElement (env, args, iNumArgs, iObjectPtr);
		break;

	default:
		EXCEL_DEBUG ("callDoubleFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a character string
/////////////////////////////////////////////////////////////////////
const
char* ExcelDelegate::callStringFunctions (JNIEnv* env, jobjectArray args, 
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	const char *value = NULL;

	switch (iFunctionIndex)
	{
	case ExcelPluginCaller_STRING_GET_VALUE:
		value = getStringValue (iObjectPtr);
		break;

	default:
		EXCEL_DEBUG ("callStringFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return value;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of doubles
/////////////////////////////////////////////////////////////////////
vector<double>
ExcelDelegate::callDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										 unsigned int iNumArgs,
										 long iObjectPtr,
										 unsigned int iFunctionIndex)
{
	vector<double> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		EXCEL_DEBUG ("callDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 1D array of integers
/////////////////////////////////////////////////////////////////////
vector<int>
ExcelDelegate::callIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	vector<int> result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		EXCEL_DEBUG ("callIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of integers
/////////////////////////////////////////////////////////////////////
vector< vector<int> >
ExcelDelegate::call2DIntegerArrayFunctions (JNIEnv* env, jobjectArray args,
											unsigned int iNumArgs,
											long iObjectPtr,
											unsigned int iFunctionIndex)
{
	vector < vector<int> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	default:
		EXCEL_DEBUG ("call2DIntegerArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}


/////////////////////////////////////////////////////////////////////
// invoke all functions that return a 2D array of doubles
/////////////////////////////////////////////////////////////////////
vector< vector<double> > 
ExcelDelegate::call2DDoubleArrayFunctions (JNIEnv* env, jobjectArray args,
										   unsigned int iNumArgs,
										   long iObjectPtr,
										   unsigned int iFunctionIndex)
{
	vector< vector<double> > result;

	// call the appropriate function
	switch (iFunctionIndex)
	{
	case ExcelPluginCaller_MATRIX_GET_VALUES:
		result = getMatrixElements (iObjectPtr);
		break;

	default:
		EXCEL_DEBUG ("call2DDoubleArrayFunctions: Undefined function index ("
					 << iFunctionIndex << ")" << endl);
	}

	return result;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// void functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////////
// load the model
/////////////////////////////////////////////////////
void ExcelDelegate::loadModel (long iObjectPtr)
{
	ExcelModel *model = (ExcelModel*) iObjectPtr;

	if (model == NULL) {
		EXCEL_DEBUG ("loadModel: Model does not exist" << endl);
	}
	else
	{
		model->loadModel ();
	}
}


/////////////////////////////////////////////////////
// unload the model
/////////////////////////////////////////////////////
void ExcelDelegate::unloadModel (long iObjectPtr)
{
	ExcelModel *model = (ExcelModel*) iObjectPtr;

	if (model == NULL) {
		EXCEL_DEBUG ("unloadModel: Model does not exist" << endl);
	}
	else
	{
		model->unloadModel ();
	}
}


/////////////////////////////////////////////////////
// destroy the model
/////////////////////////////////////////////////////
void ExcelDelegate::destroyModel (long iObjectPtr)
{
	ExcelModel *model = (ExcelModel*) iObjectPtr;

	if (model == NULL) {
		EXCEL_DEBUG ("destroyModel: Model does not exist" << endl);
	}
	else
	{
		delete model;
	}
}


/////////////////////////////////////////////////////
// set real values
/////////////////////////////////////////////////////
void ExcelDelegate::setRealValue (JNIEnv* env, jobjectArray args,
								   unsigned int iNumArgs,
								   long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 1) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setValue function");
	}
	else 
	{
		//ExcelReal *parameter = (ExcelReal*) iObjectPtr;
		ExcelData *parameter = (ExcelData*) iObjectPtr;
		if (parameter == NULL) {
			EXCEL_DEBUG ("setValue: Parameter does not exist" << endl);
		}
		else
		{
			int err;
			void *value = NULL;

			// unpack the argument and call the function
			err = getArgument (env, args, 0, JAVA_DOUBLE, value);
			if (err != ERR_OK) {
				EXCEL_DEBUG ("setValue: Invalid parameter value (ERR = "
							 << err << ")" << endl);
			}
			else {
				//cout << "setRealValue: " << *((double*)value) << endl;
				((ExcelReal*)parameter)->setValue ( *((double*)value) );
			}

			// clean up
			if (value != NULL) delete (double*) value;
		}
	}
}

/////////////////////////////////////////////////////
// set string values
/////////////////////////////////////////////////////
void ExcelDelegate::setStringValue (JNIEnv* env, jobjectArray args,
								   unsigned int iNumArgs,
								   long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 1) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setValue function");
	}
	else 
	{
		ExcelData *parameter = (ExcelData*) iObjectPtr;
		if (parameter == NULL) {
			EXCEL_DEBUG ("setValue: Parameter does not exist" << endl);
		}
		else
		{
			int err;
			void *value = NULL;

			// unpack the argument and call the function
			err = getArgument (env, args, 0, JAVA_STRING, value);
			if (err != ERR_OK) {
				EXCEL_DEBUG ("setValue: Invalid parameter value (ERR = "
							 << err << ")" << endl);
			}
			else {
				((ExcelString*)parameter)->setValue ( (char*)value);
			}

			// clean up
			if (value != NULL) delete value;
		}
	}
}


/////////////////////////////////////////////////////
// set matrix element
/////////////////////////////////////////////////////
void ExcelDelegate::setMatrixElement (JNIEnv* env, jobjectArray args,
									   unsigned int iNumArgs,
									   long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 3) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixElement function");
	}
	else 
	{
		int err1, err2, err3;
		void *row = NULL;
		void *col = NULL;
		void *value = NULL;


		// unpack the argument and call the function
		//ExcelMatrix *parameter = (ExcelMatrix*) iObjectPtr;
		ExcelData *parameter = (ExcelData*) iObjectPtr;

		err1 = getArgument (env, args, 0, JAVA_INTEGER, row);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, col);
		err3 = getArgument (env, args, 2, JAVA_DOUBLE, value);

		if (parameter == NULL) {
			EXCEL_DEBUG ("setMatrixElement: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			EXCEL_DEBUG ("setMatrixElement: Invalid row "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			EXCEL_DEBUG ("setMatrixElement: Invalid column "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			EXCEL_DEBUG ("setMatrixElement: Invalid parameter value "
						 << "(ERR = " << err3 << ")" << endl);
		}
		else {
			((ExcelMatrix*)parameter)->setElement (*((int*)row), 
								   *((int*)col),
								   *((double*)value) );
		}

		// clean up
		if (row != NULL) delete (int*) row;
		if (col != NULL) delete (int*) col;
		if (value != NULL) delete (double*) value;
	}
}


/////////////////////////////////////////////////////
// set matrix values
/////////////////////////////////////////////////////
void ExcelDelegate::setMatrixValues (JNIEnv* env, jobjectArray args,
									  unsigned int iNumArgs,
									  long iObjectPtr)
{

	//for debug
	//cout<<"Get into ExcelDelegate::setMatrixValues!" << endl;
	
	// check argument list length
	if (iNumArgs != 3) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixValues function");
	}
	else 
	{
		int err1, err2, err3;
		void *rows = NULL;
		void *cols = NULL;
		void *values = NULL;


		// unpack the argument and call the function
		//ExcelMatrix *parameter = (ExcelMatrix*) iObjectPtr;
		//*****iObjectPtr should be cast to ExcelData first since it has been sent to dome as ExcelData
		//*****otherwise, there will be a problem
		ExcelData *parameter = (ExcelData*) iObjectPtr;

		err1 = getArgument (env, args, 0, JAVA_2D_DOUBLE_ARRAY, values);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, rows);
		err3 = getArgument (env, args, 2, JAVA_INTEGER, cols);

		//for debug
		//cout<<"ExcelDelegate::setMatrixValues unpack parameters!" << endl;

		if (parameter == NULL) {
			EXCEL_DEBUG ("setMatrixValues: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			EXCEL_DEBUG ("setMatrixValues: Invalid parameter value "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			EXCEL_DEBUG ("setMatrixValues: Invalid number of rows "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else
		if (err3 != ERR_OK) {
			EXCEL_DEBUG ("setMatrixValues: Invalid number of columns "
						 << "(ERR = " << err3 << ")" << endl);
		}
		else {
			// create matrix
			int iCols = *((int*)cols);
			int iRows = *((int*)rows);

			//for debug
			//cout<< "Rows: " << iRows << ", Cols:" << iCols << endl;

			double** dValues = (double**)values;

			//for debug
			//cout<< "Pointer of values: " << values  << endl;

			//*****Two ways to create: vector< vector<double> > mat
			//Way one:
/*			vector< vector<double> > mat;
			for (int i = 0; i < iRows; i++) 
			{
				vector <double> row;
				for (int j = 0; j < iCols; j++) {
					//for debug
					cout<<"("<<i<<","<<j<<"): "<<dValues[i][j]<<endl;

					row.push_back(dValues[i][j]);
				}
				mat.push_back(row);
			}
*/
			//Way two:
			vector <vector <double> > mat(iRows);
			for (int i = 0; i < iRows; i++) 
			{
				vector <double> vec(iCols);
				for (int j = 0; j < iCols; j++) {
					vec[j] = dValues[i][j];
					
					//for debug
					//cout<<"("<<i<<","<<j<<"): "<<dValues[i][j]<<endl;
				}	
				mat[i] = vec;
			}

			//for debug
			//cout<<"ExcelDelegate::setMatrixValues is done!" << endl;

			// set matrix values
			((ExcelMatrix*)parameter)->setValues (mat); //There is a problem to pass STL class as an argument

		}

		// clean up
		if (values != NULL) delete (double**) values;
		if (rows != NULL) delete (int*) rows;
		if (cols != NULL) delete (int*) cols;
	}
}



/////////////////////////////////////////////////////
// set number of rows or columns for a matrix
/////////////////////////////////////////////////////
void ExcelDelegate::setMatrixDimension (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr,
										  unsigned int iFunctionIndex)
{
	// check argument list length
	if (iNumArgs != 1) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixDimension function");
	}
	else 
	{
		int err;
		void *dim = NULL;

		// unpack the argument and call the function
		//ExcelMatrix *parameter = (ExcelMatrix*) iObjectPtr;
		ExcelData *parameter = (ExcelData*) iObjectPtr;
		err = getArgument (env, args, 0, JAVA_INTEGER, dim);

		if (parameter == NULL) {
			EXCEL_DEBUG ("setMatrixElement: Parameter does not exist" << endl);
		}
		else
		if (err != ERR_OK) {
			EXCEL_DEBUG ("setMatrixElement: Invalid dimension "
						 << "(ERR = " << err << ")" << endl);
		}
		else {
			switch (iFunctionIndex)
			{
			case ExcelPluginCaller_MATRIX_SET_COLS:
				((ExcelMatrix*)parameter)->setColumns ( *((int*)dim) );
				break;

			case ExcelPluginCaller_MATRIX_SET_ROWS:
				((ExcelMatrix*)parameter)->setRows ( *((int*)dim) );
				break;

			default:
				EXCEL_DEBUG ("setMatrixDimension: Undefined function index ("
							  << iFunctionIndex << ")" << endl);
			}
		}

		// clean up
		if (dim != NULL) delete (int*) dim;
	}
}


/////////////////////////////////////////////////////
// set matrix dimensions
/////////////////////////////////////////////////////
void ExcelDelegate::setMatrixDimensions (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	// check argument list length
	if (iNumArgs != 2) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the setMatrixDimensions function");
	}
	else 
	{
		int err1, err2;
		void *rows = NULL;
		void *cols = NULL;


		// unpack the argument and call the function
		//ExcelMatrix *parameter = (ExcelMatrix*) iObjectPtr;
		ExcelData *parameter = (ExcelData*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_INTEGER, rows);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, cols);

		if (parameter == NULL) {
			EXCEL_DEBUG ("setMatrixElement: Parameter does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			EXCEL_DEBUG ("setMatrixElement: Invalid number of rows "
						 << "(ERR = " << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			EXCEL_DEBUG ("setMatrixElement: Invalid number of columns "
						 << "(ERR = " << err2 << ")" << endl);
		}
		else {
			((ExcelMatrix*)parameter)->setDimension (*((int*)rows),
									 *((int*)cols));
		}

		// clean up
		if (cols != NULL) delete (int*) cols;
		if (rows != NULL) delete (int*) rows;
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// object functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// create the model
///////////////////////////////////////////
ExcelModel *ExcelDelegate::initializeModel (JNIEnv* env, jobjectArray args,
											  unsigned int iNumArgs)
{
	ExcelModel *model = NULL;

	// check argument list length
	if (iNumArgs != 2) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the initializeModel function" << endl);
	}
	else 
	{
		int err1, err2;
		void* szFileName = NULL;
		void* bIsVisible = NULL;

		// unpack the arguments
		err1 = getArgument (env, args, 0, JAVA_STRING, szFileName);
		err2 = getArgument (env, args, 1, JAVA_BOOLEAN, bIsVisible);

		if (err1 != ERR_OK) {
			EXCEL_DEBUG ("initializeModel: Invalid model file name (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			EXCEL_DEBUG ("initializeModel: Invalid argument: bIsVisible (ERR = " 
						 << err2 << ")" << endl);
		}
		else {
			// call the constructor
			model = new ExcelModel (  (char*)szFileName, 
									 *((bool*)bIsVisible));
		}

		// clean up
		if (szFileName != NULL) delete (char*) szFileName;
		if (bIsVisible != NULL)	delete (bool*) bIsVisible;
	}

	return model;
}


///////////////////////////////////////////
// create individual model objects
///////////////////////////////////////////
ExcelData *ExcelDelegate::createModelObject (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	ExcelData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 2) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createModelObject function" << endl);
	}
	else 
	{
		int err1,err2;
		void* szSheetName = NULL;
		void* szRangeName = NULL;

		// unpack the arguments
		ExcelModel *model = (ExcelModel*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_STRING, szSheetName);
		err2 = getArgument (env, args, 1, JAVA_STRING, szRangeName);

		if (model == NULL) {
			EXCEL_DEBUG ("createModelObject: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			EXCEL_DEBUG ("createModelObject: Invalid argument: SheetName (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			EXCEL_DEBUG ("createModelObject: Invalid argument: RangeName (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// call the function
			string name1 ((char*)szSheetName);
			string name2 ((char*)szRangeName);

			modelObject = model->createReal (name1,name2);
		}

		// clean up
		if (szSheetName != NULL) delete (char*) szSheetName;
		if (szRangeName != NULL) delete (char*) szRangeName;
	}

	return modelObject;
}


///////////////////////////////////////////
// create individual string objects
///////////////////////////////////////////
ExcelData *ExcelDelegate::createStringObject (JNIEnv* env, jobjectArray args,
											   unsigned int iNumArgs,
											   long iObjectPtr)
{
	ExcelData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 2) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createStringObject function" << endl);
	}
	else 
	{
		int err1,err2;
		void* szSheetName = NULL;
		void* szRangeName = NULL;

		// unpack the arguments
		ExcelModel *model = (ExcelModel*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_STRING, szSheetName);
		err2 = getArgument (env, args, 1, JAVA_STRING, szRangeName);

		if (model == NULL) {
			EXCEL_DEBUG ("createStringObject: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			EXCEL_DEBUG ("createModelObject: Invalid argument: SheetName (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			EXCEL_DEBUG ("createModelObject: Invalid argument: RangeName (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// call the function
			string name1 ((char*)szSheetName);
			string name2 ((char*)szRangeName);

			modelObject = model->createString (name1,name2);
		}

		// clean up
		if (szSheetName != NULL) delete (char*) szSheetName;
		if (szRangeName != NULL) delete (char*) szRangeName;
	}

	return modelObject;
}


///////////////////////////////////////////
// create matrix
///////////////////////////////////////////
ExcelData *ExcelDelegate::createMatrix (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	ExcelData *modelObject = NULL;

	// check argument list length
	if (iNumArgs != 2) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the createMatrix function" << endl);
	}
	else 
	{
		int err1, err2;
		void* szSheetName = NULL;
		void* szRangeName = NULL;

		// unpack the arguments
		ExcelModel *model = (ExcelModel*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_STRING, szSheetName);
		err2 = getArgument (env, args, 1, JAVA_STRING, szRangeName);

		if (model == NULL) {
			EXCEL_DEBUG ("createMatrix: Model does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			EXCEL_DEBUG ("createMatrix: Invalid argument: SheetName (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			EXCEL_DEBUG ("createMatrix: Invalid argument: RangeName (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// call the function
			modelObject = model->createMatrix ((char*)szSheetName,(char*)szRangeName);
		}

		// clean up
		if (szSheetName != NULL) delete (char*) szSheetName;
		if (szRangeName != NULL) delete (char*) szRangeName;
	}

	return modelObject;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// real functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
double ExcelDelegate::getDoubleValue (long iObjectPtr)
{
	//for debug
	//cout << "Get into ExcelDelegate::getDoubleValue" << endl;

	double value = 0;
	//ExcelReal *data = (ExcelReal*) iObjectPtr;
	ExcelData *data = (ExcelData*) iObjectPtr;

	if (data == NULL) {
		EXCEL_DEBUG ("getDoubleValue: ExcelReal does not exist" << endl);
	}
	else {
		// get the value
		value = ((ExcelReal*)data)->getValue ();
	}

	return value;
}


///////////////////////////////////////////
// create matrix
///////////////////////////////////////////
double ExcelDelegate::getMatrixElement (JNIEnv* env, jobjectArray args,
										  unsigned int iNumArgs,
										  long iObjectPtr)
{
	double value = 0;

	// check argument list length
	if (iNumArgs != 2) {
		EXCEL_DEBUG ("Incorrect number (" << iNumArgs
					 << ") of arguments to the getMatrixElement function" << endl);
	}
	else 
	{
		int err1, err2;
		void* row = NULL;
		void* col = NULL;

		// unpack the arguments
		//ExcelMatrix *data = (ExcelMatrix*) iObjectPtr;
		ExcelData *data = (ExcelData*) iObjectPtr;
		err1 = getArgument (env, args, 0, JAVA_INTEGER, row);
		err2 = getArgument (env, args, 1, JAVA_INTEGER, col);

		if (data == NULL) {
			EXCEL_DEBUG ("getMatrixElement: ExcelMatrix does not exist" << endl);
		}
		else
		if (err1 != ERR_OK) {
			EXCEL_DEBUG ("getMatrixElement: Invalid row (ERR = "
						 << err1 << ")" << endl);
		}
		else
		if (err2 != ERR_OK) {
			EXCEL_DEBUG ("getMatrixElement: Invalid column (ERR = "
						 << err2 << ")" << endl);
		}
		else
		{
			// call the function
			value = ((ExcelMatrix*)data)->getElement (*((int*)row),
									  *((int*)col) );
		}

		// clean up
		if (row != NULL) delete (int*) row;
		if (col != NULL) delete (int*) col;
	}

	return value;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// integer functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get an integer value
///////////////////////////////////////////
int ExcelDelegate::getIntegerValue (long iObjectPtr,
									 unsigned int iFunctionIndex)
{
	int value = 0;

	//ExcelMatrix *data = (ExcelMatrix*) iObjectPtr;
	ExcelData *data = (ExcelData*) iObjectPtr;

	if (data == NULL) {
		EXCEL_DEBUG ("getIntegerValue: ExcelMatrix does not exist" << endl);
	}
	else {
		switch (iFunctionIndex)
		{
		case ExcelPluginCaller_MATRIX_GET_ROWS:
			value = ((ExcelMatrix*)data)->getRows ();
			break;

		case ExcelPluginCaller_MATRIX_GET_COLS:
			value = ((ExcelMatrix*)data)->getColumns ();
			break;

		default:
			EXCEL_DEBUG ("getIntegerValue: Undefined function index ("
						  << iFunctionIndex << ")" << endl);
		}
	}

	return value;
}


///////////////////////////////////////////
// execute the model
///////////////////////////////////////////
void ExcelDelegate::executeModel (long iObjectPtr)
{
	ExcelModel *model = (ExcelModel*) iObjectPtr;

	if (model == NULL) {
		EXCEL_DEBUG ("executeModel: ExcelModel does not exist" << endl);
	}
	else {
		// execute the model
		model->execute ();
	}
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// boolean functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a real parameter
///////////////////////////////////////////
bool ExcelDelegate::getBooleanValue (long iObjectPtr)
{
	bool value = false;
	return value;
}

///////////////////////////////////////////
// check if model is loaded
///////////////////////////////////////////
bool ExcelDelegate::isModelLoaded (long iObjectPtr)
{
	bool value = false;
	ExcelModel *model = (ExcelModel*) iObjectPtr;

	if (model == NULL) {
		EXCEL_DEBUG ("isModelLoaded: Model does not exist" << endl);
	}
	else {
		// get the value
		value = model->isModelLoaded ();
	}

	return value;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// string functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


///////////////////////////////////////////
// get the value from a string parameter
///////////////////////////////////////////
char* ExcelDelegate::getStringValue (long iObjectPtr)
{
	ExcelData *data = (ExcelData*) iObjectPtr;

	if (data == NULL) {
		EXCEL_ERROR ("getStringValue: ExcelString does not exist");
		return NULL;
	}
	else {
		// get the value
		string str = ((ExcelString*)data)->getValue ();

		char *value = new char[str.length()+1];
		strcpy(value,str.c_str());
		
		return value;
	}

}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// 1D array functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


/////////////////////////////////////////////////
// get the value from a double array parameter
/////////////////////////////////////////////////
vector<double> 
ExcelDelegate::getDoubleArrayValue (JNIEnv* env, jobjectArray args,
									 unsigned int iNumArgs, long iObjectPtr)
{
	vector<double> value;
	return value;
}




//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
// 2D array functions
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


//////////////////////////////////////////////////////
// get the values from a 2D double array parameter
//////////////////////////////////////////////////////
vector< vector<double> > 
ExcelDelegate::getMatrixElements (long iObjectPtr)
{
	vector< vector<double> > value;

	// unpack the arguments
	//ExcelMatrix *data = (ExcelMatrix*) iObjectPtr;
	ExcelData *data = (ExcelData*) iObjectPtr;

	if (data == NULL) {
		EXCEL_DEBUG ("getMatrixElements: ExcelMatrix does not exist" << endl);
	}
	else
	{
		// call the function
		value = ((ExcelMatrix*)data)->getValues ();
	}

	return value;
}
