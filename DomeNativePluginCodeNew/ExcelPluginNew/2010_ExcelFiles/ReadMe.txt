These are for Excel in Windows 64 bit obtained using classwizard
https://msdn.microsoft.com/en-us/library/ms229859(v=vs.110).aspx

https://support.microsoft.com/en-us/kb/178749
https://msdn.microsoft.com/en-us/library/office/ff955606(v=office.14).aspx

Meena Ganesh July 2015

1. This project was created to extract the classes from the Excel.exe
2. Create a MFC Application Project - Dialog based 
3. Go To Class wizard and add class "MFC Class from Typelib"
4. Search in the typelib list and select "MicroSoft Excel 14.0 Object Library<1.7> - 

5. Note this typelib appears because I had Office 2010 installed and this gets registered in the registry.
6. Select all classes.
7. We have copied all the "Applicable" classes that JNI would support for DOME and kept in excel_2010.h 
8. Testing is done using CApplication.h - note we had to comment the line
//#import "C:\\Program Files (x86)\\Microsoft Office\\Office14\\EXCEL.EXE" no_namespace

9. If you need more classes from the typelib to be exposed go see the repository Project Util_ExcelPlugin_2010.sln

