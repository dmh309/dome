# Microsoft Developer Studio Project File - Name="MatlabPlugin" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=MatlabPlugin - Win32 R12 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "MatlabPlugin.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "MatlabPlugin.mak" CFG="MatlabPlugin - Win32 R12 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "MatlabPlugin - Win32 R13 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "MatlabPlugin - Win32 R13 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "MatlabPlugin - Win32 R12 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "MatlabPlugin - Win32 R12 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "MatlabPlugin - Win32 R14 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "MatlabPlugin - Win32 R13 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Win32_R13_Release"
# PROP BASE Intermediate_Dir "Win32_R13_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "R13_Release"
# PROP Intermediate_Dir "R13_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
# ADD BASE CPP /nologo /MD /W3 /GR /GX /O2 /I "." /I "../" /I "../DomePlugin" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "R13_EXPORTS" /D "_WINDLL" /D "_AFXDLL" /FR /FD /c
# SUBTRACT BASE CPP /YX
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /I "." /I "../" /I "../DomePlugin" /D "_MATLAB_R13" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "R13_EXPORTS" /D "_WINDLL" /D "_AFXDLL" /Fr /FD /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ole32.lib /nologo /dll /machine:I386 /out:"Release/MatlabPluginR13.dll" /libpath:"./matlablib/msvc60" /libpath:"../DomePlugin/Release"
# SUBTRACT BASE LINK32 /nodefaultlib
# ADD LINK32 ole32.lib libmx.lib libfixedpoint.lib libmat.lib libmatlb.lib libmatlbmx.lib libmex.lib libmwarpack.lib libmwlapack.lib libmwrefblas.lib libeng.lib /nologo /dll /machine:I386 /out:"R13_Release/MatlabPluginR13.dll" /libpath:"./R13_matlablib/msvc60" /libpath:"../DomePlugin/Release"
# SUBTRACT LINK32 /nodefaultlib

!ELSEIF  "$(CFG)" == "MatlabPlugin - Win32 R13 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Win32_R13_Debug"
# PROP BASE Intermediate_Dir "Win32_R13_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "R13_Debug"
# PROP Intermediate_Dir "R13_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
# ADD BASE CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "." /I "../" /I "../DomePlugin" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "R13_EXPORTS" /D "_WINDLL" /D "_AFXDLL" /FD /GZ /c
# SUBTRACT BASE CPP /YX
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "." /I "../" /I "../DomePlugin" /D "_MATLAB_R13" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "R13_EXPORTS" /D "_WINDLL" /D "_AFXDLL" /FR /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ole32.lib /nologo /dll /debug /machine:I386 /out:"Debug/MatlabPluginR13.dll" /pdbtype:sept /libpath:"./matlablib/msvc60" /libpath:"../DomePlugin/Release"
# ADD LINK32 ole32.lib libmx.lib libfixedpoint.lib libmat.lib libmatlb.lib libmatlbmx.lib libmex.lib libmwarpack.lib libmwlapack.lib libmwrefblas.lib libeng.lib /nologo /dll /debug /machine:I386 /out:"R13_Debug/MatlabPluginR13.dll" /pdbtype:sept /libpath:"./R13_matlablib/msvc60" /libpath:"../DomePlugin/Release"

!ELSEIF  "$(CFG)" == "MatlabPlugin - Win32 R12 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Win32_R12_Release"
# PROP BASE Intermediate_Dir "Win32_R12_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "R12_Release"
# PROP Intermediate_Dir "R12_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
# ADD BASE CPP /nologo /MD /W3 /GR /GX /O2 /I "." /I "../" /I "../DomePlugin" /D "_MATLAB_R13" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "R13_EXPORTS" /D "_WINDLL" /D "_AFXDLL" /FR /FD /c
# SUBTRACT BASE CPP /YX
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /I "." /I "../" /I "../DomePlugin" /D "_MATLAB_R12" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "R13_EXPORTS" /D "_WINDLL" /D "_AFXDLL" /Fr /FD /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"R12_Release/R12.bsc"
LINK32=link.exe
# ADD BASE LINK32 ole32.lib /nologo /dll /machine:I386 /out:"R13_Release/MatlabPluginR13.dll" /libpath:"./matlablib/msvc60" /libpath:"../DomePlugin/Release"
# SUBTRACT BASE LINK32 /nodefaultlib
# ADD LINK32 ole32.lib libmx.lib libmat.lib libmatlb.lib libmatlbmx.lib libmex.lib libmwarpack.lib libmwlapack.lib libeng.lib /nologo /dll /machine:I386 /out:"R12_Release/MatlabPluginR12.dll" /libpath:"./R12_matlablib/msvc60" /libpath:"../DomePlugin/Release"
# SUBTRACT LINK32 /nodefaultlib

!ELSEIF  "$(CFG)" == "MatlabPlugin - Win32 R12 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Win32_R12_Debug"
# PROP BASE Intermediate_Dir "Win32_R12_Debug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "R12_Debug"
# PROP Intermediate_Dir "R12_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
# ADD BASE CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "." /I "../" /I "../DomePlugin" /D "_MATLAB_R12" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "R12_EXPORTS" /D "_WINDLL" /D "_AFXDLL" /FD /GZ /c
# SUBTRACT BASE CPP /YX
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "." /I "../" /I "../DomePlugin" /D "_MATLAB_R12" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "R12_EXPORTS" /D "_WINDLL" /D "_AFXDLL" /FR /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo /o"R12_Debug/R12.bsc"
LINK32=link.exe
# ADD BASE LINK32 ole32.lib /nologo /dll /debug /machine:I386 /out:"R12_Debug/MatlabPluginR12.dll" /pdbtype:sept /libpath:"./matlablib/msvc60" /libpath:"../DomePlugin/Release"
# ADD LINK32 ole32.lib libmx.lib libmat.lib libmatlb.lib libmatlbmx.lib libmex.lib libmwarpack.lib libmwlapack.lib libeng.lib /nologo /dll /debug /machine:I386 /out:"R12_Debug/MatlabPluginR12.dll" /pdbtype:sept /libpath:"./R12_matlablib/msvc60" /libpath:"../DomePlugin/Release"

!ELSEIF  "$(CFG)" == "MatlabPlugin - Win32 R14 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "MatlabPlugin___Win32_R14_Release"
# PROP BASE Intermediate_Dir "MatlabPlugin___Win32_R14_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "R14_Release"
# PROP Intermediate_Dir "R14_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
# ADD BASE CPP /nologo /MD /W3 /GR /GX /O2 /I "." /I "../" /I "../DomePlugin" /D "_MATLAB_R13" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "R13_EXPORTS" /D "_WINDLL" /D "_AFXDLL" /Fr /FD /c
# SUBTRACT BASE CPP /YX
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /I "." /I "../" /I "../DomePlugin" /D "_MATLAB_R14" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "R13_EXPORTS" /D "_WINDLL" /D "_AFXDLL" /Fr /FD /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ole32.lib libmx.lib libfixedpoint.lib libmat.lib libmatlb.lib libmatlbmx.lib libmex.lib libmwarpack.lib libmwlapack.lib libmwrefblas.lib libeng.lib /nologo /dll /machine:I386 /out:"R13_Release/MatlabPluginR13.dll" /libpath:"./R13_matlablib/msvc60" /libpath:"../DomePlugin/Release"
# SUBTRACT BASE LINK32 /nodefaultlib
# ADD LINK32 ole32.lib libmx.lib libfixedpoint.lib libmat.lib libmex.lib libeng.lib /nologo /dll /machine:I386 /out:"R14_Release/MatlabPluginR14.dll" /libpath:"./R14_matlablib/msvc60" /libpath:"../DomePlugin/Release"
# SUBTRACT LINK32 /nodefaultlib

!ENDIF 

# Begin Target

# Name "MatlabPlugin - Win32 R13 Release"
# Name "MatlabPlugin - Win32 R13 Debug"
# Name "MatlabPlugin - Win32 R12 Release"
# Name "MatlabPlugin - Win32 R12 Debug"
# Name "MatlabPlugin - Win32 R14 Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\JNIHelper.cpp
# End Source File
# Begin Source File

SOURCE=.\MatlabData.cpp
# End Source File
# Begin Source File

SOURCE=.\MatlabDelegate.cpp
# End Source File
# Begin Source File

SOURCE=.\MatlabModel.cpp
# End Source File
# Begin Source File

SOURCE=.\MatlabPluginCaller.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\JNIHelper.h
# End Source File
# Begin Source File

SOURCE=.\MatlabData.h
# End Source File
# Begin Source File

SOURCE=.\MatlabDelegate.h
# End Source File
# Begin Source File

SOURCE=.\MatlabIncludes.h
# End Source File
# Begin Source File

SOURCE=.\MatlabModel.h
# End Source File
# Begin Source File

SOURCE=.\MatlabPluginCaller.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Source File

SOURCE=..\DomePlugin\Release\DomePlugin.lib
# End Source File
# End Target
# End Project
